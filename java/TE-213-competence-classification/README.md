# Competence Classification

This provides the OpenNLP Doccat model and SKOS Thesaurus for the Competence Classification. Those are used as parameters for the OpenNLP Text Classification Extractor.