package eu.mico.extractors.opennlp.ner;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.daemon.DaemonInitException;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.marmotta.ldpath.parser.ParseException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectConnection;
import org.openrdf.repository.object.RDFObject;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.RDFHandlerBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.Annotation;
import com.github.anno4j.model.Body;
import com.github.anno4j.model.Selector;
import com.github.anno4j.model.namespaces.DC;
import com.github.anno4j.model.namespaces.OADM;
import com.github.anno4j.querying.QueryService;

import eu.mico.platform.anno4j.model.PartMMM;
import eu.mico.platform.anno4j.model.fam.LanguageBody;
import eu.mico.platform.anno4j.model.fam.NamedEntityBody;
import eu.mico.platform.anno4j.model.fam.TopicBody;
import eu.mico.platform.anno4j.model.namespaces.FAM;
import eu.mico.platform.anno4j.model.namespaces.MMM;
import eu.mico.platform.event.test.mock.EventManagerMock;
import eu.mico.platform.persistence.api.PersistenceService;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;

public class OpenNlpNerServiceTest {
    
    private static final Logger log = LoggerFactory.getLogger(OpenNlpNerServiceTest.class);

    //NOTE: This test uses relative links to the models stored in an other module
    private static final List<String> MODEL_PATHS = Arrays.asList(
            "../models/src/deb/resources/models/de-clusters-conll03.bin"//,
//            "../models/src/deb/resources/models/en-light-clusters-conll03.bin",
//            "../models/src/deb/resources/models/es-clusters-dictlbj-conll02.bin",
//            "../models/src/deb/resources/models/it-clusters-evalita09.bin"
            );

    
    private static final String TEST_CONTENT = "tests.txt";

    private static final ClassLoader cl = OpenNlpNerServiceTest.class.getClassLoader();
    
 
    private static final Charset UTF8 = Charset.forName("UTF-8");


    private static final URI DUMMY_LANGUAGE_EXTRACTOR_ID = new URIImpl("urn:dummy.mico.extractor:langDetect");

    private static List<String[]> testTexts = new LinkedList<>();

    private static File testRoot;
   
    private EventManagerMock eventManager;


    private static List<File> nerModelFiles;

    
    @BeforeClass
    public static void initClass() throws IOException, DaemonInitException {
        //we need to determine the base directory of the test to resolve the
        //relative link to the models
        String baseDir = System.getProperty("basedir");
        if(baseDir == null){
            baseDir = System.getProperty("user.dir");
        }
        testRoot = new File(baseDir);
        Assert.assertTrue(testRoot.isDirectory());
        nerModelFiles = new ArrayList<>();
        for(String path : MODEL_PATHS){
            File nerModelFile = new File(testRoot, FilenameUtils.separatorsToSystem(path)).getCanonicalFile();
            Assert.assertTrue(nerModelFile.isFile());
            nerModelFiles.add(nerModelFile);
        }
        
        
        InputStream testContentIn = cl.getResourceAsStream(TEST_CONTENT);
        Assert.assertNotNull(testContentIn);
        for(Iterator<String> lines = IOUtils.lineIterator(testContentIn,"UTF-8");lines.hasNext();){
            String line = lines.next();
            if(line.charAt(0) == '#'){
                continue; //ignore comment
            }
            int tabIdx = line.indexOf('\t');
            Assert.assertTrue(tabIdx > 0);
            String lang = line.substring(0, tabIdx);
            Assert.assertTrue(StringUtils.isNotBlank(lang));
            String text = line.substring(tabIdx+1);
            Assert.assertTrue(StringUtils.isNotBlank(text));
            testTexts.add(new String[]{lang,text});
        }
    }
    
    
    
    @Before
    public void setup() throws URISyntaxException, IOException {
        eventManager = new EventManagerMock();
        eventManager.init();
        eventManager.registerService(new OpenNlpNerService("dummy", nerModelFiles));
    }

    @After
    public void shutdown() throws IOException {
        eventManager.shutdown();
    }

    @Test
    public void testNER() throws RepositoryException, IOException, MalformedQueryException, QueryEvaluationException, ParseException, RDFHandlerException {
        final PersistenceService persistenceService = eventManager.getPersistenceService();

        for(String[] testData : testTexts){
            
            final Item contentItem = persistenceService.createItem();
            final ObjectConnection con = contentItem.getObjectConnection();
            log.info("> Use Item {} for test (lang: {}, content: {})", contentItem.getURI(), testData[0], StringUtils.abbreviate(testData[1], 40));
            Asset asset = contentItem.getAsset(); //NOTE: getAsset creates a new one if none exists already
            contentItem.setSyntacticalType("text/plain");
            asset.setFormat("text/plain");
            log.debug("- created Assert {} (location: {} | format: {})", asset, asset.getLocation(), asset.getFormat());
            String content = testData[1];
            log.debug(" - content: {}", StringUtils.abbreviate(content, 80));
            final PrintStream ps = new PrintStream(asset.getOutputStream());
            ps.print(content);
            ps.close();
            
            //create the language Annotation
            Part langAnnoPart = contentItem.createPart(DUMMY_LANGUAGE_EXTRACTOR_ID);
            langAnnoPart.setSyntacticalType("application/x-mico-fam-language-rdf");
            langAnnoPart.addInput(contentItem); //NOTE this is important!!
            LanguageBody langAnno = OpenNlpNerService.createObject(con, LanguageBody.class);
            langAnnoPart.setBody(langAnno);
            langAnno.setLanguage(testData[0]);
            //NOTE: we do not set a confidence here
            
            //inject the content item
            eventManager.injectItem(contentItem);
            
            debugRDF(contentItem);
    
            //those where the Parts added by the extractor
            final Set<URI> expectedItems = new HashSet<>(eventManager.getResponsesCollector().getNewItemResponses());
            Assert.assertFalse(expectedItems.isEmpty()); //we expect some new parts
    
            QueryService query = persistenceService.createQuery(contentItem.getURI());
            query.addPrefix("mmm", MMM.NS);
            query.addPrefix("oa", OADM.NS);
            query.addPrefix("fam", FAM.NS);
            query.addCriteria("mmm:hasBody[rdf:type is fam:EntityMention]");
            List<? extends Annotation> annotations = query.execute(PartMMM.class);
            Assert.assertNotNull(annotations);
            Assert.assertEquals(8, annotations.size());
            Annotation annotation = annotations.get(0);
            //assrt the classification as notified as new part
            Assert.assertTrue(expectedItems.remove(annotation.getResource()));
            
            Body body = annotations.get(0).getBody();
            Assert.assertTrue(body instanceof NamedEntityBody);
            NamedEntityBody namedEntityAnno = (NamedEntityBody)body;
            //Assert the mention
            Assert.assertNotNull(namedEntityAnno.getMention());
            Assert.assertNotNull(namedEntityAnno.getMention().toString());
            Assert.assertEquals(testData[0], namedEntityAnno.getMention().getLang());

            //Assert that we do have a confidence
            Assert.assertNotNull(namedEntityAnno.getConfidence());
            Assert.assertTrue(namedEntityAnno.getConfidence() >= 0.0 && namedEntityAnno.getConfidence() <= 1.0);
            
            //Assert that their is a selector
            Assert.assertFalse(namedEntityAnno.getSelectors().isEmpty());
            //TODO: assert properties of the selector
            
            //Assert that the Named Entity is extracted from the text/plain content
            Assert.assertNotNull(namedEntityAnno.getContent());
            Assert.assertEquals(contentItem.getRDFObject(), namedEntityAnno.getContent());
        }
        
    }

    private void debugRDF(Item item) throws RepositoryException, RDFHandlerException {
        if(!log.isDebugEnabled()){
            return;
        }
        //we copy all statements to a TreeModel as this one sorts them by SPO
        //what results in a much nicer TURTLE serialization
        final Model model = new TreeModel();
        //we also set commonly used namespaces
        model.setNamespace(OADM.PREFIX, OADM.NS);
        model.setNamespace(FAM.PREFIX, FAM.NS);
        model.setNamespace(RDF.PREFIX, RDF.NAMESPACE);
        model.setNamespace(RDFS.PREFIX, RDF.NAMESPACE);
        model.setNamespace(DC.PREFIX, DC.NS);
        model.setNamespace("xsd", "http://www.w3.org/2001/XMLSchema#");
        model.setNamespace(MMM.PREFIX, MMM.NS);
        model.setNamespace("test", "http://localhost/mem/");
        model.setNamespace("services", "http://www.mico-project.eu/services/");
        RepositoryConnection con = item.getObjectConnection();
        try {
            con.begin();
            con.exportStatements(null, null, null, true, new RDFHandlerBase(){
                @Override
                public void handleStatement(Statement st) {
                    model.add(st);
                }
            });
        } finally {
            con.rollback();
            con.close();
        }
        log.debug("--- START generated RDF ---");
        PrintWriter out = new PrintWriter(System.out);
        Rio.write(model, out, RDFFormat.TURTLE);
        out.close();
        log.debug("--- END generated RDF ---");
    }

    private void assertTopic(TopicBody topic, RDFObject itemOrPart, Iterable<Selector> selectors) {
        
        Assert.assertNotNull(topic.getTopicLabels());
        Assert.assertFalse(topic.getTopicLabels().isEmpty());
        Assert.assertNotNull(topic.getTopic());
        Assert.assertNotNull(topic.getConfidence());
        Assert.assertTrue(topic.getConfidence() >= 0d && topic.getConfidence() <= 1d);
        
        Assert.assertNotNull(topic.getContent());
        Assert.assertEquals(itemOrPart, topic.getContent());
        if(selectors != null){
            Assert.assertNotNull(topic.getSelectors());
            for(Selector s : selectors){
                topic.getSelectors().contains(s);
            }
        }
        
    }

    private void assertResponses(final Map<URI, String> responses) {
        log.info(" - {} response(s): ", responses.size());
        for(Entry<URI, String> entry : responses.entrySet()){
            log.info("   {}: {}",entry.getKey(), entry.getValue());
        }
    }

}
