package eu.mico.extractors.opennlp.ner;

import static eu.mico.platform.anno4j.model.namespaces.FAM.ENTITY_MENTION_ANNOTATION;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.openrdf.idGenerator.IDGenerator;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.LangString;
import org.openrdf.repository.object.ObjectConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.impl.targets.SpecificResource;

import eu.mico.platform.anno4j.model.fam.LanguageBody;
import eu.mico.platform.anno4j.model.fam.NamedEntityBody;
import eu.mico.platform.anno4j.model.fam.TextPositionQuoteSelector;
import eu.mico.platform.event.api.AnalysisResponse;
import eu.mico.platform.event.api.AnalysisService;
import eu.mico.platform.event.impl.AnalysisServiceUtil;
import eu.mico.platform.event.model.AnalysisException;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;
import eu.mico.platform.persistence.model.Resource;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.sentdetect.SentenceDetector;
import opennlp.tools.tokenize.SimpleTokenizer;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.util.Span;

/**
 * OpenNLP Named Entity Extraction Service
 *
 * @author Rupert Westenthaler
 */
public class OpenNlpNerService implements AnalysisService {

    private static Logger log = LoggerFactory.getLogger(OpenNlpNerService.class);

    private static final Charset UTF8 = Charset.forName("UTF-8");

    public static final java.lang.String SCHEMA_NS = "http://schema.org/";
            
    private static final Map<String,Collection<String>> NAMED_ENTITY_TYPE_MAPPINGS;
    /**
     * The minimum required confidence so that language annotations are accepted
     */
    private static final double MIN_CONF = 0.5;
    
    static {
        Map<String, Collection<String>> mappings = new HashMap<>();
        //TAGS used by all models (en, de, es, it)
        mappings.put("PER", Collections.singleton(SCHEMA_NS + "Person"));
        mappings.put("ORG", Collections.singleton(SCHEMA_NS + "Organization"));
        mappings.put("LOC", Collections.singleton(SCHEMA_NS + "Place"));
        mappings.put("MISC", Collections.<String>emptyList()); //no type
        //TAGS specific to it
        mappings.put("GPE", Arrays.asList(SCHEMA_NS + "AdministrativeArea", SCHEMA_NS + "Place"));
        NAMED_ENTITY_TYPE_MAPPINGS = Collections.unmodifiableMap(mappings);
    }
    
    
    private Map<String, List<TokenNameFinderModel>> models = new HashMap<>();
    
    /**
     * DocumentCategorizerME are not thread save so create one instance per thread
     */
    private ThreadLocal<Map<String,List<NameFinderME>>> nameFinders;
    

    SentenceDetector sentDetect = RegexSentenceSplitter.getInstance();
    Tokenizer tokenizer = SimpleTokenizer.INSTANCE;


    public OpenNlpNerService(String queueName, Iterable<File> modelFiles) throws IOException {
        assert queueName != null && !queueName.isEmpty();
        assert modelFiles != null;
        for(File modelFile : modelFiles){
            log.info(" ... init {}", modelFile);
            TokenNameFinderModel model = new TokenNameFinderModel(modelFile);
            String lang = StringUtils.lowerCase(model.getLanguage());
            log.info("   - lang: {}", lang);
            List<TokenNameFinderModel> langModels = models.get(lang);
            if(langModels == null){
                langModels = new LinkedList<>();
                models.put(lang, langModels);
            }
            langModels.add(model);
        }
        //NameFinderME are not thread save, so we will create instances for every thread
        nameFinders = new ThreadLocal<Map<String,List<NameFinderME>>>(){
            @Override
            protected Map<String,List<NameFinderME>> initialValue() {
                Map<String,List<NameFinderME>> nameFinders = new HashMap<>();
                for(Entry<String,List<TokenNameFinderModel>> entry : models.entrySet()){
                    List<NameFinderME> langNameFinders = new ArrayList<>(entry.getValue().size());
                    for(TokenNameFinderModel langModel : entry.getValue()){
                        langNameFinders.add(new NameFinderME(langModel));
                    }
                    nameFinders.put(entry.getKey(), langNameFinders);
                }
                return Collections.unmodifiableMap(nameFinders);
            }
        };
    }

	private String getRegistrationProperties(String propName){
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("registration.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			log.error("Error reading registration properties", e);
		}
		return props.getProperty(propName);
	}
	
	@Override
	public String getExtractorID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorId");
	}

	@Override
	public String getExtractorModeID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorModeId");
	}

	@Override
	public String getExtractorVersion() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorVersion");
	}

    @Override
    public String getProvides() {
        return "application/x-mico-rdf";
    }

    @Override
    public String getRequires() {
        return "application/x-mico-fam-language-rdf"; //language annotations
    }

//    @Override
//    public String getQueueName() {
//        return queueName;
//    }

    @Override
    public void call(AnalysisResponse response, Item ci,  List<Resource> resourceList,  Map<String, String> params) throws AnalysisException, RepositoryException, IOException {
        if(response == null){
            throw new AnalysisException("The parsed response MUST NOT be NULL!");
        }
        if(ci == null){
            throw new AnalysisException("The parsed content item MUST NOT be NULL!");
        }
        //search for the text asset
        log.trace("> process Item {}", ci.getURI());
        log.trace(" - resourceList (size:{}): {}", resourceList.size(), resourceList);
        List<ExtractionContext> extractionContexts = new LinkedList<>();
        for(Resource resource : resourceList){ //we expect parts with LanugageBody
            if(resource instanceof Part){
                Part langAnnoPart = (Part)resource;
                if(langAnnoPart.getBody() instanceof LanguageBody){
                    LanguageBody langAnno = (LanguageBody)langAnnoPart.getBody();
                    if(StringUtils.isNotBlank(langAnno.getLanguage()) && 
                            (langAnno.getConfidence() == null || langAnno.getConfidence().doubleValue() > MIN_CONF)){
                        for(Resource input : langAnnoPart.getInputs()){
                            if(input.hasAsset()){
                                Asset inputAsset = input.getAsset();
                                String format = inputAsset.getFormat() == null ? null : 
                                    inputAsset.getFormat().split(";")[0].trim().toLowerCase(Locale.ROOT);
                                if("text/plain".equals(format)){
                                    ExtractionContext context = new ExtractionContext();
                                    context.setLangAnnoPart(langAnnoPart);
                                    context.setLangAnno(langAnno);
                                    context.setTextResource(input);
                                    context.setTextAsset(inputAsset);
                                    extractionContexts.add(context);
                                } else {
                                    log.debug("Input Resource {} of Language Annotation Part {} does not have a 'text/plain' Asset (format: {})",
                                            input.getURI(), langAnnoPart.getURI(), inputAsset.getFormat());
                                }
                            } else {
                                log.debug("Input Resource {} of Language Annotation Part {} does not have an Asset assigned",
                                        input.getURI(), langAnnoPart.getURI());
                            }
                        } //end for all input resources
                    } else {
                        log.debug("fam:LanguageAnnotation for Part {} is not valid or has a low confidence {} < {}",
                                resource.getURI(), langAnno.getConfidence() < MIN_CONF);
                    }
                } else {
                    log.debug(" - Part: {} does not have a fam:LanguageAnnotation as Body (body: {})", resource.getURI(), langAnnoPart.getBody().getResource());
                }
            } else {
                log.debug(" - Resource: {} is not a Part (type: {})", resource.getURI(),resource.getClass().getSimpleName());
            }
        }
        log.debug("found {} extraction contexts for Item {}", extractionContexts.size(), ci.getURI());
        Map<URI, List<ExtractionContext>> asset2Contexts = new HashMap<>();
        for(ExtractionContext c : extractionContexts){
            List<ExtractionContext> cs = asset2Contexts.get(c.getTextResource().getURI());
            if(cs == null){
                cs = new ArrayList<>();
                asset2Contexts.put(c.getTextResource().getURI(), cs);
            }
            cs.add(c);
        }
        
        //now we need to extract Named Entities for all text/plain assets with
        //a language annotation
        log.debug("found {} text/plain assets with valid fam:LanugageAnnotation", asset2Contexts.size());
        
        for(List<ExtractionContext> contexts : asset2Contexts.values()){
            //but first sort the language annotations based on their confidence
            Collections.sort(contexts, new Comparator<ExtractionContext>() {
                @Override
                public int compare(ExtractionContext c1, ExtractionContext c2) {
                    Double conf2 = c2.getLangAnno().getConfidence();
                    Double conf1 = c1.getLangAnno().getConfidence();
                    //assume 1.0 if no confidence is provided
                    return Double.compare(conf2 == null ? 1d : conf2.doubleValue(), 
                            conf1 == null ? 1d : conf1.doubleValue());
                }
            });
            
            extractNamedEntities(response, ci, contexts.get(0));
        
        }
    }

    /**
     * Extracts Named Entities from the parsed ExtractionContext
     * @param response
     * @param ci
     * @param context
     * @throws IOException
     * @throws RepositoryException
     */
    private void extractNamedEntities(AnalysisResponse response, Item ci, ExtractionContext context)
                    throws IOException, RepositoryException {
        ObjectConnection con = ci.getObjectConnection();

        String txtContent = IOUtils.toString(context.getTextAsset().getInputStream(), UTF8);
        String contentLang = context.getLangAnno().getLanguage().trim().toLowerCase(Locale.ROOT);
        
        log.info("> processing item {}, resource {}, asset {}, language", ci.getURI(), context.getTextResource().getURI(), context.getTextAsset(), contentLang);
        if(log.isDebugEnabled()){
            log.debug("  - content: {}",StringUtils.abbreviate(txtContent, 50));
        }
        if(StringUtils.isBlank(txtContent)){
            log.info(" - no text to extract Named Entities from ");
            return;
        }
        
        List<NameFinderME> nameFinders = this.nameFinders.get().get(contentLang);
        if(nameFinders == null || nameFinders.isEmpty()){
            log.info(" - no models for language {} present ", contentLang);
            return;
        }
        try {
            List<NamedEntityBody> namedEntities = new LinkedList<>();
            Span[] sentSpans = sentDetect.sentPosDetect(txtContent);
            String[] sentTxts = Span.spansToStrings(sentSpans, txtContent);
            for(int sIdx = 0; sIdx < sentSpans.length; sIdx++){
                String sent = sentTxts[sIdx];
                Span sentSpan = sentSpans[sIdx];
                int sentOffset = sentSpan.getStart();
                Span[] tokenSpans = tokenizer.tokenizePos(sent);
                String[] tokens = Span.spansToStrings(tokenSpans, sent);
                for(NameFinderME nameFinder : nameFinders){
                    Span[] neSpans = nameFinder.find(tokens);
                    if(neSpans != null){
                        double[] probs = nameFinder.probs();
                        for(int i = 0; i< neSpans.length; i++){
                            Span entitySpan = neSpans[i];
                            //determine the type (based on the String tag)
                            final String tag = entitySpan.getType();
                            Collection<String> types = NAMED_ENTITY_TYPE_MAPPINGS.get(tag);
                            if(types == null){ //try with upper case
                                types = NAMED_ENTITY_TYPE_MAPPINGS.get(tag.toUpperCase(Locale.ROOT));
                            }
                            if(types == null){ //not a mapped type ... write an unmapped type
                                if(StringUtils.isBlank(tag)){
                                    types = Collections.<String>emptySet();
                                } else {
                                    log.warn("Unmapped Named Entity Tag '{}' for language {}", tag, contentLang);
                                    types = Collections.singleton("urn:mico-project.eu:unmapped-named-entity-type:" + tag);
                                }
                            }
                            double prob = probs[i];
                            Part nePart = ci.createPart(AnalysisServiceUtil.getServiceID(this));
                            SpecificResource neSr = createObject(con, SpecificResource.class);
                            nePart.getTargets().add(neSr);
                            neSr.setSource(context.getTextResource().getRDFObject());
                            TextPositionQuoteSelector selector = createObject(con, TextPositionQuoteSelector.class);
                            int start = sentOffset + tokenSpans[entitySpan.getStart()].getStart();
                            int end = sentOffset + tokenSpans[entitySpan.getEnd()-1].getEnd();
                            String name = txtContent.substring(start, end);
                            selector.setStart(start);
                            selector.setEnd(end);
                            selector.setExact(name);
                            selector.setPrefix(txtContent.substring(Math.max(0, start-5), start));
                            selector.setSuffix(txtContent.substring(end, Math.min(txtContent.length(), end+5)));
                            neSr.setSelector(selector);
                            NamedEntityBody namedEntity = createObject(con, NamedEntityBody.class);
                            
                            nePart.setSyntacticalType(ENTITY_MENTION_ANNOTATION);
                            nePart.setSemanticType("Named entity recognized in the input text file");
                            nePart.addInput(context.getTextResource());
                            nePart.addInput(context.getLangAnnoPart());
                            
                            namedEntity.addSelector(selector);
                            namedEntity.setContent(context.getTextResource().getRDFObject());
                            namedEntity.setMention(new LangString(name, contentLang)); 
                            for(String type : types){
                                namedEntity.addType(type);
                            }
                            namedEntity.setConfidence(prob);
                            nePart.setBody(namedEntity);
                            log.debug(" - Named Entity [{},{} | prob: {}, tag: {}, types: {}] {}",
                                    start, end, prob, tag, types, name);
                            response.sendNew(ci, nePart.getURI());
                            namedEntities.add(namedEntity);
                        } //end for all extracted named entities
                    } //else no entities extracted
                }
            }
        } finally { //clear any adaptive data from the shared name finders
            for(NameFinderME nameFinder : nameFinders){
                nameFinder.clearAdaptiveData();
            }
        }
    }

    static <T> T createObject(ObjectConnection con, Class<T> type) throws RepositoryException {
        return createObject(con, type, null);
    }
    
    static <T> T createObject(ObjectConnection con, Class<T> type, org.openrdf.model.Resource resource) throws RepositoryException {
        return con.addDesignation(con.getObjectFactory().createObject(
                resource == null ? IDGenerator.BLANK_RESOURCE : resource, type), type);
    }
    
    private class ExtractionContext {
        
        private Part langAnnoPart;
        private LanguageBody langAnno;
        
        private Resource textResource;
        private Asset textAsset;
        
        public Part getLangAnnoPart() {
            return langAnnoPart;
        }
        public void setLangAnnoPart(Part langAnnoPart) {
            this.langAnnoPart = langAnnoPart;
        }
        public LanguageBody getLangAnno() {
            return langAnno;
        }
        public void setLangAnno(LanguageBody langAnno) {
            this.langAnno = langAnno;
        }
        public Resource getTextResource() {
            return textResource;
        }
        public void setTextResource(Resource textResource) {
            this.textResource = textResource;
        }
        public Asset getTextAsset() {
            return textAsset;
        }
        public void setTextAsset(Asset textAsset) {
            this.textAsset = textAsset;
        }
        
    }
    
    
}
