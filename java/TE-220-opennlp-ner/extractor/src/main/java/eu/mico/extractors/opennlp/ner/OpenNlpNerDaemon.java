package eu.mico.extractors.opennlp.ner;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.mortbay.log.Log;

import eu.mico.platform.event.api.EventManager;
import eu.mico.platform.event.impl.EventManagerImpl;

/**
 * OpenNLP classifier daemon
 *
 * @author Rupert Westenthaler
 */
public class OpenNlpNerDaemon implements AutoCloseable, Daemon {

    private String username;
    private String password;
    private String hostname;

    private EventManager eventManager;

    private OpenNlpNerService nerService;
    private String modelPath;

    /**
     * Path to the root folder of Language Modles (relative to the install path) 
     */
    public static final String MODEL_PATH = "/usr/share/mico-extractor-opennlp-ner/models";
    
    @Override
    public void close() throws Exception {
        System.out.println("OpenNLP ner for model " + modelPath + "closed");
    }

    @Override
    public void init(DaemonContext context) throws DaemonInitException {
        Collection<File> models;
        try {
            CommandLineParser parser = new GnuParser();
            Log.info("arguments: {}", context.getArguments());
            CommandLine cmd = parser.parse(createOptions(), context.getArguments());
            username = cmd.getOptionValue('u');
            password = cmd.getOptionValue('p');
            hostname = cmd.getOptionValue('h');
            modelPath = cmd.getOptionValue('m');
            if(modelPath == null){
                modelPath = MODEL_PATH;
            }
            File modelFile = new File(modelPath);
            if(!modelFile.exists()){
                throw new DaemonInitException("Parsed model directory " + modelFile.getAbsolutePath() + "('-m' option) does not exist!"); 
            }
            models = lookupModels(modelFile);
            if(models.isEmpty()){
                throw new DaemonInitException("Parsed model Directory " + modelFile.getAbsolutePath() + "('-m' option) does not contain any OpenNLP model files (*.zip, *.jar or *.bin files)!"); 
            }
            
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }
        try {
            nerService = new OpenNlpNerService("opennlp-ner", models);
        } catch (IOException e) {
            throw new DaemonInitException("Unable to initialise service for model file " + modelPath, e); 
        }

        try {
            eventManager = new EventManagerImpl(hostname, username, password);
            System.out.println("OpenNLP text classifier daemon for model '" +modelPath + "' ("+models.size()+" models) initialized");
        } catch (IOException e) {
            throw new DaemonInitException("Unable to initialize event manager", e);
        }
    }

    private Collection<File> lookupModels(File modelFile) throws DaemonInitException {
        IOFileFilter modelFileFilter = FileFilterUtils.or(
                FileFilterUtils.suffixFileFilter(".zip", IOCase.INSENSITIVE),
                FileFilterUtils.suffixFileFilter(".jar", IOCase.INSENSITIVE),
                FileFilterUtils.suffixFileFilter(".bin", IOCase.INSENSITIVE));
        if(modelFile.isDirectory()){
            //OpenNLP Models can use zip, jar and bin as extension
            return FileUtils.listFiles(modelFile, modelFileFilter,
                    TrueFileFilter.INSTANCE); //recursive
        } else if(modelFile.isFile()){
            if(modelFileFilter.accept(modelFile)){
                return Collections.singleton(modelFile);
            }
        }
        return Collections.emptySet();
    }

    @Override
    public void start() throws Exception {
        eventManager.init();
        eventManager.registerService(nerService);
        System.out.println("OpenNLP Text Classification Daemon started");
    }

    @Override
    public void stop() throws Exception {
        eventManager.unregisterService(nerService);
        eventManager.shutdown();
        System.out.println("OpenNLP Text Classification Daemon stopped");
        close();
    }

    @Override
    public void destroy() {
        nerService = null;
        eventManager = null;
        System.out.println("ner daemon destroyed");
    }

    @SuppressWarnings("static-access")
    private static Options createOptions() {
        Options options = new Options();

        options.addOption(
                OptionBuilder
                        .withArgName("username")
                        .hasArg()
                        .withLongOpt("username")
                        .isRequired()
                        .create('u')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("password")
                        .hasArg()
                        .withLongOpt("password")
                        .isRequired()
                        .create('p')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("hostname")
                        .hasArg()
                        .withLongOpt("hostname")
                        .isRequired()
                        .create('h')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("modeldir")
                        .withDescription("The root directory containing the model files")
                        .hasArg()
                        .withLongOpt("modeldir")
                        .create('m')
        );
        return options;
    }

}
