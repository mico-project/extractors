# OpenNLP Document Classifier Extractor

This extractor uses the [OpenNLP Name Finder API](https://opennlp.apache.org/documentation/1.5.2-incubating/manual/opennlp.html#tools.namefind.recognition.api) to extract named entities from parsed text. It has two modules. First the `/extractor` modules that provides the implementation of the Mico Extractor and second the `/models` that does build multiple Debian packages with pre-trained NER models for different languages.
