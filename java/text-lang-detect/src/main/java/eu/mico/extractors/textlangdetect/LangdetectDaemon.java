package eu.mico.extractors.textlangdetect;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.mortbay.log.Log;

import com.cybozu.labs.langdetect.LangDetectException;

import eu.mico.platform.event.api.EventManager;
import eu.mico.platform.event.impl.EventManagerImpl;

/**
 * OpenNLP classifier daemon
 *
 * @author Rupert Westenthaler
 */
public class LangdetectDaemon implements AutoCloseable, Daemon {

    private String username;
    private String password;
    private String hostname;

    private EventManager eventManager;

    private LangdetectService langDetectService;

    @Override
    public void close() throws Exception {
        System.out.println("Text Language Dedection Deamon closed");
    }

    @Override
    public void init(DaemonContext context) throws DaemonInitException {
        try {
            CommandLineParser parser = new GnuParser();
            Log.info("arguments: {}", context.getArguments());
            CommandLine cmd = parser.parse(createOptions(), context.getArguments());
            username = cmd.getOptionValue('u');
            password = cmd.getOptionValue('p');
            hostname = cmd.getOptionValue('h');
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }
        try {
            langDetectService = new LangdetectService("text-lang-detect", new LanguageIdentifier());
        } catch (LangDetectException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }

        try {
            eventManager = new EventManagerImpl(hostname, username, password);
            System.out.println("Text Language Detection Service initialized");
        } catch (IOException e) {
            throw new DaemonInitException("Unable to initialize event manager", e);
        }
    }

    @Override
    public void start() throws Exception {
        eventManager.init();
        eventManager.registerService(langDetectService);
        System.out.println("OpenNLP Text Classification Daemon started");
    }

    @Override
    public void stop() throws Exception {
        eventManager.unregisterService(langDetectService);
        eventManager.shutdown();
        System.out.println("OpenNLP Text Classification Daemon stopped");
        close();
    }

    @Override
    public void destroy() {
        langDetectService = null;
        eventManager = null;
        System.out.println("OpenNLP Text Classification Daemon destroyed");
    }

    @SuppressWarnings("static-access")
    private static Options createOptions() {
        Options options = new Options();

        options.addOption(
                OptionBuilder
                        .withArgName("username")
                        .hasArg()
                        .withLongOpt("username")
                        .isRequired()
                        .create('u')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("password")
                        .hasArg()
                        .withLongOpt("password")
                        .isRequired()
                        .create('p')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("hostname")
                        .hasArg()
                        .withLongOpt("hostname")
                        .isRequired()
                        .create('h')
        );
        return options;
    }

}
