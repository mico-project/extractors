package eu.mico.extractors.textlangdetect;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.openrdf.idGenerator.IDGenerator;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cybozu.labs.langdetect.LangDetectException;
import com.cybozu.labs.langdetect.Language;
import com.github.anno4j.model.impl.targets.SpecificResource;

import eu.mico.platform.anno4j.model.fam.LanguageBody;
import eu.mico.platform.anno4j.model.namespaces.FAM;
import eu.mico.platform.event.api.AnalysisResponse;
import eu.mico.platform.event.api.AnalysisService;
import eu.mico.platform.event.impl.AnalysisServiceUtil;
import eu.mico.platform.event.model.AnalysisException;
import eu.mico.platform.event.model.Event.ErrorCodes;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;
import eu.mico.platform.persistence.model.Resource;

/**
 * OpenNLP Text Classifier Service
 *
 * @author Rupert Westenthaler
 */
public class LangdetectService implements AnalysisService {

    private static Logger log = LoggerFactory.getLogger(LangdetectService.class);

    private static final Charset UTF8 = Charset.forName("UTF-8");

    private final LanguageIdentifier langId;

    public LangdetectService(String queueName, LanguageIdentifier langId){
        assert queueName != null && !queueName.isEmpty();
        assert langId != null;
        this.langId = langId;
    }

	private String getRegistrationProperties(String propName){
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("registration.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			log.error("Error reading registration properties", e);
		}
		return props.getProperty(propName);
	}
	
	@Override
	public String getExtractorID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorId");
	}

	@Override
	public String getExtractorModeID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorModeId");
	}

	@Override
	public String getExtractorVersion() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorVersion");
	}

    @Override
    public String getProvides() {
        return "application/x-mico-fam-language-rdf"; //language annotations
    }

    @Override
    public String getRequires() {
        return "text/plain";
    }

//    @Override
//    public String getQueueName() {
//        return queueName;
//    }

    @Override
    public void call(AnalysisResponse response, Item ci,  List<Resource> resourceList,  Map<String, String> params) throws AnalysisException, RepositoryException, IOException {
        if(response == null){
            throw new AnalysisException("The parsed response MUST NOT be NULL!");
        }
        if(ci == null){
            throw new AnalysisException("The parsed content item MUST NOT be NULL!");
        }
        //search for the text asset
        log.trace("> process Item {}", ci.getURI());
        log.trace(" - resourceList (size:{}): {}", resourceList.size(), resourceList);
        Asset txtAsset = null;
        Resource txtResource = null;
        for(Resource resource : resourceList){
            if(resource.hasAsset()){
                log.debug(" - check Assert for Resource: {} (type: {})", resource.getURI(),resource.getClass().getSimpleName());
                Asset asset = resource.getAsset();
                String format = asset.getFormat().split(";")[0].trim();
                log.debug("   - format: {}",format);
                if(getRequires().equalsIgnoreCase(format)){ //we can process this asset
                    log.debug(" - use Asset {} of {} {} with format {}",
                            asset, resource instanceof Item ? "item" : "part" , resource.getURI(), asset.getFormat());
                    txtAsset = asset;
                    txtResource = resource;
                    break;
                } else if(log.isDebugEnabled()){ 
                    log.debug(" - ignore Asset {} of {} {} because its format {} is not supported (supported: {})",
                            asset, resource instanceof Item ? "item" : "part" , resource.getURI(), format, getRequires());
                }
            } else {
                log.debug(" - Resource: {} (type: {}) has no asset", resource.getURI(),resource.getClass().getSimpleName());
            }
        }
        if(txtAsset == null){
            log.warn(" - got call for Item {} and resources {} where none had an asset with the supported format {}",
                    ci.getURI(), resourceList, getRequires());
            throw new AnalysisException(ErrorCodes.MISSING_ANNOTATION, "No text/plain asset", null);
        }
        String txtContent = IOUtils.toString(txtAsset.getInputStream(), UTF8);
        if(StringUtils.isBlank(txtContent)){
            throw new AnalysisException(ErrorCodes.MISSING_ANNOTATION, "The text/plain asset contains an empty text",null);
        }
        try {
            List<Language> languages = langId.getLanguages(txtContent);
            if(languages != null && !languages.isEmpty()){
                ObjectConnection con = ci.getObjectConnection();
                for(Language lang : languages){
                    Part langPart = ci.createPart(AnalysisServiceUtil.getServiceID(this));
                    langPart.setSyntacticalType(FAM.LANGUAGE_ANNOTATION);
                    langPart.setSemanticType("Language Annotation for the plain/text asset of " + txtResource);
                    langPart.addInput(txtResource);
                    SpecificResource sr = createObject(con, SpecificResource.class);
                    langPart.getTargets().add(sr);
                    sr.setSource(txtResource.getRDFObject());
                    LanguageBody body = createLanguageBody(con, lang);
                    langPart.setBody(body);
                    body.setContent(txtResource.getRDFObject());
                    response.sendNew(ci, langPart.getURI());
                }
            } else {
                log.info("No language detected for Item {} (text resource: {}) ",
                        ci.getURI(), txtResource.getURI());
            }
        } catch (LangDetectException e){
            Enum<?> errorCode = e.getCode(); //this enum is not visible :(
            if("CantDetectError".equals(errorCode.name()) || "NoTextError".equals(errorCode.name())){
                //in this case just do not add an language annotation
                log.info("Unable to detect language for Item {} (text resource: {}) because the text is "
                        + "empty or does not contain any features to extract the language from.",
                        ci.getURI(), txtResource.getURI());
            } else {
                throw new AnalysisException(ErrorCodes.UNEXPECTED_ERROR, 
                        "Exception while detecting the language for Item '"+ci.getURI()
                        + "' (text part: " + txtResource.getURI() + ")", e);
            }
        }
    }

    private LanguageBody createLanguageBody(ObjectConnection con, Language lang) throws RepositoryException {
        LanguageBody lb = createObject(con, LanguageBody.class);
        lb.setLanguage(lang.lang.toLowerCase(Locale.ROOT));
        lb.setConfidence(lang.prob);
        return lb;
    }
    
    private <T> T createObject(ObjectConnection con, Class<T> type) throws RepositoryException {
        return createObject(con, type, null);
    }
    
    private <T> T createObject(ObjectConnection con, Class<T> type, org.openrdf.model.Resource resource) throws RepositoryException {
        return con.addDesignation(con.getObjectFactory().createObject(
                resource == null ? IDGenerator.BLANK_RESOURCE : resource, type), type);
    }

}
