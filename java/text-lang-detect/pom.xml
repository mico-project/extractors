<?xml version="1.0"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>eu.mico-project.extractors</groupId>
        <artifactId>mico-extractors</artifactId>
        <version>3.1.1-SNAPSHOT</version>
        <relativePath>../pom.xml</relativePath>
    </parent>

    <groupId>eu.mico-project.extractors</groupId>
    <artifactId>text-lang-detect</artifactId>
    <name>Language Detection for Texts</name>
    <description>
      Classifier to detect the language of a text
    </description>
    <version>1.2.1-SNAPSHOT</version>

    <organization>
        <name>MICO</name>
        <url>http://mico-project.eu</url>
    </organization>

    <properties>
      <!-- Required for unit tests -->
      <sesame.version>2.8.6</sesame.version>
    </properties>

    <developers>
        <developer>
            <name>Rupert Westenthaler</name>
            <email>rupert.westenthaler@salzburgresearch.at</email>
            <organization>Salzburg Research</organization>
        </developer>
    </developers>

    <dependencies>
        <!-- This is a Mico 2.0 extractor-->
        <dependency>
            <groupId>eu.mico-project.platform</groupId>
            <artifactId>event</artifactId>
        </dependency>
        <dependency>
            <groupId>com.rabbitmq</groupId>
            <artifactId>amqp-client</artifactId>
        </dependency>
        <dependency>
            <groupId>eu.mico-project.platform</groupId>
            <artifactId>persistence</artifactId>
        </dependency>
        <dependency>
            <groupId>eu.mico-project.platform</groupId>
            <artifactId>fam-anno4j</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openrdf.sesame</groupId>
            <artifactId>sesame-rio-api</artifactId>
            <version>${sesame.version}</version>
        </dependency>
        <dependency>
            <groupId>org.openrdf.sesame</groupId>
            <artifactId>sesame-model</artifactId>
            <version>${sesame.version}</version>
        </dependency>
        <dependency>
            <groupId>org.openrdf.sesame</groupId>
            <artifactId>sesame-rio-turtle</artifactId>
            <version>${sesame.version}</version>
        </dependency>
        <dependency>
            <groupId>commons-daemon</groupId>
            <artifactId>commons-daemon</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-cli</groupId>
            <artifactId>commons-cli</artifactId>
        </dependency>
        
        <!-- Language detection dependencies -->
        <dependency>
          <groupId>com.cybozu.labs</groupId>
          <artifactId>langdetect</artifactId>
          <version>1.1-20120112</version>
        </dependency>
        
        <!-- Common dependencies -->
        <dependency>
          <groupId>org.apache.commons</groupId>
          <artifactId>commons-lang3</artifactId>
        </dependency>
        
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>eu.mico-project.platform</groupId>
            <artifactId>event</artifactId>
            <type>test-jar</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>eu.mico-project.platform</groupId>
            <artifactId>persistence</artifactId>
            <type>test-jar</type>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
    	<resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>**/*.properties</include>
                </includes>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <configuration>
                    <tagNameFormat>textlang#@{project.version}</tagNameFormat>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <createDependencyReducedPom>false</createDependencyReducedPom>
                            <transformers>
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ApacheLicenseResourceTransformer" />
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>eu.mico.extractors.textlangdetect.LangdetectDaemon</mainClass>
                                </transformer>
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer" />
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ApacheNoticeResourceTransformer">
                                    <addHeader>false</addHeader>
                                </transformer>
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <!-- include the sonatype repo for the redlink api-client SNAPSHOT version -->
    <repositories>
        <repository>
            <id>oss-sonatype</id>
            <name>oss-sonatype</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

    <profiles>
        <profile>
            <id>debian</id>
            <activation>
              <activeByDefault>true</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <artifactId>maven-resources-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>copy-resources</id>
                                <!-- here the phase you need -->
                                <phase>validate</phase>
                                <goals>
                                    <goal>copy-resources</goal>
                                </goals>
                                <configuration>
                                    <outputDirectory>${basedir}/target/deb</outputDirectory>
                                    <resources>
                                        <resource>
                                            <directory>src/deb</directory>
                                            <filtering>true</filtering>
                                        </resource>
                                    </resources>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <artifactId>jdeb</artifactId>
                        <groupId>org.vafer</groupId>
                        <!-- configuration - see eu.mico-project.platform:parent -->
                        <executions>
                            <execution>
                                <phase>package</phase>
                                <goals>
                                    <goal>jdeb</goal>
                                </goals>
                                <configuration>
                                    <name>mico-extractor-${project.artifactId}</name>
                                    <dataSet>
                                        <data>
                                            <src>${project.build.directory}/${project.build.finalName}.jar</src>
                                            <dst>mico-extractor-${project.artifactId}.jar</dst>
                                            <type>file</type>
                                            <mapper>
                                                <type>perm</type>
                                                <prefix>/usr/share/mico</prefix>
                                            </mapper>
                                        </data>
                                        <data>
                                            <src>
                                                ${project.build.directory}/deb/resources/${project.artifactId}.jar-config
                                            </src>
                                            <dst>mico-extractor-${project.artifactId}.jar-config</dst>
                                            <type>file</type>
                                            <conffile>true</conffile>
                                            <mapper>
                                                <type>perm</type>
                                                <prefix>/usr/share/mico</prefix>
                                                <filemode>0755</filemode>
                                            </mapper>
                                        </data>
                                    </dataSet>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
