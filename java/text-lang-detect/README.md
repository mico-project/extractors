# Language Detection Extractor for Textual Content

This extractor uses detects the language of textual content and adds Items with FAM [LanguageAnnotation](https://github.com/fusepoolP3/overall-architecture/blob/master/wp3/fp-anno-model/fp-anno-model.md#language-annotation) Bodies for detected languages.