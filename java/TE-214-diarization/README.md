![MICO](http://www.mico-project.eu/wp-content/uploads/2014/04/mico_logo.png)

[TOC]

# LIUM speaker diarization
This extractor uses the LIUM_SpkDiarization tool to segment incoming audio by speaker diarization analysis.

# Changes
* V1.2.1 
** FIX: do not fail if asset to analyse belongs to an item