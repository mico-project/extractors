package se.umu.cs.mico;

import eu.mico.platform.anno4j.model.impl.bodymmm.DiarizationBodyMMM;
import static eu.mico.platform.anno4j.model.namespaces.MMMTERMS.DIARIZATION_BODY;
import eu.mico.platform.event.api.AnalysisResponse;
import eu.mico.platform.event.api.AnalysisService;
import eu.mico.platform.event.model.AnalysisException;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;
import eu.mico.platform.persistence.model.Resource;

import org.apache.commons.io.FileUtils;
import org.openrdf.idGenerator.IDGenerator;
import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.impl.targets.SpecificResource;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Does speaker diarization using LIUM SpkDiarization tool.
 *
 * @author Adam Dahlgren
 *
 */
public class SpeakerDiarization implements AnalysisService {

	private static final String PROP_LIUM_PATH = "lium.path";

	private static final String JAVASETTINGS =
			"-Xmx2024m";

	private static Logger log = LoggerFactory
			.getLogger(SpeakerDiarization.class);

	private static SimpleDateFormat isodate = new SimpleDateFormat(
			"yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'",
			DateFormatSymbols.getInstance(Locale.US));

	static {
		isodate.setTimeZone(TimeZone.getTimeZone("UTC"));
	}

	public URI getServiceID() {
		return new URIImpl("http://www.mico-project.eu/services/"+getQueueName());
	}

	public String getProvides() {
		return "text/vnd.umu-diarization+xml";
	}

	public String getRequires() {
		return "audio/wav";
	}

	public String getQueueName() {
		return getExtractorID()+"-"+getExtractorVersion()+"-"+getExtractorModeID();
	}
	
	private String getRegistrationProperties(String propName){
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("registration.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			log.error("Error reading registration properties", e);
		}
		return props.getProperty(propName);
	}
	
	@Override
	public String getExtractorID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorId");
	}

	@Override
	public String getExtractorModeID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorModeId");
	}

	@Override
	public String getExtractorVersion() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorVersion");
	}

	@Override
    public void call(AnalysisResponse resp, Item item, List<Resource> resourceList, Map<String, String> params)
            throws AnalysisException, IOException, RepositoryException {
        try {
            ObjectConnection con = item.getObjectConnection();
            if (resourceList.size() > 1) {
                throw new IllegalArgumentException(
                        "Resource list only allows one item to be processed.");
            }

            Resource content = resourceList.get(0);
            InputStream inputStream = content.getAsset().getInputStream();

            /* Initiate Kaldi input audio file */
			File tmpFolder = Files.createTempDirectory("mico_lium-").toFile();
			log.info("Using tmp folder: " + tmpFolder.getAbsolutePath());


			File targetFile = File.createTempFile("mico_audio", ".wav",
					tmpFolder);

			FileUtils.copyInputStreamToFile(inputStream, targetFile);


			/* Runs diarization, creates temp_for_segm.xml file */
			runDiarization(tmpFolder, targetFile);

			File segmentXMLFile = new File(tmpFolder.getAbsolutePath() +
										   "/temp_for_segm.xml");

            /* Check segment file */
			if (!segmentXMLFile.exists()) {
				log.error("Error executing LIUM SpkDiarization, " +
						  "output file not found");

			}


			Part result = item.createPart(getServiceID());

			OutputStream outputStream = result.getAsset().getOutputStream();
			outputStream.write(FileUtils.readFileToByteArray(segmentXMLFile));
			outputStream.close();
			result.getAsset().setFormat(getProvides());

			result.setSyntacticalType(DIARIZATION_BODY);
			result.setSemanticType("Diarization of the input audio ");
			result.setBody(createObject(con, DiarizationBodyMMM.class));
			
			SpecificResource sr = createObject(con, SpecificResource.class);
			sr.setSource(content.getRDFObject());
            result.getTargets().add(sr);
            
			result.addInput(content);
			
			

			// report newly available results to broker
			resp.sendNew(item, result.getURI());
			resp.sendFinish(item);

			FileUtils.deleteDirectory(tmpFolder);

		} catch (RepositoryException e) {
			log.error("error accessing metadata repository", e);

			throw new AnalysisException("error accessing metadata repository",
					e);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void runDiarization(File tmpFolder, File targetFile)
			throws AnalysisException {
		Process diarizationProcess = null;
		try {
			diarizationProcess = Runtime.getRuntime().exec(
					"java -jar " + JAVASETTINGS + " " + getDiarizationPath()
					+ " --fInputMask=" + targetFile.getAbsolutePath()
					+ " --sOutputMask=" + tmpFolder.getAbsolutePath()
					+ "/temp_for_segm.xml"
					+ " --sOutputFormat=seg.xml,UTF8"
					+ " --doCEClustering temp_for_segm");
		} catch (IOException e) {
			e.printStackTrace();
		}


		BufferedReader diarizationError = new BufferedReader(
				new InputStreamReader(diarizationProcess.getErrorStream()));

		String diarizationErrorResult = null, s = null;
		try {
			while ((s = diarizationError.readLine()) != null) {
				diarizationErrorResult += s;
				System.out.println(s);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			diarizationProcess.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		    /* TODO Check error handling and status */

		if (diarizationProcess.exitValue() != 0) {
			log.error("Error executing LIUM SpkDiarization, " +
					  "exited with failure: " + diarizationErrorResult);
			System.out.println(System.getProperty("user.dir"));
			throw new AnalysisException();
		}
	}

	private static String getDiarizationPath() {
		InputStream in = SpeakerDiarization.class.getClassLoader().getResourceAsStream("lium.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			log.error("Error reading LIUM SpkDiarization path", e);
		}
		return props.getProperty(PROP_LIUM_PATH);
	}

    private <T> T createObject(ObjectConnection con, Class<T> type)
            throws RepositoryException {
        return createObject(con, type, null);
    }

    private <T> T createObject(ObjectConnection con, Class<T> type,
            org.openrdf.model.Resource resource) throws RepositoryException {
        return con.addDesignation(
                con.getObjectFactory().createObject(
                        resource == null ? IDGenerator.BLANK_RESOURCE
                                : resource, type), type);
    }

}
