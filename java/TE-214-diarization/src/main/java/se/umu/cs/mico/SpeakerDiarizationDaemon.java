package se.umu.cs.mico;

import eu.mico.platform.event.api.EventManager;
import eu.mico.platform.event.impl.EventManagerImpl;

import org.apache.commons.cli.*;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.concurrent.TimeoutException;

/**
 * NER Daemon
 *
 * @author Sergio FernÃ¡ndez
 */
public class SpeakerDiarizationDaemon implements AutoCloseable, Daemon {

    private String username;
    private String password;
    private String hostname;

    private EventManager eventManager;

    private SpeakerDiarization diarization;
    private static final String daemonName =
            "mico-extractor-diarization daemon";

    @Override
    public void close() throws Exception {
        System.out.println(daemonName+ " closed");
    }

    @Override
    public void init(DaemonContext context) throws DaemonInitException {
        try {
            CommandLineParser parser = new GnuParser();
            CommandLine cmd = parser.parse(createOptions(), context.getArguments());
            username = cmd.getOptionValue("u");
            password = cmd.getOptionValue("p");
            hostname = cmd.getOptionValue("h");
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }

        diarization = new SpeakerDiarization();

        try {
            eventManager = new EventManagerImpl(hostname, username, password);
            System.out.println(daemonName + " initialized");
        } catch (IOException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }
    }

    @Override
    public void start() throws Exception {
        eventManager.init();
        eventManager.registerService(diarization);

        System.out.println(daemonName + " started");
    }

    @Override
    public void stop() throws Exception {
        eventManager.unregisterService(diarization);
        eventManager.shutdown();

        System.out.println(daemonName + " stopped");
        close();
    }

    @Override
    public void destroy() {
        diarization = null;
        eventManager = null;

        System.out.println(daemonName + " destroyed");
    }

    @SuppressWarnings("static-access")
    private static Options createOptions() {
        Options options = new Options();

        options.addOption(
                OptionBuilder
                        .withArgName("username")
                        .hasArg()
                        .withLongOpt("username")
                        .isRequired()
                        .create('u')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("password")
                        .hasArg()
                        .withLongOpt("password")
                        .isRequired()
                        .create('p')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("hostname")
                        .hasArg()
                        .withLongOpt("hostname")
                        .isRequired()
                        .create('h')
        );

        return options;
    }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 Logger log = LoggerFactory
					.getLogger(SpeakerDiarizationDaemon.class);
		try {
			if (args.length != 3) {
				System.err.println("Usage: java Diarization " +
								   "<hostname> <user> <password>");
				System.exit(1);
			}

			String mico_host = args[0];
			String mico_user = args[1];
			String mico_pass = args[2];

			try {
				EventManager eventManager = new EventManagerImpl(mico_host,
						mico_user, mico_pass);
				eventManager.init();
				SpeakerDiarization s2t = new SpeakerDiarization();
				eventManager.registerService(s2t);

				// service (other approaches might be more sensible for a service,
				// e.g. commons-daemon)
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));

				char c = ' ';
				while (Character.toLowerCase(c) != 'q') {
					System.out.print("enter 'q' to quit: ");
					System.out.flush();

					c = in.readLine().charAt(0);
				}

				eventManager.unregisterService(s2t);

				eventManager.shutdown();

			} catch (IOException e) {
				log.error("error while accessing event manager:", e);
			} catch (TimeoutException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			log.error("ERROR "+e.getMessage());
		}
	}

}
