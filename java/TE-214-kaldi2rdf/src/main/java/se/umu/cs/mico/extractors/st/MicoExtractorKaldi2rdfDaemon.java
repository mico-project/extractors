package se.umu.cs.mico.extractors.st;

import eu.mico.platform.event.api.EventManager;
import eu.mico.platform.event.impl.EventManagerImpl;
import org.apache.commons.cli.*;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;

import java.io.IOException;


public class MicoExtractorKaldi2rdfDaemon implements AutoCloseable, Daemon {

    private String username;
    private String password;
    private String hostname;

    private EventManager eventManager;

    private MicoExtractorKaldi2rdf extractor;
	private static final String daemonName ="mico-extractor-kaldi2rdf daemon";

    @Override
    public void close() throws Exception {
        System.out.println(daemonName + " closed");
    }

    @Override
    public void init(DaemonContext context) throws DaemonInitException {
        try {
            CommandLineParser parser = new GnuParser();
            CommandLine cmd = parser.parse(createOptions(), context.getArguments());
            username = cmd.getOptionValue("u");
            password = cmd.getOptionValue("p");
            hostname = cmd.getOptionValue("h");
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }

        extractor = new MicoExtractorKaldi2rdf();

        try {
            eventManager = new EventManagerImpl(hostname, username, password);
			System.out.println(daemonName + " initialized");
        } catch (IOException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }
    }

    @Override
    public void start() throws Exception {
        eventManager.init();
        eventManager.registerService(extractor);
        System.out.println(daemonName + " started");
    }

    @Override
    public void stop() throws Exception {
        eventManager.unregisterService(extractor);
        eventManager.shutdown();
        System.out.println(daemonName + " stopped");
        close();
    }

    @Override
    public void destroy() {
        extractor = null;
        System.out.println(daemonName + " destroyed");
    }

    private static Options createOptions() {
        Options options = new Options();

        options.addOption(
                OptionBuilder
                        .withArgName("username")
                        .hasArg()
                        .withLongOpt("username")
                        .isRequired()
                        .create('u')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("password")
                        .hasArg()
                        .withLongOpt("password")
                        .isRequired()
                        .create('p')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("hostname")
                        .hasArg()
                        .withLongOpt("hostname")
                        .isRequired()
                        .create('h')
        );

        return options;
    }

}
