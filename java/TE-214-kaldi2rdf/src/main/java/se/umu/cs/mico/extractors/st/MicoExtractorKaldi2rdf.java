package se.umu.cs.mico.extractors.st;

import eu.mico.platform.anno4j.model.impl.bodymmm.SpeechToTextBodyMMM;
import static eu.mico.platform.anno4j.model.namespaces.MMMTERMS.STT_BODY_MICO;
import eu.mico.platform.event.api.AnalysisResponse;
import eu.mico.platform.event.api.AnalysisService;
import eu.mico.platform.event.model.AnalysisException;
import eu.mico.platform.event.model.Event.ErrorCodes;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;
import eu.mico.platform.persistence.model.Resource;

import org.openrdf.idGenerator.IDGenerator;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.LangString;
import org.openrdf.repository.object.ObjectConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.impl.selector.FragmentSelector;
import com.github.anno4j.model.impl.targets.SpecificResource;

import eu.mico.platform.event.api.EventManager;
import eu.mico.platform.event.impl.AnalysisServiceUtil;
import eu.mico.platform.event.impl.EventManagerImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import se.umu.cs.mico.extractors.st.model.Speech2TextResults;
import se.umu.cs.mico.extractors.st.model.Speech2TextResults.Words.Word;

public class MicoExtractorKaldi2rdf implements AnalysisService {


    private static Logger log = LoggerFactory.getLogger(MicoExtractorKaldi2rdf.class);

    private JAXBContext speech2TextResultContext;

    public MicoExtractorKaldi2rdf() {
        try {
            speech2TextResultContext = JAXBContext.newInstance(Speech2TextResults.class);
        } catch (JAXBException e) {
            throw new IllegalStateException("Unable to create JAXBContext for "+Speech2TextResults.class.getName(), e);
        }
    }
    
	private String getRegistrationProperties(String propName){
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("registration.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			log.error("Error reading registration properties", e);
		}
		return props.getProperty(propName);
	}
	
	@Override
	public String getExtractorID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorId");
	}

	@Override
	public String getExtractorModeID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorModeId");
	}

	@Override
	public String getExtractorVersion() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorVersion");
	}

    @Override
    public String getProvides() {
        return "application/x-mico-rdf";
    }

    @Override
    public String getRequires() {
        return "text/vnd.umu-kaldis+xml";
    }

 
    @Override
    public void call(AnalysisResponse response, Item ci, List<Resource> resourceList, Map<String, String> properties)
                    throws AnalysisException, IOException, RepositoryException {
        if(response == null){
            throw new AnalysisException("The parsed response MUST NOT be NULL!");
        }
        if(ci == null){
            throw new AnalysisException("The parsed content item MUST NOT be NULL!");
        }
        //search for the text asset
        log.trace("> process Item {}", ci.getURI());
        log.trace(" - resourceList (size:{}): {}", resourceList.size(), resourceList);
        Asset kaldisXmlAsset = null;
        Resource kaldisXmlResource = null;
        for(Resource resource : resourceList){
            if(resource.hasAsset()){
                log.debug(" - check Assert for Resource: {} (type: {})", resource.getURI(),resource.getClass().getSimpleName());
                Asset asset = resource.getAsset();
                String format = asset.getFormat().split(";")[0].trim();
                log.debug("   - format: {}",format);
                if(getRequires().equalsIgnoreCase(format)){ //we can process this asset
                    log.debug(" - use Asset {} of {} {} with format {}",
                            asset, resource instanceof Item ? "item" : "part" , resource.getURI(), asset.getFormat());
                    kaldisXmlAsset = asset;
                    kaldisXmlResource = resource;
                    break;
                } else if(log.isDebugEnabled()){ 
                    log.debug(" - ignore Asset {} of {} {} because its format {} is not supported (supported: {})",
                            asset, resource instanceof Item ? "item" : "part" , resource.getURI(), format, getRequires());
                }
            } else {
                log.debug(" - Resource: {} (type: {}) has no asset", resource.getURI(),resource.getClass().getSimpleName());
            }
        }
        if(kaldisXmlAsset == null){
            log.warn(" - got call for Item {} and resources {} where none had an asset with the supported format {}",
                    ci.getURI(), resourceList, getRequires());
            throw new AnalysisException(ErrorCodes.MISSING_ANNOTATION,"No " + getRequires()+ " asset",null);
        }

        // Get stream of contentPart
        InputStream is = kaldisXmlAsset.getInputStream();
        
        if(is == null) {
            throw new AnalysisException(ErrorCodes.UNEXPECTED_ERROR, 
                    "Unable to open InputStream for Asset (Location: " + kaldisXmlAsset.getLocation() 
                    + "| Resource: "+ kaldisXmlResource.getURI() + ")",null);
        }
        Speech2TextResults xmlData;
        try {
            Unmarshaller unmarshaller = speech2TextResultContext.createUnmarshaller();
            xmlData = (Speech2TextResults) unmarshaller.unmarshal(is);
        } catch(JAXBException e){
            throw new AnalysisException(ErrorCodes.UNEXPECTED_ERROR, 
                    "Unable to parse XML content of " + getRequires() + "Asset (Location: " 
                    + kaldisXmlAsset.getLocation() + " | Resource: "+ kaldisXmlResource.getURI() + ")", e);
        }
        List<Word> words = xmlData.getWords().getWord();
        log.debug("Discovered {} words.", words.size());
        
        ObjectConnection con = ci.getObjectConnection();
        
        //if necessary, instantiate output stream and start processing the xml 
        if(words != null && !words.isEmpty()) {
            log.info("- discovered {} words.", words.size());
            int currentTime = 0;

            for (Word word : words) {

                // Create a new content part for each annotation (so each word
                // creates an own annotation)
                Part cp = ci.createPart(AnalysisServiceUtil.getServiceID(this));
                cp.addInput(kaldisXmlResource);
                cp.setSyntacticalType(STT_BODY_MICO);
                cp.setSemanticType("Single word retrieved from the input part at " + kaldisXmlResource.getURI());


                // Create the corresponding body for the annotation
                SpeechToTextBodyMMM body = createObject(con, SpeechToTextBodyMMM.class);
                // Take the word from the XML output file and add it to the body
                body.setValue(LangString.valueOf(word.getValue()));
                cp.setBody(body);
                SpecificResource sr = createObject(con, SpecificResource.class);
                cp.getTargets().add(sr);
                sr.setSource(kaldisXmlResource.getRDFObject());
                // To specify where the word has been detected in the video,
                // specify start and end time of the word
                int start = currentTime;
                currentTime += word.getLength().intValue();
                int end = currentTime;
                FragmentSelector fragment = createObject(con, FragmentSelector.class);
                fragment.setTemporalFragment((double)start, (double)end);
                sr.setSelector(fragment);
                response.sendNew(ci, cp.getURI());
            }
        } //no text extracted from the item
    }

    /**
     * 
     * @param rec
     * @return selector for the given object
     */
        // usage: java MicoExtractorKaldi2rdf <hostname> <user> <password>
    public static void main(String[] args) {
        if(args.length != 3) {
            System.err.println("Usage: java MicoExtractorKaldi2rdf <hostname> <user> <password>");
            System.exit(1);
        }

        String mico_host = args[0];
        String mico_user = args[1];
        String mico_pass = args[2];

        try {
            // create event manager instance, providing the correct host, user and password, and initialise it
            EventManager eventManager = new EventManagerImpl(mico_host,mico_user,mico_pass);
            eventManager.init();

            // create analyzer service instance and register it with event manager
            MicoExtractorKaldi2rdf svc_wc = new MicoExtractorKaldi2rdf();
            eventManager.registerService(svc_wc);


            // keep running service in the background, and wait for user command "q" on the frontent to terminate
            // service (other approaches might be more sensible for a service, e.g. commons-daemon)
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

            char c = ' ';
            while(Character.toLowerCase(c) != 'q') {
                System.out.print("enter 'q' to quit: ");
                System.out.flush();

                c = in.readLine().charAt(0);
            }

            // unregister service before quiting
            eventManager.unregisterService(svc_wc);

            // shutdown event manager properly
            eventManager.shutdown();

            System.out.println("Kaldi2rdf shutdown completed");
            // DONE
        } catch (IOException e) {
            log.error("error while accessing event manager:",e);
        } catch (URISyntaxException e) {
            log.error("invalid hostname:", e);
        } catch (TimeoutException e) {
            log.error("fetching configuration timed out:", e);
        }

    }
    
    private <T> T createObject(ObjectConnection con, Class<T> type) throws RepositoryException {
        return createObject(con, type, null);
    }
    
    private <T> T createObject(ObjectConnection con, Class<T> type, org.openrdf.model.Resource resource) throws RepositoryException {
        return con.addDesignation(con.getObjectFactory().createObject(
                resource == null ? IDGenerator.BLANK_RESOURCE : resource, type), type);
    }


}
