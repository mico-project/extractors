package de.fhg.idmt.mico.extractors.or;

import com.google.common.io.Resources;

import de.fhg.idmt.mico.extractors.or.utils.Rectangle;
import de.fhg.idmt.mico.extractors.or.model.Frame;
import de.fhg.idmt.mico.extractors.or.model.Object;
import de.fhg.idmt.mico.extractors.or.model.ObjectRefMapping;
import de.fhg.idmt.mico.extractors.or.model.ObjectType;
import de.fhg.idmt.mico.extractors.or.model.Point;
import de.fhg.idmt.mico.extractors.or.model.Region;
import de.fhg.idmt.mico.extractors.or.model.WrongNumberOfElementsException;
import de.fhg.idmt.mico.extractors.or.model.ObjectRecognitionResults.Meta;
import de.fhg.idmt.mico.extractors.or.utils.ObjectRecognitionUtils;
import static de.fhg.idmt.mico.extractors.or.utils.ObjectRecognitionUtils.getObjectRegion;
import static de.fhg.idmt.mico.extractors.or.utils.ObjectRecognitionUtils.getRectangle;

import org.junit.Assert;
import org.junit.Test;

import javax.xml.bind.JAXBException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

/**
 * Author: Marcel Sieland (marcel.sieland@idmt.fraunhofer.de)
 */
public class TestObjectDetectionToRdf {
	
	private static final String animaL_xml = "example.xml";
	private static final String face5s_xml = "facedetection_step5s.xml";
	private static final String face_xml = "facedetection_step0.04s.xml";
	private static final String BLANK_HOG_CLASS = "01-vsBlank-HOG-CLASS.xml";
	

    @Test
    public void testParsing() throws IOException, JAXBException, WrongNumberOfElementsException {

        URL url = Resources.getResource(animaL_xml);
        InputStream in = url.openStream();

        ObjectRecognitionUtils util = new ObjectRecognitionUtils();
        Meta meta = util.parse(in);
        List<Frame> frames = util.getFrames().getFrame();

		// foreach ANIMAL
		Frame frame = frames.get(0);
		List<Object> objects = frame.getObjects().getObject();
		Assert.assertEquals(2, objects.size());
		Assert.assertEquals(ObjectType.ANIMAL, util.getAnnotationType());
		Assert.assertEquals(true, util.isImageAnnotation());
		Object obj = objects.get(0); 
		boolean objFound = false;
		for (java.lang.Object o : obj.getRegionOrSubobjectOrMarker()) {
			if (o instanceof Region) {
				objFound = true;
				List<Point> points = ((Region) o).getPoint();

				// Use absolute position values and not the normalized
				// values from the xml file
				double left = points.get(0).getPosX();
				double top = points.get(0).getPosY();
				double right = points.get(2).getPosX();
				double bottom = points.get(2).getPosY();

				Assert.assertEquals(0.398333, left, 0.001);
				Assert.assertEquals(0.543943, top, 0.001);
				Assert.assertEquals(0.691667, right, 0.001);
				Assert.assertEquals(0.752969, bottom, 0.001);
				
				Rectangle rec = getRectangle(points.get(0), points.get(2), meta,frame.getTimestamp());
				Assert.assertEquals(238, rec.getX());
				Assert.assertEquals(229, rec.getY());
				Assert.assertEquals(176, rec.getWidth());
				Assert.assertEquals(87, rec.getHeight());

			}
		}
		Assert.assertTrue("No annotation object in parsed file",objFound);
	}

    @Test
    public void testFaceVideo() throws IOException, JAXBException, WrongNumberOfElementsException {

        URL url = Resources.getResource(face5s_xml);
        InputStream in = url.openStream();

        ObjectRecognitionUtils util = new ObjectRecognitionUtils();
        Meta meta = util.parse(in);
        List<Frame> frames = util.getFrames().getFrame();

		// foreach Object
		Frame frame = frames.get(0);
		Frame frame2 = frames.get(2);
		List<Object> objects = frame.getObjects().getObject();
		Assert.assertEquals(4, frames.size());
		Assert.assertEquals(1, objects.size());
		Assert.assertEquals(ObjectType.FACE, util.getAnnotationType());
		Assert.assertEquals(false, util.isImageAnnotation());

		Object obj = objects.get(0);
		Object obj2 = frame2.getObjects().getObject().get(0);
		Rectangle rec = getObjectRegion(obj, meta, frame.getTimestamp());
		Rectangle rec2 = getObjectRegion(obj2, meta, frame2.getTimestamp());
		Assert.assertEquals(394, rec.getX());
		Assert.assertEquals(58, rec.getY());
		Assert.assertEquals(64, rec.getWidth());
		Assert.assertEquals(64, rec.getHeight());
		Assert.assertEquals(394, rec2.getX());
		Assert.assertEquals(58, rec2.getY());
		Assert.assertEquals(128, rec2.getWidth());
		Assert.assertEquals(136, rec2.getHeight());

	}

    @Test
    public void testBlankHog() throws IOException, JAXBException, WrongNumberOfElementsException {

        URL url = Resources.getResource(BLANK_HOG_CLASS);
        InputStream in = url.openStream();

        ObjectRecognitionUtils util = new ObjectRecognitionUtils();
        Meta meta = util.parse(in);
        // check metadata
        Assert.assertEquals("01-vsBlank-HOG-CLASS", meta.getAnnotationDetails().getAlgorithm());
        Assert.assertEquals(421, meta.getFramesize().getHeight());
        Assert.assertEquals(600, meta.getFramesize().getWidth());
        Assert.assertEquals(100, meta.getTimebase().getDenum());
        Assert.assertEquals(0, meta.getTimebase().getNum());

        List<Frame> frames = util.getFrames().getFrame();

        // foreach Object
        Assert.assertEquals(1, frames.size());
        Frame frame0 = frames.get(0);
        List<Object> objects = frame0.getObjects().getObject();
        Assert.assertEquals(3, objects.size());
        Assert.assertEquals(ObjectType.ANIMAL, util.getAnnotationType());
        Assert.assertEquals(true, util.isImageAnnotation());

        Object obj = objects.get(0);
        Object obj2 = objects.get(2);
        Rectangle rec = getObjectRegion(obj, meta, frame0.getTimestamp());
        Rectangle rec2 = getObjectRegion(obj2, meta, frame0.getTimestamp());
        Assert.assertEquals(-37, rec.getX());
        Assert.assertEquals(232, rec.getY());
        Assert.assertEquals(65, rec.getWidth());
        Assert.assertEquals(92, rec.getHeight());
        Assert.assertEquals(-34, rec2.getX());
        Assert.assertEquals(238, rec2.getY());
        Assert.assertEquals(61, rec2.getWidth());
        Assert.assertEquals(87, rec2.getHeight());

        // check mappings
        List<ObjectRefMapping> mappings = frame0.getMappings().getMapping();
        Assert.assertEquals(3, mappings.size());
        ObjectRefMapping mapping0 = mappings.get(0);
        ObjectRefMapping mapping2 = mappings.get(2);
        Assert.assertEquals("0", mapping0.getObjectRef());
        Assert.assertEquals("0", mapping0.getLabelRef());
        Assert.assertEquals(2.35051, mapping0.getConfidence().floatValue(),0.0001);
        Assert.assertEquals("2", mapping2.getObjectRef());
        Assert.assertEquals("2", mapping2.getLabelRef());
        Assert.assertEquals(2.00531, mapping2.getConfidence().floatValue(),0.0001);
    }
}
