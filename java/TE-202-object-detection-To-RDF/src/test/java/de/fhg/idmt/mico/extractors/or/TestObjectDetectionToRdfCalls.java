package de.fhg.idmt.mico.extractors.or;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.net.URL;

import org.junit.Assert;
import org.apache.commons.daemon.DaemonInitException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.idGenerator.IDGenerator;
import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Resources;

import eu.mico.platform.anno4j.model.impl.bodymmm.ObjectDetectionBodyMMM;
import eu.mico.platform.event.test.mock.AnalysisResponseCollector;
import eu.mico.platform.event.test.mock.EventManagerMock;
import eu.mico.platform.persistence.api.PersistenceService;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;

/**
 * simulate test calls from broker though event- and persistence- api mocks
 * 
 * @author sld
 *
 */
public class TestObjectDetectionToRdfCalls {

    private static final Logger log = LoggerFactory
            .getLogger(TestObjectDetectionToRdfCalls.class);

    private static final URI DUMMY_EXTRACTOR_ID = new URIImpl(
            "urn:dummy.mico.extractor:object-detector");

    private static final String BLANK_HOG_CLASS_xml = "01-vsBlank-HOG-CLASS.xml";
    private static final String FACE_5s_xml = "facedetection_step5s.xml";

    private EventManagerMock eventManager;


    @BeforeClass
    public static void initClass() throws IOException, DaemonInitException {
    }

    @Before
    public void setup() throws URISyntaxException, IOException {
        eventManager = new EventManagerMock();
        eventManager.init();
        eventManager.registerService(new ObjectToRDF());
    }

    @After
    public void shutdown() throws IOException {
        eventManager.shutdown();
    }

    @Test(timeout=10000)
    public void TestCallWithBlankHog() throws IOException, DaemonInitException,
            RepositoryException {
        final PersistenceService ps = eventManager.getPersistenceService();
        final Item contentItem = ps.createItem();
        final ObjectConnection con = contentItem.getObjectConnection();

        // add objectdetection xml
        Part detectionPart = contentItem.createPart(DUMMY_EXTRACTOR_ID);
        detectionPart.setSyntacticalType("text/vnd.fhg-objectdetection+xml");
        detectionPart.addInput(contentItem); // NOTE this is important!!
        ObjectDetectionBodyMMM detectAnno = createObject(con,
                ObjectDetectionBodyMMM.class);
        detectionPart.setBody(detectAnno);
        Asset asset = detectionPart.getAsset(); 
        asset.setFormat("text/vnd.fhg-objectdetection+xml");
        log.debug("- created Assert {} (location: {} | format: {})", asset,
                asset.getLocation(), asset.getFormat());
        URL url = Resources.getResource(BLANK_HOG_CLASS_xml);
        InputStream in = url.openStream();
        String content = IOUtils.toString(in);
        log.debug(" - content: {}", StringUtils.abbreviate(content, 80));
        final PrintStream stream = new PrintStream(asset.getOutputStream());
        stream.print(content);
        stream.close();

        // inject the content item
        eventManager.injectItem(contentItem);
        AnalysisResponseCollector responsesCollector = null;
        responsesCollector = eventManager.getResponsesCollector();
        
        try {
            while(!responsesCollector.isFinished() && !responsesCollector.isError()){
                Thread.sleep(300);
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Assert.assertFalse("Error during event processing",responsesCollector.isError());

    }

    @Test(timeout=10000)
    public void TestCallFaces() throws IOException, DaemonInitException,
            RepositoryException {
        final PersistenceService ps = eventManager.getPersistenceService();
        final Item contentItem = ps.createItem();
        final ObjectConnection con = contentItem.getObjectConnection();

        URL url = Resources.getResource(FACE_5s_xml);
        InputStream in = url.openStream();

        // add objectdetection xml
        Part detectionPart = contentItem.createPart(DUMMY_EXTRACTOR_ID);
        detectionPart.setSyntacticalType("text/vnd.fhg-objectdetection+xml");
        detectionPart.addInput(contentItem); // NOTE this is important!!
        ObjectDetectionBodyMMM detectAnno = createObject(con,
                ObjectDetectionBodyMMM.class);
        detectionPart.setBody(detectAnno);
        Asset asset = detectionPart.getAsset(); 
        asset.setFormat("text/vnd.fhg-objectdetection+xml");
        log.debug("- created Assert {} (location: {} | format: {})", asset,
                asset.getLocation(), asset.getFormat());
        String content = IOUtils.toString(in);
        log.debug(" - content: {}", StringUtils.abbreviate(content, 80));
        final PrintStream stream = new PrintStream(asset.getOutputStream());
        stream.print(content);
        stream.close();

        // inject the content item
        eventManager.injectItem(contentItem);
        AnalysisResponseCollector responsesCollector = eventManager.getResponsesCollector();
        
        try {
            while(!responsesCollector.isFinished() && !responsesCollector.isError()){
                Thread.sleep(300);
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Assert.assertFalse(responsesCollector.isError());

    }
    
private static <T> T createObject(ObjectConnection con, Class<T> type)
        throws RepositoryException {
    return createObject(con, type, null);
}

private static <T> T createObject(ObjectConnection con, Class<T> type,
        org.openrdf.model.Resource resource) throws RepositoryException {
    return con.addDesignation(
            con.getObjectFactory().createObject(
                    resource == null ? IDGenerator.BLANK_RESOURCE
                            : resource, type), type);
}

}
