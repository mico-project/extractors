package de.fhg.idmt.mico.extractors.or;

import de.fhg.idmt.mico.extractors.or.model.*;
import de.fhg.idmt.mico.extractors.or.model.Object;
import eu.mico.platform.event.api.AnalysisResponse;
import eu.mico.platform.event.api.AnalysisServiceAnno4j;
import eu.mico.platform.event.impl.AnalysisServiceUtil;
import eu.mico.platform.event.model.AnalysisException;
import eu.mico.platform.anno4j.model.impl.bodymmm.AnimalDetectionBodyMMM;
import eu.mico.platform.anno4j.model.impl.bodymmm.FaceDetectionBodyMMM;
import eu.mico.platform.anno4j.model.impl.bodymmm.ImageDimensionBodyMMM;
import eu.mico.platform.anno4j.model.impl.targetmmm.SpecificResourceMMM;
import static eu.mico.platform.anno4j.model.namespaces.MMMTERMS.ANIMAL_DETECTION_BODY;
import static eu.mico.platform.anno4j.model.namespaces.MMMTERMS.FACE_DETECTION_BODY;
import static eu.mico.platform.anno4j.model.namespaces.MMMTERMS.IMAGE_DIMENSION_BODY;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;
import eu.mico.platform.persistence.model.Resource;

import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.Transaction;
import com.github.anno4j.model.Body;
import com.github.anno4j.model.Selector;
import com.github.anno4j.model.impl.selector.FragmentSelector;

import de.fhg.idmt.mico.extractors.or.model.ObjectRecognitionResults.Meta;
import de.fhg.idmt.mico.extractors.or.utils.ObjectRecognitionUtils;
import de.fhg.idmt.mico.extractors.or.utils.Rectangle;
import de.fhg.idmt.mico.extractors.or.utils.TemporalRectangle;

import javax.xml.bind.JAXBException;

import static de.fhg.idmt.mico.extractors.or.utils.ObjectRecognitionUtils.getObjectRegion;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * ...
 * <p/>
 * Author: Marcel Sieland (marcel.sieland@idmt.fraunhofer.de)
 */
public class ObjectToRDF implements AnalysisServiceAnno4j {
	
	public class Label {         
	    public final String name;
	    public final Float confidence;

	    public Label(String first, Float second) {         
	        this.name= first;
	        this.confidence= second;
	     }
	 }

    private Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * TODO: should be taken from other analysis
     */
    public static int framesPerSecond = 25;

//    private static final URI SERVICE_ID = ValueFactoryImpl.getInstance()
//            .createURI("http://www.mico-project.org/services/mico-extractor-object2RDF");

	
	private String getRegistrationProperties(String propName){
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("registration.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			log.error("Error reading registration properties", e);
		}
		return props.getProperty(propName);
	}
	
	@Override
	public String getExtractorID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorId");
	}

	@Override
	public String getExtractorModeID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorModeId");
	}

	@Override
	public String getExtractorVersion() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorVersion");
	}

    @Override
    public String getProvides() {
        return "application/x-mico-rdf";
    }

    @Override
    public String getRequires() {
        return "text/vnd.fhg-objectdetection+xml";
    }

    @Override
    public void call(AnalysisResponse resp, Item item, List<Resource> resourceList, Map<String, String> params, Transaction anno4j) 
        throws AnalysisException, IOException, RepositoryException {
        try {
            if(resp == null){
                throw new AnalysisException("The parsed response MUST NOT be NULL!");
            }
            if(item == null){
                throw new AnalysisException("The parsed content item MUST NOT be NULL!");
            }
            if(resourceList == null || resourceList.size() < 1){
                throw new AnalysisException("Atleast one Resource MUST be AVAILABLE!");
            }
            //search for the text asset
            log.trace("> process Item {}", item.getURI());
            log.trace(" - resourceList (size:{}): {}", resourceList.size(), resourceList);

            // Get stream if contentPart
            Resource parentCP = resourceList.get(0);
            InputStream is = parentCP.getAsset().getInputStream();


            // parse the content
            ObjectRecognitionUtils orUtil = new ObjectRecognitionUtils();
            orUtil.parse(is);
            is.close();
            Meta meta = orUtil.getMeta();

            // annotate parent with image/frame dimensions
            if (meta != null) {
                ImageDimensionBodyMMM imageDim = anno4j.createObject(ImageDimensionBodyMMM.class);
                imageDim.setConfidence(1.d);
                imageDim.setHeight(meta.getFramesize().getHeight());
                imageDim.setWidth(meta.getFramesize().getWidth());

                Part dimPart = item.createPart(AnalysisServiceUtil.getServiceID(this));
                dimPart.setSemanticType("Conversion of the input part into a " + IMAGE_DIMENSION_BODY);
                dimPart.setSyntacticalType(IMAGE_DIMENSION_BODY);
                dimPart.setInputs(new HashSet<Resource>(resourceList));
                dimPart.addTarget(anno4j.createObject(SpecificResourceMMM.class));
                dimPart.setBody(imageDim);
                
            } else {
                log.info("Not able to retrieve image dimension, orUtil.getMeta() returned null.");
            }
            
            AnnotationDetails det = meta.getAnnotationDetails();
            
            String algoName = null;
            
            if (det != null) {
            	algoName = det.getAlgorithm();
            }
            List<Frame> frames = orUtil.getFrames().getFrame();
            
            
            log.info("- discovered {} frames in video result.", frames.size());
            // foreach Object
            for (Frame frame : frames) {
                log.info("- discovered {} objects in frame.", frame.getObjects().getObject().size());
                
                for (Object obj : frame.getObjects().getObject()) {

                    // Use absolute position values and not the
                    // normalized values from the xml file
                    Rectangle rec = getObjectRegion(obj, meta, frame.getTimestamp());
                    Selector selector;
                    if (rec instanceof TemporalRectangle) {
                        selector = getObjectSelector((TemporalRectangle) rec, anno4j);
                    } else {
                        selector = getObjectSelector(rec,anno4j);
                    }
                    SpecificResourceMMM target = anno4j.createObject(SpecificResourceMMM.class);
                    target.setSelector(selector);
                    

                    // create a new body
                    Body detectionBody = getDetectionBody(obj.getType(), anno4j);
                    
                    if (detectionBody instanceof AnimalDetectionBodyMMM) {                  	
                    	
                    	Label label = findLabel(orUtil.getLabels().getLabel(),
                    			frame.getMappings().getMapping(),obj.getUid());
                    	
                    	((AnimalDetectionBodyMMM)detectionBody).setExtractionVersion(algoName);
                    	if (label != null) {
                    	    log.debug("adding detection label {} with confidence {}", label.name, label.confidence);
                    		((AnimalDetectionBodyMMM)detectionBody).setValue(label.name);
                            ((AnimalDetectionBodyMMM)detectionBody).setConfidence(label.confidence.doubleValue());
                    	}
                    }
                    // create new content part for each annotation
                    // object
                    Part cp = item.createPart(AnalysisServiceUtil.getServiceID(this));
                                        
                    if(detectionBody instanceof AnimalDetectionBodyMMM) {
                    	cp.setSemanticType("Conversion of the input part into a " + ANIMAL_DETECTION_BODY);
                    	cp.setSyntacticalType(ANIMAL_DETECTION_BODY);
                    }
                    else if (detectionBody instanceof FaceDetectionBodyMMM ) {
                    	cp.setSemanticType("Conversion of the input part into a " + FACE_DETECTION_BODY);
                    	cp.setSyntacticalType(FACE_DETECTION_BODY);
                    }
                    else{
                    	log.error("Cannot determine type of the input part, proceding with dummy types");
                    	cp.setSemanticType("Conversion of the input part");
                    	cp.setSyntacticalType(getProvides());
                    }
                    
                    
                    cp.setBody(detectionBody);
                    cp.addInput(parentCP);
                    cp.addTarget(target);


                    resp.sendNew(item, cp.getURI());

                    // End foreach
                }
            }
            resp.sendFinish(item);
        } catch (RepositoryException | IllegalAccessException | InstantiationException e) {
            log.error("Error accessing the repository: {}", e.getMessage(), e);
            throw new IOException(e);
        } catch (JAXBException | WrongNumberOfElementsException e) {
            log.error("Error parsing input: {}", e.getMessage(), e);
            throw new AnalysisException("Error parsing input: " + e.getMessage());
        } catch (AbstractMethodError e) {
            log.error("Error persisting results: {}", e.getMessage(), e);
            throw new AnalysisException("Error persisting results: " + e.getMessage());
        }
    }


    /**
     * Creates a Selector as defined by the Open Annotation data model
     * from a given <code>Rectangle</code>.
     *
     * @param rect Rectangle to be converted.
     * @return SpatialFragmentSelector for the given object
     * @throws RepositoryException 
     * @throws InstantiationException 
     * @throws IllegalAccessException 
     */
    private FragmentSelector getObjectSelector(Rectangle rect, Transaction anno4j) throws RepositoryException, IllegalAccessException, InstantiationException {

        FragmentSelector selector = anno4j.createObject(FragmentSelector.class);
        selector.setSpatialFragment(rect.getX(),
                rect.getY(),
                rect.getWidth(),
                rect.getHeight()
        );
        return selector;
    }


    /**
     * Creates a Selector as defined by the Open Annotation data model from a
     * given <code>TemporalRectangle</code>.
     * 
     * @param rect
     *            Temporal rectangle Rectangle to be converted.
     * @param con
     * @return a temporal fragment selector
     * @throws RepositoryException
     * @throws InstantiationException 
     * @throws IllegalAccessException 
     */
    private FragmentSelector getObjectSelector(TemporalRectangle rect, Transaction anno4j) throws RepositoryException, IllegalAccessException, InstantiationException {

        FragmentSelector selector = anno4j.createObject(FragmentSelector.class);
        selector.setSpatialFragment(rect.getX(),
                rect.getY(),
                rect.getWidth(),
                rect.getHeight()
        );
        selector.setTemporalFragment(Double.valueOf(rect.getStart()), Double.valueOf(rect.getEnd()));
        return selector;
    }


    private Body getDetectionBody(ObjectType type, Transaction anno4j) throws RepositoryException, IllegalAccessException, InstantiationException {
        switch (type) {
            case ANIMAL:
                return anno4j.createObject(AnimalDetectionBodyMMM.class);
            case FACE:
                return anno4j.createObject(FaceDetectionBodyMMM.class);
            default:
                log.error("unsupported ObjectType detected");
                return null;
        }
    }

    private Label findLabel(List<ObjectLabel> labels, List<ObjectRefMapping> mappings, String objectID) {
    	String labelID = null;
    	float confidence = -1.0f;
    	for (ObjectRefMapping m : mappings) {
    		if (m.getObjectRef().equals(objectID)) {
    			labelID = m.getLabelRef();
    			confidence = m.getConfidence();
    			break;
    		}
    	}
    	for (ObjectLabel l : labels) {
    		if (l.getUid().equals(labelID)) {
    			return new Label(l.getName(),new Float(confidence));
    		}
    	}
    	return null;
    }

}
