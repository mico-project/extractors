package de.fhg.idmt.mico.extractors.or.utils;


public class Rectangle {

	public Rectangle(int x, int y, int height, int width) {
		super();
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	private int x, y, height, width;

	@Override
	public String toString() {
		return "x,y:"+ x + "," + y + " | w,h: " + width + "," +height;
	}
}