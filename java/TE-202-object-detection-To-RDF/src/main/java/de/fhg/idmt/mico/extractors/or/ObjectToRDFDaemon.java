package de.fhg.idmt.mico.extractors.or;

import eu.mico.platform.event.api.EventManager;
import eu.mico.platform.event.impl.EventManagerImpl;
import org.apache.commons.cli.*;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.concurrent.TimeoutException;

/**
 * Temporal Segments to RDF Daemon
 *
 * @author Sergio Fernández
 */
public class ObjectToRDFDaemon implements AutoCloseable, Daemon {

    private String username;
    private String password;
    private String hostname;

    private EventManager eventManager;

    private ObjectToRDF extractor;
	private static final String daemonName ="mico-extractor-object-2-rdf daemon";

    @Override
    public void close() throws Exception {
        System.out.println(daemonName + " closed");
    }

    @Override
    public void init(DaemonContext context) throws DaemonInitException {
        try {
            CommandLineParser parser = new GnuParser();
            CommandLine cmd = parser.parse(createOptions(), context.getArguments());
            username = cmd.getOptionValue("u");
            password = cmd.getOptionValue("p");
            hostname = cmd.getOptionValue("h");
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }

        extractor = new ObjectToRDF();

        try {
            eventManager = new EventManagerImpl(hostname, username, password);
			System.out.println(daemonName + " initialized");
        } catch (IOException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }
    }

    @Override
    public void start() throws Exception {
        eventManager.init();
        eventManager.registerService(extractor);
        System.out.println(daemonName + " started");
    }

    @Override
    public void stop() throws Exception {
        eventManager.unregisterService(extractor);
        eventManager.shutdown();
        System.out.println(daemonName + " stopped");
        close();
    }

    @Override
    public void destroy() {
        extractor = null;
        System.out.println(daemonName + " destroyed");
    }

    private static Options createOptions() {
        Options options = new Options();

        options.addOption(
                OptionBuilder
                        .withArgName("username")
                        .hasArg()
                        .withLongOpt("username")
                        .isRequired()
                        .create('u')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("password")
                        .hasArg()
                        .withLongOpt("password")
                        .isRequired()
                        .create('p')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("hostname")
                        .hasArg()
                        .withLongOpt("hostname")
                        .isRequired()
                        .create('h')
        );

        return options;
    }

    public static void main(String[] args) {
    	
        Logger log = LoggerFactory.getLogger(ObjectToRDF.class);

        if(args.length != 3) {
            System.err.println("Usage: java ObjectToRDFDaemon <hostname> <user> <password>");
            System.exit(1);
        }

        String mico_host = args[0];
        String mico_user = args[1];
        String mico_pass = args[2];

        try {
            // create event manager instance, providing the correct host, user and password, and initialise it
            EventManager eventManager = new EventManagerImpl(mico_host,mico_user,mico_pass);
            eventManager.init();

            // create analyzer service instance and register it with event manager
            ObjectToRDF svc = new ObjectToRDF();
            eventManager.registerService(svc);


            // keep running service in the background, and wait for user command "q" on the frontent to terminate
            // service (other approaches might be more sensible for a service, e.g. commons-daemon)
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

            char c = ' ';
            while(Character.toLowerCase(c) != 'q') {
                System.out.print("enter 'q' to quit: ");
                System.out.flush();

                c = in.readLine().charAt(0);
            }

            // unregister service before quiting
            eventManager.unregisterService(svc);

            // shutdown event manager properly
            eventManager.shutdown();

            System.out.println("mico-extractor-object-2-rdf shutdown completed");
            // DONE
        } catch (IOException e) {
            log.error("error while accessing event manager:",e);
        } catch (URISyntaxException e) {
            log.error("invalid hostname:", e);
        } catch (TimeoutException e) {
            log.error("fetching configuration timed out:", e);
        }

    }

}
