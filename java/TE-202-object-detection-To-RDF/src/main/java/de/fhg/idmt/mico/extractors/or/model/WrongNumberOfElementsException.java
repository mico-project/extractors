package de.fhg.idmt.mico.extractors.or.model;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public class WrongNumberOfElementsException extends Exception {

    public WrongNumberOfElementsException(String name) {super("Number of '"+name+"' elements is not one ");}
    public WrongNumberOfElementsException(String name, int count) {super("Found " + count + " instead of one element(s) of type '"+name+"'");}
    public WrongNumberOfElementsException(String name, int count, int expected) {super("Found '" + count + "' instead of '"+expected+"' element(s) of type '"+name+"'");}

}
