package de.fhg.idmt.mico.extractors.or.utils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import de.fhg.idmt.mico.extractors.or.model.ObjectRecognitionResults;
import de.fhg.idmt.mico.extractors.or.model.ObjectType;
import de.fhg.idmt.mico.extractors.or.model.Point;
import de.fhg.idmt.mico.extractors.or.model.Region;
import de.fhg.idmt.mico.extractors.or.model.WrongNumberOfElementsException;
import de.fhg.idmt.mico.extractors.or.model.ObjectRecognitionResults.Frames;
import de.fhg.idmt.mico.extractors.or.model.ObjectRecognitionResults.Labels;
import de.fhg.idmt.mico.extractors.or.model.ObjectRecognitionResults.Meta;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;


/**
 * Author: Marcel Sieland (marcel.sieland@idmt.fraunhofer.de)
 */
public class ObjectRecognitionUtils {

    private List<Frames> frames;
	private List<Labels> labels;
	private List<Meta> meta;

	public Meta parse(InputStream in) throws JAXBException, WrongNumberOfElementsException {
        JAXBContext jc = JAXBContext.newInstance(ObjectRecognitionResults.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        ObjectRecognitionResults ObjectRecognitionResults = (ObjectRecognitionResults) unmarshaller.unmarshal(in);

        frames = new LinkedList<Frames>();
        meta = new LinkedList<Meta>();
        labels = new LinkedList<Labels>();
        for(java.lang.Object element : ObjectRecognitionResults.getMetaOrLabelsOrFrames()) {
        	if (element instanceof Frames) {
        		frames.add((Frames)element);
        	}
        	if (element instanceof Labels) {
        		labels.add((Labels)element);
        	}
        	if (element instanceof Meta) {
        		meta.add((Meta)element);
        	}
        }
                
        if (frames.size() != 1 ){
        	throw new WrongNumberOfElementsException("frames",frames.size(),1); 
        }
        if (labels.size() > 1 ){
        	throw new WrongNumberOfElementsException("labels",labels.size()); 
        }
        if (meta.size() != 1 ){
        	throw new WrongNumberOfElementsException("meta",meta.size(),1); 
        }
        
    	return meta.get(0);
    }
	
	/** indicates whether an annotation is for a video or an image
	 * @return true if annotation is for an image, false otherwise 
	 */
	public boolean isImageAnnotation(){
		return meta.get(0).getTimebase().getNum() == 0;
	}

	/**
	 * indicates whether an annotation is for a video or an image
	 * 
	 * @return true if annotation is for an image, false otherwise
	 */
	public static boolean isImageAnnotation(Meta meta) {
		return meta.getTimebase().getNum() == 0;
	}
	
	/**
	 * this functions returns an rectangle of the objects position <br>
	 * - in frame {@link Rectangle} <br>
	 * - in time and time {@link TemporalRectangle}
	 * 
	 * @param obj
	 *            the object
	 * @param meta
	 *            meta data about the content (framerate, etc.)
	 * @param timestamp
	 *            timestamp of the frame, in which this object was detected
	 * @return
	 */
	public static Rectangle getObjectRegion(de.fhg.idmt.mico.extractors.or.model.Object obj, Meta meta, float timestamp){
		for (java.lang.Object o : obj.getRegionOrSubobjectOrMarker()) {
			if (o instanceof Region) {
				List<Point> points = ((Region) o).getPoint();

				// the rectangle uses explicit position values and not the
				// normalized values from the xml file
				Rectangle rec = getRectangle(points.get(0), points.get(2), meta,timestamp);
				return rec;
			}
		}
		return null;
	}
	
	/**
	 * checks the type of an annotated object
	 * 
	 * @return
	 */
	public ObjectType getAnnotationType() {
		return frames.get(0).getFrame().get(0).getObjects().getObject().get(0).getType();
	}
	
	/**
	 * the created rectangle contains explicit position values and not the
	 * normalized values from the xml file
	 * 
	 * @param topLeft
	 *            relative position of top left corner
	 * @param bottomRight
	 *            relative position of bottom right corner
	 * @param meta
	 *            data about the content (image or video) like resolution
	 * @return
	 */
	public static Rectangle getRectangle(Point topLeft, Point bottomRight, Meta meta, float timestamp) {
		double left = topLeft.getPosX();
		double top = topLeft.getPosY();
		double right = bottomRight.getPosX();
		double bottom = bottomRight.getPosY();
		int x = Double.valueOf( left * meta.getFramesize().getWidth()).intValue();
		int y = Double.valueOf( top * meta.getFramesize().getHeight()).intValue();
		int width = Double.valueOf( (right - left) * meta.getFramesize().getWidth()).intValue();
		int height = Double.valueOf( (bottom - top) * meta.getFramesize().getHeight()).intValue();

		if (isImageAnnotation(meta)){
			return new Rectangle(x, y, height, width);
		}
		long num = meta.getTimebase().getNum();
		long denum = meta.getTimebase().getDenum();
		int start = (int) (timestamp * 1000 *((float)num/denum));
		int end = (int) ((timestamp+1) * 1000 *((float)num/denum));
		return new TemporalRectangle(x, y, height, width, start, end);
	}
	
	
	
	public Frames getFrames() {
		return frames.get(0);
	}

	public Labels getLabels() {
		if (labels != null && labels.size() > 0 )
			return labels.get(0);
		else
			return null;
	}


	public Meta getMeta() {
		return meta.get(0);
	}

	public void setFrames(List<Frames> frames) {
		this.frames = frames;
	}

	public void setLabels(List<Labels> labels) {
		this.labels = labels;
	}
	public void setMeta(List<Meta> meta) {
		this.meta = meta;
	}
	

}
