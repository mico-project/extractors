package de.fhg.idmt.mico.extractors.or.utils;

/**
 * Author: Marcel Sieland (marcel.sieland@idmt.fraunhofer.de)
 */
public class TemporalRectangle extends Rectangle {

	private int start;
	private int end;
	
	public TemporalRectangle(int x, int y, int height, int width, int start, int end) {
		super(x, y, height, width);
		this.setEnd(end);
		this.setStart(start);
	}

	/**
	 * @return start in ms
	 */
	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	/**
	 * @return end in ms
	 */
	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + " time: "+ start+"-"+end;
	}
	
}
