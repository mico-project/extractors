package eu.mico.project.extractors.mt2rdf.model;

/**
 * ...
 * </p>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public interface AudioInfo {

    public String getID_String();
    public String getFormat();
    public String getFormat_Info();
    public String getFormat_Profile();
    public String getCodecID();
    public String getDuration_String();
    public String getDuration_LastFrame_String();
    public String getBitRate_Mode_String();
    public String getBitRate_String();
    public String getChannel_s__String();
    public String getChannel_s__Original_String();
    public String getChannelPositions();
    public String getSamplingRate_String();
    public String getCompression_Mode_String();
    public String getStreamSize_String();

}
