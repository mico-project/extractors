package eu.mico.project.extractors.mt2rdf;

import eu.mico.platform.anno4j.model.impl.bodymmm.MediaInfoBodyMMM;
import eu.mico.platform.event.api.AnalysisResponse;
import eu.mico.platform.event.api.AnalysisService;
import eu.mico.platform.event.api.EventManager;
import eu.mico.platform.event.impl.AnalysisServiceUtil;
import eu.mico.platform.event.impl.EventManagerImpl;
import eu.mico.platform.event.model.AnalysisException;
import eu.mico.platform.event.model.Event.ErrorCodes;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;
import eu.mico.platform.persistence.model.Resource;
import eu.mico.project.extractors.mt2rdf.model.Mediainfo;
import eu.mico.project.extractors.mt2rdf.model.MediainfoUtils;

import org.openrdf.idGenerator.IDGenerator;
import org.openrdf.model.Model;
import org.openrdf.model.URI;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectConnection;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.Rio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.impl.targets.SpecificResource;

import javax.xml.bind.JAXBException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public class MediaTagToRDFExtractor implements AnalysisService {

    private static Logger log = LoggerFactory.getLogger(MediaTagToRDFExtractor.class);
    
    public static final URI SERVICE_ID = ValueFactoryImpl.getInstance().createURI("http://www.mico-project.org/services/MediaTagToRDF");
    
    private String getRegistrationProperties(String propName){
      InputStream in = this.getClass().getClassLoader().getResourceAsStream("registration.properties");
      Properties props = new Properties();
      try {
        props.load(in);
      } catch (IOException e) {
        log.error("Error reading registration properties", e);
      }
      return props.getProperty(propName);
    }
    
    @Override
    public String getExtractorID() {
        return getRegistrationProperties("extractorId");
    }

    @Override
    public String getExtractorModeID() {
        return "annotate-all";
    }

    @Override
    public String getExtractorVersion() {
        return getRegistrationProperties("extractorVersion");
    }

    @Override
    public String getProvides() {
        return RDFFormat.TURTLE.getDefaultMIMEType();
    }

    @Override
    public String getRequires() {
        return "text/vnd.fhg-mediainfo+xml";
    }

    @Override
    public void call(AnalysisResponse response, Item item, List<Resource> resourceList, Map<String, String> params)
            throws AnalysisException, IOException, RepositoryException  {
        try {
            //parse module
            Resource res = resourceList.get(0);
            Part annotationPart = null;
            if(res instanceof Part){
                annotationPart = (Part) res;
                log.debug("Try to create RDF annotations for {}", annotationPart.getURI());
            }
            if(annotationPart==null || !annotationPart.hasAsset()){
                throw new AnalysisException(ErrorCodes.MISSING_ASSET,"Can not retrieve media info of empty asset",null);
            }
            if(annotationPart.getBody() instanceof MediaInfoBodyMMM){
              log.warn("  The part with annotation xml has a body of type MediaInfoBodyMMM.");
            }
            Asset xmlAsset = annotationPart.getAsset();
            if (!xmlAsset.getFormat().equalsIgnoreCase(getRequires())){
              log.warn("  Expected asset format {} but found {}.",getRequires(), xmlAsset.getFormat());
            }
            InputStream is = xmlAsset.getInputStream();
            if(is == null){
              throw new AnalysisException(ErrorCodes.MISSING_ASSET,"Can not retrieve media info, asset inputstream is null",null);
            }

            Mediainfo mediainfo = null;
            try{
              mediainfo = MediainfoUtils.parse(is);
            }catch(JAXBException e){
              throw new AnalysisException(ErrorCodes.INSUFFICIENT_RESOURCE,"Can not parse media info of asset",e);
            }

            ObjectConnection con = item.getObjectConnection();
            Part result = item.createPart(AnalysisServiceUtil.getServiceID(this));

            //get video resource, to link as target
            Resource video = null;
            try {
                video = annotationPart.getInputs().toArray(new Resource[0])[0];
            } catch (NullPointerException e) {
                log.error("Error getting video source uri: {}", e.getMessage(), e);
                throw new AnalysisException("Error getting media source uri: " + e.getMessage());
            }

            //write module to RDF
            Model model = mediainfo.toRdfModel(result.getURI(), video.getURI());
            OutputStream outputStream = result.getAsset().getOutputStream();
            Rio.write(model, outputStream, RDFFormat.TURTLE);
            outputStream.flush();
            outputStream.close();

            //add some metadata
            result.setSyntacticalType(getProvides());
            result.setSemanticType("Information retrieved by MediaInfo from the asset of "+video.getURI().stringValue());
            result.addInput(annotationPart);
            MediaInfoBodyMMM infoBodyMMM = createObject(con, MediaInfoBodyMMM.class);
            mediainfo.writeInfoBody(infoBodyMMM, mediainfo);
            result.setBody(infoBodyMMM);
            
            // create the target and set a reference to the part/item on which
            // the body refers to
            SpecificResource targetRes = createObject(con, SpecificResource.class);
            targetRes.setSource(video.getRDFObject());
            // adding the target to the new part
            result.addTarget(targetRes);

            response.sendNew(item, result.getURI());
            
            response.sendFinish(item);
            log.debug("Done for {}", annotationPart.getURI());

        } catch (RepositoryException e) {
            log.error("Error accessing the repository: {}", e.getMessage(), e);
            throw new IOException(e);
        } catch (RDFHandlerException e) {
            log.error("Error generating RDF: {}", e.getMessage(), e);
            throw new AnalysisException("Error generating RDF: " + e.getMessage());
        }
    }

  private <T> T createObject(ObjectConnection con, Class<T> type)
      throws RepositoryException {
    return createObject(con, type, null);
  }

  private <T> T createObject(ObjectConnection con, Class<T> type,
      org.openrdf.model.Resource resource) throws RepositoryException {
    return con.addDesignation(
        con.getObjectFactory().createObject(
            resource == null ? IDGenerator.BLANK_RESOURCE : resource, type),
        type);
  }

}
