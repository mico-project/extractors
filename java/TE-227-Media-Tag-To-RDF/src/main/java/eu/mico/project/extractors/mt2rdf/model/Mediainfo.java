package eu.mico.project.extractors.mt2rdf.model;

import java.util.List;

import eu.mico.platform.anno4j.model.impl.bodymmm.MediaInfoBodyMMM;
import eu.mico.platform.anno4j.model.namespaces.gen.MA;
import eu.mico.project.extractors.mt2rdf.MediaTagToRDFExtractor;

import org.apache.directory.api.util.Strings;
import org.openrdf.model.Model;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.repository.RepositoryException;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
@XmlRootElement(name = "File")
public class Mediainfo {

    @XmlElement(name = "track", required=true)
    private List<Track> tracks;
  
    public List<Track> getTracks() {
        return tracks;
    }
    
    public AudioInfo getAudioInfo() {
        for(Track track : this.getTracks()) {
            if(track.getType() == Track.Type.Audio) return track;
        }
        return null;
    }

    public VideoInfo getVideoInfo() {
        for(Track track : this.getTracks()) {
            if(track.getType() == Track.Type.Video) return track;
        }
        return null;
    }

    public GeneralInfo getGeneralInfo() {
        for(Track track : this.getTracks()) {
            if(track.getType() == Track.Type.General) return track;
        }
        return null;
    }

    public Model toRdfModel(URI content, URI video) {

        final Model model = new TreeModel();
        final ValueFactory vf = ValueFactoryImpl.getInstance();;

        if(getGeneralInfo() != null) writeGeneralInfo(model, vf, content, video);
        if(getVideoInfo() != null) writeVideoInfo(model, vf, content, video);
        if(getAudioInfo() != null) writeAudioInfo(model, vf, content, video);

        model.add(video, RDF.TYPE, MA.MediaResource);
        model.add(video, MA.hasContributor, MediaTagToRDFExtractor.SERVICE_ID);

        return model;
    }

    public MediaInfoBodyMMM writeInfoBody(MediaInfoBodyMMM infoBodyMMM,
            Mediainfo mediainfo) throws RepositoryException {

      GeneralInfo generalInfo = mediainfo.getGeneralInfo();
      if (Strings.isNotEmpty(generalInfo.getFormat())) {
        infoBodyMMM.setFormat(generalInfo.getFormat());
      }
      if (Strings.isNotEmpty(generalInfo.getOverallBitRate_String())) {
        infoBodyMMM.setAvgBitRate(generalInfo.getOverallBitRate_String());
      }

      VideoInfo videoInfo = mediainfo.getVideoInfo();
      if(videoInfo != null && Strings.isNotEmpty(videoInfo.getHeight_String())){
        infoBodyMMM.setHeight(Long.valueOf(firstToken(videoInfo.getHeight_String())));
        infoBodyMMM.setWidth(Long.valueOf(firstToken(videoInfo.getWidth_String())));
      }

      AudioInfo audioInfo = mediainfo.getAudioInfo();
      if(audioInfo != null && Strings.isNotEmpty(audioInfo.getSamplingRate_String())){
        infoBodyMMM.setSamplingRate(firstToken(audioInfo.getSamplingRate_String()));
      }

      return infoBodyMMM;
    }
    
    /**
     * writes general info. TODO extend
     * @param model
     * @param vf
     * @param content
     * @param video
     */
    private void writeGeneralInfo(Model model, ValueFactory vf, URI content, URI video) {
        GeneralInfo info = getGeneralInfo();
        model.add(video, MA.hasFormat, vf.createLiteral(info.getFormat()));
        model.add(video, MA.duration, vf.createLiteral(MediainfoUtils.parseDurationString(info.getDuration_String())));
        model.add(video, MA.averageBitRate, vf.createLiteral(firstToken(info.getOverallBitRate_String())));
    }

    /**
     * writes video info. TODO extend
     * @param model
     * @param vf
     * @param content
     * @param video
     */
    private void writeVideoInfo(Model model, ValueFactory vf, URI content, URI video) {
        VideoInfo info = getVideoInfo();
        URI track = vf.createURI(video.stringValue() + "#video");
        model.add(video, MA.frameWidth, vf.createLiteral(firstToken(info.getWidth_String())));
        model.add(video, MA.frameHeight, vf.createLiteral(firstToken(info.getHeight_String())));
        model.add(video, MA.frameRate, vf.createLiteral(firstToken(info.getFrameRate_String())));
        model.add(video, MA.hasTrack, track);
        model.add(track, RDF.TYPE, MA.VideoTrack);
        model.add(track, MA.hasFormat, vf.createLiteral(info.getFormat()));
        model.add(track, MA.averageBitRate, vf.createLiteral(info.getBitRate_String()));
    }

    /**
     * writes audio info. TODO extend
     * @param model
     * @param vf
     * @param content
     * @param video
     */
    private void writeAudioInfo(Model model, ValueFactory vf, URI content, URI video) {
        AudioInfo info = getAudioInfo();
        URI track = vf.createURI(video.stringValue() + "#audio");
        model.add(video, MA.hasTrack, track);
        model.add(track, RDF.TYPE, MA.AudioTrack);
        model.add(track, MA.hasFormat, vf.createLiteral(info.getFormat()));
        model.add(track, MA.averageBitRate, vf.createLiteral(info.getBitRate_String()));
    }

    private String firstToken(String s) {
        return s.indexOf(" ") > 0 ? s.substring(0, s.indexOf(" ")) : s;
    }

}
