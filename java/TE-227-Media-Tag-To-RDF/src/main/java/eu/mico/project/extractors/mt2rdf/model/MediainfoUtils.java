package eu.mico.project.extractors.mt2rdf.model;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public class MediainfoUtils {

    private static final Pattern DURATION = Pattern.compile("(([1-9]+)mn.)?(([1-9]+)s)");

    public static Mediainfo parse(InputStream in) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(Mediainfo.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        Mediainfo mediaInfo = (Mediainfo) unmarshaller.unmarshal(in);
        return mediaInfo;
    }

    public static String parseDurationString(String d) {
        Matcher m = DURATION.matcher(d);

        int sec = 0;

        if(!m.matches()) return "0";

        if(m.group(2) != null) sec += 60 * Integer.parseInt(m.group(2));
        if(m.group(4) != null) sec += Integer.parseInt(m.group(4));

        return String.valueOf(sec);
    }

}
