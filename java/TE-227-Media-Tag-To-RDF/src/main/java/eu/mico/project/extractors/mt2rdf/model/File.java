package eu.mico.project.extractors.mt2rdf.model;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public class File {

    @XmlElement(name = "track")
    private List<Track> tracks;

    public List<Track> getTracks() {
        return tracks;
    }
}
