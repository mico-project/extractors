package eu.mico.project.extractors.mt2rdf.model;

/**
 * ...
 * </p>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public interface VideoInfo {

    public String getID_String();
    public String getFormat();
    public String getFormat_Info();
    public String getFormat_Profile();
    public String getFormat_Settings_CABAC_String();
    public String getFormat_Settings_RefFrames_String();
    public String getCodecID();
    public String getCodecID_Info();
    public String getDuration_String();
    public String getBitRate_String();
    public String getWidth_String();
    public String getHeight_String();
    public String getDisplayAspectRatio_String();
    public String getFrameRate_Mode_String();
    public String getFrameRate_String();
    public String getColorSpace();
    public String getChromaSubsampling();
    public String getBitDepth_String();
    public String getScanType_String();
    public String getBits_Pixel_Frame_();
    public String getStreamSize_String();
    public String getEncoded_Library_String();
    public String getEncoded_Library_Settings();

}
