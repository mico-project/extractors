package eu.mico.project.extractors.mt2rdf.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * ...
 * </p>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public class Track implements GeneralInfo, AudioInfo, VideoInfo {

    public static enum Type {
        General, Video, Audio
    }

    @XmlAttribute
    private Type type;

    @XmlElement(name = "Complete_name")
    private String completeName;

    @XmlElement(name = "File_size")
    private String fileSize_String;

    @XmlElement(name = "Overall_bit_rate")
    private String overallBitRate_String;

    @XmlElement(name = "Encoded_Application")
    private String encoded_Application;

    @XmlElement(name = "Format_Settings_CABAC_String")
    private String format_Settings_CABAC_String;

    @XmlElement(name = "Format_Settings_RefFrames_String")
    private String format_Settings_RefFrames_String;

    @XmlElement(name = "Width")
    private String width_String;

    @XmlElement(name = "Height")
    private String height_String;

    @XmlElement(name = "Display_aspect_ratio")
    private String displayAspectRatio_String;

    @XmlElement(name = "Frame_rate_mode")
    private String frameRate_Mode_String;

    @XmlElement(name = "Frame_rate")
    private String frameRate_String;

    @XmlElement(name = "Color_space")
    private String colorSpace;

    @XmlElement(name = "Chroma_subsampling")
    private String chromaSubsampling;

    @XmlElement(name = "Bit_depth")
    private String bitDepth_String;

    @XmlElement(name = "Scan_type")
    private String scanType_String;

    @XmlElement(name = "Bits__Pixel_Frame_")
    private String bits_Pixel_Frame_;

    @XmlElement(name = "Stream_size")
    private String streamSize_String;

    @XmlElement(name = "Encoded_Library_String")
    private String encoded_Library_String;

    @XmlElement(name = "Encoded_Library_Settings")
    private String encoded_Library_Settings;

    @XmlElement(name = "ID")
    private String iD_String;

    @XmlElement(name = "Format")
    private String format;

    @XmlElement(name = "Format_Info")
    private String format_Info;

    @XmlElement(name = "Format_profile")
    private String format_Profile;

    @XmlElement(name = "Codec_ID")
    private String codecID;

    @XmlElement(name = "Codec_ID_Info")
    private String codecID_Info;

    @XmlElement(name = "Duration")
    private String duration_String;

    @XmlElement(name = "Duration_LastFrame")
    private String duration_LastFrame_String;

    @XmlElement(name = "Bit_rate_mode")
    private String bitRate_Mode_String;

    @XmlElement(name = "Bit_rate")
    private String bitRate_String;

    @XmlElement(name = "Channel_s_")
    private String channel_s__String;

    @XmlElement(name = "Channel_s__Original")
    private String channel_s__Original_String;

    @XmlElement(name = "Channel_positions")
    private String channelPositions;

    @XmlElement(name = "Sampling_rate")
    private String samplingRate_String;

    @XmlElement(name = "Compression_mode")
    private String compression_Mode_String;

    public Type getType() {
        return type;
    }

    public String getCompleteName() {
        return completeName;
    }

    public String getFileSize_String() {
        return fileSize_String;
    }

    public String getOverallBitRate_String() {
        return overallBitRate_String;
    }

    public String getEncoded_Application() {
        return encoded_Application;
    }

    public String getFormat_Settings_CABAC_String() {
        return format_Settings_CABAC_String;
    }

    public String getFormat_Settings_RefFrames_String() {
        return format_Settings_RefFrames_String;
    }

    public String getWidth_String() {
        return width_String;
    }

    public String getHeight_String() {
        return height_String;
    }

    public String getDisplayAspectRatio_String() {
        return displayAspectRatio_String;
    }

    public String getFrameRate_Mode_String() {
        return frameRate_Mode_String;
    }

    public String getFrameRate_String() {
        return frameRate_String;
    }

    public String getColorSpace() {
        return colorSpace;
    }

    public String getChromaSubsampling() {
        return chromaSubsampling;
    }

    public String getBitDepth_String() {
        return bitDepth_String;
    }

    public String getScanType_String() {
        return scanType_String;
    }

    public String getBits_Pixel_Frame_() {
        return bits_Pixel_Frame_;
    }

    public String getStreamSize_String() {
        return streamSize_String;
    }

    public String getEncoded_Library_String() {
        return encoded_Library_String;
    }

    public String getEncoded_Library_Settings() {
        return encoded_Library_Settings;
    }

    @Override
    public String getID_String() {
        return iD_String;
    }

    public String getFormat() {
        return format;
    }

    public String getFormat_Info() {
        return format_Info;
    }

    public String getFormat_Profile() {
        return format_Profile;
    }

    public String getCodecID() {
        return codecID;
    }

    @Override
    public String getCodecID_Info() {
        return codecID_Info;
    }

    public String getDuration_String() {
        return duration_String;
    }

    public String getDuration_LastFrame_String() {
        return duration_LastFrame_String;
    }

    public String getBitRate_Mode_String() {
        return bitRate_Mode_String;
    }

    public String getBitRate_String() {
        return bitRate_String;
    }

    public String getChannel_s__String() {
        return channel_s__String;
    }

    public String getChannel_s__Original_String() {
        return channel_s__Original_String;
    }

    public String getChannelPositions() {
        return channelPositions;
    }

    public String getSamplingRate_String() {
        return samplingRate_String;
    }

    public String getCompression_Mode_String() {
        return compression_Mode_String;
    }
}
