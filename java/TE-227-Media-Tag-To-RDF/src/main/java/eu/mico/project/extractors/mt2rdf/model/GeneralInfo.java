package eu.mico.project.extractors.mt2rdf.model;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public interface GeneralInfo {

    public String getCompleteName();
    public String getFormat();
    public String getFormat_Profile();
    public String getCodecID();
    public String getFileSize_String();
    public String getDuration_String();
    public String getOverallBitRate_String();
    public String getEncoded_Application();
}
