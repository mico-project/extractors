package eu.mico.project.extractors.mt2rdf;

import com.google.common.io.Resources;

import eu.mico.platform.anno4j.model.namespaces.gen.MA;
import eu.mico.project.extractors.mt2rdf.model.*;

import org.junit.Assert;
import org.junit.Test;
import org.openrdf.model.Model;
import org.openrdf.model.URI;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.Rio;

import javax.xml.bind.JAXBException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public class MediaTagToRDFExtractorTest {

    @Test
    public void parserTest() throws IOException, JAXBException {
        URL url = Resources.getResource("example.xml");
        InputStream in = url.openStream();

        Mediainfo info = MediainfoUtils.parse(in);

        GeneralInfo gi = info.getGeneralInfo();
        VideoInfo vi = info.getVideoInfo();
        AudioInfo ai = info.getAudioInfo();

        Assert.assertNotNull(gi);
        Assert.assertNotNull(ai);
        Assert.assertNotNull(vi);

        Assert.assertEquals("MPEG-4", gi.getFormat());
        Assert.assertEquals("1mn 36s", gi.getDuration_String());
        Assert.assertEquals("AVC", vi.getFormat());
        Assert.assertEquals("25.000 fps", vi.getFrameRate_String());
        Assert.assertEquals("640 pixels", vi.getWidth_String());
        Assert.assertEquals("360 pixels", vi.getHeight_String());
        Assert.assertEquals("AAC", ai.getFormat());
        Assert.assertEquals("44.1 KHz", ai.getSamplingRate_String());
    }

    @Test
    public void parserDurationTest() {
        String duration1 = "1mn 36s";
        String duration2 = "3s";
        String duration3 = "11mn 6s";

        String d1 = MediainfoUtils.parseDurationString(duration1);
        String d2 = MediainfoUtils.parseDurationString(duration2);
        String d3 = MediainfoUtils.parseDurationString(duration3);

        Assert.assertEquals("96", d1);
        Assert.assertEquals("3",d2);
        Assert.assertEquals("666",d3);
    }

    @Test
    public void testRDFizing() throws IOException, JAXBException, RDFHandlerException {

        URL url = Resources.getResource("example.xml");
        InputStream in = url.openStream();

        Mediainfo m = MediainfoUtils.parse(in);

        URI videoURI = ValueFactoryImpl.getInstance().createURI("http://example.org/video.mp4");
        URI contentURI = ValueFactoryImpl.getInstance().createURI("http://example.org/content");

        Model model = m.toRdfModel(contentURI, videoURI);

        URI videoTrack = ValueFactoryImpl.getInstance().createURI(videoURI.stringValue() + "#video");
        URI audioTrack = ValueFactoryImpl.getInstance().createURI(videoURI.stringValue() + "#audio");

        Assert.assertTrue(model.contains(videoURI, MA.hasTrack, videoTrack));
        Assert.assertTrue(model.contains(videoURI, MA.hasTrack, audioTrack));
        Assert.assertTrue(model.contains(videoURI, MA.duration, ValueFactoryImpl.getInstance().createLiteral("96")));

        Rio.write(model, System.out, RDFFormat.TURTLE);
    }

}
