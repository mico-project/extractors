package eu.mico.extractors.ner;

import static eu.mico.extractors.ner.TestConfig.*;

import org.junit.*;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.RDFHandlerBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.namespaces.OADM;

import eu.mico.extractors.ner.impl.RedlinkTextAnalysis;
import eu.mico.extractors.ner.model.LanguageAnnotation;
import eu.mico.extractors.ner.model.NamedEntityAnnotation;
import eu.mico.extractors.ner.model.SentimentAnnotation;
import eu.mico.extractors.ner.model.TextAnnotation;
import eu.mico.extractors.ner.model.TopicAnnotation;
import eu.mico.platform.anno4j.model.namespaces.FAM;
import eu.mico.platform.anno4j.model.namespaces.MMM;
import eu.mico.platform.persistence.model.Item;

import java.io.PrintWriter;
import java.util.Collection;

/**
 * Tests the Redlink Analysis Service
 *
 * @author Sergio Fernández
 * @author Rupert Westenthaler
 */
public class RedlinkAnalysisServiceTest{

    private Logger log = LoggerFactory.getLogger(getClass());
    
    private static TextAnalysis ner;

    
    @BeforeClass
    public static void setup() {
        ner = new RedlinkTextAnalysis(TEST_APP,TEST_KEY);
    }

    @AfterClass
    public static void tearDown() {
        ner = null;
    }

    @Test
    public void testLowercase() {
        final Collection<TextAnnotation> annotations = ner.exec("uh first of all this is where we're projected to go with the u._s. contribution the global warming under business as usual efficiency and and use electricity and and use of all energy is the low hanging fruit deficiency in conservation it's not a cost it's a profit the sign is wrong it's not a negative it's positive these are investments that hey for themselves but they are also very effective injured flexing our pat");
        Assert.assertNotNull(annotations);
        int las = 0;
        int neas = 0;
        int sas = 0;
        int tas = 0;
        for(TextAnnotation ta : annotations){
            if(ta instanceof LanguageAnnotation){
                las++;
            } else if(ta instanceof NamedEntityAnnotation){
                neas++;
                //we want mentions for NamedEntityAnnotations
                Assert.assertTrue(!ta.getMentions().isEmpty());
            } else if(ta instanceof SentimentAnnotation) {
                sas++;
            } else if(ta instanceof TopicAnnotation){
                tas++;
            }
        }
        Assert.assertEquals(1, las);
        Assert.assertEquals(1, sas);
        Assert.assertTrue(neas > 5);
        Assert.assertTrue(tas > 0);
    }
    
    @Test
    public void testText() {
        final Collection<TextAnnotation> annotations = ner.exec("It looks like a cross between a Lion and a Wildebeest, but I suspect the horns are blades of grass, and the short, white legs strongly suggest a lion.  The apparent wrinkling on the neck is a puzzle, though. I suppose the many little disks are water droplets on the lens, but it lends a weird look to the scene!");
        Assert.assertNotNull(annotations);
        int las = 0;
        int neas = 0;
        int sas = 0;
        int tas = 0;
        for(TextAnnotation ta : annotations){
            if(ta instanceof LanguageAnnotation){
                las++;
            } else if(ta instanceof NamedEntityAnnotation){
                neas++;
                //we want mentions for NamedEntityAnnotations
                Assert.assertTrue(!ta.getMentions().isEmpty());
            } else if(ta instanceof SentimentAnnotation) {
                sas++;
            } else if(ta instanceof TopicAnnotation){
                tas++;
            }
        }
        Assert.assertEquals(1, las);
        Assert.assertEquals(1, sas);
        Assert.assertTrue(neas > 5);
        Assert.assertTrue(tas > 0);
    }
    
}
