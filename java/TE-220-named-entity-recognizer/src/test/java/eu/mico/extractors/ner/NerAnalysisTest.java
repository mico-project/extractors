package eu.mico.extractors.ner;

import static eu.mico.extractors.ner.TestConfig.TEST_APP;
import static eu.mico.extractors.ner.TestConfig.TEST_KEY;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectConnection;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.RDFHandlerBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.Body;
import com.github.anno4j.model.Selector;
import com.github.anno4j.model.impl.selector.TextPositionSelector;
import com.github.anno4j.model.impl.selector.TextQuoteSelector;
import com.github.anno4j.model.namespaces.OADM;

import eu.mico.platform.anno4j.model.fam.FAMBody;
import eu.mico.platform.anno4j.model.fam.TextPositionQuoteSelector;
import eu.mico.platform.anno4j.model.namespaces.FAM;
import eu.mico.platform.anno4j.model.namespaces.MMM;
import eu.mico.platform.event.api.AnalysisService;
import eu.mico.platform.event.impl.AnalysisServiceUtil;
import eu.mico.platform.event.test.mock.AnalysisResponseCollector;
import eu.mico.platform.event.test.mock.EventManagerMock;
import eu.mico.platform.persistence.api.PersistenceService;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;

/**
 * Simple tests for TE-220
 *
 * @author Sergio Fernández
 */
public class NerAnalysisTest {
    
    private static final Logger log = LoggerFactory.getLogger(NerAnalysisTest.class);

    private EventManagerMock eventManager;

    private AnalysisService extractor;

    @Before
    public void setup() throws IOException, URISyntaxException {
        eventManager = new EventManagerMock();
        eventManager.init();
        extractor = new TextAnalysisService("dummy",TEST_APP,TEST_KEY);
        eventManager.registerService(extractor);
    }

    @After
    public void shutdown() throws IOException {
        eventManager.shutdown();
    }

    @Test
    public void textTest() throws RepositoryException, IOException, RDFHandlerException {
        final PersistenceService persistenceService = eventManager.getPersistenceService();
        final Item contentItem = persistenceService.createItem();
        ObjectConnection con = contentItem.getObjectConnection();
        //TODO: parsing an URI makes it easier to track the input content part in the logs
        final Part cp = contentItem.createPart(con.getValueFactory().createURI("urn:test:dummyUri"));
        cp.setSyntacticalType("text/plain");
        final String json = IOUtils.toString(this.getClass().getClassLoader().getResourceAsStream("test.txt"));
        Asset asset = cp.getAsset();
        asset.setFormat("text/plain");
        final PrintStream ps = new PrintStream(asset.getOutputStream());
        ps.print(json);
        ps.close();

        eventManager.injectItem(contentItem);
        debugRDF(contentItem);

        log.info(" - contentParts: ");
        Iterable<? extends Part> contentParts = contentItem.getParts();
        int numContentParts = assertContentParts(contentParts);
        log.info("  ... {} contentParts asserted", numContentParts);
        
        AnalysisResponseCollector collector = eventManager.getResponsesCollector();
        Assert.assertTrue(collector.hasNew());
        //AnalysisService implementation do no longer need to send a finish message
        //as the EventManagerImpl does care about it
        //Assert.assertTrue(collector.isFinished());
        Assert.assertFalse(collector.isError());
    }

    private int assertContentParts(Iterable<? extends Part> contentParts) throws RepositoryException {
        int numContentParts = 0;
        for(Part part : contentParts){
            numContentParts++;
            log.info("   {}. {}", numContentParts, part.getURI());
            Body body = part.getBody();
            if(body == null){ //assert that this is an Part created for an Asset
                Assert.assertTrue(part.hasAsset());
                continue;
            }
            log.debug("    - body: {}", Arrays.toString(body.getClass().getInterfaces()));
            log.debug("    - extractor: {} ", part.getSerializedBy());
            Assert.assertNotNull(part.getSerializedBy());
            if(AnalysisServiceUtil.getServiceID(extractor).equals(part.getSerializedBy())){
                Assert.assertTrue(body instanceof FAMBody);
                FAMBody famAnno = (FAMBody)part.getBody();
                log.debug("    - extracted-from",famAnno.getContent());
                log.debug("    - selectors:");
                for(Selector s : famAnno.getSelectors()){
                    StringBuilder ss = new StringBuilder();
                    if(s instanceof TextPositionQuoteSelector){
                        TextPositionSelector tps = (TextPositionSelector)s;
                        ss.append("start: ").append(tps.getStart()).append(" | ");
                        ss.append("end: ").append(tps.getEnd()).append(" | ");
                        Assert.assertTrue(tps.getStart() >= 0);
                        Assert.assertTrue(tps.getEnd() > tps.getStart());
                    }
                    if(s instanceof TextQuoteSelector){
                        TextPositionQuoteSelector tqs = (TextPositionQuoteSelector)s;
                        ss.append("pre[exact]suf: ").append(tqs.getPrefix()).append('[')
                        .append(tqs.getExact()).append(']').append(tqs.getSuffix());
                        Assert.assertNotNull(tqs.getPrefix());
                        Assert.assertNotNull(tqs.getExact());
                        Assert.assertTrue(tqs.getExact().length() > 0);
                        Assert.assertNotNull(tqs.getSuffix());
                    }
                    Assert.assertTrue(ss.length() > 0); //textPositoion or text quote selector
                    log.debug("      - {}", ss);
                }
            }
        }
        return numContentParts;
    }
    private void debugRDF(Item item) throws RepositoryException, RDFHandlerException {
        if(!log.isDebugEnabled()){
            return;
        }
        //we copy all statements to a TreeModel as this one sorts them by SPO
        //what results in a much nicer TURTLE serialization
        final Model model = new TreeModel();
        //we also set commonly used namespaces
        model.setNamespace(OADM.PREFIX, OADM.NS);
        model.setNamespace(FAM.PREFIX, FAM.NS);
        model.setNamespace(RDF.PREFIX, RDF.NAMESPACE);
        model.setNamespace(RDFS.PREFIX, RDF.NAMESPACE);
        model.setNamespace("xsd", "http://www.w3.org/2001/XMLSchema#");
        model.setNamespace(MMM.PREFIX, MMM.NS);
        model.setNamespace("test", "http://localhost/mem/");
        model.setNamespace("services", "http://www.mico-project.eu/services/");
        RepositoryConnection con = item.getObjectConnection();
        boolean startedTransaction = false;
        try {
            if(!con.isActive()){
                con.begin();
                startedTransaction = true;
            }
            con.exportStatements(null, null, null, true, new RDFHandlerBase(){
                @Override
                public void handleStatement(Statement st) {
                    log.debug("{},{},{},{}",st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());
                    model.add(st);
                }
            });
        } finally {
            if(startedTransaction){
                con.rollback();
            }
        }
        log.debug("--- START generated RDF ---");
        PrintWriter out = new PrintWriter(System.out);
        Rio.write(model, out, RDFFormat.TURTLE);
        out.close();
        log.debug("--- END generated RDF ---");
    }

}
