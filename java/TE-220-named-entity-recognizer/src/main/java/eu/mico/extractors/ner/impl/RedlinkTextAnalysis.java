package eu.mico.extractors.ner.impl;

import io.redlink.sdk.RedLink;
import io.redlink.sdk.RedLinkFactory;
import io.redlink.sdk.analysis.AnalysisRequest;
import io.redlink.sdk.impl.analysis.model.Enhancements;
import io.redlink.sdk.impl.analysis.model.Entity;
import io.redlink.sdk.impl.analysis.model.EntityAnnotation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import eu.mico.extractors.ner.TextAnalysis;
import eu.mico.extractors.ner.model.LanguageAnnotation;
import eu.mico.extractors.ner.model.NamedEntityAnnotation;
import eu.mico.extractors.ner.model.SentimentAnnotation;
import eu.mico.extractors.ner.model.TextAnnotation;
import eu.mico.extractors.ner.model.TextMention;
import eu.mico.extractors.ner.model.TopicAnnotation;

/**
 * NER implementation based on Redlink API
 *
 * @author Sergio Fernández
 * @author Rupert Westenthaler
 * @see <a href="http://dev.redlink.io/api">Redlink API</a>
 */
public class RedlinkTextAnalysis implements TextAnalysis {


    //TODO: make this configurable
    private static final Double MIN_CONFIDENCE = Double.valueOf(0.8);

    private final RedLink.Analysis analysis;

    private String appName;

    public RedlinkTextAnalysis(String appName, String appKey) {
        assert appName != null && !appName.isEmpty();
        assert appKey != null && !appKey.isEmpty();
        analysis = RedLinkFactory.createAnalysisClient(appKey);
        this.appName = appName;
    }

    @Override
    public Collection<TextAnnotation> exec(String text) {
        List<TextAnnotation> annotationList = new ArrayList<>();
        AnalysisRequest request = AnalysisRequest.builder()
                .setAnalysis(appName)
                .setContent(text)
                .setOutputFormat(AnalysisRequest.OutputFormat.TURTLE)
                .build();
        Enhancements enhancements = analysis.enhance(request);
        //remember Annotations made for Entities to avoid duplicates
        Map<String, NamedEntityAnnotation> entityAnnotations = new HashMap<>();
        //remember processed TextAnnotations
        Set<io.redlink.sdk.impl.analysis.model.TextAnnotation> processedTa = new HashSet<>();
        //in the first step we 
        for(Entry<io.redlink.sdk.impl.analysis.model.TextAnnotation, Collection<EntityAnnotation>> entry : enhancements.getBestAnnotations().asMap().entrySet()){
            if(entry.getValue() != null && !entry.getValue().isEmpty()){ 
                processedTa.add(entry.getKey());
                TextMention mention = createTextMention(text, entry.getKey());
                for(EntityAnnotation ea : entry.getValue()){
                    Entity entity = ea.getEntityReference();
                    NamedEntityAnnotation anno = entityAnnotations.get(entity.getUri());
                    if(anno == null) {
                        anno = new NamedEntityAnnotation(new TextAnnotation.Label(ea.getEntityLabel(),ea.getEntityLabelLang()), 
                                entity.getUri(), ea.getConfidence());
                        anno.getEntityTypes().addAll(ea.getEntityTypes());
                        entityAnnotations.put(entity.getUri(), anno);
                    }
                    if(ea.getConfidence() != null){ //update confidence if higher
                        if(anno.getConfidence() == null || ea.getConfidence() > anno.getConfidence()){
                            anno.setConfidence(ea.getConfidence());
                        }
                    }
                    anno.getMentions().add(mention);
                }
            } //not a Linked Entity
        }
        for(NamedEntityAnnotation nea : entityAnnotations.values()){
            if(nea.getConfidence() == null || nea.getConfidence() >= MIN_CONFIDENCE){
                annotationList.add(nea);
            }
        }
        for(io.redlink.sdk.impl.analysis.model.TextAnnotation ta : enhancements.getTextAnnotations()){
           if(!processedTa.contains(ta)){
               processedTa.add(ta);
               TextMention mention = createTextMention(text, ta);
               //Named Entity Support
               if(ta.getType() != null && ta.getSelectedText() != null){
                   NamedEntityAnnotation nea = new NamedEntityAnnotation(
                           new TextAnnotation.Label(ta.getSelectedText(), ta.getSelectedTextLang()), ta.getConfidence());
                   if(mention != null){
                       nea.getMentions().add(mention);
                   }
                   nea.getEntityTypes().add(ta.getType());
               } // else unsupported TA
               
           }
        }
        //Sentiments
        Double documentSentiment = enhancements.getDocumentSentiment();
        if(documentSentiment != null){
            annotationList.add(new SentimentAnnotation(documentSentiment));
        }
        //language annotations
        for(String language : enhancements.getLanguages()){
            annotationList.add(new LanguageAnnotation(language));
        }
        //Topics
        for(io.redlink.sdk.impl.analysis.model.TopicAnnotation ta : enhancements.getTopicAnnotations()){
            annotationList.add(new TopicAnnotation(
                    new TextAnnotation.Label(ta.getTopicLabel(),ta.getTopicLabelLang()),
                    ta.getTopicReference().getUri(), ta.getConfidence()));
        }
        
        return annotationList;
    }
    
    private TextMention createTextMention(String content, io.redlink.sdk.impl.analysis.model.TextAnnotation ta){
        if(ta.getStarts() != 0 || ta.getEnds() != 0){
            TextMention mention = new TextMention(ta.getStarts(), ta.getEnds(), 
                    ta.getSelectedText(),ta.getSelectionPrefix(), ta.getSelectionSuffix());
            TextMention.setContext(mention, content, false);
            return mention;
        } else {
            return null;
        }
    }

}
