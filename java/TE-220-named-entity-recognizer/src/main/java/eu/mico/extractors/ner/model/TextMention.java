package eu.mico.extractors.ner.model;

import java.util.Arrays;

public class TextMention extends Mention {

    private static final int PREFIX_SUFFIX_LENGTH = 5;
    private final int[] span;
    private String anchor;
    private String prefix;
    private String suffix;
    
    public TextMention(int start, int end){
        assert start >= 0;
        assert end > start;
        this.span = new int[]{start,end};
    }
    
    public TextMention(int start, int end, String anchor){
        this(start,end);
        assert anchor != null;
        assert end - anchor.length() == start;
        this.anchor = anchor;
    }

    public TextMention(int start, int end, String anchor, String prefix, String suffix){
        this(start,end, anchor);
        assert prefix != null;
        assert suffix != null;
        this.prefix = prefix;
        this.suffix = suffix;
    }
    
    void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    
    void setSuffix(String suffix) {
        this.suffix = suffix;
    }
    
    /**
     * The start of the mention
     * @return
     */
    public final int start(){
        return span[0];
    }
    /**
     * The end of the mention
     * @return
     */
    public final int end(){
        return span[1];
    }
    
    /**
     * 
     */
    public final String anchor(){
        return anchor;
    }
    
    public final String prefix(){
        return prefix;
    }
    
    public final String suffix(){
        return suffix;
    }
    
    /**
     * Sets {@link #anchor()}, {@link #prefix()} and {@link #suffix()} for
     * the parsed {@link TextMention} based on the parsed content. Existing
     * values are NOT overridden. 
     * @param mention the mention
     * @param content the content
     */
    public static void setContext(TextMention mention, String content){
        setContext(mention, content, false);
    }
    
    /**
     * Sets {@link #anchor()}, {@link #prefix()} and {@link #suffix()} for
     * the parsed {@link TextMention} based on the parsed content.
     * @param mention the mention
     * @param content the content
     * @param override if existing values shall be overridden.
     */
    public static void setContext(TextMention mention, String content, boolean override){
        if(mention == null || content == null || mention.span[0] < 0 ||
                mention.span[0] > mention.span[1] || mention.span[1] > content.length()){
            return;
        }
        if(mention.anchor == null || override){
            mention.anchor = content.substring(mention.span[0], mention.span[1]);
        } //do not override exis
        if(mention.prefix == null || override){
            int prefixStart = Math.max(0, mention.span[0] - PREFIX_SUFFIX_LENGTH);
            if(prefixStart <= mention.span[0]){
                mention.prefix = content.substring(prefixStart, mention.span[0]);
            }
        }
        if(mention.suffix == null || override){
            int suffixEnd = Math.min(content.length(), mention.span[1] - PREFIX_SUFFIX_LENGTH);
            if(suffixEnd > mention.span[1]){
                mention.prefix = content.substring(mention.span[1], suffixEnd);
            } else if(suffixEnd == mention.span[1]){
                mention.prefix = "";
            }
        }
    }

    @Override
    public int hashCode() {
        //include the SpanTypeEnum in the hash
        return Arrays.hashCode(span);
    }
    
    @Override
    public boolean equals(Object obj) {
        return obj instanceof TextMention &&
                Arrays.equals(this.span, ((TextMention)obj).span); 
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(getClass().getSimpleName()).append(Arrays.toString(span));
        if(anchor != null || prefix != null || suffix != null){
            sb.append(" - ");
            if(prefix != null){
                sb.append(prefix).append(">>");
            }
            sb.append(anchor);
            if(suffix != null){
                sb.append("<<").append(suffix);
            }
        }
        return sb.toString();
    }
    
    @Override
    public int compareTo(Mention other) {
        final TextMention otm;
        if(other instanceof TextMention){
            otm = (TextMention) other;
        } else {
            throw new IllegalStateException("Unable to compare different types of Mentions (other: "
                    + other.getClass().getName() +" with "+ getClass().getName()+")!");
        }
        //Compare Integers ASC (used here three times)
        //    (x < y) ? -1 : ((x == y) ? 0 : 1);
        int start = (span[0] < otm.span[0]) ? -1 : ((span[0] == otm.span[0]) ? 0 : 1);
        if(start == 0){
            //sort end in DESC order
            return (span[1] < otm.span[1]) ? 1 : ((span[1] == otm.span[1]) ? 0 : -1);
        } else {
            return start;
        }
    }    
}
