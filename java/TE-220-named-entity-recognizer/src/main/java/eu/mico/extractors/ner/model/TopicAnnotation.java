package eu.mico.extractors.ner.model;

public class TopicAnnotation extends TextAnnotation {

    private final String topic;
    private final Label name;
    
    public TopicAnnotation(String name, String topic, Double confidence) {
        this(name == null ? null : new Label(name,null), topic, confidence);
    }
    public TopicAnnotation(Label name, String topic, Double confidence) {
        super(confidence);
        this.name = name;
        this.topic = topic;
    }
    
    
    public Label getName() {
        return name;
    }
    
    public String getTopic() {
        return topic;
    }
    
}
