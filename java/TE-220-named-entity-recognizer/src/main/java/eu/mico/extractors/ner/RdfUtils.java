package eu.mico.extractors.ner;

import org.openrdf.model.Literal;
import org.openrdf.model.Model;
import org.openrdf.model.Value;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.Rio;

import java.io.OutputStream;

/**
 * RDF Utilities
 *
 * @author Sergio Fernández
 */
public class RdfUtils {

    /**
     * Flats a RDF Graph into a textual representation with all literals
     *
     * @param model
     * @return
     */
    public static String getAllLiterals(Model model) {
        StringBuffer sb = new StringBuffer();
        for (Value value: model.objects()) {
            if (value instanceof Literal) {
                sb.append(value.stringValue());
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    /**
     * Writes the model
     *
     * @param model
     * @param out
     * @param format
     * @throws org.openrdf.rio.RDFHandlerException
     */
    public static void writeRdf(Model model, OutputStream out, RDFFormat format) throws RDFHandlerException {
        Rio.write(model, out, format);
    }

}
