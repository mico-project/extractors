package eu.mico.extractors.ner;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.openrdf.idGenerator.IDGenerator;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.LangString;
import org.openrdf.repository.object.ObjectConnection;
import org.openrdf.repository.object.RDFObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.Selector;
import com.github.anno4j.model.impl.selector.TextPositionSelector;
import com.github.anno4j.model.impl.targets.SpecificResource;

import static eu.mico.platform.anno4j.model.namespaces.MMMTERMS.NER_BODY;
import eu.mico.extractors.ner.impl.RedlinkTextAnalysis;
import eu.mico.extractors.ner.model.LanguageAnnotation;
import eu.mico.extractors.ner.model.NamedEntityAnnotation;
import eu.mico.extractors.ner.model.SentimentAnnotation;
import eu.mico.extractors.ner.model.TextAnnotation;
import eu.mico.extractors.ner.model.TextAnnotation.Label;
import eu.mico.extractors.ner.model.TextMention;
import eu.mico.extractors.ner.model.TopicAnnotation;
import eu.mico.platform.anno4j.model.fam.FAMBody;
import eu.mico.platform.anno4j.model.fam.LanguageBody;
import eu.mico.platform.anno4j.model.fam.LinkedEntityBody;
import eu.mico.platform.anno4j.model.fam.NamedEntityBody;
import eu.mico.platform.anno4j.model.fam.SentimentBody;
import eu.mico.platform.anno4j.model.fam.TextPositionQuoteSelector;
import eu.mico.platform.anno4j.model.fam.TopicBody;
import eu.mico.platform.anno4j.model.namespaces.FAM;
import eu.mico.platform.event.api.AnalysisResponse;
import eu.mico.platform.event.api.AnalysisService;
import eu.mico.platform.event.impl.AnalysisServiceUtil;
import eu.mico.platform.event.model.AnalysisException;
import eu.mico.platform.event.model.Event.ErrorCodes;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;
import eu.mico.platform.persistence.model.Resource;

/**
 * NER Analysis Service from text
 *
 * @author Sergio Fernández
 * @author Rupert Westenthaler
 */
public class TextAnalysisService implements AnalysisService {

    private static Logger log = LoggerFactory.getLogger(TextAnalysisService.class);

    private static final Charset UTF8 = Charset.forName("UTF-8");

    private TextAnalysis textAnalysis;

    public TextAnalysisService(String queueName, String appName, String appKey) {
        assert queueName != null && !queueName.isEmpty();
        textAnalysis = new RedlinkTextAnalysis(appName, appKey);
    }


	private String getRegistrationProperties(String propName){
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("registration.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			log.error("Error reading registration properties", e);
		}
		return props.getProperty(propName);
	}
	
	@Override
	public String getExtractorID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorId");
	}

	@Override
	public String getExtractorModeID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorModeId");
	}

	@Override
	public String getExtractorVersion() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorVersion");
	}
    

    @Override
    public String getProvides() {
        return "application/x-mico-rdf";
    }

    @Override
    public String getRequires() {
        return "text/plain";
    }

//    @Override
//    public String getQueueName() {
//        return queueName;
//    }

    @Override
    public void call(AnalysisResponse resp, Item ci, List<Resource> resourceList, Map<String, String> params)
                    throws AnalysisException, IOException, RepositoryException {
        if(resp == null){
            throw new AnalysisException("The parsed response MUST NOT be NULL!");
        }
        if(ci == null){
            throw new AnalysisException("The parsed content item MUST NOT be NULL!");
        }
        //search for the text asset
        log.trace("> process Item {}", ci.getURI());
        log.trace(" - resourceList (size:{}): {}", resourceList.size(), resourceList);
        Asset txtAsset = null;
        Resource txtResource = null;
        for(Resource resource : resourceList){
            if(resource.hasAsset()){
                log.debug(" - check Assert for Resource: {} (type: {})", resource.getURI(),resource.getClass().getSimpleName());
                Asset asset = resource.getAsset();
                String format = asset.getFormat().split(";")[0].trim();
                log.debug("   - format: {}",format);
                if(getRequires().equalsIgnoreCase(format)){ //we can process this asset
                    log.debug(" - use Asset {} of {} {} with format {}",
                            asset, resource instanceof Item ? "item" : "part" , resource.getURI(), asset.getFormat());
                    txtAsset = asset;
                    txtResource = resource;
                    break;
                } else if(log.isDebugEnabled()){ 
                    log.debug(" - ignore Asset {} of {} {} because its format {} is not supported (supported: {})",
                            asset, resource instanceof Item ? "item" : "part" , resource.getURI(), format, getRequires());
                }
            } else {
                log.debug(" - Resource: {} (type: {}) has no asset", resource.getURI(),resource.getClass().getSimpleName());
            }
        }
        if(txtAsset == null){
            log.warn(" - got call for Item {} and resources {} where none had an asset with the supported format {}",
                    ci.getURI(), resourceList, getRequires());
            throw new AnalysisException(ErrorCodes.MISSING_ANNOTATION,"No text/plain asset",null);
        }
        String text = IOUtils.toString(txtAsset.getInputStream(), UTF8);
        if(StringUtils.isBlank(text)){
            throw new AnalysisException(ErrorCodes.MISSING_ANNOTATION, 
                    "The text/plain asset contains an empty text", null);
        }

        ObjectConnection con = ci.getObjectConnection();

        log.info(" - text content of length {}", text.length());
        Collection<TextAnnotation> annos;
        try {
            annos = textAnalysis.exec(text);
            log.info(" - extracted {} annotations", annos.size());
        } catch (RuntimeException e){
            log.warn(" - unable to perform text analysis using remote service ", e);
            annos = Collections.emptyList();
        }
        for(TextAnnotation anno : annos){
            // create new content part for each annotation body
            //END test workaround
            FAMBody body = createBody(con, anno);
            if(body != null){
                Part cp = ci.createPart(AnalysisServiceUtil.getServiceID(this));
                cp.setBody(body);
                cp.addInput(txtResource);
                cp.setSemanticType("Relevant entity recognized in the txt asset of " + txtResource.getURI());
                
                if(body instanceof LanguageBody){
                	cp.setSyntacticalType(FAM.LANGUAGE_ANNOTATION);
                }
                else if(body instanceof NamedEntityBody){
                	cp.setSyntacticalType(FAM.ENTITY_MENTION_ANNOTATION);
                }
                else if(body instanceof LinkedEntityBody){
                	cp.setSyntacticalType(FAM.LINKED_ENTITY_ANNOTATION);
                }
                else if(body instanceof SentimentBody){
                	cp.setSyntacticalType(FAM.SENTIMENT_ANNOTATION);
                }
                else if(body instanceof TopicBody){
                	cp.setSyntacticalType(FAM.TOPIC_ANNOTATION);
                }else{
                	log.warn("Unable to determine the specific syntactic type, defaulting to {}",NER_BODY);
                	cp.setSyntacticalType(NER_BODY);
                }
                
                body.setContent(txtResource.getRDFObject());
                log.info("- created new content part {} for {}", 
                        cp.getURI().stringValue(), anno);
                body.setContent(txtResource.getRDFObject());
                if(!anno.getMentions().isEmpty()){
                    for(TextMention mention : anno.getMentions()){
                        Selector selector = createTextSelector(con, mention);
                        SpecificResource sr = createObject(con, SpecificResource.class);
                        sr.setSource(txtResource.getRDFObject());
                        if(selector != null){
                            body.addSelector(selector);
                            sr.setSource(txtResource.getRDFObject());
                        }
                    }
                }
                resp.sendNew(ci, cp.getURI());
            }
        }
    }

    FAMBody createBody(ObjectConnection con, TextAnnotation anno) throws RepositoryException {
        if(anno == null){
            return null;
        } else if(anno instanceof LanguageAnnotation){
            LanguageAnnotation la = (LanguageAnnotation)anno;
            LanguageBody lb = createObject(con, LanguageBody.class);
            lb.setLanguage(la.getLanguage());
            lb.setConfidence(la.getConfidence());
            return lb;
        } else if(anno instanceof NamedEntityAnnotation){
            NamedEntityAnnotation nea = (NamedEntityAnnotation)anno;
            if(nea.getReference() == null){
                NamedEntityBody ne = createObject(con, NamedEntityBody.class);
                ne.setMention(toLangString(nea.getEntity()));
                Set<RDFObject> types = new HashSet<>();
                for(String type : nea.getEntityTypes()){
                    if(type != null){
                        types.add(RDFObject.class.cast(con.getObject(type)));
                    }
                }
                if(!types.isEmpty()){
                    ne.setTypes(types);
                }
                ne.setConfidence(nea.getConfidence());
                return ne;
            } else {
                LinkedEntityBody le = createObject(con, LinkedEntityBody.class);
                le.setLabel(toLangString(nea.getEntity()));
                le.setEntity(RDFObject.class.cast(con.getObject(nea.getReference())));
                Set<RDFObject> types = new HashSet<>();
                for(String type : nea.getEntityTypes()){
                    if(type != null){
                        types.add(RDFObject.class.cast(con.getObject(type)));
                    }
                }
                if(!types.isEmpty()){
                    le.setTypes(types);
                }
                le.setConfidence(nea.getConfidence());
                return le;
            }
        } else if(anno instanceof SentimentAnnotation){
            SentimentBody s = createObject(con, SentimentBody.class);
            s.setSentiment(((SentimentAnnotation)anno).getSentiment());
            s.setConfidence(anno.getConfidence());
            return s;
        } else if(anno instanceof TopicAnnotation){
            //TODO: we should write a TopicClassification for TopicAnnlations
            TopicAnnotation ta = (TopicAnnotation)anno;
            TopicBody t = createObject(con, TopicBody.class);
            t.addTopicLabel(toLangString(ta.getName()));
            t.setTopic(RDFObject.class.cast(con.getObject(ta.getTopic())));
            t.setConfidence(ta.getConfidence());
            return t;
        } else {
            log.warn("Unsupported TextAnnotation {} (type: {})", anno, anno.getClass().getName());
            return null;
        }
    }
    /**
     * Converts a {@link TextAnnotation.Label} to a {@link LangString}
     * @param label the label
     * @return the {@link LangString} or <code>null</code> if the parsed label was <code>null</code>
     */
    private static LangString toLangString(Label label) {
        if(label == null){
            return null;
        } else {
            return new LangString(label.getLabel(), label.getLanguage());
        }
    }

    Selector createTextSelector(ObjectConnection con, TextMention mention) throws RepositoryException {
        TextPositionSelector tps;
        if(mention.anchor() != null){
            TextPositionQuoteSelector tpqs = createObject(con, TextPositionQuoteSelector.class);
            tpqs.setExact(mention.anchor());
            tpqs.setPrefix(mention.prefix());
            tpqs.setSuffix(mention.suffix());
            tps = tpqs;
        } else {
            tps = createObject(con, TextPositionSelector.class);
        }
        tps.setStart(mention.start());
        tps.setEnd(mention.end());
        return tps;
    }
    
    private <T> T createObject(ObjectConnection con, Class<T> type) throws RepositoryException {
        return createObject(con, type, null);
    }
    
    private <T> T createObject(ObjectConnection con, Class<T> type, org.openrdf.model.Resource resource) throws RepositoryException {
        return con.addDesignation(con.getObjectFactory().createObject(
                resource == null ? IDGenerator.BLANK_RESOURCE : resource, type), type);
    }

}
