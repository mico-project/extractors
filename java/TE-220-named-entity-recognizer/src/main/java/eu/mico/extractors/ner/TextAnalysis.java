package eu.mico.extractors.ner;

import java.util.Collection;

import eu.mico.extractors.ner.model.TextAnnotation;

/**
 * NER basic service definition
 *
 * @author Sergio Fernández
 * @author Rupert Westenthaler
 */
public interface TextAnalysis {

    /**
     * Returns a collection of {@link TextAnnotation}
     * @param text the text to analyse
     * @return the extracted {@link TextAnnotation}s
     */
    Collection<TextAnnotation> exec(String text);

}
