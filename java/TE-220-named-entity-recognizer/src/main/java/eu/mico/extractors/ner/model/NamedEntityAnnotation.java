package eu.mico.extractors.ner.model;

import java.util.LinkedHashSet;
import java.util.Set;

public class NamedEntityAnnotation extends TextAnnotation {

    
    private final Label entity;
    private final String uri;
    private Set<String> entityTypes = new LinkedHashSet<>();
    
    public NamedEntityAnnotation(String entity) {
        this(entity == null ? null : new Label(entity, null), null, null);
    }
    public NamedEntityAnnotation(Label entity) {
        this(entity, null, null);
    }
    
    public NamedEntityAnnotation(String entity, Double confidence) {
        this(entity, null, confidence);
    }

    public NamedEntityAnnotation(Label entity, Double confidence) {
        this(entity, null, confidence);
    }
    public NamedEntityAnnotation(String entity, String uri) {
        this(entity, uri, null);
    }
    
    public NamedEntityAnnotation(String entity, String uri, Double confidence) {
        this(entity == null ? null : new Label(entity,null), uri, confidence);
    }
    public NamedEntityAnnotation(Label entity, String uri, Double confidence) {
        super(confidence);
        this.entity = entity;
        this.uri = uri;
    }
    /**
     * The name of the Linked Entity (might be different to the 
     * {@link TextMention#anchor() anchor} of mentions
     * @return
     */
    public Label getEntity() {
        return entity;
    }
    /**
     * The URI of the linked Entity.
     * @return
     */
    public String getReference() {
        return uri;
    }
    
    /**
     * The <code>rdf:type</code>'s of the referenced entity.
     * @return
     */
    public Set<String> getEntityTypes() {
        return entityTypes;
    }

    @Override
    public String toString() {
        return "NamedEntityAnnotation [entity=" + entity + ", uri=" + uri + "]";
    }
    
}
