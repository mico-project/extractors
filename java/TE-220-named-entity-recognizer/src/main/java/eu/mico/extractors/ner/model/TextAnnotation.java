package eu.mico.extractors.ner.model;

/**
 * An Annotation in a plain text content
 * @author Rupert Westenthaler
 *
 */
public abstract class TextAnnotation extends Annotation<TextMention> {

	public TextAnnotation() {
	    super();
    }

	public TextAnnotation(Double confidence) {
        super(confidence);
    }

	public static class Label {
	    
	    private final String label;
	    private final String language;
	    
	    public Label(String label, String language) {
	        this.label = label;
	        this.language = language;
        }
	    
	    public String getLabel() {
            return label;
        }
	    
	    public String getLanguage() {
            return language;
        }
	    
	    @Override
	    public String toString() {
	        return label + '@' + language;
	    }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((label == null) ? 0 : label.hashCode());
            result = prime * result
                    + ((language == null) ? 0 : language.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Label other = (Label) obj;
            if (label == null) {
                if (other.label != null)
                    return false;
            } else if (!label.equals(other.label))
                return false;
            if (language == null) {
                if (other.language != null)
                    return false;
            } else if (!language.equals(other.language))
                return false;
            return true;
        }
	   
	    
	}
	
}
