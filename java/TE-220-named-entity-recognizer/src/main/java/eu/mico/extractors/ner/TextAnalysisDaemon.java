package eu.mico.extractors.ner;

import eu.mico.platform.event.api.EventManager;
import eu.mico.platform.event.impl.EventManagerImpl;

import org.apache.commons.cli.*;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;

/**
 * NER Daemon
 *
 * @author Sergio Fernández
 * @author Rupert Westenthaler
 */
public class TextAnalysisDaemon implements AutoCloseable, Daemon {

    private final Logger log = LoggerFactory.getLogger(getClass());
    
    private String username;
    private String password;
    private String hostname;

    private EventManager eventManager;

    private TextAnalysisService nerFromText;
    private String queue;
    private String app;
    private String key;

    @Override
    public void close() throws Exception {
        System.out.println("ner daemon closed");
    }

    @Override
    public void init(DaemonContext context) throws DaemonInitException {
        try {
            CommandLineParser parser = new DefaultParser();
            log.info("arguments: {}", Arrays.toString(context.getArguments()));
            CommandLine cmd = parser.parse(createOptions(), context.getArguments());
            username = cmd.getOptionValue('u');
            password = cmd.getOptionValue('p');
            hostname = cmd.getOptionValue('h');
            queue = cmd.getOptionValue('q');
            if(queue == null){
                throw new DaemonInitException("Missing requesd option '-q' (queue)");
            }
            app = cmd.getOptionValue('a');
            if(app == null){
                throw new DaemonInitException("Missing requesd option '-a' (application name)");
            }
            key = cmd.getOptionValue('k');
            if(key == null){
                throw new DaemonInitException("Missing requesd option '-k' (application key)");
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }
        nerFromText = new TextAnalysisService(queue, app, key);

        try {
            eventManager = new EventManagerImpl(hostname, username, password);
            System.out.println("ner daemon initialized");
        } catch (IOException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }
    }

    @Override
    public void start() throws Exception {
        eventManager.init();
        eventManager.registerService(nerFromText);
        System.out.println("ner daemon started");
    }

    @Override
    public void stop() throws Exception {
        eventManager.unregisterService(nerFromText);
        eventManager.shutdown();
        System.out.println("ner daemon stopped");
        close();
    }

    @Override
    public void destroy() {
        nerFromText = null;
        eventManager = null;
        System.out.println("ner daemon destroyed");
    }

    private static Options createOptions() {
        Options options = new Options();

        options.addOption(
                Option.builder("u")
                        .argName("username")
                        .hasArg()
                        .longOpt("username")
                        .required()
                        .build()
        );

        options.addOption(
                Option.builder("p")
                        .argName("password")
                        .hasArg()
                        .longOpt("password")
                        .required()
                        .build()
        );

        options.addOption(
                Option.builder("h")
                        .argName("hostname")
                        .hasArg()
                        .longOpt("hostname")
                        .required()
                        .build()
        );

        options.addOption(
                Option.builder("q")
                        .argName("queue")
                        .desc("The queue name for this extractor configuration")
                        .hasArg()
                        .longOpt("queue")
                        .required()
                        .build()
        );
        options.addOption(
                Option.builder("a")
                        .argName("app")
                        .desc("The Redlink application name")
                        .hasArg()
                        .longOpt("app")
                        .required()
                        .build()
        );
        options.addOption(
                Option.builder("k")
                        .argName("key")
                        .desc("The Redlink application key")
                        .hasArg()
                        .longOpt("key")
                        .required()
                        .build()
        );
        return options;
    }

}
