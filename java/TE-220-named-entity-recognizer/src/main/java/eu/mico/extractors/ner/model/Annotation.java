package eu.mico.extractors.ner.model;

import java.util.NavigableSet;
import java.util.TreeSet;

public abstract class Annotation<T extends Mention> {
    
    private static final Double ZERO = Double.valueOf(0);
    private static final Double ONE = Double.valueOf(1);
    
    private NavigableSet<T> mentions = new TreeSet<>();
    private Double confidence;
    
    public Annotation() {
        this(null);
    }
    
    public Annotation(Double confidence){
        setConfidence(confidence);
    }
    /**
     * Setter for the confidence
     * @param confidence the confidence. If {@link Double#NaN} or <code>&lt; 0</code>
     * the confidence will be set to <code>null</code>. If <code>&gt; 0</code> the
     * confidence will be set to <code>1</code> (one).
     */
    public void setConfidence(Double confidence) {
        if(confidence == null || confidence < ZERO || Double.NaN == confidence.doubleValue()){
            this.confidence = null;
        } else if(confidence > ONE){ 
            this.confidence = ONE;
        } else {
            this.confidence = confidence;
        }
    }
    
    /**
     * Read/Writeable set with the mentions of this Annotation
     * @return
     */
    public NavigableSet<T> getMentions() {
        return mentions;
    }

    /**
     * The confidence [0..1] or <code>null</code> if not set
     * @return The confidence [0..1] or <code>null</code> if not set
     */
    public Double getConfidence() {
        return confidence;
    }
    
}
