package eu.mico.extractors.ner.model;

public class SentimentAnnotation extends TextAnnotation {

    /**
     * The sentiment
     */
    private double sentiment;

    /**
     * Creates a sentiment annotation with a given sentiment in the range 
     * <code>[-1..+1]</code>
     * @param sentiment the sentiment
     */
    public SentimentAnnotation(double sentiment){
        super();
        assert sentiment >= -1;
        assert sentiment <= 1;
        this.sentiment = sentiment;
    }
    
    public double getSentiment() {
        return sentiment;
    }
}
