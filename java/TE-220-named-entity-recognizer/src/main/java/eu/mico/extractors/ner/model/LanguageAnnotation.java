package eu.mico.extractors.ner.model;

import java.util.Locale;


public class LanguageAnnotation extends TextAnnotation {

    
    private final Locale language;

    public LanguageAnnotation(String language) {
        this(language,null);
    }
    
    public LanguageAnnotation(Locale language) {
        this(language,null);
    }
    
    public LanguageAnnotation(String language, Double confidence) {
        this(Locale.forLanguageTag(language), confidence);
    }
    
    public LanguageAnnotation(Locale locale, Double confidence) {
        super(confidence);
        assert locale != null;
        this.language = locale;
    }
    
    public String getLanguage() {
        return language.getLanguage();
    }
    
    public Locale getLocale(){
        return language;
    }
    
}
