# Redlink Analysis Extractor

This extractor allows to use the [Redlink API](http://dev.redlink.io/api)
for analysing textual (`plain/text`) content.

The current version is derived from the _Named-Enity Recognizer (TE-220)_

### Supported Features

* Named Entity Recognition
* Entity Linking

### Usage

In addition to the default host (`-h`), username (`-u`) and password (`-p`) of
the EventManager that need to be parsed to every MICO extractor this provides 
three additional options:

* _application name_ (`-a`): This is the name of the Redlink application
* _application key_ (`-k`): The key assigned by Redlink to the application
* _queue name_ (`-q`): The name if the in-queue for the extractor