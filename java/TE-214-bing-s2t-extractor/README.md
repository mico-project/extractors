![MICO](http://www.mico-project.eu/wp-content/uploads/2014/04/mico_logo.png)

[TOC]


# Copyright notice
   Copyright 2016 Adam Dahlgren <dali@cs.umu.se>
       2016 Umea University

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
      http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

# Bing Speech to text
This extractor uses Microsofts Bing services via the Azure Datamarket to perform speech to text and is partially based on sample code provided by Microsoft. In order to run this extractor a client ID and secret connected to a valid Bing Voice Recognition subscription is needed. 

# Running the extractor

The extractor usage follows the following command line interface:

`java BingS2TDaemon <hostname> <user> <password> <bing client secret> <bing client id> <locale>`


# Microsoft Azure Datamarket

The following steps shows how the client ID and secret are obtained. Before starting you need to sign up for the free subscription at https://datamarket.azure.com/dataset/bing/speechrecognition. This gives 5000 transcriptions (~10 seconds of audio) per month.

## 1. Account page
Accessing the account should give you this page.
![ACC](./account_page.png)

## 2. Developers page
Under this page you need to create a new 'application', which will be your extractor. In this case the application is called "Research S2T UMUFLP".
![DEVS](./developers.png)

## 3. Application page
The client ID and secret can be found when you choose to edit the application. Here it is also possible to change the client secret (i.e. if it was leaked).

![KEYS](./key.png)


# Language support
The following locales are supported:

| Locale| Locale| Locale| Locale|
| ----- | ----- | ----- | ----- |
| ar-EG | en-IN | fr-FR | pt-BR |
| ca-ES | en-NZ | it-IT | pt-PT |
| da-DK | en-US | ja-JP | ru-RU |
| de-DE | es-ES | ko-KR | sv-SE |
| en-AU | es-MX | nb-NO | zh-CN |
| en-CA | fi-FI | nl-NL | zh-HK |
| en-GB | fr-CA | pl-PL | zh-TW |

More documentation of the API can be found at https://www.microsoft.com/cognitive-services/en-us/speech-api/documentation/overview.
