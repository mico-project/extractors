package se.umu.cs.mico.s2t;

/**
 * Access token structure..
 */
public class OxfordAccessToken {
	public String access_token;
	public String token_type;
	public String expires_in;
	public String scope;
}
