package se.umu.cs.mico.s2t;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

/*
 * This is sample code provided by Microsoft.
 * This class demonstrates how to get a valid O-auth token from
 * Azure Data Market.
 */
public class OxfordAuthentication
{
	public static final String AccessTokenUri = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13";

	private String clientId;
	private String clientSecret;
	private String request;
	private OxfordAccessToken token;
	private Timer accessTokenRenewer;

	//Access token expires every 10 minutes. Renew it every 9 minutes only.
	private final int RefreshTokenDuration = 9 * 60 * 1000;
	private final String charsetName = "utf-8";
	private TimerTask nineMinitesTask = null;

	public OxfordAuthentication(String clientId, String clientSecret) throws Exception
	{
		this.clientId = clientId;
		this.clientSecret = clientSecret;

		/*
		 * If clientid or client secret has special characters, encode before sending request
		 */
		try{
			this.request = String.format("grant_type=client_credentials&client_id=%s&client_secret=%s&scope=%s",
					URLEncoder.encode(clientId,charsetName),
					URLEncoder.encode(clientSecret, charsetName),
					URLEncoder.encode("https://speech.platform.bing.com",charsetName));

		}catch (Exception e){
			e.printStackTrace();
		}
		this.token = HttpPost(AccessTokenUri, this.request);

		// renew the token every specified minutes
		accessTokenRenewer = new Timer();
		nineMinitesTask = new TimerTask(){
			public void run(){
				try {
					RenewAccessToken();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				} 
			}
		};

		accessTokenRenewer.schedule(nineMinitesTask, 0, RefreshTokenDuration);
	}

	public OxfordAccessToken GetAccessToken()
	{
		return this.token;
	}

	private void RenewAccessToken() throws Exception
	{
		OxfordAccessToken newAccessToken = HttpPost(AccessTokenUri, this.request);
		//swap the new token with old one
		//Note: the swap is thread unsafe
		System.out.println("new access token: " + newAccessToken.access_token);
		this.token = newAccessToken;
	}

	private OxfordAccessToken HttpPost(String AccessTokenUri, String requestDetails) throws Exception
        {
                InputStream inSt = null;
                HttpsURLConnection webRequest = null;

                //Prepare OAuth request
                webRequest = HttpsConnection.getHttpsConnection(AccessTokenUri);
                webRequest.setDoInput(true);
                webRequest.setDoOutput(true);
                webRequest.setConnectTimeout(5000);
                webRequest.setReadTimeout(5000);
                webRequest.setRequestProperty("content-type", "application/x-www-form-urlencoded");
                webRequest.setRequestMethod("POST");

                byte[] bytes = requestDetails.getBytes();
                webRequest.setRequestProperty("content-length", String.valueOf(bytes.length));
                webRequest.connect();

                DataOutputStream dop = new DataOutputStream(webRequest.getOutputStream());
                dop.write(bytes);
                dop.flush();
                dop.close();

                inSt = webRequest.getInputStream();
                InputStreamReader in = new InputStreamReader(inSt);
                BufferedReader bufferedReader = new BufferedReader(in);
                StringBuffer strBuffer = new StringBuffer();
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                        strBuffer.append(line);
                }

                bufferedReader.close();
                in.close();
                inSt.close();
                webRequest.disconnect();

                // parse the access token from the json format
                String result = strBuffer.toString();
                Gson gson = new Gson();
                Type type = new TypeToken<OxfordAccessToken>(){}.getType();
                OxfordAccessToken token = gson.fromJson(result, type);

                return token;
        }
}
