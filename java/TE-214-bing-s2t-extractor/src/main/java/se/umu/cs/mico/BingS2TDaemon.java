package se.umu.cs.mico.s2t;

import eu.mico.platform.event.api.EventManager;
import eu.mico.platform.event.impl.EventManagerImpl;

import org.apache.commons.cli.*;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.concurrent.TimeoutException;

/**
 * Microsoft Bing S2T Daemon
 *
 * @author Adam Dahlgren
 */
public class BingS2TDaemon implements AutoCloseable, Daemon {

    private String username;
    private String password;
    private String hostname;
    private String clientId;
    private String clientSecret;
    private String language = null;

    private EventManager eventManager;

    private BingS2T s2tEngine;
    private static final String daemonName =
            "mico-extractor-bing-s2t daemon";

    @Override
    public void close() throws Exception {
        System.out.println(daemonName+ " closed");
    }

    @Override
    public void init(DaemonContext context) throws DaemonInitException {
        try {
            CommandLineParser parser = new GnuParser();
            CommandLine cmd = parser.parse(createOptions(), context.getArguments());
            username = cmd.getOptionValue("u");
            password = cmd.getOptionValue("p");
            hostname = cmd.getOptionValue("h");
            clientSecret = cmd.getOptionValue("t");
            
            /* Will default to en-US if not provided */
            if (cmd.hasOption("l")) 
                    language = cmd.getOptionValue("l");

            /* Should be provided, not sure what happens otherwise */
            if (cmd.hasOption("i")) 
                    clientId = cmd.getOptionValue("i");
            else 
                    clientId = "research_s2t_mico";

        } catch (ParseException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }
	OxfordAuthentication auth;

        try {
                auth = new OxfordAuthentication(clientId, clientSecret);
        } catch (Exception e) {
                System.err.println("Error authenticating with Azure");
                throw new DaemonInitException(e.getMessage());
        }

        /* Default en-US */
        if (language == null) 
                s2tEngine = new BingS2T(auth);
        else 
                s2tEngine = new BingS2T(auth, language);

        try {
            eventManager = new EventManagerImpl(hostname, username, password);
            System.out.println(daemonName + " initialized");
        } catch (IOException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }
    }

    @Override
    public void start() throws Exception {
        eventManager.init();
        eventManager.registerService(s2tEngine);

        System.out.println(daemonName + " started");
    }

    @Override
    public void stop() throws Exception {
        eventManager.unregisterService(s2tEngine);
        eventManager.shutdown();

        System.out.println(daemonName + " stopped");
        close();
    }

    @Override
    public void destroy() {
        s2tEngine = null;
        eventManager = null;

        System.out.println(daemonName + " destroyed");
    }

    @SuppressWarnings("static-access")
    private static Options createOptions() {
        Options options = new Options();

        options.addOption(
                OptionBuilder
                        .withArgName("username")
                        .hasArg()
                        .withLongOpt("username")
                        .isRequired()
                        .create('u')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("password")
                        .hasArg()
                        .withLongOpt("password")
                        .isRequired()
                        .create('p')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("hostname")
                        .hasArg()
                        .withLongOpt("hostname")
                        .isRequired()
                        .create('h')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("token")
                        .hasArg()
                        .withLongOpt("token")
                        .isRequired()
                        .create('t')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("lang")
                        .hasArg()
                        .withLongOpt("lang")
                        .isRequired()
                        .create('l')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("id")
                        .hasArg()
                        .withLongOpt("id")
                        .isRequired()
                        .create('i')
        );

        return options;
    }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 Logger log = LoggerFactory
					.getLogger(BingS2TDaemon.class);
		try {
			if (args.length != 6) {
				System.err.println("Usage: java BingS2TDaemon " +
								   "<hostname> <user> <password> <bing client secret> <bing client id> <lang>");
				System.exit(1);
			}

			String mico_host = args[0];
			String mico_user = args[1];
			String mico_pass = args[2];
                        String bing_token = args[3];
                        String bing_client_id = args[4];
                        String lang = args[5];

                        /* Exceptions caused when authenticating should be thrown out here */
                        OxfordAuthentication auth = new OxfordAuthentication(bing_client_id, bing_token);
			try {
				EventManager eventManager = new EventManagerImpl(mico_host,
						mico_user, mico_pass);
				eventManager.init();
                                BingS2T s2t = new BingS2T(auth, lang);
				eventManager.registerService(s2t);

				// service (other approaches might be more sensible for a service,
				// e.g. commons-daemon)
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));

				char c = ' ';
				while (Character.toLowerCase(c) != 'q') {
					System.out.print("enter 'q' to quit: ");
					System.out.flush();

					c = in.readLine().charAt(0);
				}

				eventManager.unregisterService(s2t);

				eventManager.shutdown();

			} catch (IOException e) {
				log.error("error while accessing event manager:", e);
			} catch (TimeoutException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} 
		} catch (Exception e) {
			log.error("ERROR "+e.getMessage());
		}
                System.exit(0);
	}

}
