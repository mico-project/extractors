package se.umu.cs.mico.s2t;

import eu.mico.platform.anno4j.model.impl.bodymmm.SpeechToTextBodyMMM;
import eu.mico.platform.event.api.AnalysisResponse;
import eu.mico.platform.event.api.AnalysisService;
import eu.mico.platform.event.model.AnalysisException;
import eu.mico.platform.event.impl.AnalysisServiceUtil;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;
import eu.mico.platform.persistence.model.Resource;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.event.model.Event.ErrorCodes;

import org.openrdf.idGenerator.IDGenerator;
import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.Body;
import com.github.anno4j.model.impl.targets.SpecificResource;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;


import com.google.gson.*;
import javax.net.ssl.HttpsURLConnection;
import java.net.SocketTimeoutException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioFileFormat;

/**
 * TODO:
 *      - Document API keys
 *      - Add better fault tolerance
 *      - Save each word as rdf
 *      - Estimate time of occurance
 * @author Adam Dahlgren
 *
 */
public class BingS2T implements AnalysisService {

        private OxfordAuthentication oxfordAuth;
        private static final String SERVICE_URL = "https://speech.platform.bing.com/recognize/query";
        private String language;
        private static final String DEFAULT_LANG = "en-US";
	private static final List<String> langs = new ArrayList<>();
	static {
		langs.add("ar-EG");
		langs.add("ca-ES");
		langs.add("da-DK");
		langs.add("de-DE");
		langs.add("en-AU");
		langs.add("en-CA");
		langs.add("en-GB");
		langs.add("en-IN");
		langs.add("en-NZ");
		langs.add("en-US");
		langs.add("es-ES");
		langs.add("es-MX");
		langs.add("fi-FI");
		langs.add("fr-CA");
		langs.add("it-IT");
		langs.add("ja-JP");
		langs.add("ko-KR");
		langs.add("nb-NO");
		langs.add("nl-NL");
		langs.add("pl-PL");
		langs.add("pt-BR");
		langs.add("pt-PT");
		langs.add("ru-RU");
		langs.add("sv-SE");
		langs.add("zh-CN");
		langs.add("zh-HK");
		langs.add("zh-TW");
	}


        private static Logger log = LoggerFactory
                .getLogger(BingS2T.class);

        private static SimpleDateFormat isodate = new SimpleDateFormat(
                        "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'",
                        DateFormatSymbols.getInstance(Locale.US));

        static {
                isodate.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

	// Reused from NER extractor
	private String getRegistrationProperties(String propName) {
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("registration.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			log.error("Error reading registration properties",e);
		}
		return props.getProperty(propName);
	}

	@Override
	public String getExtractorVersion() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorVersion");
	}

	@Override
	public String getExtractorModeID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorModeId");
	}

	public String getExtractorID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorId");
	}

	@Override
        public String getProvides() {
                return "text/plain";
        }

	@Override
        public String getRequires() {
                return "audio/wav";
        }

        public BingS2T(OxfordAuthentication auth) {
                this(auth, DEFAULT_LANG);
        }

        public BingS2T(OxfordAuthentication auth, String lang) {
		if ( !langs.contains(lang)) {
			throw new IllegalArgumentException("["+lang+"] is an unsupported language, please consult documentation");
		}
                this.oxfordAuth = auth;
                this.language = lang;
        }

        @Override
        public void call(AnalysisResponse resp, Item item, List<Resource> resourceList, Map<String,String> params) throws AnalysisException ,IOException ,RepositoryException {
                if(resp == null){
                        throw new AnalysisException("The parsed response MUST NOT be NULL!");
                }
                if(item== null){
                        throw new AnalysisException("The parsed content item MUST NOT be NULL!");
                }

                /* Find audio resource */
                log.trace("> process Item {}",item.getURI());
                log.trace(" - resourceList (size:{}): {}", resourceList.size(), resourceList);
                Asset audioWavAsset = null;
                Resource audioWavResource = null;
                for(Resource resource : resourceList){
                        if(resource.hasAsset()){
                                log.debug(" - check Assert for Resource: {} (type: {})", resource.getURI(),resource.getClass().getSimpleName());
                                Asset asset = resource.getAsset();
                                String format = asset.getFormat().split(";")[0].trim();
                                log.debug("   - format: {}",format);
                                if(getRequires().equalsIgnoreCase(format)){ //we can process this asset
                                        log.debug(" - use Asset {} of {} {} with format {}",
                                                        asset, resource instanceof Item ? "item" : "part" , resource.getURI(), asset.getFormat());
                                        audioWavAsset = asset;
                                        audioWavResource = resource;
                                        break;
                                } else if(log.isDebugEnabled()){ 
                                        log.debug(" - ignore Asset {} of {} {} because its format {} is not supported (supported: {})",
                                                        asset, resource instanceof Item ? "item" : "part" , resource.getURI(), format, getRequires());
                                }
                        } else {
                                log.debug(" - Resource: {} (type: {}) has no asset", resource.getURI(),resource.getClass().getSimpleName());
                        }
                }
                if(audioWavAsset == null){
                        log.warn(" - got call for Item {} and resources {} where none had an asset with the supported format {}",
                                       item.getURI(), resourceList, getRequires());
                        throw new AnalysisException(ErrorCodes.MISSING_ANNOTATION,"No " + getRequires()+ " asset",null);
                }

                ObjectConnection con = item.getObjectConnection();

                /* Get input, check if available */
                InputStream is = audioWavAsset.getInputStream();
                if(is == null) {
                        throw new AnalysisException(ErrorCodes.UNEXPECTED_ERROR, 
                                        "Unable to open InputStream for Asset (Location: " + audioWavAsset.getLocation() 
                                        + "| Resource: "+ audioWavResource.getURI() + ")",null);
                }

		/* Buffered in order to provide support for mark/reset */
		InputStream bufferedIn = new BufferedInputStream(is);

                /* Found audio, get as stream */
                AudioInputStream it_stream;
                try {
                         it_stream = AudioSystem.getAudioInputStream(bufferedIn);
                } catch (UnsupportedAudioFileException e) {
                        log.info("Audio resource had a bad format");
                        return;
                }

                /* Get frame information */
                float framerate = it_stream.getFormat().getFrameRate();
                int framesize = it_stream.getFormat().getFrameSize();
                int frames_10sec = 10*((int)framerate);
                int bytes_10sec = frames_10sec*it_stream.getFormat().getFrameSize();

                byte [] it_buf = new byte[bytes_10sec];

                log.debug("Audio resource {} has format {}", audioWavResource.getURI(), it_stream.getFormat());
                log.debug("Audio resource {} has {} bytes available", audioWavResource.getURI(), it_stream.available());

                int nrOfReadBytes = 0;
                int byteOffset = 0;
                int nrOfReadFrames = 0;

                /* All requests need to use a unique ID for the session */
                UUID uuid = UUID.randomUUID();

                /* Result transcription for all successful responses */
                StringBuffer resultBuffer = new StringBuffer();

                /* Reads audio in 10 sec segments, limit by Bing */
                while(it_stream.available() > 0) {
                        log.info("Reading next 10 seconds of audio resource {}", audioWavResource.getURI());

                        /* Read next 10 sec, or what's left */
                        nrOfReadBytes = it_stream.read(it_buf);

                        byteOffset += nrOfReadBytes;

                        /* Create stream for 10 sec audio wav */
                        ByteArrayInputStream it_bytes = new ByteArrayInputStream(it_buf);
                        AudioInputStream it_stream_cut = new AudioInputStream(it_bytes, it_stream.getFormat(), (int)(nrOfReadBytes/framesize));
                        ByteArrayOutputStream it_file_cut = new ByteArrayOutputStream();
                        AudioSystem.write(it_stream_cut, AudioFileFormat.Type.WAVE, it_file_cut);

                        byte [] bytes = it_file_cut.toByteArray();


                        /* Create URI for request based on given language using same session id for all requests */
                        String sttServiceUri = buildServiceUri(language, uuid.toString());

                        /* Update token for auth */
                        OxfordAccessToken token = oxfordAuth.GetAccessToken();

                        log.info("Sending request to Bing");

                        /* Create POST request to Bing with audio segment */
                        HttpsURLConnection webRequest;
                        try {
                        	webRequest = HttpsConnection.getHttpsConnection(sttServiceUri);
                        } catch (Exception e) {
                                log.info("Error connecting to Bing {}", e.getMessage());
                                continue;
                        }
                        webRequest.setDoInput(true);
                        webRequest.setDoOutput(true);
                        webRequest.setConnectTimeout(5000);
                        webRequest.setReadTimeout(20000);
                        webRequest.setRequestMethod("POST");
                        webRequest.setRequestProperty("Content-Type", "audio/wav; samplerate="+((int)it_stream.getFormat().getSampleRate()));
                        webRequest.setRequestProperty("Authorization", "Bearer " + token.access_token);
                        webRequest.setRequestProperty("content-length", String.valueOf(bytes.length));
                        webRequest.connect();


                        /* Send audio segment */
                        DataOutputStream dop = new DataOutputStream(webRequest.getOutputStream());
                        dop.write(bytes);
                        dop.flush();
                        dop.close();

                        log.info("Reading response from Bing");

                        /* Get the response from Bing */
			InputStream responseStream;
			try {
				responseStream = webRequest.getInputStream();
			} catch (SocketTimeoutException e) {
				log.error("Socket timed out when requesting results from Bing at byte offset "+byteOffset);
				continue;
			}
			
                        final BufferedReader rd = new BufferedReader(new InputStreamReader(webRequest.getInputStream()));
                        final StringBuffer stringBuffer = new StringBuffer();
                        String line;
                        while ((line = rd.readLine()) != null) {
                                stringBuffer.append(line);
                        }
                        rd.close();
                        webRequest.disconnect();

                        /* Bing provides json, parse results  */
                        JsonElement jelement = new JsonParser().parse(stringBuffer.toString());
                        JsonObject  jobject = jelement.getAsJsonObject();
                        JsonObject  jheader = jobject.getAsJsonObject("header");
                        JsonPrimitive  jstatus = jheader.getAsJsonPrimitive("status");

                        if (jstatus.getAsString().contains("error")) {
                                log.info("Error occured calling Bing S2T for audio resource {}", audioWavResource.getURI());
                                log.debug("Json header from Bing: {}", jheader.toString());
                                continue;
                        }

                        JsonArray jarray = jobject.getAsJsonArray("results");
                        String result = "";
                        for (int k = 0; k < jarray.size(); k++) {
                                jobject = jarray.get(k).getAsJsonObject();
                                result += jobject.get("name").toString();
                        }
                        resultBuffer.append(result);
                }

                log.info("Creating new content part containing plain text for resource {}", audioWavResource.getURI());

                Part textResultPart = item.createPart(AnalysisServiceUtil.getServiceID(this));
                textResultPart.addInput(audioWavResource);
                textResultPart.setSyntacticalType(getProvides());
                textResultPart.setSemanticType("Speech transcription from audio part at "+audioWavResource.getURI());
                textResultPart.setBody(createObject(con, SpeechToTextBodyMMM.class));

                SpecificResource sr = createObject(con, SpecificResource.class);
                sr.setSource(audioWavResource.getRDFObject());
                textResultPart.addTarget(sr);

                OutputStream outputStream = textResultPart.getAsset().getOutputStream();
                if(outputStream == null){
                        throw new AnalysisException(ErrorCodes.UNEXPECTED_ERROR, 
                                        "Unable to create OutputStream for "+ getProvides() + 
                                        "Asset (Location: " + audioWavAsset.getLocation() + 
                                        " | Resource: "+ audioWavResource.getURI() + ")", null);
                }
                outputStream.write(resultBuffer.toString().getBytes());
                outputStream.close();

                // report newly available results to broker
                resp.sendNew(item, textResultPart.getURI());
                resp.sendFinish(item);
                log.info("Finished extraction");
}

        private String buildServiceUri(String lang, String requestId) {
                String uri = SERVICE_URL;
                uri += "?scenarios=smd";                                  // websearch is the other main option.
                uri += "&appid=D4D52672-91D7-4C74-8AD8-42B1D98141A5";     // You must use this ID.
                uri += "&locale="+lang;                                   // We support several other languages.  Refer to README file.
                uri += "&device.os=wp7";
                uri += "&version=3.0";
                uri += "&format=json";
                uri += "&instanceid=565D69FF-E928-4B7E-87DA-9A750B96D9E3";
                uri += "&requestid=" + requestId;
                return uri;
        }


        private <T> T createObject(ObjectConnection con, Class<T> type)
                throws RepositoryException {
                        return createObject(con, type, null);
                }

        private <T> T createObject(ObjectConnection con, Class<T> type,
                        org.openrdf.model.Resource resource) throws RepositoryException {
                return con.addDesignation(
                                con.getObjectFactory().createObject(
                                        resource == null ? IDGenerator.BLANK_RESOURCE
                                        : resource, type), type);
        }

}
