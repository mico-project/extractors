package se.umu.cs.mico.extractors.st;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;
import org.openrdf.idGenerator.IDGenerator;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.impl.targets.SpecificResource;

import eu.mico.platform.anno4j.model.impl.bodymmm.SpeechToTextBodyMMM;
import eu.mico.platform.event.api.AnalysisResponse;
import eu.mico.platform.event.api.AnalysisService;
import eu.mico.platform.event.impl.AnalysisServiceUtil;
import eu.mico.platform.event.model.AnalysisException;
import eu.mico.platform.event.model.Event.ErrorCodes;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;
import eu.mico.platform.persistence.model.Resource;
import se.umu.cs.mico.extractors.st.model.Speech2TextResults;
import se.umu.cs.mico.extractors.st.model.Speech2TextResults.Words.Word;

public class MicoExtractorKaldi2txt implements AnalysisService {


    private Logger log = LoggerFactory.getLogger(getClass());

    private JAXBContext speech2TextResultContext;

    
    public MicoExtractorKaldi2txt() {
        try {
            speech2TextResultContext = JAXBContext.newInstance(Speech2TextResults.class);
        } catch (JAXBException e) {
            throw new IllegalStateException("Unable to create JAXBContext for "+Speech2TextResults.class.getName(), e);
        }
    }
    
	private String getRegistrationProperties(String propName){
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("registration.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			log.error("Error reading registration properties", e);
		}
		return props.getProperty(propName);
	}
	
	@Override
	public String getExtractorID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorId");
	}

	@Override
	public String getExtractorModeID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorModeId");
	}

	@Override
	public String getExtractorVersion() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorVersion");
	}

    @Override
    public String getProvides() {
        return "text/plain";
    }

    @Override
    public String getRequires() {
        return "text/vnd.umu-kaldis+xml";
    }

    @Override
    public void call(AnalysisResponse response, Item ci, List<Resource> resourceList,
            Map<String, String> props) throws AnalysisException, IOException, RepositoryException {
        if(response == null){
            throw new AnalysisException("The parsed response MUST NOT be NULL!");
        }
        if(ci == null){
            throw new AnalysisException("The parsed content item MUST NOT be NULL!");
        }
        ObjectConnection con = ci.getObjectConnection();
        //search for the text asset
        log.trace("> process Item {}", ci.getURI());
        log.trace(" - resourceList (size:{}): {}", resourceList.size(), resourceList);
        Asset kaldisXmlAsset = null;
        Resource kaldisXmlResource = null;
        for(Resource resource : resourceList){
            if(resource.hasAsset()){
                log.debug(" - check Assert for Resource: {} (type: {})", resource.getURI(),resource.getClass().getSimpleName());
                Asset asset = resource.getAsset();
                String format = asset.getFormat().split(";")[0].trim();
                log.debug("   - format: {}",format);
                if(getRequires().equalsIgnoreCase(format)){ //we can process this asset
                    log.debug(" - use Asset {} of {} {} with format {}",
                            asset, resource instanceof Item ? "item" : "part" , resource.getURI(), asset.getFormat());
                    kaldisXmlAsset = asset;
                    kaldisXmlResource = resource;
                    break;
                } else if(log.isDebugEnabled()){ 
                    log.debug(" - ignore Asset {} of {} {} because its format {} is not supported (supported: {})",
                            asset, resource instanceof Item ? "item" : "part" , resource.getURI(), format, getRequires());
                }
            } else {
                log.debug(" - Resource: {} (type: {}) has no asset", resource.getURI(),resource.getClass().getSimpleName());
            }
        }
        if(kaldisXmlAsset == null){
            log.warn(" - got call for Item {} and resources {} where none had an asset with the supported format {}",
                    ci.getURI(), resourceList, getRequires());
            throw new AnalysisException(ErrorCodes.MISSING_ANNOTATION,"No " + getRequires()+ " asset",null);
        }

        // Get stream of contentPart
        InputStream is = kaldisXmlAsset.getInputStream();
        
        if(is == null) {
            throw new AnalysisException(ErrorCodes.UNEXPECTED_ERROR, 
                    "Unable to open InputStream for Asset (Location: " + kaldisXmlAsset.getLocation() 
                    + "| Resource: "+ kaldisXmlResource.getURI() + ")",null);
        }
        Speech2TextResults xmlData;
        try {
            Unmarshaller unmarshaller = speech2TextResultContext.createUnmarshaller();
            xmlData = (Speech2TextResults) unmarshaller.unmarshal(is);
        } catch(JAXBException e){
            throw new AnalysisException(ErrorCodes.UNEXPECTED_ERROR, 
                    "Unable to parse XML content of " + getRequires() + "Asset (Location: " 
                    + kaldisXmlAsset.getLocation() + " | Resource: "+ kaldisXmlResource.getURI() + ")", e);
        }
        
        List<Word> words = xmlData.getWords().getWord();
        log.debug("Discovered {} words.", words.size());
        
        //if necessary, instantiate output stream and start processing the xml 
        if(words != null && !words.isEmpty()) {
            Part txtPart = ci.createPart(AnalysisServiceUtil.getServiceID(this));
            txtPart.addInput(kaldisXmlResource);
            txtPart.setSyntacticalType("mico:Text");
            txtPart.setSemanticType("Plain text representation of the xml asset of" + ci.getPart(kaldisXmlResource.getURI()).getURI());
            txtPart.setBody(createObject(con, SpeechToTextBodyMMM.class));
            
            SpecificResource sr = createObject(con, SpecificResource.class);
			sr.setSource(kaldisXmlResource.getRDFObject());
			txtPart.addTarget(sr);
            
            Asset asset = txtPart.getAsset();
			OutputStream outputStream = asset.getOutputStream();
			if(outputStream == null){
			    throw new AnalysisException(ErrorCodes.UNEXPECTED_ERROR, 
			            "Unable to create OutputStream for "+ getProvides() + 
			            "Asset (Location: " + kaldisXmlAsset.getLocation() + 
			            " | Resource: "+ kaldisXmlResource.getURI() + ")", null);
			}
			PrintStream printStream = new PrintStream(outputStream);
			try {
    			log.debug("Parsing {} words.", words.size());
                boolean isNotFirstWord = false;
                for (Word word : words) {
    
                  //check if it's the beginning of the paragraph, and add new lines accordingly
                    if (isNotFirstWord && (word.getTimestamp().intValue() == 0)) {
                        if (word.getValue().equals("<eps>")) {
                            printStream.println();
                        } else {
                            printStream.println(System.lineSeparator());
                        }
                    }
    
                    // check if there's a pause, and add a new line or the word
                    // itself, accordingly
                    if (word.getValue().equals("<eps>")) {
                        System.out.println();
                        printStream.println();
                    } else {
                        System.out.print(word.getValue() + " ");
                        printStream.print(word.getValue() + " ");
                    }
    
                    isNotFirstWord = true;
                }
                asset.setFormat("text/plain");
                log.debug("Parsing complete.");
                response.sendNew(ci, txtPart.getURI());
			} finally {
			    IOUtils.closeQuietly(printStream);
			}
        } else {
            // tell the broker that there was nothing to be processed
        	log.info("An empty list of words was received for Item {}. No "
        	        + "{} asset was created", ci.getURI(), getProvides());
        }
    }
    
    private <T> T createObject(ObjectConnection con, Class<T> type)
            throws RepositoryException {
        return createObject(con, type, null);
    }

    private <T> T createObject(ObjectConnection con, Class<T> type,
            org.openrdf.model.Resource resource) throws RepositoryException {
        return con.addDesignation(
                con.getObjectFactory().createObject(
                        resource == null ? IDGenerator.BLANK_RESOURCE
                                : resource, type), type);
    }

}
