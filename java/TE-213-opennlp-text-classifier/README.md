# OpenNLP Document Classifier Extractor

This extractor uses the [OpenNLP Document Classifier API](https://opennlp.apache.org/documentation/1.5.2-incubating/manual/opennlp.html#tools.doccat.classifying.api) to categorize documents. This can be used for any categorization task over texts included sentiment analysis.
