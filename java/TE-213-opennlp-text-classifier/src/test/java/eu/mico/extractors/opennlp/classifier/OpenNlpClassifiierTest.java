package eu.mico.extractors.opennlp.classifier;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.daemon.DaemonInitException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.marmotta.ldpath.parser.ParseException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.RDFObject;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.RDFHandlerBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.Annotation;
import com.github.anno4j.model.Body;
import com.github.anno4j.model.Selector;
import com.github.anno4j.model.namespaces.OADM;
import com.github.anno4j.querying.QueryService;

import eu.mico.platform.anno4j.model.ItemMMM;
import eu.mico.platform.anno4j.model.PartMMM;
import eu.mico.platform.anno4j.model.fam.TopicBody;
import eu.mico.platform.anno4j.model.fam.TopicClassificationBody;
import eu.mico.platform.anno4j.model.namespaces.FAM;
import eu.mico.platform.anno4j.model.namespaces.MMM;
import eu.mico.platform.event.test.mock.EventManagerMock;
import eu.mico.platform.persistence.api.PersistenceService;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;

/**
 * Simple tests for TE-220
 *
 * @author Sergio Fernández
 */
public class OpenNlpClassifiierTest {
    
    private static final Logger log = LoggerFactory.getLogger(OpenNlpClassifiierTest.class);

    private static final String THESAURUS = "compclass.ttl";
    private static final String MODEL = "test-model.zip";
    private static final String TEST_CONTENT = "tests.txt";

    private static final ClassLoader cl = OpenNlpClassifiierTest.class.getClassLoader();
    
 
    private static final Charset UTF8 = Charset.forName("UTF-8");

    private static Model thesaurus;
    private static List<String> testTexts;
   
    private EventManagerMock eventManager;

    
    
    @BeforeClass
    public static void initClass() throws IOException, DaemonInitException {
        InputStream thesaurusIn = cl.getResourceAsStream(THESAURUS);
        Assert.assertNotNull(thesaurusIn);
        thesaurus = TextClassifierDaemon.loadThesaurus(thesaurusIn, THESAURUS);
        IOUtils.closeQuietly(thesaurusIn);
        InputStream testContentIn = cl.getResourceAsStream(TEST_CONTENT);
        Assert.assertNotNull(testContentIn);
        testTexts = IOUtils.readLines(testContentIn,UTF8);
        Assert.assertFalse(testTexts.isEmpty());
    }
    
    @Before
    public void setup() throws IOException, URISyntaxException {
        InputStream modelIn = cl.getResourceAsStream(MODEL);
        Assert.assertNotNull(modelIn);
        eventManager = new EventManagerMock();
        eventManager.init();
        eventManager.registerService(new OpenNlpTextClassifierService("dummy", modelIn, thesaurus));
    }

    @After
    public void shutdown() throws IOException {
        eventManager.shutdown();
    }

    @Test
    public void classificationTest() throws RepositoryException, IOException, MalformedQueryException, QueryEvaluationException, ParseException, RDFHandlerException {
        final PersistenceService persistenceService = eventManager.getPersistenceService();
        final Item contentItem = persistenceService.createItem();
        log.debug("> Use Item {} for test", contentItem.getURI());

        Asset asset = contentItem.getAsset(); //NOTE: getAsset creates a new one if none exists already
        contentItem.setSyntacticalType("text/plain");
        asset.setFormat("text/plain");
        log.debug("- created Assert {} (location: {} | format: {})", asset, asset.getLocation(), asset.getFormat());
        String content = testTexts.get(0);
        log.debug(" - set asset content to: {}", StringUtils.abbreviate(content, 40));
        final PrintStream ps = new PrintStream(asset.getOutputStream());
        ps.print(content);
        ps.close();

        eventManager.injectItem(contentItem);
        
        debugRDF(contentItem);

        //those where the Parts added by the extractor
        final Set<URI> expectedItems = new HashSet<>(eventManager.getResponsesCollector().getNewItemResponses());
        Assert.assertFalse(expectedItems.isEmpty()); //we expect some new parts

        QueryService query = persistenceService.createQuery(contentItem.getURI());
        query.addPrefix("mmm", MMM.NS);
        query.addPrefix("oa", OADM.NS);
        query.addPrefix("fam", FAM.NS);
        query.addCriteria("mmm:hasBody[rdf:type is fam:TopicClassification]");
        List<? extends Annotation> topicClassificationAnnos = query.execute(PartMMM.class);
        Assert.assertNotNull(topicClassificationAnnos);
        Assert.assertEquals(1, topicClassificationAnnos.size());
        Annotation topicClassificationAnno = topicClassificationAnnos.get(0);
        //assrt the classification as notified as new part
        Assert.assertTrue(expectedItems.remove(topicClassificationAnno.getResource()));
        
        Body body = topicClassificationAnnos.get(0).getBody();
        Assert.assertTrue(body instanceof TopicClassificationBody);
        TopicClassificationBody topicClassification = (TopicClassificationBody)body;
        Assert.assertNotNull(topicClassification.getClassificationScheme());
        
        Assert.assertNotNull(topicClassification.getContent());
        Assert.assertEquals(contentItem.getRDFObject(), topicClassification.getContent());
        
        Collection<TopicBody> topics = topicClassification.getTopics();
        Assert.assertNotNull(topics);
        Assert.assertFalse(topics.isEmpty());
        
        for(TopicBody topic : topics){
            assertTopic(topic, contentItem.getRDFObject(), null);
        }
        
        //finally query for the Parts created for the topics and check if they
        //are linked to the topics referenced by the classification
        query = persistenceService.createQuery(contentItem.getURI());
        query.addPrefix("mmm", MMM.NS);
        query.addPrefix("oa", OADM.NS);
        query.addPrefix("fam", FAM.NS);
        query.addCriteria("mmm:hasBody[rdf:type is fam:TopicAnnotation]");
        List<? extends Annotation> topicAnnos = query.execute(PartMMM.class);
        Assert.assertNotNull(topicAnnos);

        for(Annotation topicAnno : topicAnnos){
            Assert.assertTrue(topics.contains(topicAnno.getBody())); //this part as a topic as body
            Assert.assertTrue(expectedItems.remove(topicAnno.getResource())); //this part was notified
        }
        Assert.assertTrue(expectedItems.isEmpty()); //all expected parts found

    }

    private void debugRDF(Item item) throws RepositoryException, RDFHandlerException {
        if(!log.isDebugEnabled()){
            return;
        }
        //we copy all statements to a TreeModel as this one sorts them by SPO
        //what results in a much nicer TURTLE serialization
        final Model model = new TreeModel();
        //we also set commonly used namespaces
        model.setNamespace(OADM.PREFIX, OADM.NS);
        model.setNamespace(FAM.PREFIX, FAM.NS);
        model.setNamespace(RDF.PREFIX, RDF.NAMESPACE);
        model.setNamespace(RDFS.PREFIX, RDF.NAMESPACE);
        model.setNamespace("xsd", "http://www.w3.org/2001/XMLSchema#");
        model.setNamespace(MMM.PREFIX, MMM.NS);
        model.setNamespace("test", "http://localhost/mem/");
        model.setNamespace("services", "http://www.mico-project.eu/services/");
        RepositoryConnection con = item.getObjectConnection();
        try {
            con.begin();
            con.exportStatements(null, null, null, true, new RDFHandlerBase(){
                @Override
                public void handleStatement(Statement st) {
                    log.debug("{},{},{},{}",st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());
                    model.add(st);
                }
            });
        } finally {
            con.rollback();
            con.close();
        }
        log.debug("--- START generated RDF ---");
        PrintWriter out = new PrintWriter(System.out);
        Rio.write(model, out, RDFFormat.TURTLE);
        out.close();
        log.debug("--- END generated RDF ---");
    }

    private void assertTopic(TopicBody topic, RDFObject itemOrPart, Iterable<Selector> selectors) {
        
        Assert.assertNotNull(topic.getTopicLabels());
        Assert.assertFalse(topic.getTopicLabels().isEmpty());
        Assert.assertNotNull(topic.getTopic());
        Assert.assertNotNull(topic.getConfidence());
        Assert.assertTrue(topic.getConfidence() >= 0d && topic.getConfidence() <= 1d);
        
        Assert.assertNotNull(topic.getContent());
        Assert.assertEquals(itemOrPart, topic.getContent());
        if(selectors != null){
            Assert.assertNotNull(topic.getSelectors());
            for(Selector s : selectors){
                topic.getSelectors().contains(s);
            }
        }
        
    }

    private void assertResponses(final Map<URI, String> responses) {
        log.info(" - {} response(s): ", responses.size());
        for(Entry<URI, String> entry : responses.entrySet()){
            log.info("   {}: {}",entry.getKey(), entry.getValue());
        }
    }

}
