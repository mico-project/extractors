package eu.mico.extractors.opennlp.classifier;

import eu.mico.platform.event.api.EventManager;
import eu.mico.platform.event.impl.EventManagerImpl;
import info.aduna.io.IOUtil;

import org.apache.commons.cli.*;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.mortbay.log.Log;
import org.openrdf.model.Model;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.Rio;
import org.openrdf.rio.UnsupportedRDFormatException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * OpenNLP classifier daemon
 *
 * @author Rupert Westenthaler
 */
public class TextClassifierDaemon implements AutoCloseable, Daemon {

    private String username;
    private String password;
    private String hostname;

    private EventManager eventManager;

    private OpenNlpTextClassifierService classifierService;
    private String queue;
    private String model;
    private String thesaurus;

    @Override
    public void close() throws Exception {
        System.out.println("OpenNLP classifier for model " + model + "closed");
    }

    @Override
    public void init(DaemonContext context) throws DaemonInitException {
        File modelFile;
        Model thesaurusModel = null;
        try {
            CommandLineParser parser = new GnuParser();
            Log.info("arguments: {}", context.getArguments());
            CommandLine cmd = parser.parse(createOptions(), context.getArguments());
            username = cmd.getOptionValue('u');
            password = cmd.getOptionValue('p');
            hostname = cmd.getOptionValue('h');
            queue = cmd.getOptionValue('q');
            if(queue == null){
                throw new DaemonInitException("Missing requesd option '-q' (queue)");
            }
            model = cmd.getOptionValue('m');
            if(model == null){
                throw new DaemonInitException("Missing requesd option '-m' (OpenNLP classification model)");
            }
            modelFile = new File(model);
            if(!modelFile.isFile()){
                throw new DaemonInitException("Parsed model file " + modelFile.getAbsolutePath() + "('-m' option) does not exist!"); 
            }
            thesaurus = cmd.getOptionValue('t', null);
            if(thesaurus != null){
                File thesaurusFile = new File(thesaurus);
                if(!thesaurusFile.isFile()){
                    throw new DaemonInitException("Parsed thesaurus file " + thesaurusFile.getAbsolutePath() + "('-t' option) does not exist!"); 
                }
                InputStream in = null;
                try {
                    in = new FileInputStream(thesaurusFile);
                    thesaurusModel = loadThesaurus(in, thesaurus);
                } catch (FileNotFoundException e) {
                    throw new DaemonInitException("Parsed thesaurus file " + thesaurusFile.getAbsolutePath() + "('-t' option) not found!", e); 
                } finally {
                    IOUtils.closeQuietly(in);
                }
            }
            
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DaemonInitException(e.getMessage());
        }
        try {
            classifierService = new OpenNlpTextClassifierService(queue, modelFile, thesaurusModel);
        } catch (IOException e) {
            throw new DaemonInitException("Unable to initialise service for model file " 
                    + modelFile.getAbsolutePath(), e); 
        }

        try {
            eventManager = new EventManagerImpl(hostname, username, password);
            System.out.println("OpenNLP text classifier daemon for model '" +modelFile.getAbsolutePath() + "' initialized");
        } catch (IOException e) {
            throw new DaemonInitException("Unable to initialize event manager", e);
        }
    }

    public static Model loadThesaurus(InputStream in, String resourcePath)
            throws DaemonInitException {
        try {
            return Rio.parse(in, "http://www.dummy.org/dummy#", 
                    Rio.getParserFormatForFileName(FilenameUtils.getName(resourcePath)));
        } catch(IOException e){
            throw new DaemonInitException("Unable to read thesaurus file " + resourcePath +"!",e); 
        } catch (RDFParseException | UnsupportedRDFormatException e) {
            throw new DaemonInitException("Unable to parse thesaurus file " + resourcePath +"!",e); 
        }
    }

    @Override
    public void start() throws Exception {
        eventManager.init();
        eventManager.registerService(classifierService);
        System.out.println("OpenNLP Text Classification Daemon started");
    }

    @Override
    public void stop() throws Exception {
        eventManager.unregisterService(classifierService);
        eventManager.shutdown();
        System.out.println("OpenNLP Text Classification Daemon stopped");
        close();
    }

    @Override
    public void destroy() {
        classifierService = null;
        eventManager = null;
        System.out.println("OpenNLP Text Classification Daemon destroyed");
    }

    @SuppressWarnings("static-access")
    private static Options createOptions() {
        Options options = new Options();
         
        options.addOption(
                OptionBuilder
                        .withArgName("username")
                        .hasArg()
                        .withLongOpt("username")
                        .isRequired()
                        .create('u')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("password")
                        .hasArg()
                        .withLongOpt("password")
                        .isRequired()
                        .create('p')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("hostname")
                        .hasArg()
                        .withLongOpt("hostname")
                        .isRequired()
                        .create('h')
        );

        options.addOption(
                OptionBuilder
                        .withArgName("queue")
                        .withDescription("The queue name for this extractor configuration")
                        .hasArg()
                        .withLongOpt("queue")
                        .isRequired()
                        .create('q')
        );
        options.addOption(
                OptionBuilder
                        .withArgName("model")
                        .withDescription("Path to the OpenNLP Doccat Model")
                        .hasArg()
                        .withLongOpt("model")
                        .isRequired()
                        .create('m')
        );
        options.addOption(
                OptionBuilder
                        .withArgName("Thesaurus")
                        .withDescription("Path to the SKOS thesaurus for the Categories of the model")
                        .hasArg()
                        .withLongOpt("thesaurus")
                        .isRequired(false)
                        .create('t')
        );
        return options;
    }

}
