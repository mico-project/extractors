package eu.mico.extractors.opennlp.classifier;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.openrdf.idGenerator.IDGenerator;
import org.openrdf.model.Literal;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.vocabulary.SKOS;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.LangString;
import org.openrdf.repository.object.ObjectConnection;
import org.openrdf.repository.object.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.anno4j.model.impl.targets.SpecificResource;

import eu.mico.platform.anno4j.model.fam.TopicBody;
import eu.mico.platform.anno4j.model.fam.TopicClassificationBody;
import static eu.mico.platform.anno4j.model.namespaces.FAM.TOPIC_ANNOTATION;
import static eu.mico.platform.anno4j.model.namespaces.FAM.TOPIC_CLASSIFICATION_ANNOTATION;
import eu.mico.platform.event.api.AnalysisResponse;
import eu.mico.platform.event.api.AnalysisService;
import eu.mico.platform.event.impl.AnalysisServiceUtil;
import eu.mico.platform.event.model.AnalysisException;
import eu.mico.platform.event.model.Event.ErrorCodes;
import eu.mico.platform.persistence.model.Asset;
import eu.mico.platform.persistence.model.Item;
import eu.mico.platform.persistence.model.Part;
import eu.mico.platform.persistence.model.Resource;
import opennlp.tools.doccat.DoccatModel;
import opennlp.tools.doccat.DocumentCategorizerME;

/**
 * OpenNLP Text Classifier Service
 *
 * @author Rupert Westenthaler
 */
public class OpenNlpTextClassifierService implements AnalysisService {

    private static Logger log = LoggerFactory.getLogger(OpenNlpTextClassifierService.class);

    private static final Charset UTF8 = Charset.forName("UTF-8");

    private DoccatModel model;
    
    /**
     * DocumentCategorizerME are not thread save so create one instance per thread
     */
    private ThreadLocal<DocumentCategorizerME> categorizer;
    
    private List<CategoryInfo> categories;
    


    /**
     * Compares to classification results (highest {@link Pair#getRight()} first)
     */
    private static final Comparator<Pair<CategoryInfo,Double>> CLASSIFICATION_RESULT_COMPARATOR = 
            new Comparator<Pair<CategoryInfo,Double>>() {

                @Override
                public int compare(Pair<CategoryInfo, Double> o1, Pair<CategoryInfo, Double> o2) {
                    return Double.compare(o2.getRight().doubleValue(), o1.getRight().doubleValue());
                }
            };

    private static final double MIN_SUM_CONF = 0.9;

    public OpenNlpTextClassifierService(String queueName, File modelFile, Model thesaurus) throws IOException {
        this(queueName, modelFile == null ? null : new FileInputStream(modelFile), thesaurus);
    }
    public OpenNlpTextClassifierService(String queueName, InputStream modelStream, Model thesaurus) throws IOException {
        assert queueName != null && !queueName.isEmpty();
        model = new DoccatModel(modelStream);
        IOUtils.closeQuietly(modelStream);
        categorizer = new ThreadLocal<DocumentCategorizerME>(){
            @Override
            protected DocumentCategorizerME initialValue() {
                return new DocumentCategorizerME(model);
            }
        };
        Map<String,ConceptInfo> concepts = new HashMap<>();
        if(thesaurus != null){
            for(Iterator<Statement> it = thesaurus.filter(null, SKOS.NOTATION, null).iterator(); it.hasNext();){
                Statement s = it.next();
                if(s.getObject() instanceof Literal && s.getSubject() instanceof URI){
                    String notation = ((Literal)s.getObject()).stringValue();
                    if(concepts.containsKey(notation)){
                        log.warn("Two concepts with the same notation '{}' (use: {} | ignore: {})!", 
                                notation, concepts.get(notation), s.getSubject());
                    } else {
                        concepts.put(notation, new ConceptInfo(thesaurus,(URI)s.getSubject()));
                    }
                }
            }
        }
        DocumentCategorizerME cat = new DocumentCategorizerME(model);
        int numCats = cat.getNumberOfCategories();
        categories = new ArrayList<>(numCats);
        for(int i=0; i<numCats; i++){
            String catName = cat.getCategory(i);
            categories.add(new CategoryInfo(catName, concepts.get(catName)));
        }
    }

	private String getRegistrationProperties(String propName){
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("registration.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			log.error("Error reading registration properties", e);
		}
		return props.getProperty(propName);
	}
	
	@Override
	public String getExtractorID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorId");
	}

	@Override
	public String getExtractorModeID() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorModeId");
	}

	@Override
	public String getExtractorVersion() {
		// TODO Auto-generated method stub
		return getRegistrationProperties("extractorVersion");
	}

    @Override
    public String getProvides() {
        return "application/x-mico-rdf";
    }

    @Override
    public String getRequires() {
        return "text/plain";
    }


    @Override
    public void call(AnalysisResponse response, Item ci,  List<Resource> resourceList,  Map<String, String> params) throws AnalysisException, RepositoryException, IOException {
        if(response == null){
            throw new AnalysisException("The parsed response MUST NOT be NULL!");
        }
        if(ci == null){
            throw new AnalysisException("The parsed content item MUST NOT be NULL!");
        }
        //search for the text asset
        log.trace("> process Item {}", ci.getURI());
        log.trace(" - resourceList (size:{}): {}", resourceList.size(), resourceList);
        Asset txtAsset = null;
        Resource txtResource = null;
        for(Resource resource : resourceList){
            if(resource.hasAsset()){
                log.debug(" - check Assert for Resource: {} (type: {})", resource.getURI(),resource.getClass().getSimpleName());
                Asset asset = resource.getAsset();
                String format = asset.getFormat().split(";")[0].trim();
                log.debug("   - format: {}",format);
                if(getRequires().equalsIgnoreCase(format)){ //we can process this asset
                    log.debug(" - use Asset {} of {} {} with format {}",
                            asset, resource instanceof Item ? "item" : "part" , resource.getURI(), asset.getFormat());
                    txtAsset = asset;
                    txtResource = resource;
                    break;
                } else if(log.isDebugEnabled()){ 
                    log.debug(" - ignore Asset {} of {} {} because its format {} is not supported (supported: {})",
                            asset, resource instanceof Item ? "item" : "part" , resource.getURI(), format, getRequires());
                }
            } else {
                log.debug(" - Resource: {} (type: {}) has no asset", resource.getURI(),resource.getClass().getSimpleName());
            }
        }
        if(txtAsset == null){
            log.warn(" - got call for Item {} and resources {} where none had an asset with the supported format {}",
                    ci.getURI(), resourceList, getRequires());
            throw new AnalysisException(ErrorCodes.MISSING_ANNOTATION, "No text/plain asset", null);
        }
        String txtContent = IOUtils.toString(txtAsset.getInputStream(), UTF8);
        if(StringUtils.isBlank(txtContent)){
            throw new AnalysisException(ErrorCodes.MISSING_ANNOTATION, "The text/plain asset contains an empty text",null);
        }

        log.info("> processing item {}, resource {}, asset {}", ci.getURI(), txtResource.getURI(), txtAsset);
        if(log.isDebugEnabled()){
            log.debug("  - content: {}",StringUtils.abbreviate(txtContent, 50));
        }
        ObjectConnection con = ci.getObjectConnection();
        ObjectFactory factory = con.getObjectFactory();
        //here goes the code that processes the text
        DocumentCategorizerME categorizer = this.categorizer.get();
        List<Pair<CategoryInfo, Double>> results = new ArrayList<>(categorizer.getNumberOfCategories());
        double[] outcomes = categorizer.categorize(txtContent);
        for(int i = 0; i < outcomes.length ; i++){
            log.debug("idx: {} - {}@{}", i , categorizer.getCategory(i), outcomes[i]);
            results.add(new ImmutablePair<CategoryInfo, Double>(categories.get(i), Double.valueOf(outcomes[i])));
        }
        Collections.sort(results,CLASSIFICATION_RESULT_COMPARATOR);
        double confSum = 0;
        double maxConf = 0;
        double lastConf = 0;
        int i = 0;
        Part classificationPart = ci.createPart(AnalysisServiceUtil.getServiceID(this));
        TopicClassificationBody classification = con.addDesignation(
                factory.createObject(IDGenerator.BLANK_RESOURCE,TopicClassificationBody.class),
                TopicClassificationBody.class);
        classificationPart.setBody(classification);
        
        classificationPart.setSyntacticalType(TOPIC_CLASSIFICATION_ANNOTATION);
        classificationPart.setSemanticType("Describes the results of topic classification applied to the input text");
        classificationPart.addInput(txtResource);
        
        SpecificResource classificationSpecificRes = con.addDesignation(
                factory.createObject(IDGenerator.BLANK_RESOURCE,SpecificResource.class),
                SpecificResource.class);
        classificationSpecificRes.setSource(txtResource.getRDFObject());
        classification.setContent(txtResource.getRDFObject());
        //classification.setClassificationScheme(schemeResource); //TODO add SJOS support
        //no selector as we want to select the content as a whole
        while (i < results.size() && confSum < MIN_SUM_CONF && lastConf >= maxConf/5.0d){
            Pair<CategoryInfo,Double> result = results.get(i);
            CategoryInfo cat = result.getLeft();
            Double conf = result.getRight();
            if(i == 0){
                maxConf = conf.doubleValue();
            }
            lastConf = conf.doubleValue();
            confSum = confSum + lastConf;
            Part topicPart = ci.createPart(AnalysisServiceUtil.getServiceID(this));
            
            topicPart.setSyntacticalType(TOPIC_ANNOTATION);
            topicPart.setSemanticType("Describes a single topic associated to the input text");
            topicPart.addInput(txtResource);
            
            topicPart.addTarget(classificationSpecificRes);
            TopicBody topic = con.addDesignation(
                    factory.createObject(IDGenerator.BLANK_RESOURCE, TopicBody.class),
                    TopicBody.class);
            topicPart.setBody(topic);
            classification.addTopic(topic);
            topic.setContent(txtResource.getRDFObject());
            topic.setConfidence(conf);
            if(cat.concept != null){
                topic.setTopic(cat.concept.uri);
                if(cat.concept.conceptScheme != null){
                    classification.setClassificationScheme(cat.concept.conceptScheme);
                }
                topic.setTopicLabels(cat.concept.labels);
            } else {
                topic.addTopicLabel(new LangString(cat.name, (String)null));
            }
            //Notify this topic
            response.sendNew(ci, topicPart.getURI());
            i++;
        }
        //Notify the whole classification
        response.sendNew(ci, classificationPart.getURI());
        //response.sendFinish(ci); //we are done
    }

    /**
     * Internal helper to send an error response in case of an exception
     * @param response
     * @param ci
     * @param code
     * @param e
     * @throws IOException
     */
    private void sendError(AnalysisResponse response, Item ci, ErrorCodes code, Throwable e) throws IOException {
        log.warn("Error while processing Item " + ci.getURI() + " sending Error Code " + code, e);
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        response.sendError(ci, code, e.getClass().getSimpleName() + "with message " + e.getMessage(), 
                sw.toString());
    }
    
    private class ConceptInfo {
        final URI uri;
        final URI conceptScheme;
        final Set<LangString> labels;
        
        public ConceptInfo(Model thesaurus, URI concept) {
            this.uri = concept;
            Iterator<Statement> it = thesaurus.filter(concept, SKOS.IN_SCHEME, null).iterator();
            if(it.hasNext()){
                Value inSchemeValue = it.next().getObject();
                conceptScheme = inSchemeValue instanceof URI ? (URI)inSchemeValue : null;
            } else {
                conceptScheme = null;
            }
            Set<LangString> labels = new HashSet<>();
            it = thesaurus.filter(concept, SKOS.PREF_LABEL, null).iterator();
            while(it.hasNext()){
                Value prefLabel = it.next().getObject();
                if(prefLabel instanceof Literal){
                    labels.add(new LangString(((Literal)prefLabel).getLabel(),((Literal)prefLabel).getLanguage()));
                }
            }
            this.labels = Collections.unmodifiableSet(labels);
        }
        
    }

    private class CategoryInfo {
        
        final String name;
        final ConceptInfo concept;
        
        CategoryInfo(String name, ConceptInfo concept){
            this.name = name;
            this.concept = concept;
        }
    }
}
