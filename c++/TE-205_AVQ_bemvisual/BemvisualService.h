#ifndef _BEMVISUALSERVICE_H_
#define _BEMVISUALSERVICE_H_ 1


#include "IBEMVisualErrorDetectionInterface.h"
#include "IMediaQuality.h"

#include "EventManager.hpp"

// for constant RDF property definitions of common vocabularies
#include "vocabularies.hpp"


// this namespace contains EventManager and AnalysisService
using namespace mico::event;

// this namespace contains Content, ContentItem, etc
using namespace mico::persistence::model;

class BemvisualService : public AnalysisService {

public:

  BemvisualService();

  ~BemvisualService();

  void call(mico::event::AnalysisResponse& resp,
            std::shared_ptr< mico::persistence::model::Item > item,
            std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources,
            std::map<std::string,std::string>& params);

private:
  avanalysis::EAVStatus m_status;
  bemvisual::IBEMVisualErrorDetectionInterface* m_beminterface;
  avanalysis::IMediaQuality* m_pMediaQuality;

};

#endif
