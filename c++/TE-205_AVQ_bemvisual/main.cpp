#include "BemvisualService.h"
#include <Daemon.hpp>

/****** globals ******/
#ifndef EXTRACTOR_BUILD_AS_DAEMON
EventManager* mgr = 0;
BemvisualService* bemvisualService = 0;
bool loop = true;

void signal_handler(int signum) {
  std::cout << "shutting down bemvisual service ... " << std::endl;

  if (mgr)
    mgr->unregisterService(bemvisualService);

  if (bemvisualService)
    delete bemvisualService;

  if (mgr)
    delete mgr;

  loop = false;
}
#endif

/****** main ******/
int main(int argc, char **argv) {

    if(argc < 4) {
        std::cerr << "Usage: "<< argv[0] << " SERVER_IP USER PASSWORD [-k] [CONTENT_TYPE(jpeg,png,video types)] " << std::endl;
        exit(1);
    }

    const char* server_name = argv[1];
    const char* mico_user   = argv[2];
    const char* mico_pass   = argv[3];
    std::string mime_type   = "video/mp4";
    std::string simple_type = "mp4";

    bool   doKill    = false;

    unsigned int arg_opt_cnt = 5;

    if(argc >= arg_opt_cnt) {
      if(!strcmp(argv[arg_opt_cnt-1], "-k")) {
        arg_opt_cnt+=1;
        doKill = true;
      }
    }

    char *type;
    if(argc >=  arg_opt_cnt)  {
      type = argv[arg_opt_cnt-1];
      if(!strcmp(argv[arg_opt_cnt-1], "jpeg")) {
        simple_type = "jpeg";
        mime_type   = "image/jpeg";
      } else if(!strcmp(argv[arg_opt_cnt-1], "png")) {
        simple_type = "png";
        mime_type = "image/png";
      } else {
        simple_type = argv[arg_opt_cnt-1];
        mime_type = std::string("video/")+argv[arg_opt_cnt-1];
      }
      arg_opt_cnt+=1;
    }

#if defined EXTRACTOR_BUILD_AS_DAEMON
    if(!strcmp(argv[1], "-k")) {
        return mico::daemon::stop(argv[0]);
    } else {
        // create a new instance of a MICO daemon, auto-registering two instances of the OCR analysis service
        return mico::daemon::start(argv[0], server_name, mico_user, mico_pass, {new BemvisualService()});

    }
#else
    mgr = new EventManager(server_name, mico_user, mico_pass);

    bemvisualService = new BemvisualService();

    mgr->registerService(bemvisualService);

    signal(SIGINT,  &signal_handler);
    signal(SIGTERM, &signal_handler);
    signal(SIGHUP,  &signal_handler);

    while(loop) {
      sleep(1);
    }
#endif
}
