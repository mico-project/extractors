#include <ctime>
#include <fstream>
#include <Logging.hpp>
#include <MICOExtractorID.h>
#include "BemvisualService.h"
#include "outputHandler.h"

//ffmpeg includes
#include "Packet.h"
extern "C" {
#include "libavutil/opt.h"
#include "libavutil/channel_layout.h"
#include "libavutil/common.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"
#include "libavutil/samplefmt.h"
#include "libswscale/swscale.h"
}


// define dublin core vocabulary shortcut
namespace DC = mico::rdf::vocabularies::DC;

// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2011-10-08T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}
/*
static int readingStream(void* opaque, uint8_t* buf, int buf_size) {
  auto& me = *reinterpret_cast<std::istream*>(opaque);
  me.read(reinterpret_cast<char*>(buf), buf_size);
  return me.gcount();
}
*/
BemvisualService::BemvisualService()
  : AnalysisService(mico::extractors::getPrecompExtractorId(),mico::extractors::getPrecompExtractorModeId(),mico::extractors::getPrecompExtractorVersion(),
		  "video/mp4", "text/vnd.fhg-bemvisual-video+xml"),
    m_status(avanalysis::AV_OK)
{
  m_beminterface = bemvisual::IBEMVisualErrorDetectionInterface::getInstance ("4HpuH8A7kCJKBfyfmpr6vcpH");
  if(!m_beminterface) {
    std::cerr << "Could not get bemvisual instance!" << std::endl;
    throw std::string("Could not get bemvisual instance!");
  } else {
    char *libInfo = new char[128];
    m_beminterface->getLibraryId( libInfo );
    std::cout << libInfo << std::endl;
    delete []libInfo;
  }
  m_pMediaQuality = avanalysis::IMediaQuality::getInstance();
};

BemvisualService::~BemvisualService() {
  delete m_beminterface;
  avanalysis::IMediaQuality::destroyInstance( m_pMediaQuality );
}

void BemvisualService::call(mico::event::AnalysisResponse& resp, std::shared_ptr< mico::persistence::model::Item > item, std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources, std::map<std::string,std::string>& params)
{
  if (resources.size() != 1) {
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "BemvisualService expects exactly one input resource",
                          "Wrong number of input resources");
    return;
  }

  std::shared_ptr<Resource> itemResource = std::dynamic_pointer_cast<Resource>(item);
  std::shared_ptr<Resource> videoResource = resources[0];
   
  if(videoResource) {
    std::shared_ptr<Asset> videoAsset = videoResource->getAsset();
    std::istream* in = videoAsset->getInputStream();

    av_register_all();
    std::shared_ptr<AVFormatContext> avFormat(avformat_alloc_context(), &avformat_free_context);

    auto avFormatPtr = avFormat.get();
    
    ///following block reads istream and writes it to a file -TODO read direct from istream to avformat_open_input
    std::ofstream currentFile;
    currentFile.open("currentbemvisualFile");
    std::cout << "reading istream" << std::endl;
    std::vector<char> buf = std::vector<char>(std::istreambuf_iterator<char>(*in), std::istreambuf_iterator<char>());
    std::cout << "write to file - bufsize: "<< buf.size() << std::endl;
    for (unsigned int myIter=0;myIter < buf.size();++myIter)
      currentFile << buf[myIter];
    std::cout << "temp file completely written" << std::endl;
    currentFile.close();

/* ///these lines would use direct stream?! TODO check up - whats the isteam size?
    const std::shared_ptr<unsigned char> buffer(reinterpret_cast<unsigned char*>(av_malloc(8192)), &av_free);
    const std::shared_ptr<AVIOContext> avioContext(avio_alloc_context(buffer.get(), 8192, 0,
                                       reinterpret_cast<void*>(in), &readingStream, nullptr, nullptr), &av_free);

    avFormat->pb = avioContext.get();
    if (avformat_open_input(&avFormatPtr, "anyFilename", nullptr, nullptr) != 0)
*/
    delete in;

    if (avformat_open_input(&avFormatPtr, "currentbemvisualFile", nullptr, nullptr) != 0)
      {std::cout << "Error while calling avformat_open_input (probably invalid file format)" << std::endl;return;}

    if (avformat_find_stream_info(avFormat.get(), nullptr) < 0)
      {std::cout << "Error while calling avformat_find_stream_info" << std::endl;return;}

    std::cout << "find the videostream in file" << std::endl;
    AVStream* videoStream = nullptr;
    for (unsigned int i = 0; i < avFormat->nb_streams; ++i) {
      if (avFormat->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
        videoStream = avFormat->streams[i];
        break;
      }
    }

    if (!videoStream)
      {std::cout << "Didn't find any video stream in the file (probably audio file)" << std::endl;return;}

    std::cout << "get required codec" << std::endl;
    // getting the required codec structure
    const auto codec = avcodec_find_decoder(videoStream->codec->codec_id);
    if (codec == nullptr)
      {std::cout << "Codec required by video file not available" << std::endl;return;}

    // allocating a structure
    std::shared_ptr<AVCodecContext> avVideoCodec(avcodec_alloc_context3(codec),[](AVCodecContext* c) {avcodec_close(c); av_free(c);});

    // we need to make a copy of videoStream->codec->extradata and give it to the context
    // make sure that this vector exists as long as the avVideoCodec exists
    std::vector<uint8_t> codecContextExtraData(videoStream->codec->extradata, videoStream->codec->extradata + videoStream->codec->extradata_size);
    avVideoCodec->extradata = reinterpret_cast<uint8_t*>(codecContextExtraData.data());
    avVideoCodec->extradata_size = codecContextExtraData.size();

    // initializing the structure by opening the codec
    if (avcodec_open2(avVideoCodec.get(), codec, nullptr) < 0)
      {std::cout << "Could not open codec"<< std::endl;return;}

    std::cout << "allocating AVFrame" << std::endl;
    // allocating an AVFrame
    std::shared_ptr<AVFrame> avFrame(avcodec_alloc_frame(), &av_free);

    // the current packet of data
    Packet packet;
    // data in the packet of data already processed
    size_t offsetInData = 0;

    //START bemvisual stuff:
    // activate all features
    std::vector<avanalysis::AVAnalysisFeatureType> allFeatures = bemvisual::events::all();
    for (std::vector<avanalysis::AVAnalysisFeatureType>::const_iterator it = allFeatures.begin (); it != allFeatures.end (); ++it)
    {
      m_status = m_beminterface->setFeatureEnabled (*it, true);
      if (m_status == avanalysis::AV_ERROR) {
        std::cerr << m_beminterface->getLastErrorMessage () << std::endl;
      }
      if (!(*it == bemvisual::events::EDGE ||
            *it == bemvisual::events::FIELDORDER_TFF || *it == bemvisual::events::FIELDORDER_BFF ||
            *it == bemvisual::events::HBLACKBARS || *it == bemvisual::events::VBLACKBARS))
        m_status = m_pMediaQuality->setFeatureEnabled ( *it, true );
    }

    //initalize
    m_status = m_beminterface->init ();
    if (m_status == avanalysis::AV_ERROR) {
      std::cerr << m_beminterface->getLastErrorMessage () << std::endl;
    }
    m_status = m_pMediaQuality->useDefaultPreset();

    // set media parameters
    avanalysis::ECSP colorspace = avanalysis::CSP_RGB; ///TODO: get colorspace from ffmpeg
    avanalysis::EOrientation orientation = avanalysis::OriTopLeft; ///TODO: get orientation from ffmpeg
    unsigned long lFrameId = 0;

    //result map for saving
    vaoutput::qualityType qresults;
    vaoutput::globalsType globals;
    vaoutput::moduleConfigType parameters;

    // the decoding loop, running until EOF
    while (true) {//lFrameId<742) {
      // reading a packet using libavformat
      if (offsetInData >= (unsigned) packet.packet.size) {
        do {
          packet.reset(avFormat.get());
        } while (packet.packet.stream_index != videoStream->index && packet.packet.data != nullptr);
        // are we EOF?
        if (packet.packet.data == nullptr || packet.packet.size <= 0)
          break; //the while(true)-loop
      }

      // sending data to libavcodec
      int isFrameAvailable = 0;
      const auto processedLength = avcodec_decode_video2(avVideoCodec.get(), avFrame.get(), &isFrameAvailable, &packet.packet);
      if (processedLength < 0) {
        std::cout << "error " << processedLength << std::endl;
        av_free_packet(&packet.packet);
        std::cout << "Error while processing the data" << std::endl;
        return;
      }
      offsetInData += processedLength;

      // processing the image if available
      if (isFrameAvailable) {

        AVPicture rgbFrame;
        avpicture_alloc(&rgbFrame, PIX_FMT_RGB24, avFrame->width, avFrame->height);

        auto ctxt = sws_getContext(avFrame->width, avFrame->height, static_cast<PixelFormat>(avFrame->format),
                                   avFrame->width, avFrame->height, PIX_FMT_RGB24, SWS_BILINEAR, nullptr, nullptr, nullptr);
        if (ctxt == nullptr)
                throw std::runtime_error("Error while calling sws_getContext");

        sws_scale(ctxt, avFrame->data, avFrame->linesize, 0, avFrame->height, rgbFrame.data, rgbFrame.linesize);

        //process
        std::cout << "BemvisualVideoService::call(): processing frame: " << lFrameId << std::endl;
        m_status = m_beminterface->processVideoFrame ( rgbFrame.data[0], avFrame->width, avFrame->height, colorspace, orientation, lFrameId);

        if (m_status == avanalysis::AV_ERROR) {
          std::cerr << m_beminterface->getLastErrorMessage () << std::endl;
        } else {
          float result;
          for (std::vector<avanalysis::AVAnalysisFeatureType>::const_iterator it = allFeatures.begin (); it != allFeatures.end (); ++it)
          {
            m_status = m_beminterface->getResult (*it, lFrameId, result);
            if (m_status == avanalysis::AV_ERROR) {
              std::cerr << m_beminterface->getLastErrorMessage () << std::endl;
            }
            //std::cout << "BemvisualVideoService::call(): result for "<< (*it).getName () << " is: "<< result << std::endl;
            qresults[(*it)][lFrameId] = result;

            if (!(*it == bemvisual::events::EDGE ||
                  *it == bemvisual::events::FIELDORDER_TFF || *it == bemvisual::events::FIELDORDER_BFF ||
                  *it == bemvisual::events::HBLACKBARS || *it == bemvisual::events::VBLACKBARS))
              m_status = m_pMediaQuality->setMeasurementValue( *it, result, lFrameId );
          }

          float fResMQ = 100.f;
          if ( m_pMediaQuality->getResult( fResMQ, lFrameId ) == avanalysis::AV_OK_WITHINFO )
            std::cout << "  MediaQuality result: " << fResMQ << std::endl;
        }
        avpicture_free(&rgbFrame);
        ++lFrameId;
      }//end isFrameAvailable
    }//end while(true)-loop


    // write bemvisual xml text to a new content part
    std::shared_ptr<Part> bemOutPart = item->createPart(mico::persistence::model::URI("http://bemvisual_video_service"));
    std::shared_ptr<Resource> bemOutResource = std::dynamic_pointer_cast<Resource>(bemOutPart);
    bemOutResource->setSyntacticalType( "text/vnd.fhg-bemvisual-video+xml" );
    bemOutResource->setSemanticType("Bemvisual annotation in xml format");
    bemOutPart->addInput(videoResource);
    std::shared_ptr<Asset> bemOutAsset = bemOutResource->getAsset();
    std::ostream* out = bemOutAsset->getOutputStream();
    std::cout << "send bemvisual output to stream" << std::endl;
    vaoutput::outputHandler oH("bemvisual", qresults, globals, parameters);
    oH.toXmlStream( *out );
    delete out;

    // write mediaquality part
    std::shared_ptr<Part> mqOutPart = item->createPart(mico::persistence::model::URI("http://bemvisual_mediaquality_service"));
    std::shared_ptr<Resource> mqOutResource = std::dynamic_pointer_cast<Resource>(mqOutPart);
    mqOutResource->setSyntacticalType( "text/vnd.fhg-mediaquality+xml" );
    mqOutResource->setSemanticType("Mediaquality annotation in xml format");
    bemOutPart->addInput(videoResource);
    std::shared_ptr<Asset> mqOutAsset = mqOutResource->getAsset();
    std::ostream* out2 = mqOutAsset->getOutputStream();
    std::cout << "send mediaquality output to stream" << std::endl;
    m_pMediaQuality->writeResultsToXMLStream( *out2 );
    delete out2;

    // notify broker that we created a new content parts by calling the callback function passed as argument
    resp.sendNew(item, bemOutResource->getURI());
    resp.sendNew(item, mqOutResource->getURI());
    resp.sendFinish(item);
    
    std::remove("currentbemvisualFile");
  } else {
    LOG_ERROR("Resource of item %s is null!", itemResource->getURI().stringValue().c_str());
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "BemvisualService - Null resource received",
                          "Null Resource");
  }
}
