#include "TestService.h"
#include <Daemon.hpp>
#include <Logging.hpp>

#include <FileOperations.h>

#include <string>

#include <stdio.h>
#include <execinfo.h>

#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

namespace po  = boost::program_options;
namespace pt  = boost::property_tree;


/****** globals ******/
EventManager* mgr = 0;
TestService* testService = 0;
bool loop = true;

void signal_handler(int signum) {
  std::cout << "shutting down test service ... " << std::endl;

  if (mgr) {
    mgr->unregisterService(testService);
  }

  if (testService)
    delete testService;

  if (mgr)
    delete mgr;

  loop = false;
}

void sigsegv_handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stdout, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDOUT_FILENO);
  //exit(1);
}



static  std::string ip;
static  std::string user;
static  std::string passw;


static bool setCommandLineParams(int argc, char** argv, po::variables_map& vm)
{
  po::options_description desc("Allowed options");
  desc.add_options()
    ("serverIP,i",  po::value<std::string>(&ip)->required(), "IP of the MICO system server.")
    ("userName,u",  po::value<std::string>(&user)->required(), "MICO system user name")
    ("userPassword,p", po::value<std::string>(&passw)->required(), "MICO system user password")
    ("kill,k","Shuts down the service.")
    ("foreground,f","Runs the extractor as foreground process (by default it runs as daemon)")
    ("help,h","Prints this help message.");

  po::positional_options_description p;
  p.add("serverIP",1);
  p.add("userName",1);
  p.add("userPassword",1);

  bool printHelp = false;
  std::string ex("") ;
  try {
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run() , vm);
    po::notify(vm);
  } catch (std::exception& e) {
    ex = e.what();
    printHelp = true;
  }
  if (vm.count("help")) {
    printHelp = true;
  }

  if (printHelp) {
    if (ex.size() > 0)
      std::cout << std::endl << ex << "\n";
    std::cout << "\nUsage:   " << argv[0];
    for (unsigned int i=0; i < p.max_total_count(); ++i)
      std::cout << " " << p.name_for_position(i);
    std::cout << " [options]" << "\n";
    std::cout << "\n" << desc << "\n";
    return false;
  }
  return true;
}

/****** main ******/
int main(int argc, char **argv)
{
  po::variables_map vm;

  if (!setCommandLineParams(argc, argv, vm)) {
   exit(EXIT_FAILURE);
  }

  bool   doKill    = false;
  bool   asDaemon  = true;

  if (vm.count("kill")) {
    doKill = true;
  }

  if (vm.count("foreground")) {
    doKill   = false;
    asDaemon = false;
  }

  std::string s_daemon_id(argv[0]);

  if (!doKill && asDaemon)
     std::cout << "Setting up test extractor as deamon" << std::endl;
  else if (doKill && asDaemon)
     std::cout << "Shutting down test extractor deamon " << std::endl;
  else
     std::cout << "Starting test extractor " << std::endl;

  if(doKill) {
    return mico::daemon::stop(s_daemon_id.c_str());
  }

  if (asDaemon) {
    mico::log::set_log_backend (mico::daemon::createDaemonLogBackend());
    return mico::daemon::start(s_daemon_id.c_str(), ip.c_str(), user.c_str(), passw.c_str(),
    {new TestService()});
  } else {
    mico::log::set_log_backend (new mico::log::StdOutBackend());
    mgr = new EventManager(ip.c_str(), user.c_str(), passw.c_str());
    testService = new TestService();

    mgr->registerService(testService);

    signal(SIGINT,  &signal_handler);
    signal(SIGTERM, &signal_handler);
    signal(SIGHUP,  &signal_handler);

    // signal(SIGSEGV, &sigsegv_handler);   // install our handler

    while(loop) {
      sleep(1);
    }
  }
}

