[TOC]

# test extractor

# Dependencies

external dependencies:

* boost library (system, log, log_setup)
* MICO Platform API 

# Build setting and environment

A typical build call could look like:

    cmake -DMICOPlatformAPI_HOME=[path to MICO platform API] [path to extractor source]
