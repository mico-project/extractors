#include <ctime>
#include <fstream>
#include <iostream>
//#include <map>

#include <boost/regex.hpp>


#include <Logging.hpp>

#include "TestService.h"

// define dublin core vocabulary shortcut
namespace DC = mico::rdf::vocabularies::DC;

// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2016-01-19T15:26:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}

TestService::TestService()
  : AnalysisService("http://www.mico-project.org/services/mico-test-extractor",
    "text/plain", "text/plain-out", "tester-queue")
{
}

TestService::~TestService()
{
}

void TestService::call(mico::event::AnalysisResponse& resp,
                       std::shared_ptr< mico::persistence::model::Item > item,
                       std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources,
                       std::map<std::string,std::string>& params)
{ 

  if (resources.size() != 1) {
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "TestService expects exactly one input resource",
                          "Wrong number of input resources");
    return;
  }

  std::shared_ptr<Resource> itemResource = std::dynamic_pointer_cast<Resource>(item);
  std::shared_ptr<Resource> textInResource = resources[0];
  mico::persistence::PersistenceService& persistenceService = itemResource->getPersistenceService();

  if(textInResource) {
    std::shared_ptr<Asset> txtInAsset = textInResource->getAsset();
    std::istream* in = txtInAsset->getInputStream();

    //read text
    std::vector<char> buf =
        std::vector<char>(std::istreambuf_iterator<char>(*in), std::istreambuf_iterator<char>());
    delete in;

    LOG_INFO( "TestService::call %s", buf.data() );

    for (int i=1;i<=10;++i) {
      LOG_DEBUG( "TestService - send progress %f on response object %d",(float)i/10.f, &resp);
      sleep(1);
      resp.sendProgress(item, textInResource->getURI(), (float)i/10.f );
    }

    //resp.sendErrorMessage(item, object, 30, "THIS IS AN ERROR MESSAGE", "this is the description for this error");


    // write xml text to a new content part
    std::shared_ptr<Part> txtOutPart = item->createPart(mico::persistence::model::URI("http://test_analysis_service"));
    std::shared_ptr<Resource> txtOutResource = std::dynamic_pointer_cast<Resource>(txtOutPart);
    txtOutResource->setSyntacticalType( "text/plain-out" );

    std::shared_ptr<Asset> txtOutAsset = txtOutResource->getAsset();
    std::ostream* out = txtOutAsset->getOutputStream();
    *out << "hallo aus dem test extractor";
    delete out;

    resp.sendNew(item, txtOutResource->getURI());
    resp.sendFinish(item);

  } else {
    LOG_ERROR("Resource of item %s is null!", itemResource->getURI().stringValue().c_str());
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "TestService - Null resource received",
                          "Null Resource");
  }
}
