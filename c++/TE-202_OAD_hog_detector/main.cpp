#include "HOGDetectorService.h"
#include <Daemon.hpp>
#include <Logging.hpp>

#include <FileOperations.h>

#include <string>

#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

namespace MEX = mico::extractors;
namespace po  = boost::program_options;
namespace pt  = boost::property_tree;


/****** globals ******/
EventManager* mgr = 0;
HOGDetectorService* hogdetectorService = 0;
bool loop = true;

void signal_handler(int signum) {
  std::cout << "shutting down HOGDetector service ... " << std::endl;

  if (mgr) {
    mgr->unregisterService(hogdetectorService);
  }

  if (hogdetectorService)
    delete hogdetectorService;

  if (mgr)
    delete mgr;

  loop = false;
}


static HOGUnit createHOGUnitFromFiles(const std::string& name,
                                      const std::string& paramFile,
                                      const std::string& modelFile,
                                      const std::string& version,
                                      double hitThresh)
{
  std::vector<std::string> res_files;

  res_files.push_back(paramFile);
  res_files.push_back(modelFile);

  std::vector<std::string> paths =
    {std::string(RESOURCE_DIR),"/usr/share","/usr/local/share"};

  std::map<std::string,std::string> resource_files =
      MEX::FileOperations::findFiles(res_files, paths);

  if (resource_files.find(paramFile) == resource_files.end() ||
      resource_files.find(modelFile) == resource_files.end() ) {
    std::cerr << "Could not find all resource files for animal " << name << std::endl;
    exit(EXIT_FAILURE);
  }

  std::vector<int> winSizeV, blockSizeV, blockStrideV, cellSizeV, winStrideV;
  std::string svmType;
  std::vector<double> model;
  bool gammaCorrection;
  int nbins, nLevels;
  double l2Hys, winSigma, probA, probB;
  HOGDPtr phog;

  cv::FileStorage fs_p(resource_files[paramFile], cv::FileStorage::READ);

  if (!fs_p.isOpened()) {
    std::cerr << "Could not load resource files " << resource_files[0] << std::endl;
    exit(EXIT_FAILURE);
  }

  fs_p["winSize"] >> winSizeV;
  fs_p["blockSize"] >> blockSizeV;
  fs_p["blockStride"] >> blockStrideV;
  fs_p["cellSize"] >> cellSizeV;
  fs_p["winStride"] >> winStrideV;
  fs_p["gammaCorrection"] >> gammaCorrection;
  fs_p["nbins"] >> nbins;
  fs_p["winSigma"] >> winSigma;
  fs_p["l2Hys"] >> l2Hys;
  fs_p["nLevels"] >> nLevels;
  fs_p["svmType"] >> svmType;
  fs_p["probA"] >> probA;
  fs_p["probB"] >> probB;

  cv::Size winSize(winSizeV[0],winSizeV[1]);
  cv::Size blockSize(blockSizeV[0],blockSizeV[1]);
  cv::Size blockStride(blockStrideV[0],blockStrideV[1]);
  cv::Size cellSize(cellSizeV[0],cellSizeV[1]);
  cv::Size winStride(winStrideV[0],winStrideV[1]);

  cv::FileStorage fs_m(resource_files[modelFile], cv::FileStorage::READ);
  fs_m["model"] >> model;
  phog.reset(new cv::HOGDescriptor(
      winSize, blockSize, blockStride, cellSize,
      nbins, 1, winSigma, cv::HOGDescriptor::L2Hys,
      l2Hys, gammaCorrection, nLevels));
  phog->setSVMDetector(model);

  boost::trim(svmType);

  if ((svmType.compare("CLASS") != 0) && (svmType.compare("REG") != 0) &&
      (svmType.compare("CLASS_PLATT") != 0))
  {
    std::cerr << "Unsupported HOG algorithm in " << paramFile << std::endl;
    exit(EXIT_FAILURE);
  }

  return HOGUnit(name, phog, svmType, version, winStride, probA, probB, hitThresh);
}

static std::vector<HOGUnit> parseConfig(const std::string& filename)
{
  std::vector<HOGUnit> detectionUnits;

  unsigned int counterAnimals = 0;
  unsigned int counterModels  = 0;
  std::ifstream cfIn;

  cfIn.open(filename);

  if (!cfIn) {
    std::cerr << "Could not open config file for OAD extractor - quitting.";
    exit(EXIT_FAILURE);
  }

  pt::ptree propTree;
  try {
    pt::read_json(cfIn, propTree);
    BOOST_FOREACH(pt::ptree::value_type &modelSet, propTree)
    {
      std::string name    = modelSet.second.get<std::string>("name");
      std::string version = modelSet.second.get<std::string>("alg_version");
      double hitThreshold = modelSet.second.get<double>("hit_threshold");
      counterAnimals += 1;
      BOOST_FOREACH(pt::ptree::value_type &m, modelSet.second.get_child("models"))
      {
        HOGUnit unit = createHOGUnitFromFiles(name,
                                              m.second.get<std::string>("param_file"),
                                              m.second.get<std::string>("model_file"),
                                              version, hitThreshold);
        detectionUnits.push_back(unit);
        counterModels += 1;
      }
    }
  } catch (const pt::ptree_bad_path& ex) {
     std::cerr << "Error parsing json : " << ex.what() << std::endl;
     cfIn.close();
     exit(EXIT_FAILURE);
  }
  cfIn.close();

  std::cout << "+++ loaded " << counterModels << " models for " << counterAnimals << " different animals. +++" << std::endl;


  return detectionUnits;
}

static  std::string ip;
static  std::string user;
static  std::string passw;
static  std::string fmt;
static  std::string config;
static  std::string mode;


static bool setCommandLineParams(int argc, char** argv, po::variables_map& vm)
{
  po::options_description desc("Allowed options");
  desc.add_options()
    ("serverIP,i",  po::value<std::string>(&ip)->required(), "IP of the MICO system server.")
    ("userName,u",  po::value<std::string>(&user)->required(), "MICO system user name")
    ("userPassword,p", po::value<std::string>(&passw)->required(), "MICO system user password")
    ("config,c", po::value<std::string>(&config)->required(), "The json configuration for this animal detector instance.")
    ("mode,m", po::value<std::string>(&mode)->default_value(std::string("BLANK"),"BLANK"), "Detection mode (BLANK or ANIMAL)")
    ("inputFormat,f", po::value<std::string>(&fmt)->default_value(std::string("JPEG"),"JPEG"), "Expected input type (JPEG or PNG)")
    ("kill,k","Shuts down the service.")
    ("foreground,g","Runs the extractor as foreground process (by default it runs as daemon)")
    ("help,h","Prints this help message.");

  po::positional_options_description p;
  p.add("serverIP",1);
  p.add("userName",1);
  p.add("userPassword",1);

  bool printHelp = false;
  std::string ex("") ;
  try {
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run() , vm);
    po::notify(vm);
  } catch (std::exception& e) {
    ex = e.what();
    printHelp = true;
  }
  if (vm.count("help")) {
    printHelp = true;
  }

  if (printHelp) {
    if (ex.size() > 0)
      std::cout << std::endl << ex << "\n";
    std::cout << "\nUsage:   " << argv[0];
    for (unsigned int i=0; i < p.max_total_count(); ++i)
      std::cout << " " << p.name_for_position(i);
    std::cout << " [options]" << "\n";
    std::cout << "\n" << desc << "\n";
    return false;
  }
  return true;
}

/****** main ******/
int main(int argc, char **argv)
{
  po::variables_map vm;

  if (!setCommandLineParams(argc, argv, vm)) {
   exit(EXIT_FAILURE);
  }

  bool   doKill    = false;
  bool   asDaemon  = true;

  if (vm.count("kill")) {
    doKill = true;
  }

  if (vm.count("foreground")) {
    doKill   = false;
    asDaemon = false;
  }

  boost::to_lower(fmt);
  boost::to_lower(mode);

  std::string img_type = "jpeg";

  if(fmt == "png"){
    img_type = "png";
  }

  std::string mime_type = std::string("image/") + img_type;

  if ((mode != "animal") && (mode !="blank")) {
    std::cerr << "Unknown mode : " << mode << std::endl;
    exit(EXIT_FAILURE);
  }

  std::vector<HOGUnit> detectionUnits;
  if(!doKill) {
	  detectionUnits = parseConfig (config);

	  if (detectionUnits.size() == 0) {
	    std::cerr << "No models found in config json!" << std::endl;
	    exit(EXIT_FAILURE);
	  }
  }

  std::string s_daemon_id(argv[0]);
  s_daemon_id += std::string("-") + std::string(img_type);

  if (!doKill && asDaemon)
     std::cout << "Launching animal detection daemon with id [" << s_daemon_id << "]" << std::endl;
  else if (doKill && asDaemon)
     std::cout << "Shutting down animal detection daemon with id ["<< s_daemon_id <<"]" << std::endl;
  else
     std::cout << "Starting animal detection extractor to analyse "<< mime_type << std::endl;

  if(doKill) {
    return mico::daemon::stop(s_daemon_id.c_str());
  }

  if (asDaemon) {
    mico::log::LoggingBackend* log_back = mico::daemon::createDaemonLogBackend();
    log_back->setLevel(mico::log::DEBUG);
    mico::log::set_log_backend (log_back);
    return mico::daemon::start(s_daemon_id.c_str(), ip.c_str(), user.c_str(), passw.c_str(),
    {new HOGDetectorService(mime_type, mode, detectionUnits)});
  } else {
    mico::log::set_log_backend (new mico::log::StdOutBackend());
    mgr = new EventManager(ip.c_str(), user.c_str(), passw.c_str());
    hogdetectorService = new HOGDetectorService(mime_type, mode, detectionUnits);

    mgr->registerService(hogdetectorService);

    signal(SIGINT,  &signal_handler);
    signal(SIGTERM, &signal_handler);
    signal(SIGHUP,  &signal_handler);

    while(loop) {
      sleep(1);
    }
  }
}

