#include "EventManager.hpp"

#include "opencv2/opencv.hpp"

// for constant RDF property definitions of common vocabularies
#include "vocabularies.hpp"

// this namespace contains EventManager and AnalysisService
using namespace mico::event;

// this namespace contains Content, ContentItem, etc
using namespace mico::persistence::model;

typedef std::shared_ptr<cv::HOGDescriptor> HOGDPtr;

struct HOGUnit {
  HOGUnit(std::string name, HOGDPtr hog, std::string  svmType, std::string version,
          cv::Size stride, double pA, double pB, double hitThresh):
    modelName(name),
    pHog(hog),
    svmType(svmType),
    version(version),
    winStride(stride),
    probA(pA),
    probB(pB),
    hitThresh(hitThresh)
  { }

  std::string  modelName;
  std::string  svmType;
  std::string  version;
  HOGDPtr      pHog;
  cv::Size     winStride;
  double       probA;
  double       probB;
  double       hitThresh;
};

namespace objectreco {
  class ObjectRecognitionResults;
}

class HOGDetectorService : public AnalysisService {

public:

  /**
   * @brief HOGDetectorService::HOGDetectorService
   * @param mimeType The accepted input mime type (image/jpeg, image/png)
   * @param detectionUnit A vector of HoG detection units loaded from parameter and model files
   *
   */
  HOGDetectorService(std::string mimeType, std::string mode, std::vector<HOGUnit> detectionUnits);

  ~HOGDetectorService();

  void call(mico::event::AnalysisResponse& resp,
            std::shared_ptr< mico::persistence::model::Item > item,
            std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources,
            std::map<std::string,std::string>& params);

private:
  void detectAnimals(cv::Mat matImage,
                     objectreco::ObjectRecognitionResults& res,
                     double overlapThreshold);

  void mergeDifferentAnimalDetections(const std::vector<cv::Rect>& allRegionsFound,
                                      objectreco::ObjectRecognitionResults& res,
                                      double overlapThreshold);

  size_t  nonMaximaSuppression(const std::vector<cv::Rect> &rects,
                               const std::vector<double> &confs,
                               std::vector<cv::Rect> &outputRects,
                               std::vector<double> &outputConfs,
                               const double overlapThreshold);

  double PlattScalingPredict(double decision_value, double A, double B);

  std::string           m_mode;
  std::vector<HOGUnit>  m_HOGQueue;
};
