#include <ctime>
#include <fstream>
#include <algorithm>
//#include <map>

#include <boost/regex.hpp>


#include <Logging.hpp>

#include "HOGDetectorService.h"
#include "ObjectRecoOutputHandler.h"

#include <MICOExtractorID.h>


// define dublin core vocabulary shortcut
namespace DC = mico::rdf::vocabularies::DC;

// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2011-10-08T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}


void runtime_error(std::string what)
{

}


HOGDetectorService::HOGDetectorService(std::string mimeType, std::string mode, std::vector<HOGUnit> detectionUnits)
  : AnalysisService(mico::extractors::getPrecompExtractorId(),mico::extractors::getPrecompExtractorModeId(),mico::extractors::getPrecompExtractorVersion(),
    mimeType, "text/vnd.fhg-objectdetection+xml"),
    m_mode(mode),
    m_HOGQueue(detectionUnits)
{
}

HOGDetectorService::~HOGDetectorService()
{
  m_HOGQueue.clear();
}

double HOGDetectorService::PlattScalingPredict(double decision_value, double A, double B)
{
  double fApB = decision_value*A+B;
  if (fApB >= 0)
    return exp(-fApB)/(1.0+exp(-fApB));
  else
    return 1.0/(1+exp(fApB)) ;
}

size_t HOGDetectorService::nonMaximaSuppression(const std::vector<cv::Rect>& rects,
                                                const std::vector<double>& confs,
                                                std::vector<cv::Rect>& outputRects,
                                                std::vector<double>& outputConfs,
                                                const double overlapThreshold)
{
  /*	Non-maximum suppression. Greedily select high-scoring detections and skip
   * detections that are significantly covered by a previously selected detection.*/

  outputRects.clear();
  outputConfs.clear();
  if (!rects.size()) {
    return 0;
  }

  if (!rects.size() == confs.size()) {
    LOG_ERROR("Number of detections does not match number of weights!");
    return 0;
  }

  std::vector<double> x1(rects.size(),0.0);
  std::vector<double> y1(rects.size(),0.0);
  std::vector<double> x2(rects.size(),0.0);
  std::vector<double> y2(rects.size(),0.0);
  std::vector<double> s(rects.size(),0.0);
  std::vector<double> area(rects.size(),0.0);

  for(size_t idx = 0; idx < rects.size(); ++idx){
    x1.at(idx)   = rects.at(idx).tl().x;
    y1.at(idx)   = rects.at(idx).tl().y;
    x2.at(idx)   = rects.at(idx).br().x;
    y2.at(idx)   = rects.at(idx).br().y;;
    s.at(idx)    = confs.at(idx);
    area.at(idx) = (x2.at(idx) - x1.at(idx))*(y2.at(idx) - y1.at(idx));
  }

  // sort boxes by confidence and keep track of the index
  std::vector<std::pair<double, std::size_t> > vp;
  vp.reserve(s.size());
  for (std::size_t i = 0 ; i < s.size() ; i++) {
    vp.push_back(std::make_pair(s[i], i));
  }

  // Sorting will put lower values ahead of larger ones,
  // resolving ties using the original index
  std::sort(vp.begin(), vp.end());
  std::vector<unsigned int> I(vp.size(), 0);
  for (std::size_t i = 0; i < vp.size(); i++)
    I.at(i) = vp[i].second;

  //this is going to contain what to keep
  std::vector<unsigned int> indicesKept;

  size_t realSuppressionCount = 0;

  while ( I.size() ){
    int last = I.size() - 1;
    unsigned int i = I.at(last);

    std::vector<unsigned int> suppress;

    // alway keep the current best - it isn't really suppressed
    // but removed along with really suppressed ones before the
    // next iteration
    indicesKept.push_back(i);
    suppress.push_back(last);

    for (int pos=0; pos < last; pos++){
      unsigned int j = I.at(pos);

      double xx1 = std::max(x1.at(i),x1.at(j));
      double yy1 = std::max(y1.at(i),y1.at(j));
      double xx2 = std::min(x2.at(i),x2.at(j));
      double yy2 = std::min(y2.at(i),y2.at(j));
      double w = xx2-xx1;
      double h = yy2-yy1;

      //calc overlapping region ration and compare
      if ((w > 0) && (h > 0)){
        double o = w*h / area.at(j);
        if (o > overlapThreshold)
          suppress.push_back(pos);
          realSuppressionCount++;
      }
    }

    // erase indices in suppress (i.e. the one reference and all that
    // overlap to much to that one)
    std::sort(suppress.begin(),suppress.end(),std::greater<int>());
    for (unsigned int idx=0; idx < suppress.size(); idx++)
      I.erase(I.begin()+suppress.at(idx));
  }

  for (auto p : indicesKept) {
    outputRects.push_back(rects.at(p));
    outputConfs.push_back(confs.at(p));
  }

  return realSuppressionCount;

}


void HOGDetectorService::detectAnimals(cv::Mat matImage,
                                       objectreco::ObjectRecognitionResults& res,
                                       double overlapThreshold)
{
  std::vector<cv::Rect> allRegionsFound;
  unsigned int uid = 0;

  for(HOGUnit& detectorUnit : m_HOGQueue) {
    std::vector<cv::Rect> found;
    std::vector<double>   foundConf;
    std::vector<cv::Rect> foundFiltered;
    std::vector<double>   foundConfFiltered;

    cv::Size padding(cv::Size(32, 32));
    detectorUnit.pHog->detectMultiScale(matImage, found, foundConf,
      detectorUnit.hitThresh, detectorUnit.winStride, padding, 1.05, 0.0);

    if (!found.size() == foundConf.size()) {
      LOG_ERROR("Number of detections does not match number of weights!");
      return;
    }
    size_t numSuppressed =
        nonMaximaSuppression(found,foundConf,foundFiltered,foundConfFiltered, overlapThreshold);

    LOG_DEBUG("Merged %d regions from %d regions for %s.",numSuppressed, found.size(), detectorUnit.modelName.c_str());

    //print out
    if (!foundFiltered.size()) {
      LOG_DEBUG( "Nothing Detected for animal %s", detectorUnit.modelName.c_str());
    } else {
      LOG_DEBUG( "Detected %d individuals oftype %s.", foundFiltered.size(), detectorUnit.modelName.c_str());
      allRegionsFound.insert(allRegionsFound.end(), foundFiltered.begin(), foundFiltered.end());
      for (size_t i = 0; i < foundFiltered.size(); i++) {
        // normalize weight using platt scaling

        double finalConf = foundConfFiltered[i];

        if (detectorUnit.svmType == "CLASS_PLATT") {
          finalConf = PlattScalingPredict(finalConf, detectorUnit.probA, detectorUnit.probB);
        }

        objectreco::Object o;
        o.uid = uid;
        o.type = objectreco::ANIMAL;
        o.region.push_back( objectreco::Point(foundFiltered[i].x, foundFiltered[i].y, 0, "topleft") );
        o.region.push_back( objectreco::Point(foundFiltered[i].x + foundFiltered[i].width, foundFiltered[i].y, 1) );
        o.region.push_back( objectreco::Point(foundFiltered[i].x + foundFiltered[i].width, foundFiltered[i].y + foundFiltered[i].height, 2) );
        o.region.push_back( objectreco::Point(foundFiltered[i].x, foundFiltered[i].y + foundFiltered[i].height, 3) );
        res.objects.push_back(o);
        res.labelMapping[uid].push_back(objectreco::ObjectLabel(uid, detectorUnit.modelName, finalConf));
        ++uid;
      }
    }
  }
  matImage.release();

  if (m_mode == "animal") {
    if (m_HOGQueue.size() > 0 && m_HOGQueue[0].svmType != "CLASS_PLATT") {
      LOG_WARN("Mode is set to animal detection but svm type %s does not support comparing confidences!",
               m_HOGQueue[0].svmType.c_str());
    }
    mergeDifferentAnimalDetections(allRegionsFound, res, overlapThreshold);
  }

}

void HOGDetectorService::mergeDifferentAnimalDetections(const std::vector<cv::Rect>& allRegionsFound,
                                                        objectreco::ObjectRecognitionResults& res,
                                                        double overlapThreshold)
{
  //check overlapping of all rectangles is over 50% -> delete obj with lower confidence
  for(size_t i = 0; i <= allRegionsFound.size(); ++i) {
    for(size_t j = 0; j <= allRegionsFound.size(); ++j) {
      if( i<j ) {
        cv::Rect intersection = allRegionsFound[i] & allRegionsFound[j];
        unsigned int intersectionArea = intersection.width * intersection.height;
        unsigned int unionArea = (allRegionsFound[i].width * allRegionsFound[i].height) +
                                 (allRegionsFound[j].width * allRegionsFound[j].height) - intersectionArea;

        if((unionArea - intersectionArea) !=0 && intersectionArea / (unionArea - intersectionArea) >= overlapThreshold) {
          std::map<long, std::vector<objectreco::ObjectLabel> >::iterator find_i = res.labelMapping.find(i);
          std::map<long, std::vector<objectreco::ObjectLabel> >::iterator find_j = res.labelMapping.find(j);

          if(find_i != res.labelMapping.end() && find_j != res.labelMapping.end()) {
            float conf_i = find_i->second.at(0).confidence;
            float conf_j = find_j->second.at(0).confidence;

            if(conf_i < conf_j) {
              res.labelMapping.erase(i);
              for(std::vector<objectreco::Object>::iterator iter = res.objects.begin(); iter<res.objects.end(); ++iter)
                if (iter->uid == i) { res.objects.erase(iter); break; }
            } else {
              res.labelMapping.erase(j);
              for(std::vector<objectreco::Object>::iterator iter = res.objects.begin(); iter<res.objects.end(); ++iter)
                if (iter->uid == j) { res.objects.erase(iter); break; }
            }
          }
        }
      }
    }
  }
}

void HOGDetectorService::call(mico::event::AnalysisResponse& resp, std::shared_ptr< mico::persistence::model::Item > item, std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources, std::map<std::string,std::string>& params)
{
  if (resources.size() != 1) {
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "HOG Animal Detector expects exactly one input resource",
                          "Wrong number of input resources");
    return;
  }
  std::shared_ptr<Resource> itemResource = std::dynamic_pointer_cast<Resource>(item);
  std::shared_ptr<Resource> imgInResource = resources[0];

  if(imgInResource) {
    std::shared_ptr<Asset> imgInAsset = imgInResource->getAsset();
    std::istream* in = imgInAsset->getInputStream();

    //read image
    std::vector<char> buf =
        std::vector<char>(std::istreambuf_iterator<char>(*in), std::istreambuf_iterator<char>());
    cv::Mat matImage = cv::imdecode(cv::Mat(buf), 1);
    delete in;


    objectreco::ObjectRecognitionResults res;
    objectreco::VideoMetaType meta(0, 100, matImage.cols, matImage.rows,m_HOGQueue[0].version);

    //>>>>>>>>>>> DETECTION HERE <<<<<<<<<<<<<<<
    detectAnimals(matImage, res, 0.4);
    //>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<

    // write xml text to a new content part
    std::shared_ptr<Part> txtOutPart = item->createPart(mico::persistence::model::URI("http://mico-extractor-animaldetection"));
    std::shared_ptr<Resource> txtOutResource = std::dynamic_pointer_cast<Resource>(txtOutPart);
    txtOutResource->setSyntacticalType( "mico:ObjectDetectionXml" );
    txtOutResource->setSemanticType("Animal detection annotation in xml format");
    txtOutPart->addInput(imgInResource);


    std::shared_ptr<Asset> txtOutAsset = txtOutResource->getAsset();
    std::ostream* out = txtOutAsset->getOutputStream();

    txtOutAsset->setFormat("text/vnd.fhg-objectdetection+xml");

    std::map<uint64_t, objectreco::ObjectRecognitionResults > resMap;
    resMap[0] = res;
    objectreco::ObjectRecoOutputHandler outHandler(resMap, meta);
    outHandler.toXmlStream(*out);

    delete out;

    // notify broker that we created a new part
    resp.sendNew(item, txtOutResource->getURI());
    resp.sendFinish(item);

  } else {
    LOG_ERROR("Resource of item %s is null!", itemResource->getURI().stringValue().c_str());
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "HOG Animal Detector - Null resource received",
                          "Null Resource");
  }
}
