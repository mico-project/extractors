#include "FaceDetectionService.h"
#include <Daemon.hpp>
#include <Logging.hpp>

#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>

namespace po = boost::program_options;

/****** globals ******/
#ifndef EXTRACTOR_BUILD_AS_DAEMON
EventManager* mgr = 0;
FaceDetectionService* g_facedetectionService = 0;
bool loop = true;

void signal_handler(int signum) {
  std::cout << "shutting down ccv FaceDetection service ... " << std::endl;

  if (mgr)
    mgr->unregisterService(g_facedetectionService);

  if (g_facedetectionService)
    delete g_facedetectionService;

  if (mgr)
    delete mgr;

  loop = false;
}
#endif

static  std::string ip;
static  std::string user;
static  std::string passw;
static  std::string fmt;
static  double      tstep;

static bool setCommandLineParams(int argc, char** argv, po::variables_map& vm)
{
  po::options_description desc("Allowed options");
  desc.add_options()
    ("serverIP,i",  po::value<std::string>(&ip)->required(), "IP of the MICO system server.")
    ("userName,u",  po::value<std::string>(&user)->required(), "MICO system user name")
    ("userPassword,p", po::value<std::string>(&passw)->required(), "MICO system user password")
    ("inputFormat,f", po::value<std::string>(&fmt)->default_value(std::string("MP4"),"MP4"), "Expected input type (JPEG or PNG or any video container)")
    ("timeStep,t", po::value<double>(&tstep)->default_value(-1.0f,"-1.0"), "Time step of analysis for videos in seconds. -1 means every frame.")
    ("kill,k","Shuts down the service.")
    ("foreground,g","Runs the extractor as foreground process (by default it runs as daemon)")
    ("help,h","Prints this help message.");

  po::positional_options_description p;
  p.add("serverIP",1);
  p.add("userName",1);
  p.add("userPassword",1);

  bool printHelp = false;
  std::string ex("") ;
  try {
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run() , vm);
    po::notify(vm);
  } catch (std::exception& e) {
    ex = e.what();
    printHelp = true;
  }
  if (vm.count("help")) {
    printHelp = true;
  }

  if (printHelp) {
    if (ex.size() > 0)
      std::cout << std::endl << ex << "\n";
    std::cout << "\nUsage:   " << argv[0];
    for (unsigned int i=0; i < p.max_total_count(); ++i)
      std::cout << " " << p.name_for_position(i);
    std::cout << " [options]" << "\n";
    std::cout << "\n" << desc << "\n";
    return false;
  }
  return true;
}

/****** main ******/
int main(int argc, char **argv)
{
  po::variables_map vm;

  std::string mime_type   = "video/mp4";
  std::string simple_type = "mp4";

  if (!setCommandLineParams(argc, argv, vm)) {
   exit(EXIT_FAILURE);
  }

  bool   doKill    = false;
  bool   asDaemon  = true;

  if (vm.count("kill")) {
    doKill = true;
  }

  if (vm.count("foreground")) {
    doKill   = false;
    asDaemon = false;
  }

  boost::to_lower(fmt);

  if(fmt == "jpeg"){
    simple_type = "jpeg";
    mime_type   = "image/jpeg";
  } else if(fmt == "png") {
    simple_type = "png";
    mime_type = "image/png";
  } else {
    simple_type = fmt;
    mime_type = std::string("video/")+fmt;
  }

  std::string s_daemon_id(argv[0]);
  s_daemon_id+= std::string("-") + std::string(simple_type);

  if (!doKill && asDaemon)
    std::cout << "Setting up face detection extractor for mime type "<< mime_type << std::endl;
  else if (doKill && asDaemon)
    std::cout << "Shutting down face detection extractor for mime type "<< mime_type << std::endl;
  else
    std::cout << "Starting face detection extractor to analyse "<< mime_type << std::endl;

  if(doKill) {
    return mico::daemon::stop(s_daemon_id.c_str());
  }

  if (asDaemon) {
    mico::log::set_log_backend (mico::daemon::createDaemonLogBackend());
    return mico::daemon::start(s_daemon_id.c_str(), ip.c_str(), user.c_str(), passw.c_str(), {new FaceDetectionService(simple_type, mime_type,tstep)});
  } else {

    mgr = new EventManager(ip.c_str(), user.c_str(), passw.c_str());
    g_facedetectionService = new FaceDetectionService(simple_type, mime_type, tstep);
    mgr->registerService(g_facedetectionService);

    signal(SIGINT,  &signal_handler);
    signal(SIGTERM, &signal_handler);
    signal(SIGHUP,  &signal_handler);

    while(loop) {
      sleep(1);
    }
  }
}
