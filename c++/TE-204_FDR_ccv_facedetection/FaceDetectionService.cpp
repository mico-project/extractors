#include <ctime>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/random_generator.hpp>

#include <Logging.hpp>
#include <PersistenceService.hpp>
#include <anno4cpp.h>
#include <MICOExtractorID.h>

#include "FileOperations.h"
#include "FaceDetectionService.h"
#include "ObjectRecoOutputHandler.h"




//ffmpeg includes
#include "Packet.h"
extern "C" {
  #include "libavutil/opt.h"
  #include "libavutil/channel_layout.h"
  #include "libavutil/common.h"
  #include "libavutil/imgutils.h"
  #include "libavutil/mathematics.h"
  #include "libavutil/samplefmt.h"
  #include "libswscale/swscale.h"
}

#include "Magick++.h"


// define dublin core vocabulary shortcut
namespace DC  = mico::rdf::vocabularies::DC;
namespace MEX = mico::extractors;
namespace fs = boost::filesystem;

// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2011-10-08T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}

std::string createModeId(std::string mimeType){
	if(mimeType.find("image") != std::string::npos){
		return mico::extractors::getPrecompExtractorModeId() + "Image";
	} else if(mimeType.find("video") != std::string::npos){
		return mico::extractors::getPrecompExtractorModeId() + "Video";
	} else {
		return mico::extractors::getPrecompExtractorModeId();
	}

}

FaceDetectionService::FaceDetectionService(std::string simpleType, std::string mimeType, const double& timeStep)
  : AnalysisService(mico::extractors::getPrecompExtractorId(),createModeId(mimeType),mico::extractors::getPrecompExtractorVersion(),
                    mimeType, "application/x-mico-rdf"),
    m_pcascade(NULL),m_mimeType(mimeType),m_timeStep(timeStep),m_imgCounter(0)
{
  //check which resource dir can be used

  std::vector<std::string> files = {"face.sqlite3"};
  std::vector<std::string> paths = {std::string(RESOURCE_DIR),"/usr/share","/usr/local/share"};

  std::map<std::string,std::string> resource_files =
      MEX::FileOperations::findFiles(files,paths);

  if (resource_files.size() < 1) {
    LOG_ERROR("!!!!!! CLASSIFIER CASCADE NOT FOUND! ");
    exit(EXIT_FAILURE);
  }

  m_pcascade = ccv_scd_classifier_cascade_read( resource_files["face.sqlite3"].c_str() );
  if (! m_pcascade ) {
    LOG_ERROR(" !!!!!! CLASSIFIER CASCADE NOT LOADED! ");
    exit(EXIT_FAILURE);
  }

  Magick::InitializeMagick(nullptr);
}

FaceDetectionService::~FaceDetectionService()
{
  ccv_scd_classifier_cascade_free(m_pcascade);
  LOG_DEBUG("CLASSIFIER CASCADE FREED");
}

void FaceDetectionService::extractSingleFrame(ccv_dense_matrix_t* inImg,
                                              std::map<uint64_t, objectreco::ObjectRecognitionResults >& resMap,
                                              const unsigned long& lFrameId)
{
  //resize image to 480 / height-in-aspect-ratio
  ccv_dense_matrix_t* resizedimg = 0;
  int cols_resized = 480;
  int rows_resized = (int)((float)inImg->rows / inImg->cols * cols_resized);

  LOG_DEBUG("img resize from %dx%d to %dx%d", inImg->cols,inImg->rows, cols_resized, rows_resized);
  ccv_resample(inImg, &resizedimg, 0, rows_resized, cols_resized, CCV_INTER_AREA);

  //detect faces
  ccv_array_t* faces = ccv_scd_detect_objects(resizedimg, &m_pcascade, 1, ccv_scd_default_params);
  for (int i = 0; i < faces->rnum; i++) {
    ccv_comp_t* face = (ccv_comp_t*)ccv_array_get(faces, i);
    double rel_x      = (double)face->rect.x/cols_resized;
    double rel_y      = (double)face->rect.y/rows_resized;
    double rel_width  = (double)face->rect.width/cols_resized;
    double rel_height = (double)face->rect.height/rows_resized;
    LOG_DEBUG("face on x: %d y: %d w: %d h: %d", rel_x, rel_y, rel_width, rel_height);
    objectreco::Object object;
    object.uid = -1; //there is no tracking of face
    object.region.push_back( objectreco::Point( rel_x * inImg->cols,             rel_y * inImg->rows, 0, "topleft") );
    object.region.push_back( objectreco::Point((rel_x+rel_width) * inImg->cols,  rel_y * inImg->rows, 1) );
    object.region.push_back( objectreco::Point((rel_x+rel_width) * inImg->cols, (rel_y+rel_height) * inImg->rows, 2) );
    object.region.push_back( objectreco::Point( rel_x * inImg->cols,            (rel_y+rel_height) * inImg->rows, 3) );
    resMap[lFrameId].objects.push_back( object );
  }
  ccv_array_free(faces);
  ccv_matrix_free(resizedimg);

  //END ccv facedetection stuff
  //############################
}


/** extracts faces for images
 *
 *  NOTICE: deletes the instream afterwards
 *
 **/
FaceDetectionStatus FaceDetectionService::doImageExtraction(std::istream* instream,
                                                            std::map<uint64_t, objectreco::ObjectRecognitionResults >& resMap,
                                                            objectreco::VideoMetaType& meta)
{
  ccv_dense_matrix_t* ccvImg = 0;

  std::vector<char> buf =
      std::vector<char>(std::istreambuf_iterator<char>(*instream),
                        std::istreambuf_iterator<char>());

  if (buf.size() == 0) {
    LOG_ERROR("Input stream does not seem to contain any binary data - stopping analysis.");
    return FaceDetectionStatus(false, mico::event::model::INSUFFICIENT_RESOURCE,
                               "Input stream does not seem to contain any binary data - stopping analysis.");
  }

  Magick::Blob  cmpImage;
  Magick::Blob  rgbImage;
  Magick::Image image;

  cmpImage.update(&buf[0],buf.size());
  image.read(cmpImage);
  image.write(&rgbImage,"RGB");

  size_t bytesPerRow = rgbImage.length() / image.size().height();

  meta.timebase_num   = 0;
  meta.timebase_denum = 0;
  meta.width          = image.size().width();
  meta.height         = image.size().height();

  ccv_read(rgbImage.data(), &ccvImg, CCV_IO_RGB_RAW, image.size().height(),
           image.size().width(), bytesPerRow);

  extractSingleFrame(ccvImg,resMap,0);

  ccv_matrix_free(ccvImg);

  return FaceDetectionStatus(true, mico::event::model::UNEXPECTED_ERROR, "Success");
}

/** extracts faces for video
 *
 *  NOTICE:  deletes the instream afterwards
 *
 **/
FaceDetectionStatus FaceDetectionService::doVideoExtraction(std::istream* instream,
                                                            std::map<uint64_t, objectreco::ObjectRecognitionResults >& resMap,
                                                            objectreco::VideoMetaType& meta,
                                                            const double& timeStepS)
{
  av_register_all();
  std::shared_ptr<AVFormatContext> avFormat(avformat_alloc_context(), &avformat_free_context);
  auto avFormatPtr = avFormat.get();

  std::vector<char> buf =
      std::vector<char>(std::istreambuf_iterator<char>(*instream),
                        std::istreambuf_iterator<char>());

  if (buf.size() == 0) {
    LOG_ERROR("Input stream does not seem to contain any binary data - stopping analysis.");
    return FaceDetectionStatus(false, mico::event::model::INSUFFICIENT_RESOURCE,
                               "Input stream does not seem to contain any binary data - stopping analysis.");
  }

  boost::uuids::random_generator rnd_gen;

  fs::path tempFileName = boost::uuids::to_string(rnd_gen());
  fs::path tempDir("/tmp");
  fs::path tempFile = tempDir / tempFileName.filename();
  std::ofstream currentFile;
  currentFile.open(tempFile.string());

  for (unsigned int myIter=0;myIter < buf.size();++myIter)
    currentFile << buf[myIter];
  LOG_DEBUG("temp file completely written.");
  currentFile.close();

  delete instream;

  if (avformat_open_input(&avFormatPtr, tempFile.c_str(), nullptr, nullptr) != 0) {
    LOG_ERROR("Error while calling avformat_open_input (probably invalid file format)");
    return FaceDetectionStatus(false, mico::event::model::INSUFFICIENT_RESOURCE,
                               "Input stream does not seem to contain any binary data - stopping analysis.");
  }

  if (avformat_find_stream_info(avFormat.get(), nullptr) < 0) {
    LOG_ERROR("Error while calling avformat_find_stream_info");
    return FaceDetectionStatus(false, mico::event::model::DECODING_ERROR,
                               "Error while calling avformat_find_stream_info");
  }

  AVStream* videoStream = nullptr;
  AVRational avg_frame_rate;

  for (unsigned int i = 0; i < avFormat->nb_streams; ++i) {
    if (avFormat->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
      videoStream = avFormat->streams[i];

      #if LIBAVFORMAT_VERSION_MAJOR >= 56
          meta.timebase_num = videoStream->avg_frame_rate.num;
          meta.timebase_denum = videoStream->avg_frame_rate.den;
      #else
        meta.timebase_num = videoStream->r_frame_rate.num;
        meta.timebase_denum = videoStream->r_frame_rate.den;
      #endif
      avg_frame_rate = videoStream->avg_frame_rate;
      break;
    }
  }

  if (!videoStream) {
    LOG_ERROR("Didn't find any video stream in the file (probably audio file)");
    return FaceDetectionStatus(false, mico::event::model::DECODING_ERROR,
                               "Didn't find any video stream in the file (probably audio file)");
  }

  // getting the required codec structure
  const auto codec = avcodec_find_decoder(videoStream->codec->codec_id);
  if (codec == nullptr) {
    LOG_ERROR("Codec required by video file not available");
    return FaceDetectionStatus(false, mico::event::model::DECODING_ERROR,
                               "Codec required by video file not available");
  }

  // allocating a structure
  std::shared_ptr<AVCodecContext>
      avVideoCodec(avcodec_alloc_context3(codec),[](AVCodecContext* c) {avcodec_close(c); av_free(c);});

  // we need to make a copy of videoStream->codec->extradata and give it to the context
  // make sure that this vector exists as long as the avVideoCodec exists
  std::vector<uint8_t> codecContextExtraData(videoStream->codec->extradata,
                                             videoStream->codec->extradata +
                                             videoStream->codec->extradata_size);

  avVideoCodec->extradata = reinterpret_cast<uint8_t*>(codecContextExtraData.data());
  avVideoCodec->extradata_size = codecContextExtraData.size();

  // initializing the structure by opening the codec
  if (avcodec_open2(avVideoCodec.get(), codec, nullptr) < 0) {
    LOG_ERROR("Could not open codec");
    return FaceDetectionStatus(false, mico::event::model::DECODING_ERROR,
                               "Could not open codec");
  }

  // allocating an AVFrame
  std::shared_ptr<AVFrame> avFrame(av_frame_alloc(), &av_free);

  // the current packet of data
  Packet packet;
  // data in the packet of data already processed
  size_t offsetInData    = 0;
  unsigned long lFrameId = 0;
  double frameStartTime  = 0.0;
  double nextProcTime    = 0.0;

  double avg_frameLengthS = static_cast<double>(avg_frame_rate.den) / static_cast<double>(avg_frame_rate.num);
  double realStep         = timeStepS < 0 ? avg_frameLengthS : timeStepS;

  // the decoding loop, running until EOF
  while (true) {
    // reading a packet using libavformat
    if (offsetInData >= (unsigned) packet.packet.size) {
      do {
        packet.reset(avFormat.get());
      } while (packet.packet.stream_index != videoStream->index && packet.packet.data != nullptr);
      // are we EOF?
      if (packet.packet.data == nullptr || packet.packet.size <= 0)
        break; //the while(true)-loop
    }

    // sending data to libavcodec
    int isFrameAvailable = 0;
    const auto processedLength =
        avcodec_decode_video2(avVideoCodec.get(),
                              avFrame.get(), &isFrameAvailable, &packet.packet);

    if (processedLength < 0) {
      av_free_packet(&packet.packet);
      LOG_ERROR("Error while processing the data - processedLength < 0");
      return FaceDetectionStatus(false, mico::event::model::DECODING_ERROR,
                                 "Error while processing the data - processedLength < 0");
    }
    offsetInData += processedLength;

    if (frameStartTime >= nextProcTime) {
      nextProcTime = frameStartTime + realStep;
    } else {
      ++lFrameId;
      frameStartTime = static_cast<double>(lFrameId) * avg_frameLengthS;
      continue;
    }

    // processing the image if available
    if (isFrameAvailable) {
      AVPicture rgbFrame;
      avpicture_alloc(&rgbFrame, PIX_FMT_RGB24, avFrame->width, avFrame->height);

      // we need sw scale in order to produce RGB data from the image
      auto ctxt = sws_getContext(avFrame->width, avFrame->height, static_cast<PixelFormat>(avFrame->format),
                                 avFrame->width, avFrame->height, PIX_FMT_RGB24, SWS_BILINEAR, nullptr, nullptr, nullptr);
      if (ctxt == nullptr)
        throw std::runtime_error("Error while calling sws_getContext");

      sws_scale(ctxt, avFrame->data, avFrame->linesize, 0, avFrame->height, rgbFrame.data, rgbFrame.linesize);

      meta.width = avFrame->width;
      meta.height = avFrame->height;

      ccv_dense_matrix_t* currFrame = 0;
      ccv_read( rgbFrame.data[0], &currFrame, CCV_IO_RGB_RAW, avFrame->height, avFrame->width, rgbFrame.linesize[0] );

      extractSingleFrame(currFrame,resMap, lFrameId);

      ccv_matrix_free(currFrame);
      avpicture_free(&rgbFrame);

      //calc times for next frame
      ++lFrameId;
      frameStartTime = static_cast<double>(lFrameId) * avg_frameLengthS;
    }//end isFrameAvailable
  }//end while(true)-loop

  if( std::remove(tempFile.c_str()) != 0 )
    LOG_WARN("Could not remove temporary file %s", tempFile.c_str());
  else
    LOG_DEBUG("Successfully remove temporary file %s", tempFile.c_str());

  return FaceDetectionStatus(true,mico::event::model::UNEXPECTED_ERROR, "Success");
}

void FaceDetectionService::doSemanticAnnotation(mico::event::AnalysisResponse& resp,
                                                std::map<uint64_t, objectreco::ObjectRecognitionResults >& resultMap,
                                                std::shared_ptr<mico::persistence::model::Item> item,
                                                std::shared_ptr<mico::persistence::model::Resource> resource)
{
  if (mico::persistence::PersistenceService::getJVM() == nullptr) {
    resp.sendErrorMessage(item, mico::event::model::UNEXPECTED_ERROR, "JavaVM not initialized", "Could not set scope of JavaVM");
    return;
  }

  jnipp::Env::Scope scope(mico::persistence::PersistenceService::getJVM());

  if (jnipp::Env::get() == nullptr) {
    resp.sendErrorMessage(item, mico::event::model::UNEXPECTED_ERROR, "Unable to set Java Environment", "Java environment not set to current thread scope!");
    return;
  }

  for (auto kv : resultMap) {
    LOG_DEBUG("Creating annotation for frame %d", kv.first);

    objectreco::ObjectRecognitionResults &currRes = kv.second;

    for (objectreco::Object obj : currRes.objects) {

     assert(obj.region.size() == 4);

     using namespace jnipp::com::github::anno4j::model::impl::selector;
     using namespace jnipp::eu::mico::platform::anno4j::model::impl::targetmmm;
     using namespace jnipp::eu::mico::platform::anno4j::model::impl::bodymmm;
     using namespace jnipp::eu::mico::platform::anno4j::model::namespaces;
     using namespace jnipp::java::lang;
     using namespace jnipp::org::openrdf::repository::object;

     //as a workaround for gaining speed we will use this connection to create the
     //body and fragment classes
     jnipp::LocalRef<ObjectConnection> jObjectConnection =
         resource->getPersistenceService().getAnno4j()->getObjectRepository()->getConnection();

     resource->getPersistenceService().checkJavaExceptionThrow();

     if (!jObjectConnection)
       throw std::runtime_error("Could not retrieve Object connection");

     jObjectConnection->begin();

     jnipp::LocalRef<FragmentSelector> jFragmentSelector = item->createObjectNoCommit(jObjectConnection,FragmentSelector::clazz());
     jFragmentSelector->setSpatialFragment
         (Integer::construct(obj.region[0].posX),
          Integer::construct(obj.region[0].posY),
          Integer::construct(obj.region[1].posX-obj.region[0].posX),
          Integer::construct(obj.region[2].posY-obj.region[1].posY));

     jnipp::LocalRef<SpecificResourceMMM>   jBody = item->createObjectNoCommit(jObjectConnection, FaceDetectionBodyMMM::clazz());
     jnipp::LocalRef<SpecificResourceMMM> jTarget = item->createObjectNoCommit(jObjectConnection, SpecificResourceMMM::clazz());

     jTarget->setSelector(jFragmentSelector);

     jObjectConnection->commit();
     jObjectConnection->close();


     std::shared_ptr<mico::persistence::model::Part> newPart =
         item->createPart(getServiceID());

     std::shared_ptr<mico::persistence::model::Resource> newPartResource =
         std::dynamic_pointer_cast<mico::persistence::model::Resource>(newPart);

     newPartResource->setSemanticType("Face detected inside the input asset");
     newPartResource->setSyntacticalType(MMMTERMS::FACE_DETECTION_BODY->std_str());
     newPart->addInput(resource);
     newPart->setBody(jBody);
     newPart->addTarget(jTarget);

     resp.sendNew(item,newPartResource->getURI());
    }
  }

}


void FaceDetectionService::call(mico::event::AnalysisResponse& resp,
          std::shared_ptr< mico::persistence::model::Item > item,
          std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources,
          std::map<std::string,std::string>& params)
{
  std::map<uint64_t, objectreco::ObjectRecognitionResults > resMap;
  objectreco::VideoMetaType                                 meta;

  if (resources.size() != 1) {
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "Wrong number of input resources",
                          "CCV Face Detection expects exactly one input resource");
    return;
  }
  std::shared_ptr<Resource> itemResource = std::dynamic_pointer_cast<Resource>(item);
  std::shared_ptr<Resource> inResource = resources[0];

  if(inResource) {
    std::istream* instream = nullptr;
    std::string cpType = "";
    if(inResource->hasAsset()){
    	cpType = inResource->getAsset()->getFormat();
    }

    bool isPNG   = !cpType.compare("image/png");
    bool isJPEG  = !cpType.compare("image/jpeg");
    bool isVideo = boost::algorithm::find_first(m_mimeType, "video/");

    if (!isPNG && !isJPEG && !isVideo) {
      resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                            "CCV Face Detection expects png, jpeg or video as resource",
                            "Wrong resource type");
      return;
    } else {
      m_imgCounter++;
      try {
        std::shared_ptr<Asset> inAsset = inResource->getAsset();
        instream = inAsset->getInputStream();
      } catch (const std::exception& e) {
        LOG_ERROR("Could not fetch input stream of resource %s: %s ",
                  inResource->getURI().stringValue().c_str(),
                  e.what());
        resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                              "CCV Face Detection - fetching input failed",
                              "resource fetch error");
        return;
      } catch (const std::string& e) {
        LOG_ERROR("Could not fetch input stream of content part %s: %s ",
                  inResource->getURI().stringValue().c_str(),
                  e.c_str());
        resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                              "CCV Face Detection - fetching input failed",
                              "resource fetch error");
        return;
      } catch (...) {
        LOG_ERROR("Could not fetch input stream of content part %s! No message provided",
                  inResource->getURI().stringValue().c_str());
        resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                              "CCV Face Detection - fetching input failed. No message provided",
                              "resource fetch error");
        return;
      }
      if (instream == nullptr) {
        LOG_ERROR("Input stream of content part %s empty. Cannot analyse anything.",
                  inResource->getURI().stringValue().c_str());
        resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                              "CCV Face Detection - input stream is empty",
                              "empty resource error");
        return;
      }
    }

    //do the detection
    if (boost::algorithm::find_first(m_mimeType, "video/")) {
      FaceDetectionStatus stat = doVideoExtraction(instream, resMap, meta, m_timeStep);
      if (stat != true) {
        resp.sendErrorMessage(item, stat.error_code, "video face detection failed", stat.error_msg);
        return;
      }
    } else {
      FaceDetectionStatus stat = doImageExtraction(instream, resMap, meta);
      if (stat != true) {
        resp.sendErrorMessage(item, stat.error_code, "image face detection failed", stat.error_msg);
        return;
      }

    }

    //create annotation via anno4cpp
    doSemanticAnnotation(resp, resMap, item, inResource);

    //send native annotation as part
    // write xml text to a new content part
    std::shared_ptr<Part> txtOutPart = item->createPart(getServiceID());
    std::shared_ptr<Resource> txtOutResource = std::dynamic_pointer_cast<Resource>(txtOutPart);
    txtOutResource->setSyntacticalType( "mico:FaceDetectionXML" );
    txtOutResource->setSemanticType("Face detection annotation in xml format" );
    txtOutPart->addInput(inResource);

    std::shared_ptr<Asset> txtOutAsset = txtOutResource->getAsset();
    txtOutAsset->setFormat("text/vnd.fhg-ccv-facedetection+xml");
    std::ostream* out = txtOutAsset->getOutputStream();

    LOG_DEBUG("Writing result xml to ofstream...");
    objectreco::ObjectRecoOutputHandler outHandler(resMap, meta);
    outHandler.toXmlStream(*out);
    delete out;

    // notify broker that we created a new content part
    resp.sendNew(item, txtOutResource->getURI());
    resp.sendFinish(item);

  } else {
    LOG_ERROR("Resource of item %s is null!", itemResource->getURI().stringValue().c_str());
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "No resource found",
                          "CCV Face Detection - input resource does not exist");
  }
}
