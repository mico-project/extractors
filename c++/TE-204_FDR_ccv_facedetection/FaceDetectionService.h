#include "EventManager.hpp"

extern "C" {
#include <ccv.h>
}

namespace objectreco {
  class ObjectRecognitionResults;
  class VideoMetaType;
}

// for constant RDF property definitions of common vocabularies
#include "vocabularies.hpp"

// this namespace contains EventManager and AnalysisService
using namespace mico::event;

// this namespace contains Item, Part, Resource etc
using namespace mico::persistence::model;

struct FaceDetectionStatus {
public:
  FaceDetectionStatus(bool success,
                      mico::event::model::ErrorCodes code,
                      const std::string& msg)
    : success(success), error_code(code), error_msg(msg) {}

  bool                           success;
  mico::event::model::ErrorCodes error_code;
  std::string                    error_msg;

  bool operator !=(const bool& status) {
    return success != status;
  }

};

class FaceDetectionService : public AnalysisService {

public:

  FaceDetectionService(std::string simpleType, std::string mimeType, const double& timeStep);

  ~FaceDetectionService();

  void call(mico::event::AnalysisResponse& resp,
            std::shared_ptr< mico::persistence::model::Item > item,
            std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources,
            std::map<std::string,std::string>& params);
protected:

  void extractSingleFrame(ccv_dense_matrix_t* inImg,
                          std::map<uint64_t, objectreco::ObjectRecognitionResults >& resMap,
                          const unsigned long& lFrameId);

  FaceDetectionStatus doVideoExtraction(std::istream* instream, std::map<uint64_t,
                         objectreco::ObjectRecognitionResults >& resMap,
                         objectreco::VideoMetaType& meta,
                         const double& timeStepS = -1);

  FaceDetectionStatus doImageExtraction(std::istream* instream, std::map<uint64_t,
                         objectreco::ObjectRecognitionResults >& resMap,
                         objectreco::VideoMetaType& meta);

  void doSemanticAnnotation(AnalysisResponse &resp, std::map<uint64_t, objectreco::ObjectRecognitionResults >& resultMap,
                            std::shared_ptr<mico::persistence::model::Item> item, std::shared_ptr<mico::persistence::model::Resource> resource);
private:
  ccv_scd_classifier_cascade_t* m_pcascade;
  std::string                   m_mimeType;
  double                        m_timeStep;
  size_t                        m_imgCounter;
};
