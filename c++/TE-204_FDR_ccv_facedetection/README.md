[TOC]

# TE-204 CCV face detector for videos

# Dependencies

external dependencies:

* boost library (system, log, log_setup)
* MICO Platform API 
* protobuf (transitive dependency through mico_event)
* libccv
* xml_1_0 (TiXml)


# Build setting and environment

The ccv face detector uses the following additional CMake environment variables during build configuration:

* TE204_RESOURCE_DIR - directory containing the training model sqlite3 file [default: CMAKE_CURRENT_SOURCE_DIR]

A typical build call could look like:

    cmake -DMICOPlatformAPI_HOME=[path to MICO platform API] -DCMAKE_PREFIX_PATH=[semicolon seperated list of paths to local installs such as protobuf and opencv] -DTE204_RESOURCE_DIR=[path to yml-model files] [path to extractor source]
