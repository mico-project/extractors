#include <fstream>
#include <sys/wait.h>
#include "AVConvService.hpp"
#include "Daemon.hpp"
#include "Logging.hpp"



// for constant RDF property definitions of common vocabularies
#include "vocabularies.hpp"

namespace DC = mico::rdf::vocabularies::DC;
namespace MA = mico::rdf::vocabularies::MA;

namespace mico {
    namespace extractors {
        // helper function to get time stamp
        std::string getTimestamp() {
            time_t now;
            time(&now);
            char buf[sizeof "2011-10-08T07:07:09Z"];
            strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));
            return std::string(buf);
        }

        AVConvService::AVConvService(std::string const &serviceID, std::string const &requires, std::string const &provides, std::string const &queue)
                : AnalysisService(serviceID, requires, provides, queue) {

        }

        AVConvService::~AVConvService() {
        }

        void AVConvService::call(mico::event::AnalysisResponse& resp, mico::persistence::ContentItem &ci, mico::rdf::model::URI &object) {
            // create temporary files for video and audio track
            char tmpfile1 [L_tmpnam];
            char tmpfile2 [L_tmpnam];

            tmpnam(tmpfile1);
            tmpnam(tmpfile2);


            std::string videofile(tmpfile1);
            std::string audiofile(tmpfile2);
            audiofile += ".wav";


            LOG_DEBUG("writing video to temporary file %s", videofile.c_str());

            // retrieve the content part identified by the object URI
            Content* videoPart = ci.getContentPart(object);
            std::istream* vin = videoPart->getInputStream();

            // write content data to a temporary file
            std::ofstream vout(videofile);
            vout <<  vin->rdbuf();
            vout.close();
            delete vin;
            delete videoPart;

            LOG_DEBUG("converting %s to %s)", videofile.c_str(), audiofile.c_str());

            // execute avconc on file, write audio output to another temporary file
            pid_t pid = -1;
            pid = fork();
            // exit if something went wrong with the fork
            if(pid < 0)
            {
                std::cerr<<"\nFork failed!\n";
                exit(1);
            }
            // this code only gets executed by the child process
            else if(pid == 0) {
                execlp("avconv", "avconv", "-i", videofile.c_str(), "-ar", "8000", audiofile.c_str(), 0);
            }
            // the parent process executes here
            else
            {
                LOG_DEBUG("waiting for avconv to finish execution ...");
                // use the "wait" function to wait for the child to complete
                waitpid(pid, NULL, 0);
                LOG_DEBUG("avconv finished!");
            }

            // create new object, read data from second temporary file and write it to stream
            Content *audioPart = ci.createContentPart();

            audioPart->setType(getProvides());

            // set some metadata properties (provenance information etc)
            audioPart->setRelation(DC::creator, getServiceID());
            audioPart->setRelation(DC::provenance, getServiceID());
            audioPart->setProperty(DC::created, getTimestamp());
            audioPart->setRelation(DC::source, object.stringValue());
            audioPart->setRelation("http://www.w3.org/1999/02/22-rdf-syntax-ns#type", MA::AudioTrack);
            audioPart->setProperty(MA::samplingRate, "8000");
            audioPart->setProperty(MA::hasFormat, getProvides());

            std::ostream* aout = audioPart->getOutputStream();
            std::ifstream ain(audiofile);
            *aout << ain.rdbuf();
            delete aout;

            // notify broker that we created a new content part
            resp.sendNew(ci, audioPart->getURI());
            resp.sendFinish(ci, object);

            delete audioPart;

            unlink(videofile.c_str());
            unlink(audiofile.c_str());
        }
    }
}

using namespace mico::extractors;

#ifndef EXTRACTOR_BUILD_AS_DAEMON
#include "EventManager.hpp"
using namespace mico::event;

EventManager* mgr = 0;
MP4ToWavService*mp4ToWavService = 0;
bool loop = true;

void signal_handler(int signum) {
    std::cout << "shutting down a/v splitter service ... " << std::endl;

    if (mgr)
        mgr->unregisterService(mp4ToWavService);

    if (mp4ToWavService)
        delete mp4ToWavService;

    if (mgr)
        delete mgr;

    loop = false;
}
#endif


int main(int argc, char **argv) {
    if(argc != 4 && argc != 2) {
        std::cerr << "Usage: "<< argv[0] << " SERVER_IP [USER PASSWORD]" << std::endl;
        exit(1);
    }



#if defined EXTRACTOR_BUILD_AS_DAEMON
    if(!strcmp(argv[1], "-k")) {
        return mico::daemon::stop(argv[0]);
    } else {

        // create a new instance of a MICO daemon, auto-registering two instances of the OCR analysis service
        return mico::daemon::start(argv[0], argv[1], argv[2], argv[3], {new MP4ToWavService()});

    }
#else
    const char* server_name = argv[1];
    const char* mico_user   = argv[2];
    const char* mico_pass   = argv[3];

    mgr = new EventManager(server_name, mico_user, mico_pass);

    mp4ToWavService = new MP4ToWavService();

    mgr->registerService(mp4ToWavService);

    signal(SIGINT,  &signal_handler);
    signal(SIGTERM, &signal_handler);
    signal(SIGHUP,  &signal_handler);

    while(loop) {
        sleep(1);
    }
#endif
}
