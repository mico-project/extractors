#ifndef HAVE_AVCONF_SERVICE_HPP
#define HAVE_AVCONF_SERVICE_HPP 1

#include <AnalysisService.hpp>

// this namespace contains EventManager and AnalysisService
using namespace mico::event;

// this namespace contains Content, ContentItem, etc
using namespace mico::persistence;

// this namespace contains the RDF data model
using namespace mico::rdf::model;

namespace mico {
    namespace extractors {
        class AVConvService : public mico::event::AnalysisService {


        public:
            AVConvService(std::string const &serviceID, std::string const &requires, std::string const &provides, std::string const &queue);

            virtual ~AVConvService();


            virtual void call(mico::event::AnalysisResponse& resp, mico::persistence::ContentItem &ci, mico::rdf::model::URI &object) override;
        };

        class MP4ToWavService : public AVConvService {


        public:
            MP4ToWavService()
                    : AVConvService("http://www.mico-project.eu/extractors/avconv/mp4-wav-extractor", "video/mp4", "audio/x-wav", "mp4-wav-extractor") {
            }
        };
    }
}

#endif
