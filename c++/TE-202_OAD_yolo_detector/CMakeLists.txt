cmake_minimum_required(VERSION 2.8.12)

project("mico-extractor-animaldetection-yolo")

set(TARGET_NAME mico-extractor-animaldetection-yolo)
set(TARGET_SHORT_DESC "MICO service for detection of animals in images.")
set(TARGET_DESC "The service receives images and produces region annotations. Currently a Java extractor is required to obtain RDF annoations.")
set(TARGET_VERSION_MAJOR 1)
set(TARGET_VERSION_MINOR 0)
set(TARGET_VERSION_PATCH 2)
set(TARGET_VERSION ${TARGET_VERSION_MAJOR}.${TARGET_VERSION_MINOR}.${TARGET_VERSION_PATCH})

option(USE_CUDA_GPU Off "If set to On extractor will use GPU acceleration.")

add_definitions(-DMICO_EXTRACTOR_ID=${TARGET_NAME})
add_definitions(-DMICO_EXTRACTOR_VERSION=${TARGET_VERSION})
add_definitions(-DMICO_EXTRACTOR_MODE_ID=YoloDetector)

get_filename_component(ADD_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../modules/ ABSOLUTE)
get_filename_component(3RD_PARTY_HOME ${CMAKE_CURRENT_SOURCE_DIR}/../3rd_party/ ABSOLUTE)
get_filename_component(SHARED_HOME ${CMAKE_CURRENT_SOURCE_DIR}/../../share/ ABSOLUTE)

set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ADD_MODULE_PATH})
set (CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} ${3RD_PARTY_HOME})


set(EXTERNALS_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/externals)
get_filename_component(DARKNET_ROOT ${EXTERNALS_PREFIX}/src/darknet_project/ ABSOLUTE)

include(ExternalProject)

set(DARKNET_INSTALL_ROOT ${CMAKE_CURRENT_BINARY_DIR}/darknet_install)

if (USE_CUDA_GPU)
  add_definitions(-DGPU)
  set(DARKNET_CMAKE_FILE ${CMAKE_CURRENT_SOURCE_DIR}/CMakeLists.txt.darknet.gpu)
else()
  set(DARKNET_CMAKE_FILE ${CMAKE_CURRENT_SOURCE_DIR}/CMakeLists.txt.darknet)
endif()

ExternalProject_Add(darknet_project
    PREFIX ${EXTERNALS_PREFIX}
    GIT_REPOSITORY https://github.com/cvjena/darknet.git
    GIT_TAG 0742c8c80c7ea728db3475255d80fe939b718fa4
    UPDATE_COMMAND ${CMAKE_COMMAND} -E copy_if_different ${DARKNET_CMAKE_FILE} ${DARKNET_ROOT}/CMakeLists.txt
    CMAKE_ARGS
      -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
      -DCMAKE_INSTALL_PREFIX:PATH=${DARKNET_INSTALL_ROOT}
)


# ExternalProject_Add_Step(javadeps MVN_BUILD
#   COMMAND mvn clean package
#   DEPENDEES download
#   WORKING_DIRECTORY <SOURCE_DIR>
# )


find_package( Boost QUIET REQUIRED COMPONENTS log program_options filesystem system)

if (USE_CUDA_GPU)
  find_package( CUDA REQUIRED)
else()
  set(CUDA_INCLUDE_DIRS )
  set(CUDA_LIBRARIES )
  set(CUDA_CUBLAS_LIBRARIES )
  set(CUDA_curand_LIBRARY )
endif()

#set(MICOPlatformAPI_DEBUG On)
find_package( MICOPlatformAPI 3.0.0 REQUIRED)
find_package( JNI REQUIRED )

if(NOT TARGET mico_extractor_commons)
  add_subdirectory(../common common)
endif()

if (UNIX)
  add_definitions(-std=c++11)
endif()

if(NOT DEFINED TE202_RESOURCE_DIR)
  get_filename_component(TE202_RESOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/config ABSOLUTE)
  message(STATUS "TE202_RESOURCE_DIR not defined - using " ${TE202_RESOURCE_DIR})
else()
  get_filename_component(TE202_RESOURCE_DIR ${TE202_RESOURCE_DIR} ABSOLUTE)
  message(STATUS "TE202_RESOURCE_DIR is ${TE202_RESOURCE_DIR}")
endif()

add_definitions(-DRESOURCE_DIR="${TE202_RESOURCE_DIR}")
add_definitions(-DRESOURCE_INSTALL_DIR="${CMAKE_INSTALL_PREFIX}/share")
add_definitions(-DOAD_EXTRACTOR_VERSION="${TARGET_VERSION}")

set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-s")


include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}/../common
  ${Boost_INCLUDE_DIRS}
  ${DARKNET_INSTALL_ROOT}/include
  ${MICOPlatformAPI_INCLUDE_DIRS}
  ${JNI_INCLUDE_DIRS}
  ${3RD_PARTY_HOME}/include/io
  ${3RD_PARTY_HOME}/include/objectreco
  ${3RD_PARTY_HOME}/include/xml
  ${CUDA_INCLUDE_DIRS}
)

link_directories(
  ${DARKNET_INSTALL_ROOT}/lib
  ${3RD_PARTY_HOME}/lib
)

add_executable(${TARGET_NAME}
  main.cpp
  YOLODetectorService
  
)

set(EXTRACTOR_COMMON_LINK_LIBRARIES
  ${CUDA_LIBRARIES}
  ${CUDA_CUBLAS_LIBRARIES}
  ${CUDA_curand_LIBRARY}
  darknet
  mico_extractor_commons
  xml_1_0
  ${MICOPlatformAPI_LIBRARIES}
  ${JNI_LIBRARIES}
  ${Boost_LIBRARIES}	
)

if (USE_CUDA_GPU)
  list(APPEND EXTRACTOR_COMMON_LINK_LIBRARIES darknet_cuda)
endif()

target_link_libraries(${TARGET_NAME}
  ${EXTRACTOR_COMMON_LINK_LIBRARIES}
)

add_dependencies(${TARGET_NAME} darknet_project)

set_target_properties(${TARGET_NAME} PROPERTIES BUILD_WITH_INSTALL_RPATH Off)

#installation step
string (TIMESTAMP COPYRIGHT_YEAR "%Y")
set(COPYRIGHT_OWNER "Fraunhofer IDMT")
configure_file(${SHARED_HOME}/copyright_apache2.in ${CMAKE_CURRENT_BINARY_DIR}/copyright @ONLY) 
configure_file(${SHARED_HOME}/native-extractor-config-template.in ${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}-config @ONLY)

file(GLOB MODEL_FILES ${CMAKE_CURRENT_SOURCE_DIR}/models/*.*)

install(TARGETS ${TARGET_NAME} RUNTIME DESTINATION bin COMPONENT ${TARGET_NAME})
install(FILES ${MODEL_FILES} DESTINATION share/${TARGET_NAME} COMPONENT ${TARGET_NAME})
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/copyright DESTINATION share/doc/${TARGET_NAME}/ COMPONENT ${TARGET_NAME})
install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}-config DESTINATION bin COMPONENT ${TARGET_NAME})


if (NOT USE_CUDA_GPU)
  message("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
  message("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
  message("+++ You've just compiled then MICO animaldetection yolo extractor +++")
  message("+++ as CPU version. Since this extractor uses a neural network    +++")
  message("+++ for the detection of animals  it will perform  very slow      +++")
  message("+++ (about 40s per image) and thus it is highly recommended to    +++")
  message("+++ compile the extractor with GPU support. To do this, set the   +++")
  message("+++                                                               +++")
  message("+++                    -DUSE_CUDDA_GPU=On                         +++")
  message("+++                                                               +++")
  message("+++ option in this cmake make call. Make sure you have a fairly   +++")
  message("+++ recen nvidia  graphics card and the CUDA framework installed! +++")
  message("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
  message("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
endif()

# configuration for Debian packaging via CMake
IF(EXISTS "${CMAKE_ROOT}/Modules/CPack.cmake")
  INCLUDE(InstallRequiredSystemLibraries)

  SET(CPACK_GENERATOR "DEB") 
  SET(CPACK_PACKAGE_NAME ${TARGET_NAME})
  SET(CPACK_PACKAGE_FILE_NAME "${TARGET_NAME}_${TARGET_VERSION}_${CMAKE_SYSTEM_NAME}_${CMAKE_SYSTEM_PROCESSOR}")
  SET(CPACK_PACKAGE_VENDOR "Fraunhofer IDMT")
  SET(CPACK_PACKAGE_DESCRIPTION ${TARGET_DESC})
  SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY ${TARGET_SHORT_DESC})
  SET(CPACK_SET_DESTDIR On)
  SET(CPACK_INSTALL_PREFIX /usr)
  SET(CPACK_PACKAGE_CONTACT "Christian Weigel <christian.weigel@idmt.fraunhofer.de>")
  SET(CPACK_PACKAGE_VERSION_MAJOR "${TARGET_VERSION_MAJOR}")
  SET(CPACK_PACKAGE_VERSION_MINOR "${TARGET_VERSION_MINOR}")
  SET(CPACK_PACKAGE_VERSION_PATCH "${TARGET_VERSION_PATCH}")
  SET(CPACK_STRIP_FILES On)

  #Debian specifc package informations (add package dependencies here!)
  SET(CPACK_DEBIAN_PACKAGE_SHLIBDEPS On)
  SET(CPACK_DEBIAN_PACKAGE_DEPENDS "mico-extractors-3rdparty(>=2.0.0), curl(>=7.38)")
  SET(CPACK_DEBIAN_PACKAGE_SECTION "non-free/science")
  SET(CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA "${CMAKE_CURRENT_SOURCE_DIR}/debian/preinst;${CMAKE_CURRENT_SOURCE_DIR}/debian/postrm" )
  INCLUDE(CPack)
 
 ENDIF(EXISTS "${CMAKE_ROOT}/Modules/CPack.cmake")
