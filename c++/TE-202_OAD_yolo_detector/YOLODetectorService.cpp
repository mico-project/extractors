#include <ctime>
#include <fstream>
#include <algorithm>
#include <thread>
#include <chrono>

#include <boost/regex.hpp>

#include <Logging.hpp>
#include "YOLODetectorService.h"
#include "ObjectRecoOutputHandler.h"
#include <MICOExtractorID.h>


//====== copied from yolo.c BEGIN:
extern "C" {
  #include "cost_layer.h"
  #include "utils.h"
  #include "parser.h"
  // std includes
  #include "unistd.h"
  // global variables
  #include "yolo_global_variables.h"
  char ** c_class_names;
  int i_num_cl;

  void convert_yolo_detections(float *predictions,
                               int classes,
                               int num /*number of predicted boxes per cell*/,
                               int square,
                               int side /*number of cells per dimension*/,
                               int w,
                               int h,
                               float thresh,
                               float **probs,
                               box *boxes,
                               int only_objectness)
  {
    int i,j,n;
    //int per_cell = 5*num+classes;
    for (i = 0; i < side*side; ++i)
    {
      // cell index in x and y dimension
      int row = i / side;
      int col = i % side;
      // run over all predicted boxes for that cell
      for(n = 0; n < num; ++n)
      {
        int index     = i*num + n;
        int p_index   = side*side*classes + i*num + n;
        float scale   = predictions[p_index];
        int box_index = side*side*(classes + num) + (i*num + n)*4;

        boxes[index].x = (predictions[box_index + 0] + col) / side * w;
        boxes[index].y = (predictions[box_index + 1] + row) / side * h;
        boxes[index].w = pow(predictions[box_index + 2], (square?2:1)) * w;
        boxes[index].h = pow(predictions[box_index + 3], (square?2:1)) * h;

        for(j = 0; j < classes; ++j){
            int class_index = i*classes;
            float prob      = scale*predictions[class_index+j];
            probs[index][j] = (prob > thresh) ? prob : 0;
        }
        if(only_objectness){
            probs[index][0] = scale;
        }
      }
    }
  }
}
//====== copied from yolo.c END



// define dublin core vocabulary shortcut
namespace DC = mico::rdf::vocabularies::DC;

// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2011-10-08T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}


void runtime_error(std::string what)
{

}


YOLODetectorService::YOLODetectorService(std::string mimeType, std::string configFile, std::string weightFile, std::string classesFile)
  : AnalysisService(mico::extractors::getPrecompExtractorId(),mico::extractors::getPrecompExtractorModeId(),mico::extractors::getPrecompExtractorVersion(),
    mimeType, "text/vnd.fhg-objectdetection+xml")
{
  const char * c_list_with_classnames = classesFile.c_str();

  FILE * fp_classlist;

  if( access( c_list_with_classnames, F_OK ) == -1 ) {
    LOG_ERROR("YOLODetector::Constructor: Classlist does not exist!");
  }


  fp_classlist = fopen( c_list_with_classnames, "r" );
  if (fp_classlist == NULL) {
    LOG_ERROR("YOLODetector::Constructor: Classlist is not readable!");
  }
  fclose ( fp_classlist );

  // how many classes are known?
  // assume empty last line
  i_num_cl = count_lines_in_file ( c_list_with_classnames ) - 1;

  // allocate enough memory for our global variables
  c_class_names    = (char**) calloc( i_num_cl, sizeof(char*));


  fp_classlist = fopen( c_list_with_classnames, "r" );

  char * line = NULL;
  size_t len  = 0;
  ssize_t i_line_length;
  int i_line_cnt;

  // now read line by line, i.e., filename after filename for all test images
  //while ( (i_line_length = getline(&line, &len, fp_classlist)) != -1)
  //
  LOG_INFO("YOLODetector::Constructor: Known categories:");
  for ( i_line_cnt = 0; i_line_cnt < i_num_cl; i_line_cnt++ ) {
    i_line_length = getline(&line, &len, fp_classlist);

    if ( i_line_length == -1 ) {
      // this should not happen....
      break;
    }

    // read filename
    char c_classname [i_line_length];
    // remove newline character which is read returned by getline, too
    memcpy(c_classname, line+ 0 /* Offset */, i_line_length-1 /* Length */);
    // correctly end the char array in c syntax
    c_classname [i_line_length-1] = '\0';

    // now copy the category name to the global char array
    c_class_names[i_line_cnt] = (char*) calloc(i_line_length, sizeof(char));
    memcpy(c_class_names[i_line_cnt], c_classname+ 0 /* Offset */, i_line_length /* Length */);
    LOG_INFO("       %s", c_classname);
  }
  fclose ( fp_classlist );

  // step 1 - read network and load pre-trained weights
  // this needs to be done only once
  //
  // ...load network layout
  LOG_INFO("YOLODetector::Constructor: read network config: %s", configFile.c_str());
  m_net = parse_network_cfg((char*)configFile.c_str());
  // ...and pre-computed parameter values
  LOG_INFO("YOLODetector::Constructor: load weights from: %s", weightFile.c_str());
  load_weights(&m_net, (char*)weightFile.c_str());

  // grep last layer which outputs detection responses
  m_l = m_net.layers[m_net.n-1];
  set_batch_network(&m_net, 1);
  srand(2222222);

  // allocate memory for prediction results
  m_boxes = (box*)    calloc(m_l.side*m_l.side*m_l.n, sizeof(box));
  m_probs = (float**) calloc(m_l.side*m_l.side*m_l.n, sizeof(float *));
  for(int j = 0;         j < m_l.side*m_l.side*m_l.n; ++j) {
    m_probs[j] = (float*) calloc(m_l.classes, sizeof(float *));
  }
}

YOLODetectorService::~YOLODetectorService()
{
  // clean-up
  for (int i_cl=0; i_cl <  i_num_cl; i_cl++ ) {
    free( c_class_names[i_cl] );
  }
  free ( c_class_names );

  //free m_boxes and m_probs ?!
  for(int j = 0; j < m_l.side*m_l.side*m_l.n; ++j) {
    free( m_probs[j] );
  }
  free( m_probs );
  free( m_boxes );
}


void YOLODetectorService::call(mico::event::AnalysisResponse& resp, std::shared_ptr< mico::persistence::model::Item > item, std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources, std::map<std::string,std::string>& params)
{
  if (resources.size() != 1) {
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "YOLO Animal Detector expects exactly one input resource",
                          "Wrong number of input resources");
    return;
  }
  std::shared_ptr<Resource> itemResource = std::dynamic_pointer_cast<Resource>(item);
  std::shared_ptr<Resource> imgInResource = resources[0];

  if(imgInResource) {
    std::shared_ptr<Asset> imgInAsset = imgInResource->getAsset();
    std::istream* in = imgInAsset->getInputStream();

    //read image
    char * c_filename = (char*)"/tmp/currentyolodetectorimage";
    std::vector<char> buf =
        std::vector<char>(std::istreambuf_iterator<char>(*in), std::istreambuf_iterator<char>());
    std::ofstream currentFile;
    currentFile.open(c_filename);
    for (unsigned int myIter=0;myIter < buf.size();++myIter)
      currentFile << buf[myIter];
    currentFile.close();
    delete in;

    objectreco::ObjectRecognitionResults res;

    //>>>>>>>>>>> DETECTION HERE <<<<<<<<<<<<<<<
    LOG_INFO("YOLODstringetector starts ");
    float f_nms_threshold       = 0.5;
    float thresh                = .2;

    // load image from given filename
    image im = load_image_color(c_filename,0,0);
    objectreco::VideoMetaType meta(0, 100, im.w, im.h);
    LOG_INFO("YOLODetector: image loaded width: %i height: %i", im.w, im.h);

    // adapt image to network input size
    image sized = resize_image(im, m_net.w, m_net.h);
    float *X = sized.data;

    // do actual prediction
#ifdef GPU
    float *predictions = network_predict_gpu(m_net, X);
#else
    float *predictions = network_predict(m_net, X);
#endif
    LOG_INFO("YOLODetector prediction complete");

    // convert results to original image size
    convert_yolo_detections(predictions, m_l.classes, m_l.n /*number of predicted boxes per cell*/, m_l.sqrt, m_l.side, 1 /*int w*/, 1/*int h*/, thresh, m_probs, m_boxes, 0 /*int only_objectness*/);

    // apply non-maximum suppresion to filter overlapping responses
    if (f_nms_threshold) {
      do_nms_sort(m_boxes, m_probs, m_l.side*m_l.side*m_l.n, m_l.classes, f_nms_threshold);
    }

    // wite bounding boxes
    int numbox = m_l.side*m_l.side*m_l.n;

    LOG_INFO("YOLODetector found %i animals ", numbox);
    for(int ibox = 0; ibox < numbox; ++ibox) {
      int i_class = max_index(m_probs[ibox], m_l.classes);
      float prob = m_probs[ibox][i_class];
      if ( prob < thresh )
          continue;
      box b = m_boxes[ibox];

      int left  = (b.x-b.w/2.)*im.w;
      int right = (b.x+b.w/2.)*im.w;
      int top   = (b.y-b.h/2.)*im.h;
      int bot   = (b.y+b.h/2.)*im.h;

      if(left < 0) left = 0;
      if(right > im.w-1) right = im.w-1;
      if(top < 0) top = 0;
      if(bot > im.h-1) bot = im.h-1;

      objectreco::Object o;
      o.uid = ibox;
      o.type = objectreco::ANIMAL;
      o.region.push_back(objectreco::Point(left, top, 0, "topleft"));
      o.region.push_back(objectreco::Point(right, top, 1));
      o.region.push_back(objectreco::Point(right, bot, 2));
      o.region.push_back(objectreco::Point(left, bot, 3));
      res.objects.push_back(o);
      res.labelMapping[ibox].push_back(objectreco::ObjectLabel(ibox, c_class_names[i_class], prob));
      LOG_INFO("   [%i] %s %f", ibox, c_class_names[i_class], prob);
    }

     //free memory
    free_image(im);
    free_image(sized);
    //>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<

    // write xml text to a new content part
    std::shared_ptr<Part> txtOutPart = item->createPart(mico::persistence::model::URI(getServiceID()));
    std::shared_ptr<Resource> txtOutResource = std::dynamic_pointer_cast<Resource>(txtOutPart);
    txtOutResource->setSyntacticalType( "mico:ObjectDetectionXml" );
    txtOutResource->setSemanticType("Animal detection annotation in xml format");
    txtOutPart->addInput(imgInResource);


    std::shared_ptr<Asset> txtOutAsset = txtOutResource->getAsset();
    std::ostream* out = txtOutAsset->getOutputStream();

    txtOutAsset->setFormat("text/vnd.fhg-objectdetection+xml");

    std::map<uint64_t, objectreco::ObjectRecognitionResults > resMap;
    resMap[0] = res;
    objectreco::ObjectRecoOutputHandler outHandler(resMap, meta);
    outHandler.toXmlStream(*out);

    delete out;

    // notify broker that we created a new part
    resp.sendNew(item, txtOutResource->getURI());
    resp.sendFinish(item);

  } else {
    LOG_ERROR("Resource of item %s is null!", itemResource->getURI().stringValue().c_str());
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "YOLO Animal Detector - Null resource received",
                          "Null Resource");
  }
}


