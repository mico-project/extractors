#include "YOLODetectorService.h"
#include <Daemon.hpp>
#include <Logging.hpp>

#include <FileOperations.h>

#include <string>
#include <fstream>

#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>

namespace MEX = mico::extractors;
namespace po  = boost::program_options;


/****** globals ******/
EventManager* mgr = 0;
YOLODetectorService* yolodetectorService = 0;
bool loop = true;

void signal_handler(int signum) {
  std::cout << "shutting down YOLODetector service ... " << std::endl;

  if (mgr) {
    mgr->unregisterService(yolodetectorService);
  }

  if (yolodetectorService)
    delete yolodetectorService;

  if (mgr)
    delete mgr;

  loop = false;
}

static  std::string ip;
static  std::string user;
static  std::string passw;
static  std::string fmt;
static  std::string configFile;
static  std::string weightFile;
static  std::string classesFile;


static bool setCommandLineParams(int argc, char** argv, po::variables_map& vm)
{
  po::options_description desc("Allowed options");
  desc.add_options()
    ("serverIP,i",  po::value<std::string>(&ip)->required(), "IP of the MICO system server.")
    ("userName,u",  po::value<std::string>(&user)->required(), "MICO system user name")
    ("userPassword,p", po::value<std::string>(&passw)->required(), "MICO system user password")
    ("config,c", po::value<std::string>(&configFile)->required(), "The yolo *.cfg file for this animal detector instance.")
    ("weight,w", po::value<std::string>(&weightFile)->required(), "The yolo *.weights file for this animal detector instance.")
    ("classes,l", po::value<std::string>(&classesFile)->required(), "The yolo class names *.txt file for this animal detector instance.")
    ("inputFormat,f", po::value<std::string>(&fmt)->default_value(std::string("JPEG"),"JPEG"), "Expected input type (JPEG or PNG)")
    ("kill,k","Shuts down the service.")
    ("foreground,g","Runs the extractor as foreground process (by default it runs as daemon)")
    ("help,h","Prints this help message.");

  po::positional_options_description p;
  p.add("serverIP",1);
  p.add("userName",1);
  p.add("userPassword",1);

  bool printHelp = false;
  std::string ex("") ;
  try {
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run() , vm);
    po::notify(vm);
  } catch (std::exception& e) {
    ex = e.what();
    printHelp = true;
  }
  if (vm.count("help")) {
    printHelp = true;
  }

  if (printHelp) {
    if (ex.size() > 0)
      std::cout << std::endl << ex << "\n";
    std::cout << "\nUsage:   " << argv[0];
    for (unsigned int i=0; i < p.max_total_count(); ++i)
      std::cout << " " << p.name_for_position(i);
    std::cout << " [options]" << "\n";
    std::cout << "\n" << desc << "\n";
    return false;
  }
  return true;
}

/****** main ******/
int main(int argc, char **argv)
{
  po::variables_map vm;

  if (!setCommandLineParams(argc, argv, vm)) {
   exit(EXIT_FAILURE);
  }

  bool   doKill    = false;
  bool   asDaemon  = true;

  if (vm.count("kill")) {
    doKill = true;
  }

  if (vm.count("foreground")) {
    doKill   = false;
    asDaemon = false;
  }

  boost::to_lower(fmt);

  std::string img_type = "jpeg";

  if(fmt == "png"){
    img_type = "png";
  }

  std::string mime_type = std::string("image/") + img_type;

  std::string s_daemon_id(argv[0]);
  s_daemon_id += std::string("-") + std::string(img_type);

  if (!doKill && asDaemon)
     std::cout << "Launching animal detection daemon with id [" << s_daemon_id << "]" << std::endl;
  else if (doKill && asDaemon)
     std::cout << "Shutting down animal detection daemon with id ["<< s_daemon_id <<"]" << std::endl;
  else
     std::cout << "Starting animal detection extractor to analyse "<< mime_type << std::endl;

  if(doKill) {
    return mico::daemon::stop(s_daemon_id.c_str());
  }

  if (asDaemon) {
    mico::log::LoggingBackend* log_back = mico::daemon::createDaemonLogBackend();
    log_back->setLevel(mico::log::DEBUG);
    mico::log::set_log_backend (log_back);
    return mico::daemon::start(s_daemon_id.c_str(), ip.c_str(), user.c_str(), passw.c_str(),
    {new YOLODetectorService(mime_type, configFile, weightFile, classesFile)});
  } else {
    mico::log::LoggingBackend *logBack = new mico::log::StdOutBackend();
    logBack->setLevel(mico::log::DEBUG);

    mico::log::set_log_backend (logBack);

    mgr = new EventManager(ip.c_str(), user.c_str(), passw.c_str());
    yolodetectorService = new YOLODetectorService(mime_type, configFile, weightFile, classesFile);

    mgr->registerService(yolodetectorService);

    signal(SIGINT,  &signal_handler);
    signal(SIGTERM, &signal_handler);
    signal(SIGHUP,  &signal_handler);

    while(loop) {
      sleep(1);
    }
  }
}

