#include "EventManager.hpp"

// for constant RDF property definitions of common vocabularies
#include "vocabularies.hpp"

#ifdef GPU
//++++++++ USING GPU includes ++++++++
#include <cuda.h>

extern "C"{
  #include "image.h"
  #include "network.h"
}

#include "detection_layer.h"
#include "box.h"


#else

//++++++++ NOT USING GPU includes ++++++++
extern "C" {
  #include "network.h"
  #include "detection_layer.h"
  #include "box.h"
}

#endif


// this namespace contains EventManager and AnalysisService
using namespace mico::event;

// this namespace contains Content, ContentItem, etc
using namespace mico::persistence::model;

namespace objectreco {
  class ObjectRecognitionResults;
}

class YOLODetectorService : public AnalysisService {

public:

  /**
   * @brief YOLODetectorService::YOLODetectorService
   * @param mimeType The accepted input mime type (image/jpeg, image/png)
   *
   */
  YOLODetectorService(std::string mimeType, std::string configFile, std::string weightFile, std::string classesFile);

  ~YOLODetectorService();

  void call(mico::event::AnalysisResponse& resp,
            std::shared_ptr< mico::persistence::model::Item > item,
            std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources,
            std::map<std::string,std::string>& params);

private:

  network m_net;
  detection_layer m_l;
  box *m_boxes;
  float **m_probs;
};
