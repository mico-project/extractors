[TOC]

# TE-202 YOLO Detector for images

This extractor uses a convolutional neural network for animal detection. The model is not provided in the repository but will installed when you package the extractor as debian package and install it. Alternatevely you may download the model from

https://seafile.idmt.de/f/03570cf650/?raw=1

The network configuration as well as the class names (currently 10 animal classes) are located in the `models`  folder.

# Dependencies

external dependencies:

* boost library (system, log, log_setup, program_options, filesystem, serialization)
* MICO Platform API 
* protobuf (transitive dependency through mico_event)
* xml_1_0 (TiXml)
* https://github.com/cvjena/darknet
* When compiled with GPU support: CUDA Framework


# Build setting and environment

If you intend to build the extractor with GPU support (which is highly recommended), please make sure that you have installed a fairly recent NVIDIA graphics card along with the CUDA development framework.

You should then run CMake with the opion USE_CUDA_GPU=On:

    cmake -DUSE_CUDA_GPU=On [path to extractor sources]
