#include "ACDStableToneService.h"
#include <fstream>
#include<stdio.h>
#include<iostream>


// define dublin core vocabulary shortcut
namespace DC = mico::rdf::vocabularies::DC;

// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2011-10-08T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}

//helper function to convert from enum input myme type to string
std::string getServiceName(ACDStableToneService::InputFrequency f, ACDStableToneService::InputMimeType i){
  std::string out="http://www.mico-project.org/services/acd_stabletone_";
  switch(f) {
    case ACDStableToneService::ENF_50 : out=(out+std::string("50_"));
                                        break;
    case ACDStableToneService::ENF_60 : out=(out+std::string("60_"));
                                        break;
    default : std::string error_msg="ACDStableToneService() : CRITICAL ERROR: unhandled valid input frequency";
              std::cerr << error_msg << std::endl;
              throw std::logic_error(error_msg);
  }
  switch(i) {
    case ACDStableToneService::AUDIO_WAV   : return out+std::string("wav");
    case ACDStableToneService::AUDIO_X_WAV : return out+std::string("x-wav");
    case ACDStableToneService::AUDIO_WAVE  : return out+std::string("wave");
    default : std::string error_msg="ACDStableToneService() : CRITICAL ERROR: unhandled valid input mime type";
              std::cerr << error_msg << std::endl;
              throw std::logic_error(error_msg);
  }
}

std::string getServiceQueue(ACDStableToneService::InputFrequency f, ACDStableToneService::InputMimeType i){
  std::string out="acd-queue-";
  switch(f) {
    case ACDStableToneService::ENF_50 : out=(out+std::string("50-"));
                                        break;
    case ACDStableToneService::ENF_60 : out=(out+std::string("60-"));
                                        break;
    default : std::string error_msg="ACDStableToneService() : CRITICAL ERROR: unhandled valid input frequency";
              std::cerr << error_msg << std::endl;
              throw std::logic_error(error_msg);
  }
  switch(i) {
    case ACDStableToneService::AUDIO_WAV   : return out+std::string("wav");
    case ACDStableToneService::AUDIO_X_WAV : return out+std::string("x-wav");
    case ACDStableToneService::AUDIO_WAVE  : return out+std::string("wave");
    default : std::string error_msg="ACDStableToneService() : CRITICAL ERROR: unhandled valid input mime type";
              std::cerr << error_msg << std::endl;
              throw std::logic_error(error_msg);
  }
}



std::string getMimeType(ACDStableToneService::InputMimeType i){
  switch(i) {
    case ACDStableToneService::AUDIO_WAV   : return std::string("audio/wav");
    case ACDStableToneService::AUDIO_X_WAV : return std::string("audio/x-wav");
    case ACDStableToneService::AUDIO_WAVE  : return std::string("audio/wave");
    default : std::string error_msg="ACDStableToneService() : CRITICAL ERROR: unhandled valid input mime type";
              std::cerr << error_msg << std::endl;
              throw std::logic_error(error_msg);
  }
}


ACDStableToneService::ACDStableToneService(ACDStableToneService::InputFrequency f, ACDStableToneService::InputMimeType i)
  : AnalysisService(getServiceName(f,i), getMimeType(i), "text/vnd.fhg-acd-stabletone+xml", ""), m_micostabletone(0), m_targetFrequency(f)
{
  //instantiate the pointer to the component
  try{
    m_micostabletone=new mico::idmt::MicoENF("4HpuH8A7kCJKBfyfmpr6vcpH");
  }
  catch(std::runtime_error &e){
    
    //notify any error (-> wrong activation key)
    if(m_micostabletone != 0) delete m_micostabletone;
    std::string error_msg="ACDStableToneService() : Unable to allocate instance";
    std::cerr << error_msg << std::endl;
    throw std::runtime_error(error_msg);
  }
}

ACDStableToneService::~ACDStableToneService() {
  //delete the pointer to the component
  if(m_micostabletone != 0) delete m_micostabletone;
}

void ACDStableToneService::call(std::function<void(const ContentItem& ci, const URI& object)> resp, ContentItem& ci, URI& object)
{
  
  // retrieve the content part identified by the object URI
  Content* audioContent = ci.getContentPart(object);
   
  if(audioContent != NULL) {
    
    //and load istream of wav file
    std::istream* wavInputFile = audioContent->getInputStream();
    
    
    //start ACD - StableTone analysis
    
    //perform analysis
    std::string xmlOutputFile=std::to_string((long unsigned int) wavInputFile)+".xml";
    
    std::cout << "ACDStableToneService::call(): start processing" << std::endl;
    switch(m_targetFrequency) {
      case ACDStableToneService::ENF_50   : m_micostabletone->performAnalysis50Hz(wavInputFile, xmlOutputFile); 
                                            break;
      case ACDStableToneService::ENF_60   : m_micostabletone->performAnalysis60Hz(wavInputFile, xmlOutputFile); 
                                            break;
      default : std::string error_msg="ACDStableToneService() : CRITICAL ERROR: unhandled input frequency";
                std::cerr << error_msg << std::endl;
                throw std::logic_error(error_msg);
    }
    std::cout << "ACDStableToneService::call(): end processing" << std::endl;
    

    // Instantiate content part to hold the annotation
    Content *txtContent = ci.createContentPart();
    txtContent->setType("text/vnd.fhg-acd-stabletone+xml");
    
    // set some metadata properties (provenance information etc)
    txtContent->setRelation(DC::creator, getServiceID());
    txtContent->setRelation(DC::provenance, getServiceID());
    txtContent->setProperty(DC::created, getTimestamp());
    txtContent->setRelation(DC::source, object.stringValue());
    
    //load xml output file
    std::ifstream iFile;
    iFile.open(xmlOutputFile);
    if(!iFile.is_open()){
      std::string error_msg="ACDStableToneService::call(): unable to parse annotation";
      std::cerr << error_msg << std::endl;
      throw std::runtime_error(error_msg);
    }
    
    //get output stream for the content part
    std::ostream* out = txtContent->getOutputStream();
    
    //and copy all the content
    std::vector<char> buff = std::vector<char>(std::istreambuf_iterator<char>(iFile), std::istreambuf_iterator<char>());
    for(uint i = 0;i < buff.size();++i)
      (*out) << buff[i];
    
    //delete the temporary xml annotation
    iFile.close();
    std::remove(xmlOutputFile.c_str());
    delete out;

    // notify broker that we created a new content part by calling the callback function passed as argument
    resp(ci, txtContent->getURI());
    
    // clean up
    delete txtContent;
    delete wavInputFile;
    delete audioContent;
    
  } else {
    std::cerr << "content item part " << object.stringValue() << " of content item " << ci.getURI().stringValue() << " does not exist!\n";
  }
}
