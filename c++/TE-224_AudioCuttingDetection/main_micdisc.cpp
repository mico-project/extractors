#include "ACDMicrophoneDiscriminationService.h"
#include <Daemon.hpp>
#include <vector>

/****** globals ******/
#ifndef EXTRACTOR_BUILD_AS_DAEMON
EventManager* mgr = 0;

std::vector<ACDMicrophoneDiscriminationService*> acdInvdecServices;


bool loop = true;

void signal_handler(int signum) {
  std::cout << "shutting down acd_micdisc service ... " << std::endl;

  if (mgr)
  {
    for(auto &s : acdInvdecServices){
      if(s){
        mgr->unregisterService(s);
        delete s;
        s=0;
      }
    }
    delete mgr;
  }
  
  //if no manager but allocated services -> added for safety, should never happen
  for(auto &s : acdInvdecServices){
    if(s){
      mgr->unregisterService(s);
      delete s;
      s=0;
    }
  }

  loop = false;
}
#endif

/****** main ******/
int main(int argc, char **argv) {
  if(argc != 4) {
    std::cerr << "Usage: "<< argv[0] << " SERVER_IP [USER PASSWORD]" << std::endl;
    exit(1);
  }

  const char* server_name = argv[1];
  const char* mico_user   = argv[2];
  const char* mico_pass   = argv[3];

#if defined EXTRACTOR_BUILD_AS_DAEMON
  if(!strcmp(argv[1], "-k")) {
    return mico::daemon::stop(argv[0]);
  } else {
    // create a new instance of a MICO daemon, auto-registering the analysis service
    return mico::daemon::start(argv[0], server_name, mico_user, mico_pass, 
                              { new ACDMicrophoneDiscriminationService(ACDMicrophoneDiscriminationService::AUDIO_X_WAV), 
                                new ACDMicrophoneDiscriminationService(ACDMicrophoneDiscriminationService::AUDIO_WAV),
                                new ACDMicrophoneDiscriminationService(ACDMicrophoneDiscriminationService::AUDIO_WAVE)
                              });
  }
#else
  mgr = new EventManager(server_name, mico_user, mico_pass);

  acdInvdecServices.push_back(new ACDMicrophoneDiscriminationService(ACDMicrophoneDiscriminationService::AUDIO_X_WAV));
  acdInvdecServices.push_back(new ACDMicrophoneDiscriminationService(ACDMicrophoneDiscriminationService::AUDIO_WAV));
  acdInvdecServices.push_back(new ACDMicrophoneDiscriminationService(ACDMicrophoneDiscriminationService::AUDIO_WAVE));

  for(auto s : acdInvdecServices){
    mgr->registerService(s);
  }

  signal(SIGINT,  &signal_handler);
  signal(SIGTERM, &signal_handler);
  signal(SIGHUP,  &signal_handler);

  while(loop) {
    sleep(1);
  }
#endif
}
