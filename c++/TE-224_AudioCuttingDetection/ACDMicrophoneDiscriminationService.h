#include "MicoMicClass.h"
#include <iosfwd>

#include "EventManager.hpp"

// for constant RDF property definitions of common vocabularies
#include "vocabularies.hpp"


// this namespace contains EventManager and AnalysisService
using namespace mico::event;

// this namespace contains Content, ContentItem, etc
using namespace mico::persistence;

// this namespace contains the RDF data model
using namespace mico::rdf::model;

class ACDMicrophoneDiscriminationService : public AnalysisService {

public:
  
  enum InputMimeType {AUDIO_WAV, AUDIO_X_WAV, AUDIO_WAVE};

  ACDMicrophoneDiscriminationService(ACDMicrophoneDiscriminationService::InputMimeType);
  ~ACDMicrophoneDiscriminationService();

  void call(std::function<void(const ContentItem& ci, const URI& object)> resp, ContentItem& ci, URI& object);

private:
  mico::idmt::MicoMicClass* m_micomicdisc;
    
};
