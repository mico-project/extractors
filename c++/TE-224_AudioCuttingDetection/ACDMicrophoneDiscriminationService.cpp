#include "ACDMicrophoneDiscriminationService.h"

#include <MediaInfoDLL/MediaInfoDLL.h>
#include <MediaInfo/MediaInfo_Const.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>

#include <set>
#include <cmath>






// define dublin core vocabulary shortcut
namespace DC = mico::rdf::vocabularies::DC;

// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2011-10-08T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}

//helper function fot seconds to timestamps conversion
std::string secondsToTimestamp(float total_seconds)
{
  float hours  =  total_seconds / 3600; //hours
  float minutes=  (total_seconds-floor(hours)*3600 )/60;; //minutes
  float seconds=  total_seconds-floor(hours)*3600-floor(minutes)*60; //seconds

  //build output string
  std::ostringstream ss;

  ss << std::setprecision(2) << std::fixed;

  //hours
  if(hours<10)
    ss << "0";
  ss << uint(hours) << ":";

  //minutes
  if(minutes<10)
    ss << "0";
  ss << uint(minutes) << ":";

  //seconds
  if(seconds<10)
    ss << "0";
  ss << seconds;


  return ss.str();
}

//helper function to convert from enum input myme type to string
std::string getServiceName(ACDMicrophoneDiscriminationService::InputMimeType i){
  std::string out="http://www.mico-project.org/services/acd_micdisc_";
  switch(i) {
    case ACDMicrophoneDiscriminationService::AUDIO_WAV   : return out+std::string("wav");
    case ACDMicrophoneDiscriminationService::AUDIO_X_WAV : return out+std::string("x-wav");
    case ACDMicrophoneDiscriminationService::AUDIO_WAVE  : return out+std::string("wave");
    default : std::string error_msg="ACDMicrophoneDiscriminationService() : CRITICAL ERROR: unhandled valid input mime type";
              std::cerr << error_msg << std::endl;
              throw std::runtime_error(error_msg);
  }
}



std::string getMimeType(ACDMicrophoneDiscriminationService::InputMimeType i){
  switch(i) {
    case ACDMicrophoneDiscriminationService::AUDIO_WAV   : return std::string("audio/wav");
    case ACDMicrophoneDiscriminationService::AUDIO_X_WAV : return std::string("audio/x-wav");
    case ACDMicrophoneDiscriminationService::AUDIO_WAVE  : return std::string("audio/wave");
    default : std::string error_msg="ACDMicrophoneDiscriminationService() : CRITICAL ERROR: unhandled valid input mime type";
              std::cerr << error_msg << std::endl;
              throw std::runtime_error(error_msg);
  }
}

ACDMicrophoneDiscriminationService::ACDMicrophoneDiscriminationService(ACDMicrophoneDiscriminationService::InputMimeType i) 
  : AnalysisService(getServiceName(i), getMimeType(i), "text/vnd.fhg-acd-micdisc+xml", "acd-queue"), m_micomicdisc(0)
{
  //instantiate the pointer to the component
  try{
    m_micomicdisc=new mico::idmt::MicoMicClass("4HpuH8A7kCJKBfyfmpr6vcpH");
  }
  catch(std::runtime_error &e){
    
    //notify any error (-> wrong activation key)
    if(m_micomicdisc != 0) delete m_micomicdisc;
    std::string error_msg="ACDMicrophoneDiscriminationService() : Unable to allocate instance";
    std::cerr << error_msg << std::endl;
    throw std::runtime_error(error_msg);
  }
}

ACDMicrophoneDiscriminationService::~ACDMicrophoneDiscriminationService() {
  //delete the pointer to the component
  if(m_micomicdisc != 0) delete m_micomicdisc;
}

void ACDMicrophoneDiscriminationService::call(std::function<void(const ContentItem& ci, const URI& object)> resp, ContentItem& ci, URI& object)
{
  
  // retrieve the content part identified by the object URI
  Content* audioContent = ci.getContentPart(object);
   
  if(audioContent != NULL) {
    
    //and load istream of wav file
    std::istream* wavInputFile = audioContent->getInputStream();
    
    
    //start ACD - MicDisc analysis
    
    //store stream as file
    std::string temporaryWavFileName=std::to_string((long unsigned int) &wavInputFile)+".wav";

    std::ofstream outFile;
    outFile.open(temporaryWavFileName);
    if(!outFile.is_open()){
      throw std::runtime_error("ACDMicrophoneDiscriminationService::call() unable to open temporary file " + temporaryWavFileName);
    }
    std::vector<char> buff = std::vector<char>(std::istreambuf_iterator<char>(*wavInputFile), std::istreambuf_iterator<char>());
    for(uint i = 0;i < buff.size();++i)
      outFile << buff[i];

    outFile.close();
    wavInputFile->seekg(0);    
    
    
    //Extract file duration
    MediaInfoDLL::MediaInfo m_info;
    m_info.Open(temporaryWavFileName);
    float nSec = std::stof(m_info.Get(MediaInfoDLL::Stream_General, (size_t)0, "Duration"))/1000;
    m_info.Close();
    
    std::ifstream iFile;
    iFile.open(temporaryWavFileName);
    if(!iFile.is_open()){
      std::string error_msg="ACDMicrophoneDiscriminationService::call(): unable to open temporary file " + temporaryWavFileName;
      std::cerr << error_msg << std::endl;
      throw std::runtime_error(error_msg);
    }
    
    //create a set of cutting points
    float cutLength=7.0f;
    std::set<std::string> cuttingLocations;  
    for(int i=1 ; i<int(floor(nSec/cutLength)); i++)
    {
      cuttingLocations.insert(secondsToTimestamp(i*cutLength));
    }
    
    //perform analysis
    std::string xmlOutputFile=std::to_string((long unsigned int) &iFile)+".xml";
    
    std::cout << "ACDMicrophoneDiscriminationService::call(): start processing" << std::endl;
    m_micomicdisc->performAnalysis(&iFile, cuttingLocations, xmlOutputFile);
    iFile.close();
    std::cout << "ACDMicrophoneDiscriminationService::call(): end processing" << std::endl;
    std::remove(temporaryWavFileName.c_str());

    // Instantiate content part to hold the annotation
    Content *txtContent = ci.createContentPart();
    txtContent->setType("text/vnd.fhg-acd-micdisc+xml");
    
    // set some metadata properties (provenance information etc)
    txtContent->setRelation(DC::creator, getServiceID());
    txtContent->setRelation(DC::provenance, getServiceID());
    txtContent->setProperty(DC::created, getTimestamp());
    txtContent->setRelation(DC::source, object.stringValue());
    
    //load xml output file
    iFile.open(xmlOutputFile);
    if(!iFile.is_open()){
      std::string error_msg="ACDMicrophoneDiscriminationService::call(): unable to parse annotation";
      std::cerr << error_msg << std::endl;
      throw std::runtime_error(error_msg);
    }
    
    //get output stream for the content part
    std::ostream* out = txtContent->getOutputStream();
    
    //and copy all the content
    buff = std::vector<char>(std::istreambuf_iterator<char>(iFile), std::istreambuf_iterator<char>());
    for(uint i = 0;i < buff.size();++i)
      (*out) << buff[i];
    
    //delete the temporary xml annotation
    iFile.close();
    std::remove(xmlOutputFile.c_str());
    delete out;

    // notify broker that we created a new content part by calling the callback function passed as argument
    resp(ci, txtContent->getURI());
    
    // clean up
    delete txtContent;
    delete wavInputFile;
    delete audioContent;
    
  } else {
    std::cerr << "content item part " << object.stringValue() << " of content item " << ci.getURI().stringValue() << " does not exist!\n";
  }
}
