#include "ACDInverseDecoderService.h"
#include <fstream>
#include<stdio.h>
#include<iostream>


// define dublin core vocabulary shortcut
namespace DC = mico::rdf::vocabularies::DC;

// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2011-10-08T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}

//helper function to convert from enum input myme type to string
std::string getServiceName(ACDInverseDecoderService::InputMimeType i){
  std::string out="http://www.mico-project.org/services/acd_invdec_";
  switch(i) {
    case ACDInverseDecoderService::AUDIO_WAV   : return out+std::string("wav");
    case ACDInverseDecoderService::AUDIO_X_WAV : return out+std::string("x-wav");
    case ACDInverseDecoderService::AUDIO_WAVE  : return out+std::string("wave");
    default : std::string error_msg="ACDInverseDecoderService() : CRITICAL ERROR: unhandled valid input mime type";
              std::cerr << error_msg << std::endl;
              throw std::runtime_error(error_msg);
  }
}



std::string getMimeType(ACDInverseDecoderService::InputMimeType i){
  switch(i) {
    case ACDInverseDecoderService::AUDIO_WAV   : return std::string("audio/wav");
    case ACDInverseDecoderService::AUDIO_X_WAV : return std::string("audio/x-wav");
    case ACDInverseDecoderService::AUDIO_WAVE  : return std::string("audio/wave");
    default : std::string error_msg="ACDInverseDecoderService() : CRITICAL ERROR: unhandled valid input mime type";
              std::cerr << error_msg << std::endl;
              throw std::runtime_error(error_msg);
  }
}

ACDInverseDecoderService::ACDInverseDecoderService(ACDInverseDecoderService::InputMimeType i) 
  : AnalysisService(getServiceName(i), getMimeType(i), "text/vnd.fhg-acd-invdec+xml", "acd-queue"), m_micoinvdec(0)
{
  //instantiate the pointer to the component
  try{
    m_micoinvdec=new mico::idmt::MicoInvDec("4HpuH8A7kCJKBfyfmpr6vcpH");
  }
  catch(std::runtime_error &e){
    
    //notify any error (-> wrong activation key)
    if(m_micoinvdec != 0) delete m_micoinvdec;
    std::string error_msg="ACDInverseDecoderService() : Unable to allocate instance";
    std::cerr << error_msg << std::endl;
    throw std::runtime_error(error_msg);
  }
}

ACDInverseDecoderService::~ACDInverseDecoderService() {
  //delete the pointer to the component
  if(m_micoinvdec != 0) delete m_micoinvdec;
}

void ACDInverseDecoderService::call(std::function<void(const ContentItem& ci, const URI& object)> resp, ContentItem& ci, URI& object)
{
  
  // retrieve the content part identified by the object URI
  Content* audioContent = ci.getContentPart(object);
   
  if(audioContent != NULL) {
    
    //and load istream of wav file
    std::istream* wavInputFile = audioContent->getInputStream();
    
    
    //start ACD - InvDec analysis
    
    //setup target codecs for the detector
    std::set<mico::idmt::InvDecCodec> targetCodecs;

    targetCodecs.insert(mico::idmt::MP3);
    targetCodecs.insert(mico::idmt::MP3PRO);

    targetCodecs.insert(mico::idmt::LC_AAC);
    targetCodecs.insert(mico::idmt::HE_AAC);
    
    //perform analysis
    std::string xmlOutputFile=std::to_string((long unsigned int) wavInputFile)+".xml";
    
    std::cout << "ACDInverseDecoderService::call(): start processing" << std::endl;
    m_micoinvdec->performAnalysis(wavInputFile, xmlOutputFile, targetCodecs);
    std::cout << "ACDInverseDecoderService::call(): end processing" << std::endl;

    // Instantiate content part to hold the annotation
    Content *txtContent = ci.createContentPart();
    txtContent->setType("text/vnd.fhg-acd-invdec+xml");
    
    // set some metadata properties (provenance information etc)
    txtContent->setRelation(DC::creator, getServiceID());
    txtContent->setRelation(DC::provenance, getServiceID());
    txtContent->setProperty(DC::created, getTimestamp());
    txtContent->setRelation(DC::source, object.stringValue());
    
    //load xml output file
    std::ifstream iFile;
    iFile.open(xmlOutputFile);
    if(!iFile.is_open()){
      std::string error_msg="ACDInverseDecoderService::call(): unable to parse annotation";
      std::cerr << error_msg << std::endl;
      throw std::runtime_error(error_msg);
    }
    
    //get output stream for the content part
    std::ostream* out = txtContent->getOutputStream();
    
    //and copy all the content
    std::vector<char> buff = std::vector<char>(std::istreambuf_iterator<char>(iFile), std::istreambuf_iterator<char>());
    for(uint i = 0;i < buff.size();++i)
      (*out) << buff[i];
    
    //delete the temporary xml annotation
    iFile.close();
    std::remove(xmlOutputFile.c_str());
    delete out;

    // notify broker that we created a new content part by calling the callback function passed as argument
    resp(ci, txtContent->getURI());
    
    // clean up
    delete txtContent;
    delete wavInputFile;
    delete audioContent;
    
  } else {
    std::cerr << "content item part " << object.stringValue() << " of content item " << ci.getURI().stringValue() << " does not exist!\n";
  }
}
