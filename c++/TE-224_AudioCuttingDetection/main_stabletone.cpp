#include "ACDStableToneService.h"
#include <Daemon.hpp>
#include <vector>

/****** globals ******/
#ifndef EXTRACTOR_BUILD_AS_DAEMON
EventManager* mgr = 0;

std::vector<ACDStableToneService*> acdStableToneServices;


bool loop = true;

void signal_handler(int signum) {
  std::cout << "shutting down acd_stabletone service ... " << std::endl;

  if (mgr)
  {
    for(auto &s : acdStableToneServices){
      if(s){
        mgr->unregisterService(s);
        delete s;
        s=0;
      }
    }
    delete mgr;
  }
  
  //if no manager but allocated services -> added for safety, should never happen
  for(auto &s : acdStableToneServices){
    if(s){
      mgr->unregisterService(s);
      delete s;
      s=0;
    }
  }

  loop = false;
}
#endif

/****** main ******/
int main(int argc, char **argv) {
  if(argc != 4) {
    std::cerr << "Usage: "<< argv[0] << " SERVER_IP [USER PASSWORD]" << std::endl;
    exit(1);
  }

  const char* server_name = argv[1];
  const char* mico_user   = argv[2];
  const char* mico_pass   = argv[3];

#if defined EXTRACTOR_BUILD_AS_DAEMON
  if(!strcmp(argv[1], "-k")) {
    return mico::daemon::stop(argv[0]);
  } else {
    // create a new instance of a MICO daemon, auto-registering the analysis service
    return mico::daemon::start(argv[0], server_name, mico_user, mico_pass, 
                              { new ACDStableToneService(ACDStableToneService::ENF_50, ACDStableToneService::AUDIO_X_WAV), 
                                new ACDStableToneService(ACDStableToneService::ENF_50, ACDStableToneService::AUDIO_WAV),
                                new ACDStableToneService(ACDStableToneService::ENF_50, ACDStableToneService::AUDIO_WAVE),
                                new ACDStableToneService(ACDStableToneService::ENF_60, ACDStableToneService::AUDIO_X_WAV), 
                                new ACDStableToneService(ACDStableToneService::ENF_60, ACDStableToneService::AUDIO_WAV),
                                new ACDStableToneService(ACDStableToneService::ENF_60, ACDStableToneService::AUDIO_WAVE)
                              });
  }
#else
  mgr = new EventManager(server_name, mico_user, mico_pass);

  acdStableToneServices.push_back(new ACDStableToneService(ACDStableToneService::ENF_50, ACDStableToneService::AUDIO_X_WAV));
  acdStableToneServices.push_back(new ACDStableToneService(ACDStableToneService::ENF_50, ACDStableToneService::AUDIO_WAV));
  acdStableToneServices.push_back(new ACDStableToneService(ACDStableToneService::ENF_50, ACDStableToneService::AUDIO_WAVE));
  acdStableToneServices.push_back(new ACDStableToneService(ACDStableToneService::ENF_60, ACDStableToneService::AUDIO_X_WAV));
  acdStableToneServices.push_back(new ACDStableToneService(ACDStableToneService::ENF_60, ACDStableToneService::AUDIO_WAV));
  acdStableToneServices.push_back(new ACDStableToneService(ACDStableToneService::ENF_60, ACDStableToneService::AUDIO_WAVE));

  for(auto s : acdStableToneServices){
    mgr->registerService(s);
    sleep(1);
  }

  signal(SIGINT,  &signal_handler);
  signal(SIGTERM, &signal_handler);
  signal(SIGHUP,  &signal_handler);

  while(loop) {
    sleep(1);
  }
#endif
}
