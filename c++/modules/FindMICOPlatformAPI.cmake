# --------------------------------------------------------
# - Finds the MICO platform C++ API
#
# You may set the following variables to steer the process
#
#
# MICOPlatformAPI_HOME   (single path)- tells the script where it should look first.
# MICOPlatformAPI_DEBUG  (True/False) - prints Debug informations of the find process
#
# the macro defines:
#
# MICOPlatformAPI_FOUND           - true if the platform could be found
# MICOPlatformAPI_INCLUDE_DIRS - directories containing the 
#                                  required API header directories
# MICOPlatformAPI_LIBRARIES    - link libraries
# MICOPlatformAPI_VERSION      - The version that has been found
#
# --------------------------------------------------------

find_package(AMQPCPP REQUIRED)
find_package(Protobuf REQUIRED)

macro (split_version inver ver_major ver_minor ver_patch doFillHigh)

  string(REPLACE "." ";" _version_temp_list ${inver})
  list(LENGTH _version_temp_list _len)
  message(STATUS "number of version digits = " ${_len})

  if (${doFillHigh})
    set(${ver_major} "99")
    set(${ver_minor} "99")
    set(${ver_patch} "99")
  else()
    set(${ver_major} ver_major-NOTFOUND)
    set(${ver_minor} ver_minor-NOTFOUND)
    set(${ver_patch} ver_patch-NOTFOUND)
  endif()

  if (${_len} GREATER 0)
    list(GET _version_temp_list 0 ${ver_major})
  endif()
  if (${_len} GREATER 1)
    list(GET _version_temp_list 1 ${ver_minor})
  endif()
  if (${_len} GREATER 2)
    list(GET _version_temp_list 2 ${ver_patch})
  endif()

  unset(_len)
  unset(_version_temp_list)

endmacro()

# returns the first matching version that is linked with the _link_lib
macro(check_version _link_lib _check_versions _result_version)

  set(${_result_version} versions-NOTFOUND)

  if (NOT EXISTS ${_link_lib})
    return()
  endif()

  execute_process(COMMAND readlink ${_link_lib} OUTPUT_VARIABLE _link_out RESULT_VARIABLE _link_res)
  if (${MICOPlatformAPI_DEBUG})
    message(STATUS "readlink for [${_link_lib}] delivers " ${_link_out} )
  endif()

  if (_link_res)  
    message(FATAL_ERROR "internal error in find-package script")
  endif()
  unset(_found_ver)
  
  string(REGEX MATCH "(\\.[0-9]+)(\\.[0-9]+)?(\\.[0-9]+)?" _found_ver "${_link_out}")
  string(REGEX REPLACE "^\\." "" _found_ver "${_found_ver}")

  foreach(_version ${_check_versions})
    if (${MICOPlatformAPI_DEBUG})
      message(STATUS "checking version [${_version}] against resolved link version " ${_found_ver} )
    endif()
    if(NOT "${_version}" VERSION_LESS "${_found_ver}")
      set(${_result_version} ${_found_ver})
      break()
    endif()
  endforeach()
endmacro()


set(MICOPlatformAPI_DEFAULT_COMPONENTS
  mico_daemon 
  mico_event 
  mico_logging 
  mico_marmotta 
  mico_persistence 
  mico_rdf
  micoapicommons
)

if(MICOPlatformAPI_FIND_VERSION_EXACT)
  if (${MICOPlatformAPI_DEBUG})
    message(STATUS "Exact version specified - checking ...." )
  endif()
  # The version may appear in a directory with or without the patch
  # level, even when the patch level is non-zero.
  set(_MICOPlatformAPI_TEST_VERSIONS
    "${MICOPlatformAPI_FIND_VERSION_MAJOR}.${MICOPlatformAPI_FIND_VERSION_MINOR}.${MICOPlatformAPI_FIND_VERSION_PATCH}")
  if (${MICOPlatformAPI_FIND_VERSION_PATCH} EQUAL 0)
    list(APPEND _MICOPlatformAPI_TEST_VERSIONS "${MICOPlatformAPI_FIND_VERSION_MAJOR}.${MICOPlatformAPI_FIND_VERSION_MINOR}")
      if (${MICOPlatformAPI_FIND_VERSION_MINOR} EQUAL 0)
        list(APPEND _MICOPlatformAPI_TEST_VERSIONS "${MICOPlatformAPI_FIND_VERSION_MAJOR}")
      endif()
  endif()
else()
  message(STATUS "No exact version specified - checking against known ...." )
  # The user has not requested an exact version.  Among known
  # versions, find those that are acceptable to the user request.
  set(_MICOPlatformAPI_KNOWN_VERSIONS "1" "1.1.1" "1.1.3" "1.2.0" "1.2.1" "1.2.2" "1.2.3" "1.2.4" "2.0.0" "3.0.0")
  set(_MICOPlatformAPI_TEST_VERSIONS)
  if(MICOPlatformAPI_FIND_VERSION)
    set(_MICOPlatformAPI_FIND_VERSION_SHORT "${MICOPlatformAPI_FIND_VERSION_MAJOR}.${MICOPlatformAPI_FIND_VERSION_MINOR}")
    # Select acceptable versions.
    foreach(version ${_MICOPlatformAPI_KNOWN_VERSIONS})
      if (${MICOPlatformAPI_DEBUG})
        message(STATUS "Checking agains known version =" ${version} )
      endif()
      if(NOT "${version}" VERSION_LESS "${MICOPlatformAPI_FIND_VERSION}")
        # This version is high enough.
        list(APPEND _MICOPlatformAPI_TEST_VERSIONS "${version}")
      elseif("${version}.99" VERSION_EQUAL "${_MICOPlatformAPI_TEST_VERSIONSCOPlatformAPI_FIND_VERSION_SHORT}.99")
        # This version is a short-form for the requested version with
        # the patch level dropped.
        list(APPEND _MICOPlatformAPI_TEST_VERSIONS "${version}")
      endif()
    endforeach()
  else()
    # Any version is acceptable.
    set(_MICOPlatformAPI_TEST_VERSIONS "${_MICOPlatformAPI_KNOWN_VERSIONS}")
  endif()
endif()



if (${MICOPlatformAPI_DEBUG})
  foreach(test_ver ${_MICOPlatformAPI_TEST_VERSIONS})
    message(STATUS "Will search for MICO platform version : " ${test_ver})
  endforeach()
endif()

if (EXISTS ${MICOPlatformAPI_HOME})
  FIND_PATH(MICOPlatformAPI_INCLUDE_DIRS
    EventManager.hpp
    PATHS
    ${MICOPlatformAPI_HOME}/include
    NO_DEFAULT_PATH
  )
else()
  FIND_PATH(MICOPlatformAPI_INCLUDE_DIRS
    EventManager.hpp   
  )
endif()

set (MICOPlatformAPI_INCLUDE_DIRS ${MICOPlatformAPI_INCLUDE_DIRS} ${AMQPCPP_INCLUDE_DIRS} ${PROTOBUF_INCLUDE_DIRS})

set(MICOPlatformAPI_COMPONENT_VARS)
set(MICOPlatformAPI_VERSIONS_FOUND)

foreach (comp ${MICOPlatformAPI_DEFAULT_COMPONENTS})
  set(COMPATIBLE_${comp}_FOUND False)
  unset(curr_version)

  ###search HOME location first
  if (EXISTS ${MICOPlatformAPI_HOME})
     if (${MICOPlatformAPI_DEBUG})
        message(STATUS "MICOPlatformAPI_HOME specified - searching there for " ${comp})
      endif()
    FIND_LIBRARY(MICOPlatformAPI_LIB_${comp} NAMES ${comp} PATHS ${MICOPlatformAPI_HOME}/lib ${MICOPlatformAPI_HOME}/lib64 NO_DEFAULT_PATH)
  endif()

  if (MICOPlatformAPI_LIB_${comp})
    check_version(${MICOPlatformAPI_LIB_${comp}} "${_MICOPlatformAPI_TEST_VERSIONS}" curr_version)
  endif()

  if ((NOT MICOPlatformAPI_LIB_${comp}) OR (${curr_version} MATCHES "NOTFOUND"))
    if (${MICOPlatformAPI_DEBUG})
      message(STATUS "Searching default library locations for " ${comp})
    endif()
    #search default location as fallback
    FIND_LIBRARY(MICOPlatformAPI_LIB_${comp} NAMES ${comp})
    if (${MICOPlatformAPI_DEBUG})
      message(STATUS "Found " ${MICOPlatformAPI_LIB_${comp}})
    endif()

    unset(curr_version)
    if (MICOPlatformAPI_LIB_${comp})
      check_version(${MICOPlatformAPI_LIB_${comp}} "${_MICOPlatformAPI_TEST_VERSIONS}" curr_version)
    endif()
  endif()

  if (MICOPlatformAPI_LIB_${comp} AND NOT ${curr_version} MATCHES "NOTFOUND")
    if (${MICOPlatformAPI_DEBUG})
        message(STATUS "Found compatible ${comp} at ${MICOPlatformAPI_LIB_${comp}} in version ${curr_version}")
    endif()
    list(APPEND MICOPlatformAPI_COMPONENT_VARS MICOPlatformAPI_LIB_${comp})
    list(APPEND MICOPlatformAPI_VERSIONS_FOUND ${curr_version})
  else()
    unset(MICOPlatformAPI_COMPONENT_VARS)
    break()
  endif()
endforeach()

#check version compatibility
if (MICOPlatformAPI_VERSIONS_FOUND)
  list(REMOVE_DUPLICATES MICOPlatformAPI_VERSIONS_FOUND)

  message(STATUS "MICOPlatformAPI_VERSIONS_FOUND=" ${MICOPlatformAPI_VERSIONS_FOUND})

  list(LENGTH MICOPlatformAPI_VERSIONS_FOUND _num_versions)
  if (_num_versions EQUAL 1)
    # multiple versions found - quitting for now!  
    list(GET MICOPlatformAPI_VERSIONS_FOUND 0 MICOPlatformAPI_VERSION)
  else()
    message(WARNING "Found different versions of MICO platform api!")
  endif()
else()
  message(WARNING "Could not find any versions of MICO platform api!")
endif()


IF(MICOPlatformAPI_COMPONENT_VARS)
  foreach (var ${MICOPlatformAPI_COMPONENT_VARS})
    list(APPEND MICOPlatformAPI_LIBRARIES ${${var}})
  endforeach()
  foreach (amqp_var ${AMQPCPP_LIBRARIES})
    list(APPEND MICOPlatformAPI_LIBRARIES ${amqp_var})
  endforeach()
  foreach (pb_var ${PROTOBUF_LIBRARIES})
    list(APPEND MICOPlatformAPI_LIBRARIES ${pb_var})
  endforeach()
ENDIF()

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS (
  MICOPlatformAPI
  DEFAULT_MSG 
  MICOPlatformAPI_INCLUDE_DIRS
  MICOPlatformAPI_LIBRARIES 
  MICOPlatformAPI_VERSION
)

if (NOT DEFINED MICOPlatformAPI_FIND_QUIETLY)
  message(STATUS "MICO Platform API version: " ${MICOPlatformAPI_VERSION})
  message(STATUS "MICO Platform API components: " )
  foreach(comp ${MICOPlatformAPI_DEFAULT_COMPONENTS})
    message(STATUS "  " ${comp})
  endforeach()
endif()

if (${MICOPlatformAPI_FIND_REQUIRED} AND (NOT ${MICOPlatformAPI_FOUND}))
  message(FATAL_ERROR "MICO platform API could not be found!")
endif()
