# --------------------------------------------------------
# - Finds the Anno4Cpp MICO library
#
# You may set the following variables to steer the process
#
#
# Anno4Cpp_HOME        (single path) - tells the script where it should look first
#   or
# MICOPlatformAPI_HOME (single path) - platform of the mico api installation
#
# the macro defines:
#
# Anno4Cpp_FOUND           - true if the platform could be found
# Anno4Cpp_INCLUDE_DIRS    - directories containing the required header
# Anno4Cpp_LIBRARIES       - link libraries
#
# --------------------------------------------------------


SET( CMAKE_FIND_LIBRARY_SUFFIXES ".so" ".a")

SET(ANNO4CPP_POSSIBLE_INCLUDE_PATHS
  /usr/include
  /usr/local/include
  ${Anno4Cpp_HOME}/include
  ${MICOPlatformAPI_HOME}/include
)

SET(ANNO4CPP_POSSIBLE_LIBRARY_PATHS
  /usr/lib/
  /usr/local/lib/
  ${Anno4Cpp_HOME}/lib
  ${MICOPlatformAPI_HOME}/lib
)

FIND_PATH(ANNO4CPP_INCLUDE_DIR NAMES anno4cpp.h PATHS ${ANNO4CPP_POSSIBLE_INCLUDE_PATHS})
FIND_LIBRARY(ANNO4CPP_LIBRARY  NAMES anno4cpp   PATHS ${ANNO4CPP_POSSIBLE_LIBRARY_PATHS})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Anno4Cpp
  FOUND_VAR Anno4Cpp_FOUND
  REQUIRED_VARS
    ANNO4CPP_LIBRARY
    ANNO4CPP_INCLUDE_DIR
  #VERSION_VAR Foo_VERSION
)

if(Anno4Cpp_FOUND)
  set(Anno4Cpp_LIBRARIES ${ANNO4CPP_LIBRARY})
  set(Anno4Cpp_INCLUDE_DIRS ${ANNO4CPP_INCLUDE_DIR})
endif()

mark_as_advanced(
  ANNO4CPP_INCLUDE_DIR
  ANNO4CPP_LIBRARY
)

IF (Anno4Cpp_FOUND)
  IF (NOT Anno4Cpp_FIND_QUIETLY)
    MESSAGE(STATUS "Found Anno4Cpp libraries: ${Anno4Cpp_LIBRARIES}")
    MESSAGE(STATUS "Found Anno4Cpp include: ${Anno4Cpp_INCLUDE_DIRS}")
  ENDIF (NOT Anno4Cpp_FIND_QUIETLY)
ELSE (Anno4Cpp_FOUND)
  IF (Anno4Cpp_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "Could not find Anno4Cpp")
  ENDIF (Anno4Cpp_FIND_REQUIRED)
ENDIF (Anno4Cpp_FOUND)