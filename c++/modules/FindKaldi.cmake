# --------------------------------------------------------
# - Finds the MICO version of Kaldi
#
# You may set the following variable to steer the process
#
# Kaldi_HOME        (single path) - tells the script where it should look first.
#
# the macro defines:
#
# Kaldi_FOUND           - true if the platform could be found
# Kaldi_INCLUDE_DIRS    - directories containing the required header
# Kaldi_LIBRARIES       - link libraries
#
# --------------------------------------------------------

SET( CMAKE_FIND_LIBRARY_SUFFIXES ".so" )

if(NOT IS_ABSOLUTE ${KALDI_ROOT} )
  get_filename_component(KALDI_ROOT "${CMAKE_BINARY_DIR}/${KALDI_ROOT}" ABSOLUTE)
endif()


SET(KALDI_POSSIBLE_INCLUDE_PATHS
  /usr/include/kaldi
  /usr/local/include/kaldi
  ${Kaldi_HOME}/include/kaldi
)

SET(KALDI_POSSIBLE_LIBRARY_PATHS
  /usr/lib/kaldi
  /usr/local/lib/kaldi
  ${Kaldi_HOME}/lib/kaldi
)


# Look for all kaldi libraries 

find_path(KALDI_INCLUDE_DIR NAMES base/kaldi-common.h PATHS ${KALDI_POSSIBLE_INCLUDE_PATHS})
find_path(OPENFST_INCLUDE_DIR NAMES fst/fstlib.h PATHS  ${KALDI_POSSIBLE_INCLUDE_PATHS})

find_library(KALDI_BASE_LIBRARY        NAMES kaldi-base        PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_CHAIN_LIBRARY       NAMES kaldi-chain       PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_DECODER_LIBRARY     NAMES kaldi-decoder     PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_FEAT_LIBRARY        NAMES kaldi-feat        PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_FSTEXT_LIBRARY      NAMES kaldi-fstext      PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_GMM_LIBRARY         NAMES kaldi-gmm         PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_HMM_LIBRARY         NAMES kaldi-hmm         PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_LAT_LIBRARY         NAMES kaldi-lat         PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_MATRIX_LIBRARY      NAMES kaldi-matrix      PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_CUDAMATRIX_LIBRARY  NAMES kaldi-cudamatrix  PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_ONLINE_LIBRARY      NAMES kaldi-online2     PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_NNET2_LIBRARY       NAMES kaldi-nnet2       PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_NNET3_LIBRARY       NAMES kaldi-nnet3       PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_IVECTOR_LIBRARY     NAMES kaldi-ivector     PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_TRANSFORM_LIBRARY   NAMES kaldi-transform   PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_TREE_LIBRARY        NAMES kaldi-tree        PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_THREAD_LIBRARY      NAMES kaldi-thread      PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(KALDI_UTIL_LIBRARY        NAMES kaldi-util        PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})
find_library(OPENFST_LIBRARY           NAMES fst               PATHS ${KALDI_POSSIBLE_LIBRARY_PATHS})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Kaldi
  FOUND_VAR Kaldi_FOUND
  REQUIRED_VARS
    KALDI_INCLUDE_DIR	
    KALDI_BASE_LIBRARY
    KALDI_CHAIN_LIBRARY
    KALDI_DECODER_LIBRARY
    KALDI_FEAT_LIBRARY
    KALDI_FSTEXT_LIBRARY
    KALDI_GMM_LIBRARY
    KALDI_HMM_LIBRARY
    KALDI_LAT_LIBRARY
    KALDI_MATRIX_LIBRARY
    KALDI_CUDAMATRIX_LIBRARY
    KALDI_ONLINE_LIBRARY
    KALDI_NNET2_LIBRARY
    KALDI_NNET3_LIBRARY
    KALDI_IVECTOR_LIBRARY
    KALDI_TRANSFORM_LIBRARY
    KALDI_TREE_LIBRARY
    KALDI_THREAD_LIBRARY
    KALDI_UTIL_LIBRARY

    OPENFST_INCLUDE_DIR
    OPENFST_LIBRARY
)

foreach(LIBNAME KALDI_ONLINE_LIBRARY KALDI_IVECTOR_LIBRARY KALDI_CUDAMATRIX_LIBRARY KALDI_CHAIN_LIBRARY KALDI_NNET3_LIBRARY KALDI_NNET2_LIBRARY KALDI_DECODER_LIBRARY KALDI_GMM_LIBRARY KALDI_FEAT_LIBRARY KALDI_THREAD_LIBRARY KALDI_LAT_LIBRARY KALDI_HMM_LIBRARY KALDI_TRANSFORM_LIBRARY KALDI_FSTEXT_LIBRARY KALDI_TREE_LIBRARY KALDI_MATRIX_LIBRARY KALDI_UTIL_LIBRARY KALDI_BASE_LIBRARY OPENFST_LIBRARY)
  if(${LIBNAME})
    SET(KALDI_LIBRARIES ${KALDI_LIBRARIES} ${${LIBNAME}} )
  else(${LIBNAME})
    if (NOT Kaldi_FIND_QUIETLY)
      message("Kaldi library ${LIBNAME} not found.")
   endif (NOT Kaldi_FIND_QUIETLY)
  endif (${LIBNAME})
endforeach(LIBNAME)



mark_as_advanced(
  KALDI_INCLUDE_DIR
  KALDI_BASE_LIBRARY
  KALDI_CHAIN_LIBRARY
  KALDI_DECODER_LIBRARY
  KALDI_FEAT_LIBRARY
  KALDI_FSTEXT_LIBRARY
  KALDI_GMM_LIBRARY
  KALDI_HMM_LIBRARY
  KALDI_LAT_LIBRARY
  KALDI_MATRIX_LIBRARY
  KALDI_CUDAMATRIX_LIBRARY
  KALDI_ONLINE_LIBRARY
  KALDI_NNET3_LIBRARY
  KALDI_NNET2_LIBRARY
  KALDI_IVECTOR_LIBRARY
  KALDI_TRANSFORM_LIBRARY
  KALDI_TREE_LIBRARY
  KALDI_THREAD_LIBRARY
  KALDI_UTIL_LIBRARY
  KALDI_LIBRARIES
  OPENFST_INCLUDE_DIR
  #OPENFST_LIBRARY
)


if (Kaldi_FOUND)

  set(Kaldi_LIBRARIES ${KALDI_LIBRARIES})
  set(Kaldi_INCLUDE_DIRS ${KALDI_INCLUDE_DIR} ${OPENFST_INCLUDE_DIR})

  if (NOT Kaldi_FIND_QUIETLY)
    message (STATUS "Found Kaldi libraries: ${Kaldi_LIBRARIES}")    
    message (STATUS "Found Kaldi includes: ${Kaldi_INCLUDE_DIRS}")
  endif (NOT Kaldi_FIND_QUIETLY)

else (Kaldi_FOUND)

  if(Kaldi_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "Could not find Kaldi")
  endif(Kaldi_FIND_REQUIRED)

endif (Kaldi_FOUND)


