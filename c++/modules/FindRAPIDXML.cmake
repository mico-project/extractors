find_path(RAPIDXML_DIR NAMES rapidxml.hpp
	PATH_SUFFIXES rapidxml include
	PATHS
	${RAPIDXML_ROOT}
	/usr/local/
	/usr/local/include/)

if (NOT RAPIDXML_DIR)
	if(RapidXML_FIND_REQUIRED)
		message(FATAL_ERROR "Could not find RapidXML!")
	elseif(NOT RapidXML_FIND_QUIETLY)
		message("Could not find RapidXML!")
	endif(RapidXML_FIND_REQUIRED)
endif(NOT RAPIDXML_DIR)

if (NOT RapidXML_FIND_QUIETLY)
	message("Found RapidXML: ${RAPIDXML_DIR}")
endif (NOT RapidXML_FIND_QUIETLY)
