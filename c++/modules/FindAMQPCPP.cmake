if (DEFINED AMQP_HOME)
    message(STATUS "Searching in AMQP_HOME=" ${AMQP_HOME})
    find_path(AMQPCPP_INCLUDE_DIR NAMES amqpcpp.h PATHS ${AMQP_HOME}/include NO_DEFAULT_PATH)
    find_library(AMQPCPP_LIBRARY NAMES amqpcpp libamqpcpp PATHS ${AMQP_HOME}/lib NO_DEFAULT_PATH)

    message(STATUS "local search result for AMQPCPP_INCLUDE_DIR=" ${AMQPCPP_INCLUDE_DIR})
    message(STATUS "local search result for AMQPCPP_LIBRARY=" ${AMQPCPP_LIBRARY})
endif()

if (NOT AMQPCPP_INCLUDE_DIR OR NOT AMQPCPP_LIBRARY)
    find_path(AMQPCPP_INCLUDE_DIR NAMES amqpcpp.h)
    find_library(AMQPCPP_LIBRARY NAMES amqpcpp libamqpcpp)
endif()

set(AMQPCPP_INCLUDE_DIRS ${AMQPCPP_INCLUDE_DIR})
set(AMQPCPP_LIBRARIES ${AMQPCPP_LIBRARY})

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(AMQPCPP DEFAULT_MSG AMQPCPP_LIBRARY AMQPCPP_INCLUDE_DIR)

mark_as_advanced(AMQPCPP_INCLUDE_DIR AMQPCPP_LIBRARY)
