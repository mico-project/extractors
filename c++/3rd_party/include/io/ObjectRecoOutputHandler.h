/* 
 * File:   ObjectRecoOutputHandler.h
 * Author: Sascha Krämer (krm)
 */

#ifndef OBJECTRECOOUTPUTHANDLER_H
#define OBJECTRECOOUTPUTHANDLER_H

#include <map>
#include <string>
#include <sstream>
//#include <cstdint>

#include <boost/foreach.hpp>
#include <boost/smart_ptr.hpp>

#include "XMLDocumentTiXml.h"
#include "XMLDocumentJsonCpp.h"

#include "IObjectRecoDefs.h"

namespace objectreco
{
  struct VideoMetaType
  {
    unsigned int timebase_num;
    unsigned int timebase_denum;
    unsigned int width;
    unsigned int height;
    std::string  algorithm;
    VideoMetaType()
     : timebase_num(1), timebase_denum(25), width(0), height(0){};
    VideoMetaType(unsigned int w, unsigned int h)
     : timebase_num(1), timebase_denum(25), width(w), height(h){};
    VideoMetaType(unsigned int t, unsigned int f, unsigned int w, unsigned int h, std::string algorithm="")
     : timebase_num(t), timebase_denum(f), width(w), height(h), algorithm(algorithm){};
  };

  //util function convert T to string
  template<class T> std::string toString( const T& i ) {
    std::stringstream s;
    s<<i;
    return s.str();
  }

  /**
   * ObjectRecoOutputHandler class for saving videoanalysis extractions in xml
   */
  class ObjectRecoOutputHandler
  {
  public:
    /**
     * Constructor
     * @param results: holds analysed data written to xml
     */
    ObjectRecoOutputHandler ( std::map<uint64_t, ObjectRecognitionResults > results, VideoMetaType m )
     : m_results (results), m_meta(m)
     {}

    /**
     * toXmlFile
     * @param fileName: path to xml file
     */
    void toXmlFile (const std::string& fileName)
    {
      boost::scoped_ptr<xml::XMLDocumentTiXml> xmlOutput ( new xml::XMLDocumentTiXml () );
      xmlOutput->setRootElement ( getObjectRecoXml () );
      xmlOutput->write (fileName+std::string(".xml"));
    }

    /**
     * toXmlStream
     * @param os: ostream for xml output
     */
    void toXmlStream (std::ostream& os)
    {
      boost::scoped_ptr<xml::XMLDocumentTiXml> xmlOutput ( new xml::XMLDocumentTiXml () );
      xmlOutput->setRootElement ( getObjectRecoXml () );
      xmlOutput->write (os);
    }

    /**
     * toJsonFile
     * @param fileName: path to json file
     */
    void toJsonFile (const std::string& fileName)
    {
      boost::scoped_ptr<xml::XMLDocumentJsonCpp> jsonOutput ( new xml::XMLDocumentJsonCpp () );
      jsonOutput->setRootElement ( getObjectRecoXml () );
      jsonOutput->write (fileName+std::string(".json"));
    }

    /**
     * toJsonStream
     * @param os: ostream to json output
     */
    void toJsonStream (std::ostream& os)
    {
      boost::scoped_ptr<xml::XMLDocumentJsonCpp> jsonOutput ( new xml::XMLDocumentJsonCpp () );
      jsonOutput->setRootElement ( getObjectRecoXml () );
      jsonOutput->write (os);
    }

  private:

    std::map<uint64_t, ObjectRecognitionResults > m_results;
    VideoMetaType m_meta;

    xml::XMLElement* getObjectRecoXml ()
    {
      xml::XMLElement *xmlReportRoot = new xml::XMLElement ("ObjectRecognitionResults");

      //write <meta> tag
      xml::XMLElement *xmlMeta = new xml::XMLElement ("meta");
      xml::XMLElement *xmlTimebase = new xml::XMLElement ("timebase");
      xmlTimebase->addAttribute ( xml::XMLAttribute("num", toString (m_meta.timebase_num)) );
      xmlTimebase->addAttribute ( xml::XMLAttribute("denum", toString (m_meta.timebase_denum)) );
      xmlMeta->addChildElement (xmlTimebase);
      xml::XMLElement *xmlFramesize = new xml::XMLElement ("framesize");
      xmlFramesize->addAttribute ( xml::XMLAttribute("width", toString (m_meta.width)) );
      xmlFramesize->addAttribute ( xml::XMLAttribute("height", toString (m_meta.height)) );
      xmlMeta->addChildElement (xmlFramesize);
      xml::XMLElement *xmlAnnotDetails = new xml::XMLElement ("annotation_details");
      xmlAnnotDetails->addAttribute(xml::XMLAttribute("algorithm", m_meta.algorithm));
      xmlMeta->addChildElement (xmlAnnotDetails);
      xmlReportRoot->addChildElement (xmlMeta);

      std::map< long, std::string > saveLabels;

      //loop over frames <frame timestamp="...">
      xml::XMLElement *xmlFrames = new xml::XMLElement ("frames");
      typedef std::map<uint64_t, ObjectRecognitionResults > framesType;
      BOOST_FOREACH (const framesType::value_type& frameIt, m_results)
      {
        xml::XMLElement *xmlFrame = new xml::XMLElement ("frame");
        xmlFrame->addAttribute ( xml::XMLAttribute("timestamp", toString(frameIt.first)) );

        //loop over objects <object uid="..." type="...">
        xml::XMLElement *xmlObjects = new xml::XMLElement ("objects");
        BOOST_FOREACH (const std::vector< Object >::value_type& objIt, frameIt.second.objects)
        {
          xml::XMLElement *xmlObject = new xml::XMLElement ("object");
          xmlObject->addAttribute ( xml::XMLAttribute("uid", toString (objIt.uid)) );
          if (objIt.type == FACE)
            xmlObject->addAttribute ( xml::XMLAttribute("type", "FACE") );
          if (objIt.type == ANIMAL)
            xmlObject->addAttribute ( xml::XMLAttribute("type", "ANIMAL") );
          xmlObjects->addChildElement (xmlObject);

          xml::XMLElement *xmlRegion = new xml::XMLElement ("region");
          BOOST_FOREACH (const std::vector< Point >::value_type& pointIt, objIt.region)
          {
            xml::XMLElement *xmlPoint = new xml::XMLElement ("point");
            xmlPoint->addAttribute ( xml::XMLAttribute("posX", toString (pointIt.posX/m_meta.width)) );
            xmlPoint->addAttribute ( xml::XMLAttribute("posY", toString (pointIt.posY/m_meta.height)) );
            xmlPoint->addAttribute ( xml::XMLAttribute("orderIdx", toString (pointIt.orderIdx)) );
            if (!pointIt.posHint.empty())
              xmlPoint->addAttribute ( xml::XMLAttribute("posHint", pointIt.posHint) );
            xmlRegion->addChildElement (xmlPoint);
          }
          xmlObject->addChildElement (xmlRegion);

          if (objIt.markers.size()) {
            typedef std::multimap< ObjectMarkerType , Point > markersType;
            BOOST_FOREACH (const markersType::value_type& markerIt , objIt.markers)
            {
              xml::XMLElement *xmlMarker = new xml::XMLElement ("marker");
              xmlMarker->addAttribute ( xml::XMLAttribute("posX", toString (markerIt.second.posX/m_meta.width)) );
              xmlMarker->addAttribute ( xml::XMLAttribute("posY", toString (markerIt.second.posY/m_meta.height)) );
              xmlMarker->addAttribute ( xml::XMLAttribute("orderIdx", toString (markerIt.second.orderIdx)) );
              if (markerIt.first == LEFT_EYE)
                xmlMarker->addAttribute ( xml::XMLAttribute("type", "LEFT_EYE") );
              if (markerIt.first == RIGHT_EYE)
                xmlMarker->addAttribute ( xml::XMLAttribute("type", "RIGHT_EYE") );
              if (markerIt.first == MOUTH_CENTER)
                xmlMarker->addAttribute ( xml::XMLAttribute("type", "MOUTH_CENTER") );
              xmlObject->addChildElement (xmlMarker);
            }
          }

        }
        xmlFrame->addChildElement (xmlObjects);

        //loop over (labels)mappings
        if(frameIt.second.labelMapping.size()) {
          xml::XMLElement *xmlMappings = new xml::XMLElement ("mappings");
          typedef std::map<long, std::vector<ObjectLabel> > labelMappingType;
          BOOST_FOREACH (const labelMappingType::value_type& labelsIt, frameIt.second.labelMapping)
          {
            BOOST_FOREACH (const std::vector<ObjectLabel>::value_type& labelIt, labelsIt.second)
            {
              xml::XMLElement *xmlMapping = new xml::XMLElement ("mapping");
              xmlMapping->addAttribute ( xml::XMLAttribute("object_ref", toString (labelsIt.first)) );
              xmlMapping->addAttribute ( xml::XMLAttribute("label_ref",  toString (labelIt.uid)) );
              xmlMapping->addAttribute ( xml::XMLAttribute("confidence", toString (labelIt.confidence)) );
              xmlMappings->addChildElement (xmlMapping);

              saveLabels[ labelIt.uid ] = labelIt.name; //inserts or updates on uid of label
            }
          } //end mappings
          xmlFrame->addChildElement (xmlMappings);
        }

        xmlFrames->addChildElement (xmlFrame);
      } //end frames
      xmlReportRoot->addChildElement (xmlFrames);

      //write saved labels once
      if ( saveLabels.size() ) {
        xml::XMLElement *xmlLabels = new xml::XMLElement ("labels");
        typedef std::map< long, std::string > saveLabelsType;
        BOOST_FOREACH (const saveLabelsType::value_type& labelIt, saveLabels)
        {
          xml::XMLElement *xmlLabel = new xml::XMLElement ("label");
          xmlLabel->addAttribute ( xml::XMLAttribute("uid", toString (labelIt.first)) );
          xmlLabel->addAttribute ( xml::XMLAttribute("name", labelIt.second) );
          xmlLabels->addChildElement (xmlLabel);
        }
        xmlReportRoot->addChildElement (xmlLabels);
      }

      return xmlReportRoot;
    }
  };
}
#endif  /* OBJECTRECOOUTPUTHANDLER_H_ */
