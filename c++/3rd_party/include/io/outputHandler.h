/* 
 * File:   outputHandler.h
 * Author: Sascha Krämer (krm)
 */

#ifndef OUTPUTHANDLER_H
#define	OUTPUTHANDLER_H

#include <map>
#include <string>
#include <sstream>

#include <boost/foreach.hpp>
#include <boost/smart_ptr.hpp>

#include "XMLDocumentTiXml.h"
#include "XMLDocumentJsonCpp.h"

namespace vaoutput
{
  //############# structured types belonging to VideoanalysisSchemaQuality.xsd #############

  struct RoiAttType {
    unsigned int x;
    unsigned int y;
    unsigned int w;
    unsigned int h;
    unsigned long fromIdx;
    unsigned long toIdx;
    RoiAttType(): x(0), y(0), w(0), h(0), fromIdx(0), toIdx(0) {}
  };

  typedef std::vector< RoiAttType > globalsType;

  typedef std::map<avanalysis::AVAnalysisParameterType, float> parametersetType;

  typedef std::map<avanalysis::AVAnalysisFeatureType, parametersetType > moduleConfigType;

  typedef std::map<unsigned long, float> frameType;

  typedef std::map<avanalysis::AVAnalysisFeatureType, frameType > qualityType;

  typedef frameType timesType;

  //util function convert T to string
  template<class T> std::string toString( const T& i ) {
    std::stringstream s;
    s<<i;
    return s.str();
  }

  /**
   * outputHandler class for saving videoanalysis extractions in xml
   */
  class outputHandler
  {
  public:
    /**
     * Constructor
     * @param qresults: holds analysed data written to quality-xml
     * @param globals: holds global stuff (roi from-to) written to config-xml
     * @param parameters: holds parameter for each module-type written to config-xml
     * @param times: holds times for each processed frame written to time-xml (optional)
     */
    outputHandler (std::string snamespace, qualityType qresults, globalsType globals, moduleConfigType parameters, timesType times = timesType() )
     : m_namespace (snamespace),
       m_qresults (qresults),
       m_globals (globals),
       m_parameters(parameters),
       m_times(times)
     {}

    /**
     * toXmlFile
     * @param fileName: path to xml file
     */
    void toXmlFile (const std::string& fileName)
    {
      xml::XMLNamespace xmlns(std::string("http://www.idmt.fraunhofer.de/")+m_namespace,m_namespace);
      //write config-xml
      boost::scoped_ptr<xml::XMLDocumentTiXml> xmlConfigOutput( new xml::XMLDocumentTiXml () );
      xmlConfigOutput->setRootElement ( getConfigXml() );
      xmlConfigOutput->addNamespace(xmlns);
      xmlConfigOutput->write (fileName+std::string(".conf.xml"));
      //write quality-xml
      boost::scoped_ptr<xml::XMLDocumentTiXml> xmlQualityOutput ( new xml::XMLDocumentTiXml () );
      xmlQualityOutput->setRootElement ( getQualityXml (fileName+std::string(".conf.xml")) );
      xmlQualityOutput->addNamespace(xmlns);
      xmlQualityOutput->write (fileName+std::string(".xml"));

      if (!m_times.empty()) {
        //write time-xml
        boost::scoped_ptr<xml::XMLDocumentTiXml> xmlTimeOutput ( new xml::XMLDocumentTiXml () );
        xmlTimeOutput->setRootElement ( getTimeXml (fileName+std::string(".conf.xml")) );
        xmlTimeOutput->addNamespace(xmlns);
        xmlTimeOutput->write (fileName+std::string(".time.xml"));
      }
    }

    /**
     * toXmlStream
     * @param os: ostream for xml output
     */
    void toXmlStream (std::ostream& os)
    {
      xml::XMLNamespace xmlns(std::string("http://www.idmt.fraunhofer.de/")+m_namespace,m_namespace);
      //write quality-xml-stream
      boost::scoped_ptr<xml::XMLDocumentTiXml> xmlQualityOutput ( new xml::XMLDocumentTiXml () );
      xmlQualityOutput->setRootElement ( getQualityXml ("in-stream-not-set") );
      xmlQualityOutput->addNamespace(xmlns);
      xmlQualityOutput->write (os);
    }

    /**
     * toJsonFile
     * @param fileName: path to json file
     */
    void toJsonFile (const std::string& fileName)
    {
      xml::XMLNamespace xmlns(std::string("http://www.idmt.fraunhofer.de/")+m_namespace,m_namespace);
      //write config-json
      boost::scoped_ptr<xml::XMLDocumentJsonCpp> jsonConfigOutput( new xml::XMLDocumentJsonCpp () );
      jsonConfigOutput->setRootElement ( getConfigXml(true) );
      //jsonConfigOutput->addNamespace(xmlns);
      jsonConfigOutput->write (fileName+std::string(".conf.json"));
      //write quality-json
      boost::scoped_ptr<xml::XMLDocumentJsonCpp> jsonQualityOutput ( new xml::XMLDocumentJsonCpp () );
      jsonQualityOutput->setRootElement ( getQualityXml (fileName+std::string(".conf.json"),true) );
      //jsonQualityOutput->addNamespace(xmlns);
      jsonQualityOutput->write (fileName+std::string(".json"));

      if (!m_times.empty()) {
        //write time-json
        boost::scoped_ptr<xml::XMLDocumentJsonCpp> jsonTimeOutput ( new xml::XMLDocumentJsonCpp () );
        jsonTimeOutput->setRootElement ( getTimeXml (fileName+std::string(".conf.json"),true) );
        //jsonTimeOutput->addNamespace(xmlns);
        jsonTimeOutput->write (fileName+std::string(".time.json"));
      }
    }

    /**
     * toJsonStream
     * @param os: ostream to json output
     */
    void toJsonStream (std::ostream& os)
    {
      xml::XMLNamespace xmlns(std::string("http://www.idmt.fraunhofer.de/")+m_namespace,m_namespace);
      //write quality-json
      boost::scoped_ptr<xml::XMLDocumentJsonCpp> jsonQualityOutput ( new xml::XMLDocumentJsonCpp () );
      jsonQualityOutput->setRootElement ( getQualityXml ("in-stream-not-set",true) );
      //jsonQualityOutput->addNamespace(xmlns);
      jsonQualityOutput->write (os);
    }

  private:

    std::string m_namespace;

    qualityType m_qresults;
 
    globalsType m_globals;

    moduleConfigType m_parameters;

    timesType m_times;

    //-------------- Config XML --------------
    xml::XMLElement* getConfigXml (bool usedForJsonGeneration=false)
    {
      xml::XMLElement *xmlConfigRoot = new xml::XMLElement ("videoanalysis");
      if (!usedForJsonGeneration) {
        xmlConfigRoot->addAttribute ( xml::XMLAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance") );
        xmlConfigRoot->addAttribute ( xml::XMLAttribute("xsi:noNamespaceSchemaLocation", "VideoanalysisConfig.xsd") );
      }

     //loop over modules <module name="...">
      BOOST_FOREACH (const moduleConfigType::value_type& eventIt, m_parameters)
      {
        xml::XMLElement *xmlModule = new xml::XMLElement ("module");
        xmlModule->addAttribute ( xml::XMLAttribute("name", eventIt.first.getName()) );
        xmlConfigRoot->addChildElement (xmlModule);

        //loop over parameters <parameter name="..." value="..."/>...
        BOOST_FOREACH (const parametersetType::value_type& pit, eventIt.second)
        {
          xml::XMLElement *xmlReportParameter = new xml::XMLElement ("parameter");

          xmlReportParameter->addAttribute ( xml::XMLAttribute("name",  pit.first.getName()) );
          xmlReportParameter->addAttribute ( xml::XMLAttribute("value", toString (pit.second)) );
            
          xmlModule->addChildElement (xmlReportParameter);
        }
      }
      xml::XMLElement *xmlConfigGlobals = new xml::XMLElement ("globals");
      BOOST_FOREACH (const globalsType::value_type& roi, m_globals)
      {
        //add ROI <roi x="..." y="..." w="..." h="..." fromId=..." toId="..."/>
        if (roi.w != 0 && roi.h != 0) {
          xml::XMLElement *xmlReportRoi = new xml::XMLElement ("roi");
          xmlReportRoi->addAttribute ( xml::XMLAttribute("x", toString (roi.x)) );
          xmlReportRoi->addAttribute ( xml::XMLAttribute("y", toString (roi.y)) );
          xmlReportRoi->addAttribute ( xml::XMLAttribute("w", toString (roi.w)) );
          xmlReportRoi->addAttribute ( xml::XMLAttribute("h", toString (roi.h)) );
          xmlReportRoi->addAttribute ( xml::XMLAttribute("from-Idx", toString (roi.fromIdx)) );
          xmlReportRoi->addAttribute ( xml::XMLAttribute("to-Idx",   toString (roi.toIdx)) );
          xmlConfigGlobals->addChildElement (xmlReportRoi);
        }
        xmlConfigRoot->addChildElement (xmlConfigGlobals);
      }

      return xmlConfigRoot;
    }

    //-------------- Quality XML --------------
    xml::XMLElement* getQualityXml (const std::string& fileName, bool usedForJsonGeneration = false)
    {
      xml::XMLElement *xmlReportRoot = new xml::XMLElement ("videoanalysis");
      if (!usedForJsonGeneration) {
        xmlReportRoot->addAttribute ( xml::XMLAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance") );
        xmlReportRoot->addAttribute ( xml::XMLAttribute("xsi:noNamespaceSchemaLocation", "VideoanalysisQuality.xsd") );
      }
        

      xmlReportRoot->addAttribute ( xml::XMLAttribute("config-file", fileName) );

      //loop over modules <module name="...">
      BOOST_FOREACH (const qualityType::value_type& eventIt, m_qresults)
      {
        xml::XMLElement *xmlReportEventId = new xml::XMLElement ("module");
        xmlReportEventId->addAttribute ( xml::XMLAttribute("name", eventIt.first.getName()) );
        xmlReportRoot->addChildElement (xmlReportEventId);

        //loop over frames <frame id="..." value="..."/>
        //or on repeated values <frame from-id="..." to-id="..." value="..."/>
        float repeatedValue = 0.f;
        unsigned long repeatedFromId = 0;
        unsigned long repeatedToId = 0;
        bool allFramesWritten = false;
        bool isFirstCall = true;
        BOOST_FOREACH (const frameType::value_type& it, eventIt.second)
        {
          allFramesWritten = false;
          if(repeatedValue != it.second && !isFirstCall) {
            
            xml::XMLElement *xmlReportEvent = 0;
            if(repeatedFromId == repeatedToId) {
              xmlReportEvent = new xml::XMLElement ("frame");
              xmlReportEvent->addAttribute ( xml::XMLAttribute("idx", toString (repeatedFromId)) );
            } else {
              xmlReportEvent = new xml::XMLElement ("frame-range");
              xmlReportEvent->addAttribute ( xml::XMLAttribute("from-idx", toString (repeatedFromId)) );
              xmlReportEvent->addAttribute ( xml::XMLAttribute("to-idx",   toString (repeatedToId)) );
            }
            xmlReportEvent->addAttribute ( xml::XMLAttribute("value",   toString (repeatedValue)) );
            xmlReportEventId->addChildElement (xmlReportEvent);
            allFramesWritten = true;
            repeatedFromId = it.first;
          }
          repeatedValue  = it.second;
          repeatedToId = it.first;
          isFirstCall = false;
        }
        if(!allFramesWritten) {
          xml::XMLElement *xmlReportEvent = 0;

          if(repeatedFromId == repeatedToId) {
            xmlReportEvent = new xml::XMLElement ("frame");
            xmlReportEvent->addAttribute ( xml::XMLAttribute("idx", toString (repeatedFromId)) );
          } else {
            xmlReportEvent = new xml::XMLElement ("frame-range");
            xmlReportEvent->addAttribute ( xml::XMLAttribute("from-idx", toString (repeatedFromId)) );
            xmlReportEvent->addAttribute ( xml::XMLAttribute("to-idx",   toString (repeatedToId)) );
          }
          xmlReportEvent->addAttribute ( xml::XMLAttribute("value",   toString (repeatedValue)) );
          xmlReportEventId->addChildElement (xmlReportEvent);
        }
      }

      return xmlReportRoot;
    }

    //-------------- Time XML --------------
    xml::XMLElement* getTimeXml (const std::string& fileName, bool usedForJsonGeneration = false)
    {
      xml::XMLElement *xmlTimeRoot = new xml::XMLElement ("videoanalysis");
      if (!usedForJsonGeneration) {
        xmlTimeRoot->addAttribute ( xml::XMLAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance") );
        xmlTimeRoot->addAttribute ( xml::XMLAttribute("xsi:noNamespaceSchemaLocation", "VideoanalysisTime.xsd") );
      }
      xmlTimeRoot->addAttribute ( xml::XMLAttribute("config-file", fileName) );

      //loop over frames <frame id="..." value="..."/>
      BOOST_FOREACH (const frameType::value_type& it, m_times)
      {
        xml::XMLElement *xmlFrame = new xml::XMLElement ("frame");

        xmlFrame->addAttribute ( xml::XMLAttribute("idx", toString (it.first)) );
        xmlFrame->addAttribute ( xml::XMLAttribute("time",   toString (it.second)) );

        xmlTimeRoot->addChildElement (xmlFrame);
      }

      return xmlTimeRoot;
    }
  };
}
#endif	/* OUTPUTHANDLER_H_ */
