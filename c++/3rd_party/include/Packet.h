#ifndef PACKET_H_
#define PACKET_H_

extern "C" {
	#include "libavcodec/avcodec.h"
	#include "libavformat/avformat.h"
}


struct Packet {
	explicit Packet(AVFormatContext* ctxt = nullptr) {
		av_init_packet(&packet);
		packet.data = nullptr;
		packet.size = 0;
		if (ctxt)
			reset(ctxt);
	}

	Packet(Packet&& other) :
			packet(std::move(other.packet)) {
		other.packet.data = nullptr;
	}

	~Packet() {
		if (packet.data)
			av_free_packet(&packet);
	}

	void reset(AVFormatContext* ctxt) {
		if (packet.data)
			av_free_packet(&packet);
		if (av_read_frame(ctxt, &packet) < 0)
			packet.data = nullptr;
	}

	AVPacket packet;
};


#endif /* PACKET_H_ */
