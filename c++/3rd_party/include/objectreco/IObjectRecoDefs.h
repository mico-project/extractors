﻿/**
 * @file IObjectRecoDefs.h
 *
 * (c) 2014 Fraunhofer IDMT - bem-dev@idmt.fraunhofer.de
 */

#ifndef __IOBJECTRECODEFS_H__
#define __IOBJECTRECODEFS_H__

#include<string>
#include<vector>
#include<map>

namespace objectreco
{
  /**
   * @brief Configuration Mode of Face Recognition.
   */
  enum ERecoConfigMode
  {
    IMAGE_MODE = 0,     // Detection and Recognition per image
    VIDEO_MODE = 1,     // Recognition with dull track FQM analysis
    LIVE_VIDEO_MODE = 2, // Recognition with FQM used based on threshold
    IMAGE_DETECTION_ONLY_MODE = 3, // Detection only per image
    VIDEO_DETECTION_ONLY_MODE = 4  // Detection only on video
  };

  enum EObjectTypeConfigMode
  {
	  HUMAN_MODE = 0,
	  CHIMP_MODE = 1,
	  CHIMP_TINY_MODE = 2,
	  CHIMP_AGE_GROUP_MODE = 3,
	  GORILLA_MODE = 4,
	  GORILLA_TINY_MODE = 5,
	  CHIMP_AND_GORILLA_MODE = 6
  };

  /**
   * @brief Object Marker Type.
   */
  enum ObjectMarkerType
  {
    LEFT_EYE = 0,
    RIGHT_EYE = 1,
    MOUTH_CENTER = 2
  };

  /**
   * @brief Object Type.
   */
  enum ObjectType
  {
    FACE = 0,
    EYE = 1,
    NOSE = 2,
    MOUTH = 3,
    ANIMAL = 4
  };

  struct Point
  {
    double posX;
    double posY;
    unsigned int orderIdx;
    std::string posHint;
    Point()
     : posX(0), posY(0), orderIdx(0), posHint(""){};
    Point(double x, double y, unsigned int i)
     : posX(x), posY(y), orderIdx(i), posHint(""){};
    Point(double x, double y, unsigned int i, std::string h)
     : posX(x), posY(y), orderIdx(i), posHint(h){};
  };

  struct Object
  {
    long uid;
    ObjectType type;
    std::vector<Point> region;
    std::vector<Object> objects;
    std::multimap<ObjectMarkerType,Point> markers;
    std::map<std::string,double> attributes;
    Object()
     : uid(0), type(FACE){};
    Object(long id, ObjectType _type, std::vector<Point> _region, std::multimap<ObjectMarkerType,Point> _markers)
     : uid(id), type(_type), region(_region), markers(_markers){};
    void reset() {
      uid = 0;
      type = FACE;
      region.clear();
      objects.clear();
      markers.clear();
      attributes.clear();
    };
  };

  struct ObjectLabel
  {
    long uid;
    std::string name;
    float confidence;
    ObjectLabel()
     : uid(1000), name(""), confidence(0){};
    ObjectLabel(long id, std::string n, float c)
     : uid(id), name(n), confidence(c){};
    bool operator<(const ObjectLabel& other) const
    {
      return confidence < other.confidence;
    };
    bool operator==(const ObjectLabel& other) const
    {
      return uid == other.uid;
    };
  };

  struct ObjectRecognitionResults
  {
    std::vector<Object> objects;
    std::map<long, std::vector<ObjectLabel> > labelMapping;
  };

};

#endif // __IOBJECTRECODEFS_H__
