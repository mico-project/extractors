/**
            (C) copyright Fraunhofer - IDMT (2008)
                     All Rights Reserved


 @file     XchangeStreaming.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     2009/01/31
 @brief    stream and unstream functions to write common data types (STL, SPTL).

*/

#ifndef XCHANGESTREAMING_H_
#define XCHANGESTREAMING_H_

#include "XchangeDefines.h"

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>

template <class T> class BaseMatrix;
template <class T> class HeapMatrix;
template <class T> class HeapVector;
struct SChordInfo;

namespace xc
{

  /**
   *
   * @brief   Stream strings to ostream.
   *
   * @param   os  Output stream.
   * @param   str Data object to write.
   * @return  Number of bytes written.
   *
   */
  stream_size stream(std::ostream& os, const std::string& str);

  /**
   *
   * @brief   Read strings from istream.
   *
   * @param   is  Input stream.
   * @param   str Data object to read.
   * @return  Number of bytes read.
   *
   */
  stream_size unstream(std::istream&is, std::string& dst);

  /**
   *
   * @brief   Read strings from istream.
   *
   * @param   is  Input stream.
   * @return  Read string.
   *
   */
  std::string unstream(std::istream& is);

  /**
   *
   * @brief   Stream generic types to ostream.
   *
   * @param   os  Output stream.
   * @param   obj Data object to write.
   * @return  Number of bytes written.
   *
   */
  template<class T>
  stream_size stream( std::ostream& os , const T &obj );

  /**
   *
   * @brief   Read generic types from istream.
   *
   * @param   is  Input stream.
   * @param   dst Data object to write.
   * @return  Number of bytes read.
   *
   */
  template<class T>
  stream_size unstream( std::istream& is , T& dst );

  /**
   *
   * @brief   Stream generic types to ostream.
   *
   * @param   os    Output stream.
   * @param   obj   Pointer to array of data object to write.
   * @param   size  Number of elements in the array.
   * @return  Number of bytes written.
   *
   */
  template<class T>
  stream_size stream(std::ostream& os, const T* obj, stream_size size)
  {
    stream_size iSizeInByte=sizeof(T)*size;
    os.write(reinterpret_cast<const char*>(obj), iSizeInByte);
    return iSizeInByte;
  }

  /**
   *
   * @brief   Read generic types from istream.
   *
   * @param   is    Input stream.
   * @param   dst   Pointer to array of data object to fill.
   * @param   size  Number of elements in the array.
   * @return  Number of bytes read.
   *
   */
  template<class T>
  stream_size unstream(std::istream& is, T* dst, stream_size size)
  {
    stream_size iSize = (sizeof(T)*size);
    is.read(reinterpret_cast<char*>(dst), iSize);
    return iSize;
  }

  /**
   *
   * @brief   Generic streaming to a given stream position.
   *          Afterwards, the put pointer is seeked back to the previous position.
   *
   * @param   os  Output stream.
   * @param   dst Position to write data object.
   * @param   obj Data object to write.
   * @return  Number of bytes written.
   *
   */
  template<class T>
  stream_size stream(std::ostream& os, std::ostream::pos_type dst, const T& obj) //generic streaming to ostream
  {
      std::ostream::pos_type backpos=os.tellp();
      os.seekp(dst);
      stream_size iSizeInByte=stream(os, obj); //call generic stream method above
      std::ostream::pos_type now=os.tellp();
      if((long)now<(long)backpos)
          os.seekp(backpos);
      return iSizeInByte;
  }

  /**
   *
   * Stream std::vector to ostream.
   *
   * @param os    Output stream
   * @param v     Vector object to write
   * @return      Number of bytes written
   *
   */
  template<class T>
  stream_size stream(std::ostream& os, const std::vector<T>& v)
  {
    stream_size iBytes = xc::stream(os, (stream_size) v.size());
    typename std::vector<T>::const_iterator it=v.begin();
    typename std::vector<T>::const_iterator end=v.end();
    for(; it!=end; ++it)
        iBytes+=xc::stream(os,(*it));
    return iBytes;
  }

  /**
   *
   * @brief   Read std::vector from istream.
   *
   * @param   is    Input stream.
   * @param   v     Vector object to fill.
   * @return  Number of bytes read.
   *
   */
  template<class T>
  stream_size unstream(std::istream& is, std::vector<T>& v)
  {
    stream_size iBytesRead = 0;
    stream_size iSize=0;
    iBytesRead += xc::unstream(is, iSize);
    v.resize(iSize);
    for(stream_size i=0; i<iSize; ++i)
    {
        T t;
        iBytesRead += xc::unstream(is,t);
        v[i]=t;
    }
    return iBytesRead;
  }

  /**
   *
   * @brief   Stream std::set to ostream.
   *
   * @param   os    Output stream.
   * @param   s     Set object to write.
   * @return  Number of bytes written.
   *
   */
  template<class T, class C, class A>
  stream_size stream(std::ostream& os, const std::set<T,C,A>& s)
  {
    stream_size iBytes = xc::stream(os,(stream_size)s.size());
    typename std::set<T>::const_iterator it=s.begin();
    typename std::set<T>::const_iterator end=s.end();
    for(; it!=end; ++it)
        iBytes+=xc::stream(os,(*it));
    return iBytes;
  }

  /**
   *
   * @brief   Read std::set from istream.
   *
   * @param   is    Input stream.
   * @param   s     Set object to fill.
   * @return  Number of bytes read.
   *
   */
  template<class T, class C, class A>
  stream_size unstream( std::istream &is , std::set<T,C,A> &s )
  {
    stream_size iSize = 0;
    stream_size iBytesRead = xc::unstream( is , iSize );

    for( stream_size i = 0; i < iSize; ++i )
    {
      T t;
      iBytesRead += xc::unstream( is , t );
      s.insert( t );
    }

    return iBytesRead;
  }

  /**
   *
   * @brief   Stream std::multiset to ostream.
   *
   * @param   os    Output stream.
   * @param   s     Set object to write.
   * @return  Number of bytes written.
   *
   */
  template<class T, class C, class A>
  stream_size stream( std::ostream &os , const std::multiset<T,C,A> &s )
  {
    stream_size iBytes = xc::stream( os , stream_size( s.size() ) );

    typename std::multiset<T>::const_iterator it = s.begin();
    typename std::multiset<T>::const_iterator end = s.end();
    for( ; it != end; ++it )
      iBytes+=xc::stream( os ,( *it ) );

    return iBytes;
  }

  /**
   *
   * @brief   Read std::multiset from istream.
   *
   * @param   is    Input stream.
   * @param   s     Set object to fill.
   * @return  Number of bytes read.
   *
   */
  template<class T, class C, class A>
  stream_size unstream( std::istream &is , std::multiset<T,C,A> &s )
  {
    stream_size iSize = 0;
    stream_size iBytesRead = xc::unstream( is , iSize );

    for( stream_size i = 0; i < iSize; ++i )
    {
      T t;
      iBytesRead += xc::unstream( is , t );
      s.insert( t );
    }

    return iBytesRead;
  }

  /**
   *
   * @brief   Stream std::map to ostream.
   *
   * @param   os    Output stream.
   * @param   m     Map object to write.
   * @return  Number of bytes written.
   *
   */
  template<class S, class T, class U>
  stream_size stream(std::ostream& os, const std::map<S,T,U>& m)
  {
    stream_size iBytes = xc::stream(os,(stream_size)m.size());
    typename std::map<S,T,U>::const_iterator it=m.begin();
    typename std::map<S,T,U>::const_iterator end=m.end();
    for(; it!=end; ++it)
    {
        iBytes += xc::stream(os,it->first);
        iBytes += xc::stream(os,it->second);
    }
    return iBytes;
  }

  /**
   *
   * @brief   Read std::map from istream.
   *
   * @param   is    Input stream.
   * @param   m     Map object to fill.
   * @return  Number of bytes read.
   *
   */
  template<class S, class T, class U>
  stream_size unstream(std::istream& is, std::map<S,T,U>& m)
  {
    stream_size iSize=0;
    stream_size iBytesRead = xc::unstream(is, iSize);
    for(stream_size i=0; i<iSize; ++i)
    {
        S s;
        iBytesRead += xc::unstream(is,s);
        iBytesRead += xc::unstream(is,m[s]);
    }
    return iBytesRead;
  }

  /**
   *
   * @brief   Stream HeapMatrix to ostream.
   *
   * @param   os    Output stream.
   * @param   mat   HeapMatrix object to write.
   * @return  Number of bytes written.
   *
   */
  template<class T>
  stream_size stream(std::ostream& os, const HeapMatrix<T>& mat)
  {
      std::ios::pos_type pos=os.tellp();
      mat.save(os,"bin"); //forwarding
      return static_cast<stream_size>( os.tellp() - pos );
  }

  /**
   *
   * @brief   Read HeapMatrix from istream.
   *
   * @param   is    Input stream.
   * @param   dst   HeapMatrix object to fill.
   * @return  Number of bytes read.
   *
   */
  template<class T>
  stream_size unstream(std::istream& is, HeapMatrix<T>& dst)
  {
      std::ios::pos_type pos=is.tellg();
      dst.load(is); //forwarding
      return static_cast<stream_size>( is.tellg() - pos );
  }

  /**
   *
   * @brief   Stream BaseMatrix to ostream.
   *
   * @param   os    Output stream.
   * @param   mat   HeapMatrix object to write.
   * @return  Number of bytes written.
   *
   */
  template<class T>
  stream_size stream(std::ostream& os, const BaseMatrix<T>& mat)
  {
      std::ios::pos_type pos=os.tellp();
      mat.save(os,"bin"); //forwarding
      return static_cast<stream_size>( os.tellp() - pos );
  }

  /**
   *
   * @brief   Stream HeapVector to ostream.
   *
   * @param   os    Output stream.
   * @param   vec   HeapMatrix object to write.
   * @return  Number of bytes written.
   *
   */
  template<class T>
  stream_size stream(std::ostream& os, const HeapVector<T>& vec)
  {
      std::ios::pos_type pos=os.tellp();
      vec.save(os,"bin"); //forwarding
      return static_cast<stream_size>( os.tellp() - pos );
  }

  /**
   *
   * @brief   Read HeapVector from istream.
   *
   * @param   is    Input stream.
   * @param   dst   HeapVector object to fill.
   * @return  Number of bytes read.
   *
   */
  template<class T>
  stream_size unstream(std::istream& is, HeapVector<T>& dst)
  {
      std::ios::pos_type pos=is.tellg();
      dst.load(is); //forwarding
      return static_cast<stream_size>( is.tellg() - pos );
  }

}


#endif /* XCHANGESTREAMING_H_ */
