/**
            (C) copyright Fraunhofer - IDMT (2010)
                     All Rights Reserved


 @file     XMLAttribute.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     18.11.2010
 @brief    Represents a single XML attribute.

*/

#ifndef XMLATTRIBUTE_H_
#define XMLATTRIBUTE_H_

#include <string>

namespace xml
{

  /**
   *
   * @brief   Represents a single XML attribute.
   *
   */
  struct XMLAttribute
  {
    /**
     *
     * @brief   Default constructor.
     *
     */
    XMLAttribute(){};

    /**
     *
     * @brief   Constructor.
     *
     * @param   sName   Attribute name.
     * @param   sValue  Attribute value.
     *
     */
    XMLAttribute( const std::string &sName , const std::string &sValue ) : name( sName ), value( sValue ) {};

    /**
     *
     * @brief   Constructor.
     *
     * @param   sNS     Namespace URI.
     * @param   sName   Attribute name.
     * @param   sValue  Attribute value.
     *
     */
    XMLAttribute( const std::string &sNS , const std::string &sName , const std::string &sValue ) : ns ( sNS ) , name( sName ), value( sValue ) {};

    /**
     * @brief   Attribute namespace.
     */
    std::string ns;

    /**
     * @brief   Attribute name.
     */
    std::string name;

    /**
     * @brief   Attribute value.
     */
    std::string value;

  };
}

#endif /* XMLATTRIBUTE_H_ */
