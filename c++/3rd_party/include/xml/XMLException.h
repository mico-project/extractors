/**
            (C) copyright Fraunhofer - IDMT (2010)
                     All Rights Reserved


 @file     XMLException.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     17.11.2010
 @brief    Exception thrown by XML instances.

*/

#ifndef XMLEXCEPTION_H_
#define XMLEXCEPTION_H_

#include "standardexception.h"

namespace xml
{

    /**
     *
     * @brief   Exception thrown by XML instances.
     *
     */
    class XMLException: public StandardException
    {
    public:

      /**
       *
       * @brief   Standard constructor.
       *
       * @param   msg   Error message.
       *
       */
      XMLException( const std::string &msg ) : StandardException( msg ) {};

    };

}

#endif /* XMLEXCEPTION_H_ */
