/**
            (C) copyright Fraunhofer - IDMT (2010)
                     All Rights Reserved


 @file     XMLDocument.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     2010/01/28
 @brief    Base class for all XML document types.

*/

#ifndef XMLDOCUMENT_H_
#define XMLDOCUMENT_H_

#include "XMLElement.h"
#include "XMLNamespaces.h"

#include <string>

namespace xml
{

  /**
   *
   * @brief Base class for all XML document types.
   *
   */
  class XMLDocument
  {
  public:
    /**
     *
     * @brief   Release all XML elements.
     *
     */
    virtual ~XMLDocument();

    /**
     *
     * @brief   Release all XML elements.
     *
     */
    virtual void release();

    /**
     *
     * @brief   Open and parse an XML file.
     *
     * @param   sFileName   File location.
     *
     */
    virtual void open( const std::string &sFileName ) = 0;

    /**
     *
     * @brief   Write XML file formatted.
     *
     * @param   filename  Target location.
     *
     */
    virtual void write( const std::string &filename ) const = 0;

    /**
     *
     * @brief   Set the root element of the document.
     *
     *          The document will take ownership of the root element and all its children.
     *          This means that all elements are deleted when the document is destroyed.
     *          If a root element is already assigned to that document it will be deleted before assigning the new root element.
     *
     * @param   pElement  Root element.
     *
     */
    virtual void setRootElement( XMLElement *pElement );

    /**
     *
     * @brief   Get the root element of the document.
     *
     * @return  Root element.
     *
     */
    virtual const XMLElement *getRootElement() const { return m_pRootElement; }

    /**
     *
     * @brief   Get the root element of the document.
     *
     * @return  Root element.
     *
     */
    virtual XMLElement *getRootElement() { return m_pRootElement; }

    /**
     *
     * @brief   Add a namespace definition to the document.
     *
     * @param   ns XML namespace.
     *
     */
    virtual void addNamespace( const XMLNamespace &ns ) { m_namespaces.insert( ns ); }

    /**
     *
     * @brief   Get the namespace id of associated to a namespace URI.
     *
     *          If the namespace URI was not found, an XMLException is thrown.
     *
     * @param  sNSUri  Namespace URI.
     * @return Appropriate namespace id.
     *
     */
    virtual const std::string &getNamespaceId( const std::string &sNSUri ) const { return m_namespaces.getId( sNSUri ); }

    /**
     *
     * @brief   Set the xmlns definition of the document.
     *
     * @param   sNSUri  Namespace URI.
     *
     */
    virtual void setXMLNameSpace( const std::string &sNSUri ) { m_sXMLNameSpaceUri = sNSUri; }

    /**
     *
     * @brief   Set a custom XML header declaration.
     *
     * @param   sDeclaration  Header declaration to use.
     *
     */
    virtual void setDeclaration( const std::string &sDeclaration ) { m_sDeclaration = sDeclaration; }

    /**
     *
     * @brief   Compose the root element and its children to XML formatted stream and write to output stream.
     *
     * @param   os              Output stream.
     * @param   bHumanReadable  Format the stream data to make it human readable.
     * @param   iIndent         The current indent (only used if formatting is used).
     *
     */
    void compose( std::ostream &os , bool bHumanReadable = false , unsigned int iIndent = 0 ) const;

  protected:

    friend class XMLElement;

    /**
     *
     * @brief   Register a namespace URI at the document.
     *
     *          If the URI is already known, nothing is done.
     *
     * @param   sNSUri  Namespace URI.
     *
     */
    void registerNamespaceUri( const std::string &sNSUri ) { m_namespaces.registerNamespaceUri( sNSUri ); }

    /**
     * @brief   Default constructor.
     */
    XMLDocument();

    /**
     * @brief   The XML root element.
     */
    XMLElement *m_pRootElement;

    /**
     * @brief   All registered namespace definitions.
     */
    XMLNamespaces m_namespaces;

    /**
     * @brief   xmlns definition of the document.
     */
    std::string m_sXMLNameSpaceUri;

    /**
     * @brief   Custom XML header declaration. If no value is given, the standard declaration will be used.
     */
    std::string m_sDeclaration;

  private:

    /**
     * @brief   Hide copy constructor.
     */
    XMLDocument( const XMLDocument & );

    /**
     * @brief   Hide assignment operator.
     */
    const XMLDocument &operator=( const XMLDocument & );

  };

}

#endif /* XMLDOCUMENT_H_ */
