/**
 (C) copyright Fraunhofer - IDMT (2010)
 All Rights Reserved


 @file     XMLElementList.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     17.11.2010
 @brief    List of (non-constant) XML elements.

 */

#ifndef XMLELEMENTLIST_H_
#define XMLELEMENTLIST_H_

#include <list>

namespace xml
{

  class XMLElement;

  /**
   *
   * @brief   List of (non-constant) XML elements. The elements are provided by reference, so changes to them affect changes in the parent XML tree.
   *
   */
  class XMLElementList
  {
  public:

    /**
     * @brief   Iterator to access XML elements in list.
     */
    typedef std::list<XMLElement*>::iterator iterator;

    /**
     * @brief   Const iterator to access XML elements in list.
     */
    typedef std::list<XMLElement*>::const_iterator const_iterator;

    /**
     *
     * @brief   Constructor.
     *
     */
    XMLElementList(){};

    /**
     *
     * @brief   Destructor.
     *
     */
    virtual ~XMLElementList(){};

    /**
     *
     * @brief   Clear the list elements.
     *
     */
    void clear() { m_elements.clear(); }

    /**
     *
     * @brief   Insert an XML element.
     *
     *          The XML element is not copied.
     *
     * @param   pElement  Pointer to XML element.
     *
     */
    void insert( xml::XMLElement *pElement ) { m_elements.push_back( pElement ); }

    /**
     *
     * @brief   Remove single XML element from container.
     *
     * @param   it    Position of the XML element in the container.
     * @return  Iterator pointing to the new location of the element that followed the last element erased by the function call, which is the list end if the operation erased the last element in the sequence.
     *
     */
    iterator erase( iterator it ) { return m_elements.erase( it ); }

    /**
     *
     * @brief   Get the number of elements in the list.
     *
     * @return  Number of elements.
     *
     */
    unsigned int size() const { return m_elements.size(); }

    /**
     *
     * @brief   Iterate over the elements.
     *
     * @return  Iterator to the first element.
     *
     */
    const_iterator begin() const { return m_elements.begin(); }

    /**
     *
     * @brief   Iterate over the elements.
     *
     * @return  Iterator to the end.
     *
     */
    const_iterator end() const { return m_elements.end(); }

    /**
     *
     * @brief   Iterate over the elements.
     *
     * @return  Iterator to the end.
     *
     */
    iterator begin() { return m_elements.begin(); }

    /**
     *
     * @brief   Iterate over the elements.
     *
     * @return  Iterator to the end.
     *
     */
    iterator end() { return m_elements.end(); }

  private:

    typedef std::list<XMLElement*> ElementListType;

    /**
     * @brief   XML elements.
     */
    ElementListType m_elements;

  };

}

#endif /* XMLELEMENTLIST_H_ */
