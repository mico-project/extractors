/**
            (C) copyright Fraunhofer - IDMT (2010)
                     All Rights Reserved


 @file     XMLDocumentTiXml.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     2010/01/28
 @brief    XMLDocument implementation using tinyxml library.

*/

#ifndef XMLDOCUMENTTINYXML_H_
#define XMLDOCUMENTTINYXML_H_

#include "XMLDocument.h"

class TiXmlElement;
class TiXmlDocument;
class TiXmlAttribute;

namespace xml
{

  /**
   *
   * @brief   XMLDocument implementation using tinyxml library.
   *
   */
  class XMLDocumentTiXml : public XMLDocument
  {
  public:

    /**
     *
     * @brief   Default constructor.
     *
     */
    XMLDocumentTiXml();

    /**
     *
     * @brief   Destructor.
     *
     */
    virtual ~XMLDocumentTiXml();

    /**
     *
     * @brief   Open and parse an XML file using tinyxml.
     *
     * @param   sFileName   File location.
     *
     */
    void open( const std::string &sFileName );

    /**
     *
     * @brief   Parse XML structure from input stream using tinyxml.
     *
     * @param   is  Input stream.
     *
     */
    void open( std::istream &is );

    /**
     *
     * @brief   Parse a XML formed string.
     *
     * @param   sData   XML formed string that will be parsed.
     *
     */
    void parse( const std::string &sData );

    /**
     *
     * @brief   Write XML file formatted using tinyxml.
     *
     * @param   sFileName   Target location.
     *
     */
    void write( const std::string &sFileName ) const;

    /**
     *
     * @brief   Write document to stream.
     *
     * @param   os   Output stream.
     *
     */
    void write( std::ostream &os ) const;

    static std::string createXMLStringFromXMLElement( const xml::XMLElement &xmlElement );

    static XMLElement *createXMLElementFromString( const std::string &sXMLDataAsString );

  private:

    /**
     *
     * @brief   Hide copy constructor.
     *
     */
    XMLDocumentTiXml( const XMLDocumentTiXml & );

    /**
     *
     * @brief   Hide assignment operator.
     *
     */
    const XMLDocumentTiXml &operator=( const XMLDocumentTiXml & );

    /**
     * @brief   XML tag info.
     */
    struct XMLDocumentTiXmlTagInfo
    {

      /**
       * @brief   Tag name.
       */
      std::string sTag;

      /**
       * @brief   Namespace URI.
       */
      std::string sNSUri;

      /**
       * @brief   Namespace id.
       */
      std::string sNSId;

    };

    /**
     *
     * @brief   Creates TiXmlDocument document for write
     *
     * @param   doc   Document to write xml structure to
     *
     */
    void buildTiXmlDocument(TiXmlDocument& doc) const;

    /**
     *
     * @brief   Read TiXmlDocument and create the XML tree.
     *
     * @param   document  Document to read.
     *
     */
    void parse( const TiXmlDocument &document );

    /**
     *
     * @brief   Create namespace information from string value.
     *
     * @param   value   String to detect the namespace information.
     * @return  Tag info.
     *
     */
    XMLDocumentTiXmlTagInfo getTagInfo( const std::string &value ) const;

    /**
     *
     * @brief   Create XMLElement from TinyXml element.
     *
     * @param   pTiElement  TinyXml element.
     * @return  New XML element.
     *
     */
    XMLElement *createElement( const TiXmlElement *pTiElement, const std::string& comment ) const;

    /**
     *
     * @brief   Create TiXml element from XMLElement.
     *
     * @param   pElement  XMLElement.
     * @return  New TiXML element.
     *
     */
    TiXmlElement *createTiElement( const XMLElement *pElement ) const;

    /**
     *
     * @brief   Create XMLAttribute from TiXml attribute.
     *
     * @param   pTiAttribute  TiXml attribute.
     * @return  New XMLAttribute.
     *
     */
    XMLAttribute createAttribute( const TiXmlAttribute *pTiAttribute ) const;

    /**
     * @brief   Always add the standard declaration to the document, even if a custom declaration is given.
     */
    bool m_bForceStandardDeclaration;

    /**
     * @brief   If a global namespace definition is given we will save the namespace in this string.
     *          All elements without an explicit namespace will get this global namespace.
     */
    std::string m_sGlobalNamespace;

    /**
     * @brief   Add comment to TiXml Element
     * @param   pNode     Element to get comment from
     * @param   pElement  Element to add comment to
     */
    void addTiXmlComment( const XMLElement *pNode , TiXmlElement *pElement ) const;

    /**
     *
     * @brief   Add child elements from TiXmlElement to XmlElement recursively.
     *
     * @param   pTiElement  TiXml element to get children from.
     * @param   pElement    XML Element to add children to.
     */
    void addXmlChildElements( const TiXmlElement *pTiElement , xml::XMLElement *pElement ) const;

    /**
     *
     * @brief   Add child elements from XmlElement to TiXmlElement recursively.
     *
     * @param   pNode     XML element to get children from
     * @param   pElement  TiXml element to add children to.
     */
    void addTiXmlChildElements(const XMLElement *pNode, TiXmlElement *pElement) const;

  };

}

#endif /* XMLDOCUMENTTINYXML_H_ */
