/***************************************************************************\
*
*             (c) copyright Fraunhofer - IDMT (2011)
*                    All Rights Reserved
*
*   @file     emt_defs.h
*   @project  for all projects
*   @author
*   @brief    general definitions
*
*   $Date: $
*   $Id: $
*
****************************************************************************/
#ifndef __EMT_DEFS_H
#define __EMT_DEFS_H

// for memcpy //
#ifdef __cplusplus
  #include <cstring>
#else
  #include <string.h>
#endif


//inline float long2float(long x);
//inline long float2long(float x);

/*
inline float long2float(long x)
{
  //void* t = reinterpret_cast<void*>(&x);
  //float* bkTmp = reinterpret_cast<float*>(t);
  //return *bkTmp;
  float tmp;
  memcpy(&tmp,&x,sizeof(long));
  return tmp;
}


inline long float2long(float x)
{
  long tmp;
  memcpy(&tmp,&x,sizeof(float));
  //void* t = reinterpret_cast<void*>(&x);
  //long* bkTmp = reinterpret_cast<long*>(t);
  //return *bkTmp;
  return tmp;
}
*/


// TYPEDEFS..
typedef unsigned char  uchar;
typedef unsigned short ushort;
typedef unsigned int   uint;
typedef unsigned long  ulong;

#if defined (WIN32) && !defined(__GNUC__)
typedef __int64 int64_t;
typedef unsigned __int64 uint64_t;
typedef unsigned __int16 uint16;
//added by wlt
typedef __int32 int32_t;
typedef unsigned __int32 uint32_t;
#else
typedef unsigned short int uint16; // no general uint16_t to uint16, since might conflict with _stdint.h

#include <inttypes.h>

//edited by wlt
//typedef long long int64;
//typedef unsigned long long uint64;
#endif

typedef int64_t int64;
typedef uint64_t uint64;
typedef int32_t int32;
typedef uint32_t uint32;



// MACROS

// get an array dimension..
#define dimof(a) (sizeof(a)/sizeof(a[0]))

#if defined(__BORLANDC__) && defined(_DEBUG)
#define BCC_BREAKPOINT {__emit__(0xcc);}
#else
#define BCC_BREAKPOINT {}
#endif

#if defined(_MSC_VER)

#if _MSC_VER<1300
#ifdef min
#undef min
#endif
template<typename T>
inline T min(T a,T b) { return (a < b) ? a : b;}
#ifdef max
#undef max
#endif
template<typename T>
inline T max(T a,T b) { return (a > b) ? a : b;}
#else // Visual Studio .NET
#pragma warning( disable : 4290 ) // disable C++-Ausnahmespezifikation ignoriert,..
#endif

#if _MSC_VER > 1000     // VC++
#pragma warning( disable : 4786 )   // disable warning debug symbol > 255...
#endif // _MSC_VER > 1000

#endif

#ifdef HEAP_CHECK
#ifdef _MSC_VER
#include "heapcheck/swaps.h"
#endif
#endif


#ifdef MS_HEAP_CHECK
#ifdef _MSC_VER
#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif
#endif

#ifdef WIN32
#define snprintf _snprintf
#define unlink _unlink
#endif




#define GCC_VERSION (__GNUC__ * 10000 \
                    + __GNUC_MINOR__ * 100 \
                    + __GNUC_PATCHLEVEL__)


/**
 * compile time setting 32 vs 64 bit, will be checked in unit test (core/include/test/SystemUnitTest : WordSizeTest)
 */
#if _WIN32 || _WIN64
  #if _WIN64
    #define IS64BIT
  #else
    #define IS32BIT
  #endif
#endif

#if __GNUC__
  #if __x86_64__ || __ppc64__
    #define IS64BIT
  #else
    #define IS32BIT
  #endif
#endif

/**
 * @brief no generation of standard copy and assignment operators
 * @author gtr
 */
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&); \
    void operator=(const TypeName&)
#endif






