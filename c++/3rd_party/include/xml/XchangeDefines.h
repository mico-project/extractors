/**
 (C) copyright Fraunhofer - IDMT (2011)
 All Rights Reserved


 @file     XchangeDefines.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     28.06.2011
 @brief    Global variables and definitions.

 */

#ifndef XCHANGEDEFINES_H_
#define XCHANGEDEFINES_H_

#include "emt_defs.h"

namespace xc
{

  /**
   *
   * @brief   Data type of stream size.
   *
   */
  typedef int32 stream_size;

}

#endif /* XCHANGEDEFINES_H_ */
