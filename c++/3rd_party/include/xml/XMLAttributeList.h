/**
 (C) copyright Fraunhofer - IDMT (2010)
 All Rights Reserved


 @file     XMLAttributeList.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     18.11.2010
 @brief    XML attribute list.

 */

#ifndef XMLATTRIBUTELIST_H_
#define XMLATTRIBUTELIST_H_

#include "XMLAttribute.h"

#include "emt_defs.h"

#include <list>

namespace xml
{

  /**
   *
   * @brief   XML attribute list.
   *
   */
  class XMLAttributeList
  {
  public:
    /**
     *
     * @brief   Default constructor.
     *
     */
    XMLAttributeList();

    /**
     *
     * @brief   Destructor.
     *
     */
    virtual ~XMLAttributeList();

    /**
     * @brief   Const iterator to access XML attributes in the list.
     */
    typedef std::list<XMLAttribute>::const_iterator const_iterator;

    /**
     * @brief   Iterator to access XML attributes in the list.
     */
    typedef std::list<XMLAttribute>::iterator iterator;

    /**
     *
     * @brief   Add XML attribute.
     *
     * @param  attribute New attribute.
     *
     */
    void insert( const XMLAttribute &attribute ) { m_attributes.push_back( attribute ); }

    /**
     *
     * @brief   Check if an attribute name exists.
     *
     * @param   name  Attribute name.
     * @return  True, if the attribute was found.
     *
     */
    bool exists( const std::string &name ) const;

    /**
     *
     * @brief   Check if an attribute name exists.
     *
     * @param   ns    Attribute namespace URI.
     * @param   name  Attribute name.
     * @return  True, if the attribute was found.
     *
     */
    bool exists( const std::string &ns , const std::string &name ) const;

    /**
     *
     * @brief   Get attribute by name.
     *
     * @param   name  Attribute name.
     * @return  Returns the first entry found.
     *
     */
    const XMLAttribute &get( const std::string &name ) const;

    /**
     *
     * @brief   Get attribute by name and namespace URI.
     *
     * @param   name  Attribute name.
     * @param   ns    Attribute namespace URI.
     * @return  Returns the first entry found.
     *
     */
    const XMLAttribute &get( const std::string &ns , const std::string &name ) const;

    /**
     *
     * @brief   Get attribute by name and namespace URI.
     *
     * @param   name  Attribute name.
     * @param   ns    Attribute namespace URI.
     * @return  Returns the first entry found.
     *
     */
    XMLAttribute &get( const std::string &ns , const std::string &name );

    /**
     *
     * @brief   Write all attributes to ostream.
     *
     * @param   os  Output stream.
     * @return  Number of bytes written.
     *
     */
    uint32 write( std::ostream &os ) const;

    /**
     *
     * @brief   Read attributes from istream.
     *
     * @param   is  Input stream.
     * @return  Number of bytes read.
     *
     */
    uint32 read( std::istream &is );

    /**
     *
     * @brief   Iterate over contained attributes.
     *
     * @return  Iterator to the begin.
     *
     */
    const_iterator begin() const { return m_attributes.begin(); }

    /**
     *
     * @brief   Iterate over contained attributes.
     *
     * @return  Iterator to the end.
     *
     */
    const_iterator end() const { return m_attributes.end(); }

    /**
     *
     * @brief   Get number of attributes.
     *
     * @return  Number of attributes.
     *
     */
    unsigned int size() const { return m_attributes.size(); }

  private:

    /**
     * @brief   Vector containing attributes.
     */
    std::list<XMLAttribute> m_attributes;

  };

}

#endif /* XMLATTRIBUTELIST_H_ */
