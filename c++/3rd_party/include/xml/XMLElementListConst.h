/**
 (C) copyright Fraunhofer - IDMT (2010)
 All Rights Reserved


 @file     XMLElementListConst.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     17.11.2010
 @brief    List of (constant) XML elements.

 */

#ifndef XMLELEMENTLISTCONST_H_
#define XMLELEMENTLISTCONST_H_

#include "XMLElementList.h"

namespace xml
{

  /** @brief  List of (constant) XML elements. */
  class XMLElementListConst
  {
  public:

    /**
     * @brief   Iterator to access XML elements in list.
     */
    typedef std::list<const XMLElement*>::iterator iterator;

    /**
     * @brief   Const iterator to access XML elements in list.
     */
    typedef std::list<const XMLElement*>::const_iterator const_iterator;

    /**
     *
     * @brief   Constructor.
     *
     */
    XMLElementListConst(){};

    /**
     *
     * @brief   Constructor, initialization from XMLElementList.
     *
     * @param   list  List of non-constant XML elements.
     *
     */
    XMLElementListConst( const XMLElementList &list )
    {
      XMLElementList::const_iterator it = list.begin();
      for ( ; it != list.end(); ++it )
        m_elements.push_back( *it );
    };

    /**
     *
     * @brief   Destructor.
     *
     */
    virtual ~XMLElementListConst(){};

    /**
     *
     * @brief   Assignment operator, copy elements from XMLElementList.
     *
     * @param   list  List of non-constant XML elements.
     * @return  Reference to 'this'.
     *
     */
    XMLElementListConst& operator=( const XMLElementList &list )
    {
      XMLElementList::const_iterator it = list.begin();
      for ( ; it != list.end(); ++it )
        m_elements.push_back( *it );

      return *this;
    }

    /**
     *
     * @brief   Clear the list elements.
     *
     */
    void clear() { m_elements.clear(); }

    /**
     *
     * @brief   Insert an XML element.
     *          The XML element is not copied.
     *
     * @param   pElement  Pointer to XML element.
     *
     */
    void insert( const XMLElement *pElement ) { m_elements.push_back( pElement ); }

    /**
     *
     * @brief   Get the number of elements in the list.
     *
     * @return  Number of elements.
     *
     */
    unsigned int size() const { return m_elements.size(); }

    /**
     *
     * @brief   Iterate over the elements.
     *
     * @return  Iterator to the first element.
     *
     */
    const_iterator begin() const { return m_elements.begin(); }

    /**
     *
     * @brief   Iterate over the elements.
     *
     * @return  Iterator to the end.
     *
     */
    const_iterator end() const { return m_elements.end(); }

    /**
     *
     * @brief   Iterate over the elements.
     *
     * @return  Iterator to the first element.
     *
     */
    iterator begin() { return m_elements.begin(); }

    /**
     *
     * @brief   Iterate over the elements.
     *
     * @return  Iterator to the end.
     *
     */
    iterator end() { return m_elements.end(); }

  private:

    typedef std::list<const XMLElement*> ElementListType;

    /**
     * @brief   XML elements.
     */
    ElementListType m_elements;

  };

}

#endif /* XMLELEMENTLISTCONST_H_ */
