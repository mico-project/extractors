/**
            (C) copyright Fraunhofer - IDMT (2010)
                     All Rights Reserved


 @file     XMLElement.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     2010/01/28
 @brief    Represents an XML element in an XML file.

*/

#ifndef XMLELEMENT_H_
#define XMLELEMENT_H_

#include "emt_defs.h"

#include "XMLElementList.h"
#include "XMLAttributeList.h"
#include "XMLElementListConst.h"

#include "XchangeStreaming.h"

#include <string>
#include <vector>
#include <map>

namespace xml
{

  class XMLDocument;

  /**
   *
   * @brief   Represents an XML element in an XML file.
   *
   */
  class XMLElement
  {
  public:
    /**
     *
     * @brief   Default constructor.
     *
     */
    XMLElement();

    /**
     *
     * @brief   Constructor.
     *
     * @param   sTag    Tag name.
     *
     */
    XMLElement( const std::string &sTag );

    /**
     *
     * @brief   Constructor.
     *
     * @param   sNS     XML namespace.
     * @param   sTag    Tag name.
     *
     */
    XMLElement( const std::string &sNS , const std::string &sTag );

    /**
     *
     * @brief   Copy constructor.
     *
     * @param   e   Reference object.
     *
     */
    XMLElement( const XMLElement &e );

    /**
     *
     * @brief   Destructor, deletes all child elements.
     *
     */
    virtual ~XMLElement();

    /**
     *
     * @brief   Assignment operator.
     *
     * @param   e   Reference object.
     *
     */
    XMLElement& operator=( const XMLElement &e );

    /**
     *
     * @brief   Set tag name.
     *
     * @param   sTag  Tag name.
     *
     */
    void setTag( const std::string &sTag ) { m_sTag = sTag; }

    /**
     *
     * @brief   Get tag name.
     *
     * @return  Tag name.
     *
     */
    const std::string &getTag() const { return m_sTag; }

    /**
     *
     * @brief   Set namespace URI.
     *
     * @param   sNS   Namespace URI.
     *
     */
    void setNS( const std::string &sNS ) { m_sNSUri = sNS; }

    /**
     *
     * @brief   Get namespace URI.
     *
     * @return  Namespace URI.
     *
     */
    const std::string &getNS() const { return m_sNSUri; }

    /**
     *
     * @brief   Set text content.
     *
     * @param   sValue  Text content.
     *
    */
    void setValue( const std::string &sValue );

    /**
     *
     * @brief   Get text content.
     *
     * @return  Text content.
     *
     */
    const std::string &getValue() const { return m_sValue; }

    /**
     *
     * @brief   Reset XML attributes (copied).
     *
     *          Already added attributes are deleted.
     *
     * @param   attributes    XML attributes.
     *
     */
    void setAttributes( const XMLAttributeList &attributes ) { m_attributes = attributes; }

    /**
     *
     * @brief   Get all XML attributes.
     *
     * @return  All XML attributes.
     *
     */
    const XMLAttributeList &getAttributes() const { return m_attributes; }

    /**
     *
     * @brief   Get all XML attributes.
     *
     * @return  All XML attributes.
     *
     */
    XMLAttributeList &getAttributes() { return m_attributes; }

    /**
     *
     * @brief   Add XML attribute (copied).
     *
     * @param   attribute New XML attribute.
     *
     */
    void addAttribute( const XMLAttribute &attribute ) { m_attributes.insert( attribute ); }

    /**
     *
     * @brief   Add XML attribute.
     *
     * @param   sName   Attribute name.
     * @param   sValue  Attribute value.
     *
     */
    void addAttribute( const std::string &sName , const std::string &sValue );

    /**
     *
     * @brief   Add XML attribute.
     *
     * @param   sNSUri  Attribute namespace.
     * @param   sName   Attribute name.
     * @param   sValue  Attribute value.
     *
     */
    void addAttribute( const std::string &sNSUri , const std::string &sName , const std::string &sValue );

    /**
     *
     * @brief   Get XML Attribute by name.
     *
     *          If no attribute was found, an exception of type XMLException is thrown.
     *
     * @param   sName Attribute name.
     * @return  Found XML attribute (first match).
     *
     */
    const XMLAttribute &getAttribute( const std::string &sName ) const { return m_attributes.get( sName ); }

    /**
     *
     * @brief   Get XML Attribute by name and namespace URI.
     *
     *          If no attribute was found, an exception of type XMLException is thrown.
     *
     * @param   sNSUri  Attribute namespace URI.
     * @param   sName   Attribute name.
     * @return  Found XML attribute (first match).
     *
     */
    const XMLAttribute &getAttribute( const std::string &sNSUri , const std::string &sName ) const { return m_attributes.get( sNSUri , sName ); }

    /**
     *
     * @brief   Check if a given attribute exists.
     *
     * @param   sName   Attribute name.
     * @return  True if the  attribute was found.
     *
     */
    bool existsAttribute( const std::string &sName ) const { return m_attributes.exists( sName ); }

    /**
     *
     * @brief   Check if a given attribute exists.
     *
     * @param   sNSUri  Attribute namespace.
     * @param   sName   Attribute name.
     * @return  True if the  attribute was found.
     *
     */
    bool existsAttribute( const std::string &sNSUri , const std::string &sName ) const { return m_attributes.exists( sNSUri , sName ); }

    /**
     *
     * @brief   Add a child element.
     *
     *          The current element will take ownership of the child element and all its children.
     *          This means that the child element and all its children are deleted when the current element is destroyed.
     *
     * @param   child Child element to be added.
     */
    void addChildElement( XMLElement *child );

    /**
     *
     * @brief   Get a child element by name and namespace URI.
     *
     *          If the queried child element was not found or if more than one was found, an exception of type XMLException is thrown.
     *
     * @param   sNSUri  Namespace uri of queried the element.
     * @param   sName   Tag name of queried the element.
     * @return  The queried XML element.
     *
     */
    XMLElement *getChildElement( const std::string &sNSUri , const std::string &sName );

    /**
     *
     * @brief   Get a child element by name and namespace URI.
     *
     *          If the queried child element was not found or if more than one was found, an exception of type XMLException is thrown.
     *
     * @param   sNSUri  Namespace uri of queried the element.
     * @param   sName   Tag name of queried the element.
     * @return  The queried XML element.
     *
     */
    const XMLElement *getChildElement( const std::string &sNSUri , const std::string &sName ) const;

    /**
     *
     * @brief   Get a child element by name.
     *
     *          If the queried child element was not found or if more than one was found, an exception of type XMLException is thrown.
     *
     * @param   name   Tag name of queried the element.
     * @return  The queried XML element.
     *
     */
    XMLElement *getChildElement( const std::string &name );

    /**
     *
     * @brief   Get a child element by name.
     *
     *          If the queried child element was not found or if more than one was found, an exception of type XMLException is thrown.
     *
     * @param   name   Tag name of queried the element.
     * @return  The queried XML element.
     *
     */
    const XMLElement *getChildElement( const std::string &name ) const;

    /**
     *
     * @brief   Get child elements.
     *
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is returned (one recursion).
     * @return  List of XML elements.
     *
     */
    XMLElementList getChildElements( unsigned int iDepth = 1 );

    /**
     *
     * @brief   Get child elements.
     *
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is returned (one recursion).
     * @return  List of XML elements.
     *
     */
    XMLElementListConst getChildElements( unsigned int iDepth = 1 ) const;

    /**
     *
     * @brief   Get child elements by tag and namespace URI.
     *
     * @param   sNSUri  Element namespace URI.
     * @param   sTag    Element tag.
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is searched (one recursion).
     * @return  List of matched XML elements.
     *
     */
    XMLElementList getChildElements( const std::string &sNSUri , const std::string &sTag , unsigned int iDepth = 1  );

    /**
     *
     * @brief   Get child elements by tag and namespace URI.
     *
     * @param   sNSUri  Element namespace URI.
     * @param   sTag    Element tag.
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is searched (one recursion).
     * @return  List of matched XML elements.
     *
     */
    XMLElementListConst getChildElements( const std::string &sNSUri , const std::string &sTag , unsigned int iDepth = 1 ) const;

    /**
     *
     * @brief   Get child elements by tag and namespace URI.
     *
     * @param   sNSUri  Element namespace URI.
     * @param   sTag    Element tag.
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is searched (one recursion).
     * @return  List of matched XML elements.
    */
    XMLElementList getChildElements( const std::string &sNSUri , const char *sTag , unsigned int iDepth = 1 ) { return this->getChildElements( sNSUri , std::string( sTag ) , iDepth ); }

    /**
     *
     * @brief   Get child elements by tag .
     *
     * @param   sTag    Element tag.
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is searched (one recursion).
     * @return  List of matched XML elements.
     *
     */
    XMLElementList getChildElements( const std::string &sTag , unsigned int iDepth = 1 );

    /**
     *
     * @brief   Get child elements by tag .
     *
     * @param   sTag    Element tag.
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is searched (one recursion).
     * @return  List of matched XML elements.
     *
     */
    XMLElementListConst getChildElements( const std::string &sTag , unsigned int iDepth = 1 ) const;

    /**
     *
     * @brief   Delete Child elements by tag and namespace URI.
     *
     * @param   sNSUri  Element namespace URI.
     * @param   sTag    Element tag.
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is searched (one recursion).
     *
     */
    void deleteChildElements( const std::string &sNSUri , const std::string &sTag , unsigned int iDepth = 1 );

    /**
     *
     * @brief   Delete Child elements by tag.
     *
     * @param   sTag    Element tag.
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is searched (one recursion).
     *
     */
    void deleteChildElements( const std::string &sTag , unsigned int iDepth = 1 );

    /**
     *
     * @brief   Check if (at least) one child element exists.
     *
     * @param   sNSUri  Element namespace URI.
     * @param   sTag    Element tag.
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is searched (one recursion).
     * @return  True, if at at least one child element is found, false otherwise.
     *
     */
    bool existsChildElement( const std::string &sNSUri , const std::string &sTag , unsigned int iDepth = 1 ) const;

    /**
     *
     * @brief   Check if (at least) one child element exists.
     *
     * @param   sTag    Element tag.
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is searched (one recursion).
     * @return  True, if at at least one child element is found, false otherwise.
     *
     */
    bool existsChildElement( const std::string &sTag , unsigned int iDepth = 1 ) const;

    /**
     *
     * @brief   Count number of child elements having a special tag.
     *
     * @param   sTag    Element tag.
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is searched (one recursion).
     * @return  Number of matched child elements.
     *
     */
    unsigned int countChildElements( const std::string &sTag , unsigned int iDepth = 1 ) const;

    bool hasChildElements() const;

    /**
     *
     * @brief   Count number of child elements having tag and namespace URI.
     *
     * @param   sNSUri  Element namespace URI.
     * @param   sTag    Element tag.
     * @param   iDepth  Maximum number of generations to search.
     *                  If the depth is 1 (default), only the first generation is searched (one recursion).
     * @return  Number of matched child elements.
     *
     */
    unsigned int countChildElements( const std::string &sNSUri , const std::string &sTag , unsigned int iDepth = 1 ) const;

    /**
     *
     * @brief   Stream the current XMLElement as XML.
     *
     * @param   os              Output stream.
     * @param   bHumanReadable  Format the stream data to make it human readable.
     * @param   iIndent         The current indent (only used if formatting is used).
     *
     */
    void compose( std::ostream &os , bool bHumanReadable = false , unsigned int iIndent = 0 ) const;

    /**
     *
     * @brief   Add comment in front of current element.
     *
     * @param   sComment  comment.
     *
     */
    void setComment( const std::string &sComment ) { m_sComment = sComment; }

    /**
     *
     * @brief   Get comment in front of this element.
     *
     * @return  Comment string.
     *
     */
    const std::string &getComment() const { return m_sComment; }

    /**
     *
     * @brief   Write the XML element to ostream.
     *          Note: The XML data is written unformatted, if you want to write data formatted use the method above.
     *          This method can be used if the XML element should be streamed using the xchange library.
     *
     * @param   os    Output stream.
     * @return  Number of bytes written.
     *
     */
    uint32 write( std::ostream &os ) const;

    /**
     *
     * @brief   Read the XML element from istream (unformatted).
     *
     * @param   is    Input stream.
     * @return  Number of bytes read.
     *
     */
    uint32 read( std::istream &is );

  protected:

    friend class XMLDocument;

    /**
     *
     * @brief   Set the owner document.
     *
     * @param   pOwner  Owner document.
     *
     */
    void setOwner( xml::XMLDocument *pOwner );

    /**
     *
     * @brief   Stream the current XMLElement as XML.
     *
     * @param   os                    Output stream.
     * @param   bHumanReadable        Format the stream data to make it human readable.
     * @param   iIndent               The current indent.
     * @param   additionalAttributes  Additional attributes to add to the current element.
     *
     */
    void compose( std::ostream &os , bool bHumanReadable , unsigned int iIndent , const XMLAttributeList &additionalAttributes ) const;

  private:

    /**
     *
     * @brief   Release the XML element including all child nodes.
     *
     */
    void release();

    /**
     *
     * @brief   Compose attributes.
     *
     * @param   os          Output stream
     * @param   attributes  Attributes to stream.
     *
     */
    void compose( std::ostream &os , const XMLAttributeList &attributes ) const;

    /**
     * @brief   Tag name.
     */
    std::string m_sTag;

    /**
     * @brief   Text content.
     */
    std::string m_sValue;

    /**
     * @brief   Namespace URI.
     */
    std::string m_sNSUri;

    /**
     * @brief   Comment in front of this element.
     */
    std::string m_sComment;

    /**
     * @brief   XML attributes.
     */
    XMLAttributeList m_attributes;

    /**
     * @brief   Child elements.
     */
    XMLElementList m_vChildElements;

    /**
     * @brief   The XML document which has the ownership of that XML element.
     */
    XMLDocument *m_pOwner;

  };

}

namespace xc
{

  /**
    @brief  Unstream XML element (unformatted).
    @param  is    Input stream.
    @param  data  XML element to write data.
    @return Number of bytes read.
   */
  template<> int32 unstream<xml::XMLElement>( std::istream &is, xml::XMLElement &data );

  /**
    @brief  Stream XML element (unformatted).
    @param  os    Output stream.
    @param  data  XML element.
    @return Number of bytes written.
   */
  template<> int32 stream<xml::XMLElement>( std::ostream &os, const xml::XMLElement &data );

}

#endif /* XMLELEMENT_H_ */
