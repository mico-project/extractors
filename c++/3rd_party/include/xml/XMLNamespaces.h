/**
            (C) copyright Fraunhofer - IDMT (2010)
                     All Rights Reserved


 @file     XMLNamespaces.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     17.11.2010
 @brief    XML namespace declarations.

*/

#ifndef XMLNAMESPACES_H_
#define XMLNAMESPACES_H_

#include <string>
#include <list>

namespace xml
{

  class XMLDocument;

  /**
   *
   * @brief   Single XML namespace declaration.
   *
   */
  struct XMLNamespace
  {

    /**
     *
     * @brief   Constructor.
     *
     * @param   sNSUri  Namespace URI.
     * @param   sNSId   Namespace id.
     *
     */
    XMLNamespace( const std::string &sNSUri , const std::string &sNSId ) : sNSUri( sNSUri ), sNSId( sNSId ) {};

    /**
     * @brief   Namespace URI.
     */
    std::string sNSUri;

    /**
     * @brief   Namespace id.
     */
    std::string sNSId;

  };

  /**
   *
   * @brief   XML namespace declarations.
   *
   */
  class XMLNamespaces
  {
  public:

    /**
     *
     * @brief   Constructor.
     *
     */
    XMLNamespaces();

    /**
     *
     * @brief   Destructor.
     *
     */
    virtual ~XMLNamespaces();

    /**
     * @brief   Const iterator to access namespace definitions.
     */
    typedef std::list<XMLNamespace>::const_iterator const_iterator;

    /**
     *
     * @brief   Add a new namespace declaration.
     *
     * @param   ns  New declaration.
     *
     */
    void insert( const XMLNamespace &ns );

    /**
     *
     * @brief   Get namespace id for namespace URI.
     *          If no match was found, an exception of type XMLException is thrown.
     *
     * @param   sNSUri  Namespace URI.
     * @return  Appropriate namespace id.
     *
     */
    const std::string &getId( const std::string &sNSUri ) const;

    /**
     *
     * @brief   Get namespace URI for namespace id.
     *          If no match was found, an exception of type XMLException is thrown.
     *
     * @param   sNSId   Namespace id.
     * @return  Appropriate namespace URI.
     *
     */
    const std::string &getUri( const std::string &sNSId ) const;

    /**
     *
     * @brief   Iterate over namespaces.
     *
     * @return  Iterator to the first element.
     *
     */
    const_iterator begin() const { return m_namespaces.begin(); }

    /**
     *
     * @brief   Iterate over namespaces.
     *
     * @return  Iterator to the end.
     *
     */
    const_iterator end() const { return m_namespaces.end(); }

    /**
     *
     * @brief   Check, if a URI already exists.
     *
     * @param   sNSUri  Namespace URI.
     * @return  True, if a match was found.
     *
     */
    bool existsUri( const std::string &sNSUri ) const;

    /**
     *
     * @brief   Check, if am id already exists.
     *
     * @param   sNSId   Namespace id.
     * @return  True, if a match was found.
     *
     */
    bool existsId( const std::string &sNSId ) const;

    /**
     *
     * @brief   Compare two URIs to each other.
     *
     * @param   a   Namespace URI.
     * @param   b   Another namespace URI.
     * @return  True for equality.
     *
     */
    static bool equalsUri( const std::string &a , const std::string &b );

  protected:

    friend class XMLDocument;

    /**
     *
     * @brief   Register a namespace URI.
     *          If the URI is not yet known, a new namespace declaration is added (with default namespace id).
     *
     * @param   sNSUri  Namespace URI.
     *
     */
    void registerNamespaceUri( const std::string &sNSUri );

  private:

    /**
     * @brief   Registered namespace declarations.
     */
    std::list<XMLNamespace> m_namespaces;

  };

}

#endif /* XMLNAMESPACES_H_ */
