/**
 (C) copyright Fraunhofer - IDMT (2010)
 All Rights Reserved


 @file     XMLDocumentJsonCpp.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     23.05.2011

 */

#ifndef XMLDOCUMENTJSONCPP_H_
#define XMLDOCUMENTJSONCPP_H_

#include "XMLDocument.h"

namespace Json
{
  class Value;
}

namespace xml
{

  class XMLDocumentJsonCpp: public xml::XMLDocument
  {
  public:

    XMLDocumentJsonCpp();

    virtual ~XMLDocumentJsonCpp();

    void open( const std::string &filename );

    void parse( const std::string &sJsonDataAsString );

    void write( const std::string &filename ) const;

    void write( std::ostream &os ) const;

    static std::string createJsonStringFromXMLElement( const XMLElement &xmlElement );

    static XMLElement *createXMLElementFromJsonString( const std::string &sJsonDataAsString );

  private:

    void readFromStream( std::istream &is );

    static void write( std::ostream &os , const XMLElement &xmlElement );

    static xml::XMLElement *toXML( const std::string &name , const Json::Value &value );

  };

}

#endif /* XMLDOCUMENTJSONCPP_H_ */
