/**
 (C) copyright Fraunhofer - IDMT (2012)
 All Rights Reserved


 @file     XMLElementToJsonCppValue.h

 @author   Kay Wolter (wlt@idmt.fraunhofer.de)
 @date     23.03.2012

 */

#ifndef XMLELEMENTTOJSONCPPVALUE_H_
#define XMLELEMENTTOJSONCPPVALUE_H_

#include <json/json.h>
#include <set>

namespace xml
{
  class XMLElement;
  class XMLElementListConst;

  class XMLElementToJsonCppValue
  {
  public:

    static Json::Value toJsonCpp( const XMLElement &xmlElementToConvertToJsonValue );

    const static std::string sValueName;

  private:

    XMLElementToJsonCppValue( const XMLElement &xmlElementToConvertToJsonValue );

    virtual ~XMLElementToJsonCppValue();

    static Json::Value toJsonCppArray( const XMLElementListConst &listOfXMLElementsToConvertToJsonArray );

    static void checkIfXMLElementIsConvertibleToJsonCppValue( const xml::XMLElement &xmlElement );

    Json::Value createJsonCppValue();

    void addChildElementsToJsonCppValue();

    void addAttributesToJsonCppValue();

    void addXMLElementToJsonCppValue( const xml::XMLElement &xmlElementToAdd );

    bool isPartOfJsonArray( const xml::XMLElement &xmlElementToAdd );

    void addXMLElementToJsonCppValueAsPartOfArray( const xml::XMLElement &xmlElementToAdd );

    void addXMLElementToJsonCppValueAsSingleElement( const xml::XMLElement &xmlElementToAdd );

    const XMLElement &m_xmlElementToConvertToJsonValue;

    Json::Value m_jsonCppValueToCreateFromXMLElement;

    std::set<std::string> m_listOfTagNamesAlreadyAddedToJsonCppValue;

  };

}

#endif /* XMLELEMENTTOJSONCPPVALUE_H_ */
