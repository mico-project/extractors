#include <memory>
#include <boost/uuid/random_generator.hpp>

#include "ITemporalVideoSegmentationInterface.h"
#include "outputHandler.h"

#include "EventManager.hpp"

// for constant RDF property definitions of common vocabularies
#include "vocabularies.hpp"

#include "ThumbnailExport.h"

// this namespace contains EventManager and AnalysisService
using namespace mico::event;

// this namespace contains Content, ContentItem, etc
using namespace mico::persistence;

// this namespace contains the RDF data model
using namespace mico::rdf::model;

struct OutputProperties {
public:

  OutputProperties(const std::string& mimeType)
    : _mimeType(mimeType),_fileExt(""), _isText(false),
      _isImage(false), _isUnknown(true){
    if (mimeType.find("jpeg") != std::string::npos) {
        _fileExt = "jpg";
        _isImage = true;
        _isUnknown = false;
    }
    if (mimeType.find("png") != std::string::npos) {
        _fileExt = "png";
        _isImage = true;
        _isUnknown = false;
    }
    if (mimeType.find("xml") != std::string::npos)  {
        _fileExt = "xml";
        _isText = true;
        _isUnknown = false;
    }
    if ( mimeType.find("json") != std::string::npos) {
        _fileExt = "json";
        _isText = true;
        _isUnknown = false;
    }

  }

  bool operator== (const OutputProperties &o2)
  {
      return this->_mimeType == o2._mimeType;
  }

  std::string _mimeType;
  std::string _fileExt;
  bool _isText;
  bool _isImage;
  bool _isUnknown;

};


struct TimeRangeMS {
public:
  TimeRangeMS(long frameIdxBegin, long frameIdxEnd, int frameRateNum, int frameRateDen)
    : _startTimeMS(-1), _endTimeMS(-1)
  {
    _startTimeMS = std::round(
      (double) frameIdxBegin * (double) frameRateDen / (double) frameRateNum * 1000);
    _endTimeMS = std::round(
      (double) frameIdxEnd * (double) frameRateDen / (double) frameRateNum * 1000);
  }



  std::string stringStartTimeMS() const
  {
    std::stringstream ss;

    ss << _startTimeMS;
    return ss.str();
  }

  std::string stringEndTimeMS() const
  {
    std::stringstream ss;

    ss << _endTimeMS;
    return ss.str();
  }

  long startTimeMS() const
  {
    return _startTimeMS;
  }

  long endTimeMS() const
  {
    return _endTimeMS;
  }

private:

  long _startTimeMS;
  long _endTimeMS;

};

struct FrameType {
public:
  FrameType()
    :_isKeyFrame(false),_isShotBoundFrame(false)
  { }
  FrameType(bool isKeyFrame, bool isShotBoundFrame)
    :_isKeyFrame(isKeyFrame),_isShotBoundFrame(isShotBoundFrame)
  { }

  bool _isKeyFrame;
  bool _isShotBoundFrame;
};

typedef std::map<unsigned long, std::vector< unsigned char > > ResultImgBuffer;

class TvsVideoService : public AnalysisService {

public:

  TvsVideoService(const std::string& outMimeTypes,
                  const int& tbSize,
                  const std::string& tbType,
                  const std::string& tempDir="");

  ~TvsVideoService();

  void call(mico::event::AnalysisResponse& resp,
            std::shared_ptr< mico::persistence::model::Item > item,
            std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources,
            std::map<std::string,std::string>& params);

  void test(const std::string& testfile);



private:
  bool m_bDoExportThumbs;

  avanalysis::EAVStatus m_status;
  temporalvisegm::ITemporalVideoSegmentationInterface* m_tvsinterface;

  std::vector<OutputProperties> m_outputs;

  bool            m_bDoDetectShotBoundaries;
  bool            m_bDoDetectKeyFrames;
  int             m_tbSize;
  std::string     m_tbType;
  ThumbnailExport m_tbExport;
  std::string     m_tempDir;
  int             m_frameRateNum;
  int             m_frameRateDen;
  float           m_frameRate;

  boost::uuids::random_generator m_rndGen;

  std::map<std::string, ResultImgBuffer> m_resultBuffers;
  std::map<unsigned long,FrameType>      m_selectedFrames;
  vaoutput::qualityType                  m_tvsResults;

  std::vector<mico::persistence::model::URI> m_createdPartsBuffer;

  // std::string makeUUID(std::shared_ptr<mico::persistence::model::Item> item);

  bool storeTempFile(std::istream* in, const std::string& tempFilename);
  void runExtraction(const std::string& tempFilename);
  void writeTextResult();
  void writeImageResult(std::string& fileExt, const std::map<unsigned long, std::vector< unsigned char > >& resultBuffer);
  //void sendTextResult(mico::event::AnalysisResponse& resp, ContentItem& ci, URI& object, const OutputProperties& props);

//  std::string createFragmentSelector(
//      ContentItem& ci, unsigned long frameIdxStart, unsigned long frameIdxEnd);

  void createImagePart(mico::event::AnalysisResponse& resp,
      std::shared_ptr<mico::persistence::model::Item> item,
      std::shared_ptr<mico::persistence::model::Resource> parentResource,
      const std::string& mimeType, bool isKeyFrame, unsigned long frameIdx, const TimeRangeMS& timeRange,
      jnipp::Ref<jnipp::org::openrdf::repository::object::ObjectConnection> con);

  void createShotPart(mico::event::AnalysisResponse& resp,
      std::shared_ptr<mico::persistence::model::Item> item,
      std::shared_ptr<mico::persistence::model::Resource> parentResource,
      const FrameType& type,
      const TimeRangeMS& timeRange,
      jnipp::Ref<jnipp::org::openrdf::repository::object::ObjectConnection> con);

  void createAnnotations(mico::event::AnalysisResponse& resp,
      std::shared_ptr<mico::persistence::model::Item> item,
      std::shared_ptr<mico::persistence::model::Resource> parentResource);

  /**decodes the video and applies processing function to each frame
   *
   * If frame selection is empty - all frames are processed otherwise only the specified.
   *
   **/
  unsigned long decodeVideoAndProcess(const std::string& filename,
    std::function<void (unsigned long frameIdx, unsigned char* dataPtr, unsigned int width, unsigned int height, bool)> procFun,
    std::map<unsigned long,FrameType> frameSelection = std::map<unsigned long,FrameType>());

  //processing function for tvs analysis process
  void callTVSProcess(unsigned long frameIdx, unsigned char* dataPtr,
                      unsigned int width, unsigned int height, bool bIsLastFrame);

  //processing function for thumbnail generation
  void callThumbProcess(unsigned long frameIdx, unsigned char* dataPtr,
                        unsigned int width, unsigned int height, bool);



};
