[TOC]

# TE-206 TVS temporal video segmentation for videos

# Copyright notice

All rights reserved by Fraunhofer IDMT. This software represents background know-how / technologies owned by Fraunhofer IDMT. As outlined in the Consortium Agreement, Fraunhofer IDMT grants all MICO consortium partners a limited, non-exclusive, non-transferable, non-assignable, non-sublicensable, royalty-free license to use the software for the sole purpose of conducting R&D work within MICO including demonstration and evaluation purposes until Oct 31, 2016, provided that (i) this software remains under strict control of the respective consortium partner and (ii) the software and all related material is removed completely from the third parties' premises after use. Usages beyond the described scope, including commercial use and exploitation, are not allowed, and require separate license agreements.

For the sake of simplification, a respective agreement is provided in addition to this disclaimer, listing all MICO-relevant background work from Fraunhofer IDMT.

# Usage

If the temporal video segmentation extractor is part of a pipeline, it accepts video/mp4 inputs and serveral output sdepending to the pipeline configuration. When the extractor is set to full output it produces

* shot annotation (abstract annotations about shots in the video)
* shot boundary frames in jpeg and png format (i.e. the first frames of a shot)
* key frames in jpeg and png format (i.e. important frames of a video - often also close to shot boundaries)

The current RDF data model is shown in this figure (with some example values):

![TVS Model](tvs_model_proposal_v6.png)

# Querying

Here we provide two exemplary SPARQL query templates for querying the results of the TVS.

**Get all Shot annotations**

```
#!sparql
PREFIX mico: <http://www.mico-project.eu/ns/platform/1.0/schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX oa: <http://www.w3.org/ns/oa#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>

# $(ci) the unique URI of the content item (i.e. a video with shots)

SELECT ?ts WHERE {
  $(ci) mico:hasContentPart ?cp .
  ?cp mico:hasContent ?annot .
  ?annot oa:hasBody ?body .
  ?annot oa:hasTarget ?tgt .
  ?tgt  oa:hasSelector ?fs .
  ?fs rdf:value ?ts
  FILTER EXISTS {?body rdf:type mico:TVSShotBody}
}
```

**Get shot boundary frames or key frames in a specific format:**

```
#!sparql
PREFIX mico: <http://www.mico-project.eu/ns/platform/1.0/schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX oa: <http://www.w3.org/ns/oa#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX dct: <http://purl.org/dc/terms/>

# $(ci) the unique URI of the content item (i.e. a video with shots)
# $(format) the format of the image (either image/jpeg or image/png)
# $(type) the type of the image (either TVSKeyFrameBody or TVSShotBoundaryFrameBody)

SELECT ?cp ?fmt ?timestamp ?location WHERE {
  $(ci) mico:hasContentPart ?cp .
  ?cp mico:hasContent ?annot .
  ?annot oa:hasBody ?body .
  ?annot oa:hasTarget ?tgt .
  ?tgt  mico:hasLocation ?location .
  ?tgt  oa:hasSelector ?fs .
  ?fs rdf:value ?timestamp .
  ?cp dct:type ?fmt
  FILTER EXISTS {?body rdf:type mico:$(type) .
                 ?cp dct:type "$(format)"} 
}
```

# Development

## Dependencies

external dependencies:

* boost library (system, log, log_setup, program_options, filesystem, regex)
* ffmpeg library (on Debian based systems: libavcodec-dev, libavfilter-dev, libavformat-dev, libswscale-dev)
* MICO Platform API 
* protobuf (transitive dependency through mico_event)

* optional: ImageMagick-c++-devel for thumbnail extraction mode (auto detected)

located in 3rd_party folder:

* mico_bemvisual (Proprietary A/V Q component Fraunhofer)
* ipsavanalysiscommonmodules (Proprietary A/V Q component Fraunhofer)
* ipstemporalvideosegmentationmodules (Proprietary A/V Q component Fraunhofer)
* ipsvisualmodules (Proprietary component Fraunhofer)
* xml_1_0 (Proprietary xml component Fraunhofer)

## Build

    cmake -DMICOPlatformAPI_HOME=[path to MICO platform API] -DCMAKE_PREFIX_PATH=[path to local installs such as protobuf] [path to extractor source]
    
If you want to build the extractor server as daemon you must add:

    -DDAEMON=1 
    
to the cmake call. If not specified, the extractor will be build as executable running in an endless loop. You must then stop it by pressing Ctrl+C.

-DDAEMON=1 
            
## Installation

Please refer to the common MICO extractor documentation for information about packaging and deployment

## Run

Example call:

    tvs_video_service [server ip] [mico user] [mico password] [options]

The TVS service already supports a simple command line interface (CLI) in order to configure the extractor (output, temporary directory, etc.). Use the `--help` switch to get a full overview. The extractor uses temporary files and needs a writeable folder. By default `/tmp` is set. You may change this also via command line.

