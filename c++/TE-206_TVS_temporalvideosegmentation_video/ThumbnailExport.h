#ifndef THUMBNAILEXPORT_H
#define THUMBNAILEXPORT_H

#include <string>
#include <vector>

class ThumbnailExport
{
public:
  ThumbnailExport();

  bool convert(const unsigned int& width,
               const unsigned int& height,
               unsigned char* rgbData,
               const std::string& dstFmt,
               const unsigned int& dstWidth,
               const unsigned int& dstHeight,
               std::vector<unsigned char>& dstBlob);
};


#endif // THUMBNAILEXPORT_H
