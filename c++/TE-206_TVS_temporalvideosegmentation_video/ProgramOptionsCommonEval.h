#ifndef _PROGRAM_OPTIONS_COMMON_EVAL_H_
#define _PROGRAM_OPTIONS_COMMON_EVAL_H_

#include <string>
#include <vector>

#include <boost/any.hpp>
#include <boost/regex.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;

struct CLPFile 
{
  CLPFile(std::string const& val):
    value(val)
  { }
  std::string value;
};

struct CLPDirectory
{
  CLPDirectory(std::string const& val):
    value(val)
  { }
  std::string value;
};

struct CLPServerIp
{
  CLPServerIp(std::string const& val):
    value(val)
  { }
  std::string value;
};

struct CLPUsername
{
  CLPUsername(std::string const& val):
    value(val)
  { }
  std::string value;
};

struct CLPUserpass
{
  CLPUserpass(std::string const& val):
    value(val)
  { }
  std::string value;
};

void validate(boost::any& v, 
              std::vector<std::string> const& values,
              CLPDirectory* /* target_type */,
              int)
{
  po::validators::check_first_occurrence(v);

  std::string const& s = 
    po::validators::get_single_string(values);

  fs::path p(s);
  if (fs::exists(p) && fs::is_directory(p)) {
    v = boost::any(CLPDirectory(s));
  } else {
    throw po::validation_error(po::validation_error::invalid_option_value);
  }
}

void validate(boost::any& v, 
              std::vector<std::string> const& values,
              CLPFile* /* target_type */,
              int)
{
  po::validators::check_first_occurrence(v);

  std::string const& s = 
    po::validators::get_single_string(values);

  fs::path p(s);
  if (fs::exists(p) && fs::is_regular_file(p)) {
    v = boost::any(CLPFile(s));
  } else {
    throw po::validation_error(po::validation_error::invalid_option_value);
  }
}

void validate(boost::any& v,
              std::vector<std::string> const& values,
              CLPServerIp* /* target_type */,
              int)
{
  //po::validators::check_first_occurrence(v);

  // std::string const& s =
    // po::validators::get_single_string(values);
    // std::string sValidatorString("^(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]?|[0-9])$");
    // boost::regex validator(sValidatorString, boost::regex::perl);
    // if (!boost::regex_match(s, validator))
      // throw po::validation_error(po::validation_error::invalid_option_value);
}

void validate(boost::any& v,
              std::vector<std::string> const& values,
              CLPUsername* /* target_type */,
              int)
{

}

void validate(boost::any& v,
              std::vector<std::string> const& values,
              CLPUserpass* /* target_type */,
              int)
{

}

#endif
