#ifndef _PROGRAM_OPTIONS_TVS_EVAL_H_
#define _PROGRAM_OPTIONS_TVS_EVAL_H_

#include <string>
#include <vector>

#include <boost/any.hpp>
#include <boost/regex.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;

struct CLPTvsOutputMode
{
  CLPTvsOutputMode(std::string const& val):
    value(val)
  { }
  std::string value;
};

struct CLPTvsThumbType
{
  CLPTvsThumbType(std::string const& val):
    value(val)
  { }
  std::string value;
};

struct CLPTvsThumbFmt
{
  CLPTvsThumbFmt(std::string const& val):
    value(val)
  { }
  std::string value;
};

struct CLPTvsThumbSize
{
  CLPTvsThumbSize(std::string const& val):
    value(val)
  { }
  std::string value;
};

void validate(boost::any& v, 
              std::vector<std::string> const& values,
              CLPTvsThumbType* /* target_type */,
              int)
{

}

void validate(boost::any& v,
              std::vector<std::string> const& values,
              CLPTvsThumbFmt* /* target_type */,
              int)
{

}

void validate(boost::any& v,
              std::vector<std::string> const& values,
              CLPTvsThumbSize* /* target_type */,
              int)
{
 
}

void validate(boost::any& v,
              std::vector<std::string> const& values,
              CLPTvsOutputMode* /* target_type */,
              int)
{

}

#endif
