#include "ThumbnailExport.h"

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#if defined EXTRACTOR_WITH_THUMBNAIL_SUPPORT
  #include "Magick++.h"
#endif

ThumbnailExport::ThumbnailExport()
{
}

#if defined EXTRACTOR_WITH_THUMBNAIL_SUPPORT

bool ThumbnailExport::convert(const unsigned int& width,
                              const unsigned int& height,
                              unsigned char* pRgbData,
                              const std::string& mimeType,
                              const unsigned int& dstWidth,
                              const unsigned int& dstHeight,
                              std::vector<unsigned char>& dstBlob)
{
  Magick::Blob outBlob;
  Magick::Image image;

  std::string dstFmt;

  if (mimeType.find("jpeg") != std::string::npos)
    dstFmt = "JPEG";
  else if (mimeType.find("png") != std::string::npos)
    dstFmt = "PNG";
  else
    return false;

  image.read(width,height,"RGB", Magick::CharPixel, pRgbData);
  if (width != dstWidth || height != dstHeight)
    image.resize(Magick::Geometry(dstWidth,dstHeight));
  image.magick(dstFmt);
  image.write(&outBlob);

  dstBlob.clear();
  dstBlob.resize(outBlob.length());
  std::copy((unsigned char*)outBlob.data(),
            (unsigned char*)outBlob.data() + outBlob.length(),
            dstBlob.begin());
  return true;
}


#else

bool ThumbnailExport::convert(const unsigned int& width,
                              const unsigned int& height,
                              unsigned char* pRgbData,
                              const std::string& dstFmt,
                              const unsigned int& dstWidth,
                              const unsigned int& dstHeight,
                              std::vector<unsigned char>& dstBlob)
{
  dstBlob.clear();
  return false;
}

#endif
