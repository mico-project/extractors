#include <ctime>
#include <fstream>
#include <string>
#include <stdexcept>

#include "TvsVideoService.h"
#include "ThumbnailExport.h"
#include "outputHandler.h"
#include <Logging.hpp>
#include <MICOExtractorID.h>
#include <PersistenceService.hpp>
#include <anno4cpp.h>

//ffmpeg includes
#include "Packet.h"
extern "C" {
#include "libavutil/opt.h"
#include "libavutil/channel_layout.h"
#include "libavutil/common.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"
#include "libavutil/samplefmt.h"
#include "libswscale/swscale.h"
}

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/regex.hpp>

using namespace jnipp::org::openrdf::repository::object;
using namespace jnipp::com::github::anno4j::model::impl::selector;
using namespace jnipp::eu::mico::platform::anno4j::model;
using namespace jnipp::eu::mico::platform::anno4j::model::namespaces;
using namespace jnipp::eu::mico::platform::anno4j::model::impl::targetmmm;
using namespace jnipp::eu::mico::platform::anno4j::model::impl::bodymmm;
using namespace jnipp::java::lang;

using namespace mico::persistence::model;
using namespace mico::event::model;


namespace fs = boost::filesystem;


// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2011-10-08T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}


//std::string TvsVideoService::makeUUID(std::shared_ptr<mico::persistence::model::Item> item)
//{
//  return item.getBaseUrl() + "/" + boost::uuids::to_string(m_rndGen());
//}

TvsVideoService::TvsVideoService(const std::string& outMimeTypes, const int& tbSize,
                                  const std::string& tbType, const std::string& tempDir)
  : AnalysisService(mico::extractors::getPrecompExtractorId(),mico::extractors::getPrecompExtractorModeId(),mico::extractors::getPrecompExtractorVersion(),
          "video/mp4", ""),
    m_bDoDetectShotBoundaries(false),
    m_bDoDetectKeyFrames(false),
    m_bDoExportThumbs(false),
    m_tbSize(tbSize),
    m_tbType(tbType),
    m_status(avanalysis::AV_OK),
    m_tempDir(tempDir)
{

  m_tvsinterface = temporalvisegm::ITemporalVideoSegmentationInterface::getInstance ("4HpuH8A7kCJKBfyfmpr6vcpH");
  if(!m_tvsinterface) {
    std::cerr << "Could not get temporalvisegm instance!" << std::endl;
    throw std::runtime_error("Could not get temporalvisegm instance!");
  }

  std::vector<std::string> fmts;
  std::vector<std::string> types;
  boost::split(fmts, outMimeTypes, boost::is_any_of(","));
  boost::split(types, tbType, boost::is_any_of(","));

  std::string firstType;

  for (std::string e : fmts)
  {
    boost::to_lower(e);
    if (e == "jpeg") {
      LOG_INFO("Will produce image/jpeg content parts");
      m_outputs.push_back(OutputProperties("image/jpeg"));
      firstType= firstType.size() == 0 ? "image/jpeg" : firstType;
      m_resultBuffers["image/jpeg"] = ResultImgBuffer();
      m_bDoExportThumbs = true;
    }
    if (e == "png") {
      LOG_INFO("Will produce image/png content parts");
      m_outputs.push_back(OutputProperties("image/png"));
      firstType= firstType.size() == 0 ? "image/png" : firstType;
      m_resultBuffers["image/png"] = ResultImgBuffer();
      m_bDoExportThumbs = true;
    }
    if (e == "xml") {
      LOG_INFO("Will produce xml content parts");
      m_outputs.push_back(OutputProperties("text/vnd.fhg-tvs+xml"));
      firstType= firstType.size() == 0 ? "text/vnd.fhg-tvs+xml" : firstType;
    }
    if (e == "json") {
      LOG_INFO("Will produce json content parts");
      m_outputs.push_back(OutputProperties("application/vnd.fhg-tvs+json"));
      firstType= firstType.size() == 0 ? "application/vnd.fhg-tvs+json" : firstType;
    }
  }

  for (std::string t : types)
  {
    boost::to_lower(t);
    if (t == "shot_bounds") {
      LOG_INFO("Will detect shot boundaries.");
      m_bDoDetectShotBoundaries = true;
    }
    if (t == "keyframes") {
      LOG_INFO("Will detect key frames.");
      m_bDoDetectKeyFrames = true;
    }
  }

  if (!m_bDoDetectShotBoundaries && !m_bDoDetectKeyFrames)
    throw std::runtime_error("No valid detection type specified!");

  this->provides = firstType;

  av_register_all();
}

TvsVideoService::~TvsVideoService() {
  delete m_tvsinterface;
}

void TvsVideoService::test(const std::string& testfile)
{
  runExtraction(testfile);

  for (OutputProperties& e : m_outputs) {
    if (e._isImage) {
      writeImageResult(e._fileExt,m_resultBuffers[e._mimeType]);
    }
  }
  writeTextResult();
}

void TvsVideoService::call(mico::event::AnalysisResponse& resp,
                           std::shared_ptr<mico::persistence::model::Item> item,
                           std::vector<std::shared_ptr<mico::persistence::model::Resource> > resources,
                           std::map<std::string, std::string> &params)
{
  m_createdPartsBuffer.clear();

  if (resources.size() != 1) {
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "Wrong number of input resources",
                          "temporal video segmentation expects exactly one video input resource");
    return;
  }
  std::shared_ptr<mico::persistence::model::Resource> itemResource =
      std::dynamic_pointer_cast<mico::persistence::model::Resource>(item);
  std::shared_ptr<mico::persistence::model::Resource> inResource = resources[0];

  if(inResource) {

    try {
      std::istream* instream = nullptr;

      if (!inResource->hasAsset()) {
        resp.sendErrorMessage(item, MISSING_ASSET,
                              "No asset in resources",
                              "temporal video segmentation requires an asset in input resource - none found!");
      }

      std::shared_ptr<Asset> inAsset = inResource->getAsset();
      instream = inAsset->getInputStream();
      std::string cpType = inAsset->getFormat();

      bool isVideo = boost::algorithm::find_first(cpType, "video/");

      if (!isVideo) {
        resp.sendErrorMessage(item, UNSUPPORTED_CONTENT_TYPE,
                              "Wrong input mime/type",
                              "temporal video segmentation expects video format as input resource");
        return;
      }

      fs::path fileSource(boost::uuids::to_string(m_rndGen()));
      fs::path tempDir(m_tempDir);
      fs::path tempFile = tempDir / fileSource.filename();

      if (!storeTempFile(instream,tempFile.string())) {
          LOG_ERROR("Could not store temporary file %s" ,tempFile.string().c_str());
          resp.sendErrorMessage(item, INSUFFICIENT_STORAGE,
                                "Could not store temp file",
                                "Could not store temp file");
          return;
      }

      runExtraction(tempFile.string());

      createAnnotations(resp, item, inResource);

      m_tvsResults.clear();
      m_selectedFrames.clear();
      m_resultBuffers.clear();

      std::remove(tempFile.string().c_str());

      resp.sendFinish(item);

    } catch (const std::exception& e) {
      LOG_ERROR("Exception raised %s" ,e.what());
      resp.sendErrorMessage(item, UNEXPECTED_ERROR,
                            "exception raised", std::string(e.what()));
    }

   } else {
    LOG_ERROR("Resource of item %s is null!", itemResource->getURI().stringValue().c_str());
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "no resource error",
                          "resource in resource list does not exist");
  }
}

bool TvsVideoService::storeTempFile(std::istream* in, const std::string& tempFilename)
{
  std::ofstream currentFile;
  currentFile.open(tempFilename);

  if (!currentFile)
      return false;

  LOG_DEBUG("Reading video input stream to store temporary file.");

  std::vector<char> buf =
      std::vector<char>(std::istreambuf_iterator<char>(*in),
                        std::istreambuf_iterator<char>());

  LOG_DEBUG("Writing %d bytes to temporary file : %s", buf.size(), tempFilename.c_str());

  for (unsigned int myIter=0;myIter < buf.size();++myIter)
    currentFile << buf[myIter];

  currentFile.close();

  delete in;

  return true;
}

void TvsVideoService::callTVSProcess(unsigned long frameIdx, unsigned char* dataPtr,
                                     unsigned int width, unsigned int height, bool bIsLastFrame)
{
  // set media parameters
  m_status = m_tvsinterface->processVideoFrame (dataPtr, width, height, avanalysis::CSP_RGB,
                                                avanalysis::OriTopLeft, frameIdx, bIsLastFrame);

  if (m_status == avanalysis::AV_ERROR) {
    LOG_ERROR("%s", m_tvsinterface->getLastErrorMessage());
  }
}

void TvsVideoService::callThumbProcess(unsigned long frameIdx, unsigned char* dataPtr,
                                       unsigned int width, unsigned int height, bool)
{
  unsigned int tbWidth, tbHeight;

  if (m_tbSize > 0) {

    float arWToH = (float) width / (float) height;

    if (width > height) {
      tbWidth  = m_tbSize;
      tbHeight = (unsigned int) ((float) tbWidth / arWToH);
    } else {
      tbHeight = m_tbSize;
      tbWidth  = (unsigned int) ((float) tbHeight * arWToH);
    }
  } else {
    tbWidth = width;
    tbHeight= height;
  }
  std::vector<unsigned char> thumb_frame_buf;

  for (OutputProperties& e: m_outputs)
  if ( e._isImage) {
    m_tbExport.convert(width,height,dataPtr,e._mimeType, tbWidth, tbHeight, thumb_frame_buf);
    m_resultBuffers[e._mimeType][frameIdx] = thumb_frame_buf;
  }


}


void TvsVideoService::runExtraction(const std::string& tempFilename)
{

  //init the TVS library
  if (m_bDoDetectShotBoundaries) {
    m_status = m_tvsinterface->setFeatureEnabled (temporalvisegm::events::SHOT, true);
    if (m_status == avanalysis::AV_ERROR) {
      LOG_ERROR("%s",m_tvsinterface->getLastErrorMessage ());
    }
  }
  if (m_bDoDetectKeyFrames) {
    m_status = m_tvsinterface->setFeatureEnabled (temporalvisegm::events::KEYFRAME, true);
    if (m_status == avanalysis::AV_ERROR) {
      LOG_ERROR("%s",m_tvsinterface->getLastErrorMessage ());
    }
    m_tvsinterface->setParameter(temporalvisegm::properties::KEYFRAME_MINIMUM_DISTANCE, 50);
  }
  m_tvsinterface->setParameter(temporalvisegm::properties::FAST_PROCESSING,true);
  m_tvsinterface->setParameter(temporalvisegm::properties::SPLIT_PROCESSING,false);
  m_tvsinterface->setParameter(temporalvisegm::properties::DOWNSCALE_WIDTH, 30);

  m_tvsResults.clear();

  //initalize
  m_status = m_tvsinterface->init ();
  if (m_status == avanalysis::AV_ERROR) {
    LOG_ERROR("%s",m_tvsinterface->getLastErrorMessage ());
  }

  using namespace std::placeholders;

  std::function<void (unsigned long frameIdx, unsigned char* dataPtr,
                      unsigned int width, unsigned int height,  bool bIsLastFrame)> tvsFun =
       std::bind(&TvsVideoService::callTVSProcess,this,_1,_2,_3,_4,_5);

  std::function<void (unsigned long frameIdx, unsigned char* dataPtr,
                      unsigned int width, unsigned int height, bool)> thumbFun =
       std::bind(&TvsVideoService::callThumbProcess,this,_1,_2,_3,_4,_5);

  //first do the analysis
  unsigned long numFrames = decodeVideoAndProcess(tempFilename, tvsFun);
  unsigned long frameIter = 1;

  LOG_DEBUG("Decoded %d frames!", numFrames);

  m_tvsResults[temporalvisegm::events::SHOT][0] = 0.0f;
  m_tvsResults[temporalvisegm::events::KEYFRAME][0] = 0.0f;

  //now get the results
  while(frameIter < numFrames) {

    bool isKeyFrame       = false;
    bool isShotBoundFrame = false;

    float result = 0.f;
    m_status = m_tvsinterface->getResult (temporalvisegm::events::SHOT, frameIter, result);

    if (m_status == avanalysis::AV_ERROR) {
      LOG_ERROR("%s",m_tvsinterface->getLastErrorMessage ());
    }
    m_tvsResults[temporalvisegm::events::SHOT][frameIter-1] = result;
    if (result > 50) {
      isShotBoundFrame = true;
    }

    result = 0.f;
    m_status = m_tvsinterface->getResult (temporalvisegm::events::KEYFRAME, frameIter, result);
    if (m_status == avanalysis::AV_ERROR) {
      LOG_ERROR("%s", m_tvsinterface->getLastErrorMessage ());
    }
    m_tvsResults[temporalvisegm::events::KEYFRAME][frameIter-1] = result;
    if (result > 50) {
      isKeyFrame = true;
    }

    if (isKeyFrame || isShotBoundFrame) {
      m_selectedFrames[frameIter-1] = FrameType(isKeyFrame,isShotBoundFrame);
    }

    ++frameIter;
  }

  //also extract fram 0 as shot boundary...
  m_selectedFrames[0] = FrameType(false,true);


  // for thumbnail extraction we need another decoding run...
  if (m_bDoExportThumbs) {
    LOG_DEBUG("Extracting thumb nails for %d frames." , m_selectedFrames.size());
    numFrames = decodeVideoAndProcess(tempFilename, thumbFun, m_selectedFrames);

  }

}

unsigned long TvsVideoService::decodeVideoAndProcess(
    const std::string& tempFilename, std::function<void(unsigned long frameIdx, unsigned char* dataPtr,
                                                        unsigned int width, unsigned int height,  bool bIsLastFrame)> procFun,
    std::map<unsigned long,FrameType> frameSelection)
{
  LOG_INFO("Decoding and analysis process for video [%s] started...",tempFilename.c_str());

  int frameWidth  = 0;
  int frameHeight = 0;

  bool analyizeAll = false;

  if (frameSelection.size() == 0)
    analyizeAll = true;

  std::shared_ptr<AVFormatContext>
      avFormat(avformat_alloc_context(), &avformat_free_context);

  auto avFormatPtr = avFormat.get();

  if (avformat_open_input(&avFormatPtr, tempFilename.c_str(), nullptr, nullptr) != 0)
    {LOG_ERROR("Error while calling avformat_open_input (probably invalid file format)");return 0;}

  if (avformat_find_stream_info(avFormat.get(), nullptr) < 0)
    {LOG_ERROR("Error while calling avformat_find_stream_info");return 0;}

  AVStream* videoStream = nullptr;
  unsigned int videoStreamIndex = 0;
  for (unsigned int i = 0; i < avFormat->nb_streams; ++i) {
    if (avFormat->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
      videoStream = avFormat->streams[i];
      videoStreamIndex = i;
      break;
    }
  }

  if (!videoStream)
    {LOG_ERROR("Didn't find any video stream in the file (probably audio file)"); return 0;}

  int64_t nb_frames = videoStream->nb_frames;

  // getting the required codec structure
  const auto codec = avcodec_find_decoder(videoStream->codec->codec_id);
  if (codec == nullptr)
    {LOG_ERROR("Codec required by video file not available");return 0;}

  // allocating a structure
  std::shared_ptr<AVCodecContext> avVideoCodec(avcodec_alloc_context3(codec),[](AVCodecContext* c) {avcodec_close(c); av_free(c);});

  // we need to make a copy of videoStream->codec->extradata and give it to the context
  // make sure that this vector exists as long as the avVideoCodec exists
  std::vector<uint8_t> codecContextExtraData(videoStream->codec->extradata,
                                             videoStream->codec->extradata + videoStream->codec->extradata_size);
  avVideoCodec->extradata = reinterpret_cast<uint8_t*>(codecContextExtraData.data());
  avVideoCodec->extradata_size = codecContextExtraData.size();

  // initializing the structure by opening the codec
  if (avcodec_open2(avVideoCodec.get(), codec, nullptr) < 0)
    {LOG_ERROR("Could not open codec.");return 0;}

  // allocating an AVFrame
  std::shared_ptr<AVFrame> avFrame(av_frame_alloc() , &av_free);

  // the current packet of data
  std::shared_ptr<AVPacket> packet(new AVPacket());

  unsigned long lFrameIdx = 0;

#if LIBAVFORMAT_VERSION_MAJOR >= 56
    m_frameRateNum = videoStream->avg_frame_rate.num;
    m_frameRateDen = videoStream->avg_frame_rate.den;
#else
  m_frameRateNum = videoStream->r_frame_rate.num;
  m_frameRateDen = videoStream->r_frame_rate.den;
#endif
  m_frameRate    = (float) m_frameRateNum / (float) m_frameRateDen;

  av_init_packet(packet.get());

  // the decoding loop, running until no more frame is available
  while (true) {
    int isFrameAvailable = 0;
    int processedLength  = 0;
    bool doFlushDecoding = true;


    //decoding stream packets if they belong to the video stream
    while ( av_read_frame(avFormat.get(), packet.get() ) >= 0)
    {
      if (packet->stream_index == videoStreamIndex) {
        processedLength =
            avcodec_decode_video2(avVideoCodec.get(), avFrame.get(), &isFrameAvailable, packet.get());

        if (processedLength < 0) {
          av_free_packet(packet.get());
          LOG_ERROR("error in frame decoding - processedLength=%d",processedLength);
          return 0;
        }
      } else {
        av_free_packet(packet.get());
      }
      if (isFrameAvailable) {
        //we got a frame
        doFlushDecoding = false;
        break;
      }
    }

    if (doFlushDecoding) {
      packet->data = NULL;
      packet->size = 0;
      //run decoder without reading further packages
      processedLength =
          avcodec_decode_video2(avVideoCodec.get(), avFrame.get(), &isFrameAvailable, packet.get());

      if (processedLength < 0) {
        av_free_packet(packet.get());
        LOG_ERROR("error in frame decoding - processedLength=%d",processedLength);
        return 0;
      }
    }

    if (! isFrameAvailable)
      break;

    //std::cout << "decoding frame no " << lFrameId << " PTS=  "<< avFrame->pts << " " << avFrame->width << " x " << avFrame->height << std::endl;

    bool analyzeThis = false;
    if (analyizeAll) {
      analyzeThis = true;
    } else {
      if ( frameSelection.find(lFrameIdx) != frameSelection.end()) {
        analyzeThis = true;
      }
    }
    if (analyzeThis) {
      AVPicture rgbFrame;
      avpicture_alloc(&rgbFrame, PIX_FMT_RGB24, avFrame->width, avFrame->height);

      auto ctxt = sws_getContext(avFrame->width, avFrame->height, static_cast<PixelFormat>(avFrame->format),
                                 avFrame->width, avFrame->height, PIX_FMT_RGB24, SWS_BILINEAR, nullptr, nullptr, nullptr);
      if (ctxt == nullptr) {
        LOG_ERROR("Error while calling sws_getContext");
        //av_free_packet(&packet.packet);
        avpicture_free(&rgbFrame);
        return 0;
      }
      sws_scale(ctxt, avFrame->data, avFrame->linesize, 0, avFrame->height, rgbFrame.data, rgbFrame.linesize);

      frameWidth  = avFrame->width;
      frameHeight = avFrame->height;

      bool bIsLast = false;

      if ( lFrameIdx == (nb_frames -1) )
        bIsLast = true;

      // call the process function (either TVS or thumbnail extractor)
      procFun(lFrameIdx,rgbFrame.data[0], avFrame->width, avFrame->height, bIsLast);

      avpicture_free(&rgbFrame);
    }
    ++lFrameIdx;
  }
  return (lFrameIdx);
}

void TvsVideoService::writeTextResult()
{
  vaoutput::globalsType globals;
  vaoutput::moduleConfigType parameters;
  vaoutput::outputHandler oH("temporalvisegm", m_tvsResults, globals, parameters);
  oH.toXmlFile( "tvsoutput" );
}

void TvsVideoService::writeImageResult(std::string& fileExt,
  const std::map<unsigned long, std::vector< unsigned char > >& resultBuffer)
{
  std::map<unsigned long, std::vector< unsigned char > >::const_iterator bufIt;

  for(bufIt = resultBuffer.begin(); bufIt != resultBuffer.end(); ++bufIt)
  {
    std::stringstream ss;

    if (m_selectedFrames[bufIt->first]._isShotBoundFrame)
      ss << "shot_boundary_";
    else if (m_selectedFrames[bufIt->first]._isKeyFrame)
      ss << "keyframe_";
    else
      ss << "unknown_";

    ss << std::setw(5) << std::setfill('0') << bufIt->first;
    ss << "." << fileExt;

    std::ofstream fout(ss.str(),std::ios::binary);
    fout.write((const char* ) &bufIt->second[0],bufIt->second.size());
    fout.close();
  }

}

void TvsVideoService::createShotPart(mico::event::AnalysisResponse& resp,
    std::shared_ptr<mico::persistence::model::Item> item,
    std::shared_ptr<mico::persistence::model::Resource> parentResource,
    const FrameType& type, const TimeRangeMS& timeRange, jnipp::Ref<ObjectConnection> con)
{

  //create the part first
  std::shared_ptr<mico::persistence::model::Part> part =
      item->createPart(this->getServiceID());

  std::shared_ptr<mico::persistence::model::Resource> partResource =
      std::dynamic_pointer_cast<mico::persistence::model::Resource>(part);


  jnipp::LocalRef<FragmentSelector>    jSelector = item->createObjectNoCommit(con, FragmentSelector::clazz());
  jnipp::LocalRef<TVSShotBodyMMM>      jBody     = item->createObjectNoCommit(con, TVSShotBodyMMM::clazz());
  jnipp::LocalRef<SpecificResourceMMM> jTarget   = item->createObjectNoCommit(con, SpecificResourceMMM::clazz());

  jSelector->setTemporalFragment(Double::construct((jdouble)timeRange.startTimeMS()),
                                 Double::construct((jdouble)timeRange.endTimeMS()));

  jTarget->setSelector(jSelector);

  partResource->setSemanticType("Shot detected inside the input video");
  partResource->setSyntacticalType(MMMTERMS::TVS_SHOT_BODY->std_str());;
  part->addInput(parentResource);
  part->setBody(jBody);
  part->addTarget(jTarget);

  m_createdPartsBuffer.push_back(partResource->getURI());
}

void TvsVideoService::createImagePart(
    mico::event::AnalysisResponse& resp,
    std::shared_ptr<mico::persistence::model::Item> item,
    std::shared_ptr<mico::persistence::model::Resource> parentResource,
    const std::string& mimeType, bool isKeyFrame, unsigned long frameIdx,
    const TimeRangeMS& timeRange,jnipp::Ref<ObjectConnection> con)
{
  //create the part first
  std::shared_ptr<mico::persistence::model::Part> part =
      item->createPart(this->getServiceID());

  std::shared_ptr<mico::persistence::model::Resource> partResource =
      std::dynamic_pointer_cast<mico::persistence::model::Resource>(part);

  jnipp::LocalRef<FragmentSelector>    jSelector = item->createObjectNoCommit(con, FragmentSelector::clazz());
  jnipp::LocalRef<SpecificResourceMMM> jTarget   = item->createObjectNoCommit(con, SpecificResourceMMM::clazz());

  jSelector->setTemporalFragment(Double::construct((jdouble)timeRange.startTimeMS()),
                                 Double::construct((jdouble)timeRange.endTimeMS()));

  jTarget->setSelector(jSelector);

  std::string semanticType = "";
  if (isKeyFrame) {
    jnipp::Ref<ResourceMMM> jBody = item->createObjectNoCommit(con, TVSKeyFrameBodyMMM::clazz());
    part->setBody(jBody);
    semanticType="Key frame detected inside the input video";
    partResource->setSyntacticalType(MMMTERMS::TVS_KEY_FRAME_BODY->std_str());
  } else {
    jnipp::Ref<ResourceMMM> jBody = item->createObjectNoCommit(con, TVSShotBoundaryFrameBodyMMM::clazz());
    part->setBody(jBody);
    semanticType="Shot boundary frame detected inside the input video";
    partResource->setSyntacticalType(MMMTERMS::TVS_SHOT_BOUNDARY_FRAME_BODY->std_str());
  }
  partResource->setSemanticType(semanticType);

  part->addInput(parentResource);
  part->addTarget(jTarget);

  m_createdPartsBuffer.push_back(partResource->getURI());

  //create a new part with the png asset, linked to the same body, target and input, this time setting "mico:Image" as syntactic type
  part = item->createPart(this->getServiceID());
  part->addInput(parentResource);
  part->addTarget(jTarget);

  if (isKeyFrame) {
    jnipp::Ref<ResourceMMM> jBody = item->createObjectNoCommit(con, TVSKeyFrameBodyMMM::clazz());
    part->setBody(jBody);
  } else {
    jnipp::Ref<ResourceMMM> jBody = item->createObjectNoCommit(con, TVSShotBoundaryFrameBodyMMM::clazz());
    part->setBody(jBody);
  }

  partResource = std::dynamic_pointer_cast<mico::persistence::model::Resource>(part);
  partResource->setSyntacticalType("mico:Image");
  partResource->setSemanticType(semanticType);

  std::shared_ptr<Asset> imageAsset = partResource->getAsset();
  imageAsset->setFormat(mimeType);

  std::ostream* outStream = imageAsset->getOutputStream();
  outStream->write((const char*) &m_resultBuffers[mimeType][frameIdx][0],
      m_resultBuffers[mimeType][frameIdx].size());
  delete outStream;

  m_createdPartsBuffer.push_back(partResource->getURI());

}

void TvsVideoService::createAnnotations(mico::event::AnalysisResponse& resp,
    std::shared_ptr<mico::persistence::model::Item> item,
    std::shared_ptr<mico::persistence::model::Resource> parentResource)
{
  jnipp::GlobalRef<ObjectConnection> jObjectConnection;
  try {
    jnipp::Env::Scope scope(PersistenceService::getJVM());

    PersistenceService &persistenceService = parentResource->getPersistenceService();

    //as a workaround for gaining speed we will use this connection to create the
    //body and fragment classes
    jObjectConnection =
        persistenceService.getAnno4j()->getObjectRepository()->getConnection();

    persistenceService.checkJavaExceptionThrow();

    if (!jObjectConnection)
      throw std::runtime_error("Could not retrieve Object connection");

    jObjectConnection->begin();

    std::map<unsigned long,FrameType>::iterator frameIt;
    vaoutput::frameType::iterator resIt;

    unsigned long countJPEG  = 0;
    unsigned long countPNG   = 0;
    unsigned long countKF    = 0;
    unsigned long countSBF   = 0;
    unsigned long countShots = 0;

    long idxStart = 0, idxEnd = 0;


    //create shot annotation
    if (m_tvsResults[temporalvisegm::events::SHOT].size() > 0) {
      for (resIt = m_tvsResults[temporalvisegm::events::SHOT].begin();
           resIt!=m_tvsResults[temporalvisegm::events::SHOT].end(); ++resIt)
      {
        if (resIt->second > 50) {
          idxEnd = resIt->first;
          LOG_INFO("Adding Shot body from begin of frame idx %d to end of frame %d.",idxStart,idxEnd);
          TimeRangeMS ts(idxStart, idxEnd, m_frameRateNum,m_frameRateDen);
          createShotPart(resp, item, parentResource,FrameType(false,true),ts, jObjectConnection);
          countShots += 1;

          idxStart = resIt->first;
        }
      }
      //final shot
      idxEnd = m_tvsResults[temporalvisegm::events::SHOT].end()->first + 1;
      LOG_INFO("Adding Shot body from begin of frame idx %d to end of frame %d.",idxStart,idxEnd);
      TimeRangeMS ts(idxStart, idxEnd, m_frameRateNum,m_frameRateDen);
      createShotPart(resp, item, parentResource,FrameType(false,true),ts, jObjectConnection);
      countShots += 1;
    }

    bool doOutputJPEG = false;
    bool doOutputPNG  = false;

    if (std::find(m_outputs.begin(), m_outputs.end(), OutputProperties("image/jpeg"))
        != m_outputs.end())
      doOutputJPEG = true;

    if (std::find(m_outputs.begin(), m_outputs.end(), OutputProperties("image/png"))
        != m_outputs.end())
      doOutputPNG = true;

    //create annotation for KeyFrame and/or ShotBoundaryFrameBody
    for (frameIt = m_selectedFrames.begin();
         frameIt != m_selectedFrames.end(); ++frameIt)
    {
      LOG_DEBUG("Selected frame is: %d", frameIt->first);

      idxStart = frameIt->first;
      idxEnd   = frameIt->first + 1;

      TimeRangeMS ts(idxStart, idxEnd, m_frameRateNum, m_frameRateDen);

      if (frameIt->second._isKeyFrame) {
        countKF += 1;
        if (doOutputJPEG) {
          LOG_DEBUG("Adding JPEG key frame for frame no %d.",frameIt->first);
          createImagePart(resp, item, parentResource, "image/jpeg", true, frameIt->first, ts, jObjectConnection);
          countJPEG += 1;
        }
        if (doOutputPNG) {
          LOG_DEBUG("Adding PNG key frame for frame no %d.",frameIt->first);
          createImagePart(resp, item, parentResource, "image/png", true, frameIt->first, ts, jObjectConnection);
          countPNG += 1;
        }
      }

      if (frameIt->second._isShotBoundFrame) {
        countSBF += 1;
        if (doOutputJPEG) {
          LOG_DEBUG("Adding JPEG shot boundary frame for frame no %d.",frameIt->first);
          createImagePart(resp, item, parentResource, "image/jpeg", false, frameIt->first, ts, jObjectConnection);
          countJPEG += 1;
        }
        if (doOutputPNG) {
          LOG_DEBUG("Adding PNG shot boundary frame for frame no %d.",frameIt->first);
          createImagePart(resp, item, parentResource, "image/png", false, frameIt->first, ts, jObjectConnection);
          countPNG += 1;
        }
      }
    }

    jObjectConnection->commit();

    for (auto newPartURI: m_createdPartsBuffer) {
      resp.sendNew(item, newPartURI);
    }

    LOG_INFO("Created annotations for %d Shot bodies.", countShots);
    LOG_INFO("Created annotations for %d key frames and %d shot boundary frames.", countKF, countSBF);
    LOG_INFO("Created annotations for %d png images.", countPNG);
    LOG_INFO("Created annotations for %d jpeg images.", countJPEG);



  } catch (std::runtime_error& e) {
    LOG_ERROR("runtime error during annotation: %s", e.what());
    jObjectConnection->rollback();
    jObjectConnection->close();
    resp.sendErrorMessage(item, UNEXPECTED_ERROR,
                          "runtime error during annotation", e.what());

  }
}



//void TvsVideoService::sendTextResult(mico::event::AnalysisResponse& resp,
//                                     ContentItem& ci, URI& object, const OutputProperties& props)
//{
//  // write json or xml text to a new content part
//  Content *txtPart  = ci.createContentPart();

//  txtPart->setType(props._mimeType);

//  // set some metadata properties (provenance information etc)
//  txtPart->setRelation(DC::creator, getServiceID());
//  txtPart->setRelation(DC::provenance, getServiceID());
//  txtPart->setProperty(DC::created, getTimestamp());
//  txtPart->setRelation(DC::source, object.stringValue());

//  std::ostream* out = txtPart->getOutputStream();

//  std::cout << "sending output of type ["<< props._mimeType <<"]" << std::endl;
//  vaoutput::globalsType globals;
//  vaoutput::moduleConfigType parameters;
//  vaoutput::outputHandler oH("temporalvisegm", m_tvsResults, globals, parameters);

//  if (props._mimeType.find("json") != std::string::npos) {
//    oH.toJsonStream( *out );
//  } else {
//    oH.toXmlStream( *out );
//  }

//  delete out;

//  // notify broker that we created a new content part by calling the callback function passed as argument
//  resp.sendNew(ci, txtPart->getURI());
//  delete txtPart;
//}



