#include "TvsVideoService.h"
#include <Daemon.hpp>
#include <Logging.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>

#include "ProgramOptionsCommonEval.h"
#include "ProgramOptionsTvsEval.h"

/****** globals ******/
EventManager*    mgr             = nullptr;
TvsVideoService* tvsVideoService = nullptr;
bool             loop            = true;

// static CLPServerIp       gs_clpServerIp("");
// static CLPUsername       gs_clpUsername("");
// static CLPUserpass       gs_clpUserpass("");
// static CLPTvsOutputMode  gs_clpOutputMode("");
// static CLPTvsThumbType   gs_clThumbType("");
// static CLPTvsThumbFmt    gs_clThumbFmt("");
// static CLPTvsThumbSize   gs_clThumbSize("");

//TODO: implement with correct validation via own types....
static  std::string ip;
static  std::string user;
static  std::string passw;
static  std::string fmt;
static  std::string type;
static  std::string test;
static  int         size;

void signal_handler(int signum) {
  std::cout << "shutting down tvs service ... " << std::endl;

  if (mgr) {
    mgr->unregisterService(tvsVideoService);
  }

  if (tvsVideoService)
    delete tvsVideoService;

  if (mgr)
    delete mgr;

  loop = false;
}

static bool setCommandLineParams(int argc, char** argv, po::variables_map& vm)
{ 
  po::options_description desc("Allowed options");
  desc.add_options()
    ("serverIP,i",  po::value<std::string>(&ip)->required(), "IP of the MICO system server.")
    ("userName,u",  po::value<std::string>(&user)->required(), "MICO system user name")
    ("userPassword,p", po::value<std::string>(&passw)->required(), "MICO system user password")    
    ("kill,k","Shuts down the service when run as daemon.")
    ("foreground,g","Runs the extractor as foreground process. By default the extractor runs as daemon.")
    ("test,e", po::value<std::string>(&test), "test mode - does directly pass the specified file and runs extractor as foreground process.")
    ("help,h","Prints this help message.")
    ("tempDir,d", po::value<std::string>(&test)->default_value(std::string(EXTRACTOR_TEMP_FILE_PATH),EXTRACTOR_TEMP_FILE_PATH), "Process writable directory for temporary files.")
    ("outputFormat,f", po::value<std::string>(&fmt)->default_value(std::string("XML"),"XML"), "Output mode of the extractor (XML, JSON, JPEG, PNG) or a comma separated combination")
    ("thumbType,t",  po::value<std::string>(&type)->default_value(std::string("SHOT_BOUNDS"),"SHOT_BOUNDS"), "Specifies which kinds of frames are detected. (KEYFRAMES, SHOT_BOUNDS) or a comma separated combination.")
    ("thumbSize,s",  po::value<int>(&size)->default_value(-1,"-1"), "Specifies the size of the longest thumbnail edge(number). Negative values mean original size.");

  po::positional_options_description p;
  p.add("serverIP",1);
  p.add("userName",1);
  p.add("userPassword",1);

  bool printHelp = false;
  std::string ex("") ;
  try {
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run() , vm);
    //po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
    //po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
  } catch (std::exception& e) {
    ex = e.what();
    printHelp = true;
  }
  if (vm.count("help")) {
    printHelp = true;
  }

  if (printHelp) {
    if (ex.size() > 0)
      std::cout << std::endl << ex << "\n";
    std::cout << "\nUsage:   " << argv[0];
    for (unsigned int i=0; i < p.max_total_count(); ++i)
      std::cout << " " << p.name_for_position(i);
    std::cout << " [options]" << "\n";
    std::cout << "\n" << desc << "\n";
    return false;
  }
  return true;
}

/****** main ******/
int main(int argc, char **argv) {

  po::variables_map vm;

  if (!setCommandLineParams(argc, argv, vm)) {
    exit(EXIT_FAILURE);
  }

  boost::trim(fmt);
  boost::trim(type);

  tvsVideoService = new TvsVideoService(fmt, size, type, vm["tempDir"].as<std::string>());

  if (vm.count("test")) {
    std::cout << "\n\n +++ TVS extractor - testing mode ! +++" << std::endl;
    tvsVideoService->test(vm["test"].as<std::string>());
    exit(EXIT_SUCCESS);
  }

  bool bRunAsDaemon = true;

  if(vm.count("kill")) {
    return mico::daemon::stop(argv[0]);
  }

  if (vm.count("foreground")) {
    bRunAsDaemon = false;
  }

  if (bRunAsDaemon) {
    mico::log::set_log_backend(mico::daemon::createDaemonLogBackend());
    return mico::daemon::start(argv[0], ip.c_str(), user.c_str(), passw.c_str(), {tvsVideoService});
  } else {
    mgr = new EventManager(ip, user, passw);
    mgr->registerService(tvsVideoService);

    signal(SIGINT,  &signal_handler);
    signal(SIGTERM, &signal_handler);
    signal(SIGHUP,  &signal_handler);

    while(loop) {
      sleep(1);
    }
  }
}
