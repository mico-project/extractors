// for configuring logging levels


#include <boost/log/sources/severity_logger.hpp>
#include "BemvisualImageService.h"
#include "Daemon.hpp"

/****** main ******/
int main(int argc, char **argv) {
    if(argc != 4) {
        std::cerr << "Usage: "<< argv[0] << " SERVER_IP [USER PASSWORD]" << std::endl;
        exit(1);
    }

    const char* server_name = argv[1];
    const char* mico_user   = argv[2];
    const char* mico_pass   = argv[3];


    if(!strcmp(argv[1], "-k")) {
        return mico::daemon::stop(argv[0]);
    } else {
        // create a new instance of a MICO daemon, auto-registering two instances of the OCR analysis service
        return mico::daemon::start(argv[0], server_name, mico_user, mico_pass, {new BemvisualImageService()});

    }

}
