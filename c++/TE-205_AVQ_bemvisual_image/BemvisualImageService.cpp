#include <ctime>

#include "BemvisualImageService.h"
#include "outputHandler.h"

// define dublin core vocabulary shortcut
namespace DC = mico::rdf::vocabularies::DC;

// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2011-10-08T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}

BemvisualImageService::BemvisualImageService() 
  : AnalysisService("http://www.mico-project.org/services/bemvisual", "image/x-portable-pixmap", "text/xml", "bemvisual-queue"),
    m_status(avanalysis::AV_OK)
{
  m_beminterface = bemvisual::IBEMVisualErrorDetectionInterface::getInstance ("4HpuH8A7kCJKBfyfmpr6vcpH");
  if(!m_beminterface) {
    std::cerr << "Could not get bemvisual instance!" << std::endl;
    throw std::string("Could not get bemvisual instance!");
  } else {
    char *libInfo = new char[128];
    m_beminterface->getLibraryId( libInfo );
    std::cout << libInfo << std::endl;
    delete []libInfo;
  }
};

BemvisualImageService::~BemvisualImageService() {
  delete m_beminterface;
}

void BemvisualImageService::call(std::function<void(const ContentItem& ci, const URI& object)> resp, ContentItem& ci, URI& object)
{
  // retrieve the content part identified by the object URI
  Content* imgPart = ci.getContentPart(object);
   
  if(imgPart != NULL) {
    std::istream* in = imgPart->getInputStream();
    std::string header;
    unsigned int iWidth;
   unsigned int iHeight;
    unsigned int maxBrightness;
    (*in) >> header;
    if (strcmp(header.c_str(), "P6") != 0) std::cout << "BemvisualImageService::call(): Not P6 - Can't read input file" << std::endl;
    (*in) >> iWidth >> iHeight >> maxBrightness;
    std::cout << "BemvisualImageService::call(): width: "<< iWidth << " height: "<< iHeight << std::endl;
    std::vector<char> buf = std::vector<char>(std::istreambuf_iterator<char>(*in), std::istreambuf_iterator<char>());
    delete in;


    //START bemvisual stuff:
    // activate all features
    std::vector<avanalysis::AVAnalysisFeatureType> allFeatures = bemvisual::events::all();
    for (std::vector<avanalysis::AVAnalysisFeatureType>::const_iterator it = allFeatures.begin (); it != allFeatures.end (); ++it)
    {
      m_status = m_beminterface->setFeatureEnabled (*it, true);
      if (m_status == avanalysis::AV_ERROR) {
        std::cerr << m_beminterface->getLastErrorMessage () << std::endl;
      }
    }

    //initalize
    m_status = m_beminterface->init ();
    if (m_status == avanalysis::AV_ERROR) {
      std::cerr << m_beminterface->getLastErrorMessage () << std::endl;
    }

    // set media parameters
    avanalysis::ECSP colorspace = avanalysis::CSP_RGB;
    avanalysis::EOrientation orientation = avanalysis::OriTopLeft;
    unsigned long lFrameId = 0;

    //process
    std::cout << "BemvisualImageService::call(): begin processing " << std::endl;
    m_status = m_beminterface->processVideoFrame ( buf.data(), iWidth, iHeight, colorspace, orientation, lFrameId);
    std::cout << "BemvisualImageService::call(): processed frame " << std::endl;

    //result map for saving
    vaoutput::qualityType qresults;
    vaoutput::globalsType globals;
    vaoutput::moduleConfigType parameters;

    if (m_status == avanalysis::AV_ERROR) {
      std::cerr << m_beminterface->getLastErrorMessage () << std::endl;
    } else {
      float result;
      for (std::vector<avanalysis::AVAnalysisFeatureType>::const_iterator it = allFeatures.begin (); it != allFeatures.end (); ++it)
      {
        m_status = m_beminterface->getResult (*it, lFrameId, result);
        if (m_status == avanalysis::AV_ERROR) {
          std::cerr << m_beminterface->getLastErrorMessage () << std::endl;
        }
        std::cout << "BemvisualImageService::call(): result for "<< (*it).getName () << " is: "<< result << std::endl;
        qresults[(*it)][lFrameId] = result;
      }
    }

    // write json or xml text to a new content part
    Content *txtPart = ci.createContentPart();
    txtPart->setType("text/vnd.fhg-bemvisual-image+xml");
    
    // set some metadata properties (provenance information etc)
    txtPart->setRelation(DC::creator, getServiceID());
    txtPart->setRelation(DC::provenance, getServiceID());
    txtPart->setProperty(DC::created, getTimestamp());
    txtPart->setRelation(DC::source, object.stringValue());
    
    std::ostream* out = txtPart->getOutputStream();

    vaoutput::outputHandler oH("bemvisual", qresults, globals, parameters);
    oH.toXmlStream( *out );
    delete out;

    // notify broker that we created a new content part by calling the callback function passed as argument
    resp(ci, txtPart->getURI());
    
    // clean up
    delete imgPart;
    delete txtPart;
  } else {
    std::cerr << "content item part " << object.stringValue() << " of content item " << ci.getURI().stringValue() << " does not exist!\n";
  }
};
