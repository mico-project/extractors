/*   	Copyright 2015 Adam Dahlgren <dali@cs.umu.se>
 * 		  2015 Umea University
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */


#include "KaldiService.hpp"
#include "EventManager.hpp"
#include "Logging.hpp"
#include "rapidxml.hpp"
#include "rapidxml_utils.hpp"
#include <MICOExtractorID.h>

#include <PersistenceService.hpp>
#include <Item.hpp>
#include <Part.hpp>
#include <Asset.hpp>

using namespace mico::event;
using namespace mico::event::model;
using namespace mico::persistence;
using namespace mico::persistence::model;

using pResource = std::shared_ptr<Resource>;
using pItem  = std::shared_ptr<Item>;
using pPart  = std::shared_ptr<Part>;
using pAsset = std::shared_ptr<Asset>;

#include <anno4cpp.h>
#include <jnipp.h>

using namespace jnipp::eu::mico::platform::anno4j::model;
using namespace jnipp::eu::mico::platform::anno4j::model::impl::bodymmm;
using namespace jnipp::eu::mico::platform::anno4j::model::impl::targetmmm;
using namespace jnipp::com::github::anno4j::model;
using namespace jnipp::com::github::anno4j::model::impl::selector;
using namespace jnipp::java::lang;


namespace mico {
namespace extractors {


class mico_critical_error : public std::runtime_error{
public:
	mico_critical_error(std::string what) : std::runtime_error(what){};
};

/**
 * Escape XML characters as the Kaldi output is stored as XML.
 */
std::string KaldiService::escapeXMLcharacters(std::string word) {
	std::string lt("<");
	std::string gt(">");
	std::string amp("&");
	std::string apos("'");
	std::string quot("\"");

	int pos = 0;
	while((pos = word.find(amp)) != std::string::npos)
		word.replace(pos, amp.length(),"&amp;");

	pos = 0;
	while((pos = word.find(lt)) != std::string::npos)
		word.replace(pos, lt.length(), "&lt;");

	pos = 0;
	while((pos = word.find(gt)) != std::string::npos)
		word.replace(pos, gt.length(), "&gt;");

	pos = 0;
	while((pos = word.find(apos)) != std::string::npos)
		word.replace(pos, apos.length(), "&apos;");
	pos = 0;
	while((pos = word.find(quot)) != std::string::npos)
		word.replace(pos, quot.length(), "&qout;");

	return word;
}

/**
 * Get the diarization from previous extractor, if present in input resources.
 * Returns null if not present.
 */
pResource KaldiService::getInputXmlResource(std::vector< pResource >inputResources){
	for(pResource r : inputResources){
		if(r->hasAsset() &&
		   r->getAsset() != nullptr &&
		   (r->getAsset()->getFormat().compare("text/vnd.umu-diarization+xml") == 0)){
			return r;
		}
	}
	return nullptr;
}
/**
 * Get the input audio file resource
 */
pResource KaldiService::getInputWavResource(std::vector< pResource >inputResources){
	for(pResource r : inputResources){
		if(r->hasAsset() &&
		   r->getAsset() != nullptr &&
		   (r->getAsset()->getFormat().compare("audio/wav") == 0)){
			return r;
		}
	}
	return nullptr;
}


/**
 * Query for original audio file if not found in resources
 */
pPart KaldiService::getOriginalAudioContentPart(pItem item) {

	/*
	 * In java this would be
	 *
	 * QueryService qs = anno4j.createQueryService();
	 * qs.addCriteria("mmm:hasBody[is-a mmm:AudioDemuxBody]").addCriteria("^mmm:hasPart", ITEMURI);
	 * List<PartMMM> result = qs.execute(PartMMM.class);
	 */

	std::string cpp_scope="KaldiService:getOriginalAudioContentPart() : ";
	pPart out = nullptr;



	pResource rItem = std::dynamic_pointer_cast<Resource>(item);

	//0. retrieve jvm and persistence service
	PersistenceService& ps=rItem->getPersistenceService();
	jnipp::Env::Scope scope(ps.getJVM());
	LOG_INFO((cpp_scope + "set scope").c_str());

	auto check_java_object=[&] (bool obj_exists )->void{
		ps.checkJavaExceptionThrow();
		if(! obj_exists) {
			throw mico_critical_error("KaldiService: Unable to instantiate java object from jvm");
		}
	};

	//---- 1. instantiate a query service
	using namespace jnipp::com::github::anno4j::querying;

	LOG_INFO((cpp_scope + "Instantiating query service").c_str());
	jnipp::LocalRef<String> item_uri=String::create(rItem->getURI().stringValue());
	jnipp::LocalRef<QueryService> qs = ps.getAnno4j()->
			createQueryService(jnipp::org::openrdf::model::impl::URIImpl::construct(item_uri));
	check_java_object(qs);


	//---- 2. setup the query
	jnipp::LocalRef<String> body_ldpath=String::create("mmm:hasBody[is-a mmmterms:AudioDemuxBody]");
	jnipp::LocalRef<String> item_ldpath=String::create("^mmm:hasPart");


	//TODO: add criteria for filtering the sampling frequency

	qs->addCriteria(body_ldpath) //look for tuples with a audiodemux body
	  ->addCriteria(item_ldpath,item_uri); //coming from this item
	check_java_object(qs);

	qs->addPrefix("mmm", "http://www.mico-project.eu/ns/mmm/2.0/schema#");
	qs->addPrefix("mmmterms", "http://www.mico-project.eu/ns/mmmterms/2.0/schema#");
	check_java_object(qs);

	//----3. execute it
	LOG_INFO((cpp_scope + "Executing the query via anno4j").c_str());

	jnipp::LocalRef<JavaUtilList> jpartList = qs->execute(PartMMM::clazz());
	check_java_object(jpartList);
	LOG_INFO((cpp_scope + "Executed the query via anno4j").c_str());

	jnipp::LocalRef< jnipp::Array<JavaLangObject> > jpartArray  = jpartList->toArray();
	check_java_object(jpartArray);
	LOG_INFO((cpp_scope + "Casted the returned List into an Array").c_str());

	int numURIs=jpartArray->length();
	LOG_DEBUG("Retrieved %d part(s) containing an AudioDemuxBoby in array for item %s", numURIs, rItem->getURI().stringValue().c_str());

	//TODO handle the case of multiple retrieved parts
	for (auto it = jpartArray->begin();  it!= jpartArray->end(); ++it) {

		jnipp::LocalRef<jnipp::org::openrdf::repository::object::RDFObject> jObject = *it;
		jnipp::LocalRef<jnipp::org::openrdf::model::URI> jResourceURI = jObject ->getResource();
		LOG_DEBUG("found part with URI %s",jResourceURI->toString()->std_str().c_str());
		return item->getPart(mico::persistence::model::URI(jResourceURI->toString()->std_str()));
	}
	throw mico_critical_error("Unable to retrieve the required AudioDemuxBody input part");
}

/**
 * Use the diarization xml data to extract diarization segments.
 */
vector<pair<int32, int32> > KaldiService::getSegments(std::string xmlPath)
{
	vector<pair<int32, int32> > segs;

	rapidxml::file<> xmlFile(xmlPath.c_str());
	std::cout << xmlFile.data();
	rapidxml::xml_document<> xmlDoc;
	xmlDoc.parse<0|rapidxml::parse_no_data_nodes>(xmlFile.data());

        /* Check if root node for diarization data exists */
	rapidxml::xml_node<> *segmentsNode = xmlDoc.first_node("epac");
	if(segmentsNode == NULL) {
		LOG_ERROR("Input XML file does not contain <epac> node");
	} else {
		segmentsNode = segmentsNode->first_node("audiofile");
		if(segmentsNode != NULL) {
			segmentsNode = segmentsNode->first_node("segments");
		}
	}
        /* Iterate over all nodes in XML */
	for (rapidxml::xml_node<> *child = segmentsNode->first_node(); child; child = child->next_sibling()) {
		float start = -1;
		float end = -1;
		std::string attribute_holder = "";

                /* Find start/end in XML */
		for(rapidxml::xml_attribute<> *attr = child->first_attribute(); attr; attr = attr->next_attribute()) {
			attribute_holder  = std::string(attr->name());
			if(attribute_holder.compare("start") == 0) {
				start = std::stof((std::string(attr->value())).c_str());
			} else if(attribute_holder.compare("end") == 0) {
				end = std::stof((std::string(attr->value())).c_str());
			}	
		}

                /* Translate into audio segments */
		pair<int32, int32> seg;
		int32 startWithSamp = int(start*SAMPLING_RATE);	
		int32 endWithSamp = int(end*SAMPLING_RATE);	
		seg.first = startWithSamp;
		seg.second = endWithSamp;
		segs.push_back(seg);


	}

        /* Make sure segments are not too long */
	return breakDownTooLongSegments(segs);

}

/**
 * Segments that are longer than max segment size are broken down into smaller chunks.
 */
vector<pair<int32, int32> > KaldiService::breakDownTooLongSegments(vector<pair<int32,int32>> segments) {
	vector<pair<int32, int32>> splitSegments;
        /* For all segments */
	for (std::vector<int>::size_type i = 0; i != segments.size(); i++) {
                /* Check if the segment length (bitrate->seconds) exceeds limit */
		int32 nrOfSegments = segments[i].second-segments[i].first;
		if ( nrOfSegments/SAMPLING_RATE > MAX_SEGMENT_SIZE_SEC) {

			int32 offset = segments[i].first;
			int32 chunk = nrOfSegments/(int((nrOfSegments/SAMPLING_RATE)/MAX_SEGMENT_SIZE_SEC)+1);
                        /* Construct new, smaller segments */
			while(offset < segments[i].second) {
				int32 remaining = offset + chunk < segments[i].second ? chunk : segments[i].second-offset;
				pair<int32,int32> newSeg;
				newSeg.first=offset;
				newSeg.second=offset+remaining;
				splitSegments.push_back(newSeg);
				offset+=remaining;
			}

		} else { /* Copy segment */
			pair<int32, int32>newSegm;
			newSegm.first = segments[i].first;
			newSegm.second = segments[i].second;
			splitSegments.push_back(newSegm);
		}
	}
	return splitSegments;
}

/**
 * Create uniform segments over given length. Used if XML data gave no segments.
 */
vector<pair<int32, int32> > KaldiService::createDummySegments(int dim)
{
	vector<pair<int32, int32> > segs;
	int32 endtime = 0;
	int32 starttime = 0;
	while(starttime < dim){
		endtime = starttime + DEFAULT_SEGMENT_SIZE;
		endtime = (endtime < dim) ? endtime : dim;
		pair<int32, int32> seg;
		seg.first = starttime;
		seg.second = endtime;
		segs.push_back(seg);
		starttime = endtime; //starttime for next segment
	}
	return segs;
}



/**
 * Setup of Kaldi service, loads Kaldi settings and models.
 */
KaldiService::KaldiService()
: AnalysisService(mico::extractors::getPrecompExtractorId(),mico::extractors::getPrecompExtractorModeId(),mico::extractors::getPrecompExtractorVersion(),
                  "text/vnd.umu-diarization+xml", "text/vnd.umu-kaldis+xml"){

	using namespace kaldi;
	using namespace fst;

	// read Kaldi config file
	ParseOptions po(NULL);
	feature_config.Register(&po);
	nnet2_decoding_config.Register(&po);
	endpoint_config.Register(&po);
	wordboundary_config.Register(&po);
	po.ReadConfigFile("/usr/share/mico-kaldi-data/nnet_a_gpu_online/conf/online_nnet2_decoding.conf");

	// Kaldi internal parameters
	nnet2_decoding_config.decodable_opts.acoustic_scale=0.1;
	nnet2_decoding_config.decoder_opts.max_active=4000;
	nnet2_decoding_config.decoder_opts.beam=10.0;
	nnet2_decoding_config.decoder_opts.lattice_beam=4.0;

	{
		bool binary;
		Input ki(SMBR_EPOCH2_MODEL, &binary);
		trans_model.Read(ki.Stream(), binary);
		nnet.Read(ki.Stream(), binary);
	}

	decode_fst = ReadFstKaldi(HCLG_FST);

	word_syms = NULL;
	if (!(word_syms = fst::SymbolTable::ReadText(WORD_SYMBOLS_TABLE)))
		LOG_ERROR("Could not read symbol table from file %s", WORD_SYMBOLS_TABLE);

	LOG_INFO("MICO speech-to-text service initialised.");

};



KaldiService::~KaldiService() {
	if(decode_fst)
		delete decode_fst;

	if(word_syms)
		delete word_syms; // will delete if non-NULL.
}


void KaldiService::call(AnalysisResponse& response, std::shared_ptr< mico::persistence::model::Item > item, std::vector< std::shared_ptr<mico::persistence::model::Resource>>inputResources, std::map<std::string,std::string>& params) {

	using namespace kaldi;
	using namespace fst;

	std::string tempFileForInputXML("");
	std::string tempFileForInputWAV("");

	auto save_as = [&](pAsset asset, std::string filename)->void{

		std::istream* in = asset->getInputStream();

		std::ofstream out;
		out.open(filename);

		if(!out.is_open()) {

			response.sendErrorMessage(item,
					ErrorCodes::INSUFFICIENT_STORAGE,
					"KaldiService: CRITICAL ERROR",
					"Unable to open temporary file: " + filename);
			throw std::runtime_error("");
		}
		std::vector<char> buff = std::vector<char>(std::istreambuf_iterator<char>(*in), std::istreambuf_iterator<char>());
		for (uint buf_index = 0; buf_index < buff.size(); ++buf_index) {
			out << buff[buf_index];
		}
		out.close();
		in->seekg(0);
		delete in;

	};


	try{
		//1 verify correctness of inputs:

		LOG_INFO("KaldiService: START");

		//non-empty item
		if(item == nullptr){
			throw mico_critical_error("No input item is present");
		}

		//only one part
		if(inputResources.size() < 1 || inputResources.size() > 2){
			throw mico_critical_error("Received " + std::to_string(inputResources.size()) + " inputs, should be 1(xml-only) or 2 (xml+audio)");
		}


		//2 retrieve the input xml asset

		pResource rXmlInputPart = getInputXmlResource(inputResources);

		if(rXmlInputPart == nullptr){
			throw mico_critical_error("Cannot retrieve input Resource");
		}

		LOG_INFO("KaldiService: START RETRIEVING THE INPUT XML");

		tempFileForInputXML = std::to_string((unsigned long int)&item) + ".xml";

		LOG_DEBUG("Getting XML file from content part %s", rXmlInputPart->getURI().stringValue().c_str());
		pAsset    xmlInputAsset  = rXmlInputPart->getAsset();

		if(xmlInputAsset == nullptr){
			throw mico_critical_error("The input resource has no asset");
		}



		try{
			save_as(xmlInputAsset,tempFileForInputXML);
		}
		catch(...){
			return;
		}

		LOG_INFO("KaldiService: DONE RETRIEVING THE INPUT XML");



		LOG_INFO("KaldiService: START RETRIEVING THE INPUT AUDIO");

		// create temporary files for video and audio track
		tempFileForInputWAV = std::to_string((unsigned long int)&item)+ ".wav";

		pResource rAudioInputPart = getInputWavResource(inputResources);
		if(rAudioInputPart == nullptr){
			pPart audioInputPart = getOriginalAudioContentPart(item);
			rAudioInputPart = std::dynamic_pointer_cast<Resource>(audioInputPart);
		}




		LOG_INFO("Getting audio file from content part %s", rAudioInputPart->getURI().stringValue().c_str());
		pAsset    audioInputAsset  = rAudioInputPart->getAsset();

		if(audioInputAsset == nullptr){
			throw mico_critical_error("The input resource has no asset");
		}

		try{
			save_as(audioInputAsset,tempFileForInputWAV);
		}
		catch(...){
			return;
		}


		LOG_INFO("KaldiService: DONE RETRIEVING THE INPUT AUDIO");


		LOG_INFO("KaldiService: START ASR PROCESSING");

		LOG_DEBUG("- checking for segments");
		vector<pair<int32, int32> > segments = getSegments(tempFileForInputXML);
		LOG_INFO("FOUND [%d] segments",segments.size());

		LOG_INFO("- converting %s to text", tempFileForInputWAV.c_str());

		std::vector<int32> words, times, lengths;

		LOG_DEBUG("INITIALISE WAV RSPECIFIER");
		// initialise Kaldi analysis engine with configuration from example
		std::string wav_rspecifier("scp:echo utterance-id1 "+tempFileForInputWAV+"|"), clat_wspecifier("ark:/dev/null");

		LOG_DEBUG("INITIALISE FEATURE INFO");
		OnlineNnet2FeaturePipelineInfo feature_info(feature_config);
		LOG_DEBUG("INITIALISED FEATURE INFO");

		feature_info.ivector_extractor_info.use_most_recent_ivector = true;
		feature_info.ivector_extractor_info.greedy_ivector_extractor = true;


		int32 num_done = 0, num_err = 0;

		LOG_DEBUG("INFO SPK2UTT READER");
		SequentialTokenVectorReader spk2utt_reader("ark:echo utterance-id1 utterance-id1|");
		LOG_DEBUG("INIT WAV READER");
		RandomAccessTableReader<WaveHolder> wav_reader(wav_rspecifier);
		LOG_DEBUG("INIT LAT WRITER");
		CompactLatticeWriter clat_writer(clat_wspecifier);

		LOG_DEBUG("INIT WORD BOUNDARY CONF");
		// configuration for detecting word boundaries
		WordBoundaryInfo wordboundary_info(wordboundary_config, "/usr/share/mico-kaldi-data/graph/phones/word_boundary.int");

		LOG_DEBUG("Analzsis prepared, reading utterances from audio file");

		// read utterances from wav file until reader is done
		for (; !spk2utt_reader.Done(); spk2utt_reader.Next()) {
			//I have kept the utterance logic in order to not break anything.
			LOG_DEBUG("SPK2UTT READER NEXT");
			//When in a test environment, try deleting the loops over utterance and just set utt = "utterance-id1"
			std::string spk = spk2utt_reader.Key();
			LOG_DEBUG("GOT KEY FROM READER");
			const std::vector<std::string> &uttlist = spk2utt_reader.Value();
			LOG_DEBUG("READ VALUE FROM SPK2UTT READER");
			OnlineIvectorExtractorAdaptationState adaptation_state(feature_info.ivector_extractor_info);
			LOG_DEBUG("CREATED ADAPTION STATE");
			// TODO Fix this
			for (size_t i = 0; i < 1;i++){//uttlist.size(); i++) {
				std::string utt = "utterance-id1";
				if (!wav_reader.HasKey(utt)) {
					LOG_INFO("Did not find audio for utterance : %s", utt.c_str());
					num_err++;
					continue;
				}
				const WaveData &wave_data = wav_reader.Value(utt);
				LOG_DEBUG("READING WAVE DATA VALUE FROM UTTERANCE");
				// get the data for channel zero (if the signal is not mono, we only
				// take the first channel).
				SubVector<BaseFloat> data(wave_data.Data(), 0);
				LOG_DEBUG("GETTING SUBVECTOR FROM WAVE DATA");
				BaseFloat samp_freq = wave_data.SampFreq();
				LOG_DEBUG("GOT SAMP FREQ FROM WAVE DATA");


				int32 samp_offset = 0;
				//Loop this over segments
				//Check whether segments exist
				LOG_DEBUG("CHECKING SEGMENT SIZE");
				if(segments.size() == 0){
					LOG_INFO("- Creating dummy segments");
					segments = createDummySegments(data.Dim());
					LOG_INFO("Nr. of segments are: %d", segments.size());
				}
				OnlineTimer decoding_timer(utt);
				int32 time_offset = 0;


				for(int i_seg=0; i_seg < segments.size(); i_seg++)
				{

					// TODO This is not how it should be done,
					// but nothing else works.. Should be investigated
					// and fixed. Pipeline should only be created once,
					// since now time stamps are off and fixed manually.

					OnlineNnet2FeaturePipeline feature_pipeline(feature_info);
					feature_pipeline.SetAdaptationState(adaptation_state);
					SingleUtteranceNnet2Decoder decoder(nnet2_decoding_config,
							trans_model,
							nnet,
							*decode_fst,
							&feature_pipeline);
					samp_offset = segments[i_seg].first;

					LOG_INFO("Reading new segment");

					while(samp_offset < segments[i_seg].second) {

						int32 chunk_length = std::numeric_limits<int32>::max();

						chunk_length = DEFAULT_CHUNK_SIZE;//segments[i_seg].second - segments[i_seg].first;
						int32 samp_remaining = segments[i_seg].second - samp_offset;
						if (samp_remaining < 0){
							LOG_ERROR("Segment exceeds audio length");
							break;
						}


						int32 num_samp = chunk_length < samp_remaining ? chunk_length : samp_remaining;

						SubVector<BaseFloat> wave_part(data, samp_offset, num_samp);
						feature_pipeline.AcceptWaveform(samp_freq, wave_part);

						decoding_timer.WaitUntil(samp_offset / samp_freq);
						decoder.AdvanceDecoding();
						samp_offset += num_samp;
					}

					decoder.FinalizeDecoding();

					CompactLattice clat;
					decoder.GetLattice(true, &clat);

					LOG_INFO("- writing analysis results (number of states: %d)", clat.NumStates());

					// write results of analysis
					if (clat.NumStates() != 0) {
						// compute best solution from the candidates
						CompactLattice best_path_clat;
						CompactLatticeShortestPath(clat, &best_path_clat);

						// align the solution using word boundary info so we get timings for the words
						CompactLattice aligned_clat;
						if(WordAlignLattice(best_path_clat, trans_model, wordboundary_info, 0, &aligned_clat)) {
							TopSortCompactLatticeIfNeeded(&aligned_clat);
						};

						// extract words and timings from the lattice
						std::vector<int32> loc_words, loc_times, loc_lengths;
						CompactLatticeToWordAlignment(aligned_clat, &loc_words, &loc_times, &loc_lengths);
						LOG_INFO("- Aligning words");


						// Hot fix of problem with alignment, must be resolved later
						std::transform(loc_times.begin(), loc_times.end(), loc_times.begin(),std::bind2nd(std::plus<int>(),time_offset));

						words.insert( words.end(), loc_words.begin(), loc_words.end() );
						times.insert( times.end(), loc_times.begin(), loc_times.end() );
						lengths.insert( lengths.end(), loc_lengths.begin(), loc_lengths.end() );

						// Takes *100 to compensate for exps in samp rate et c., and times are given in frames (1 frame == 10ms )
						if((i_seg < segments.size()-1) && ((loc_times.back() + loc_lengths.back())*100 > segments[i_seg+1].first)) {
							time_offset =loc_times.back()+loc_lengths.back();
						}
					} else {
						LOG_WARN("empty lattice");
					}

					//end loop over segments

					num_done++;
				}
			}
		}

		LOG_INFO("KaldiService: DONE ASR PROCESSING");




		LOG_INFO("KaldiService: START PUBLISHING THE ASSET");

		// Instantiate content part to hold the annotation
		pPart      outputPart = item->createPart(mico::persistence::model::URI("http://www.mico-project.eu/extractors/speechtotext/kaldi"));
		if(outputPart == nullptr){
			throw mico_critical_error("Unable to instantiate output Part");
		}
		pResource  rOutputPart = std::dynamic_pointer_cast<Resource>(outputPart);
		rOutputPart->setSyntacticalType("mico:kaldiXML");
		rOutputPart->setSemanticType("Automatic speech transcript of the input file at " + rAudioInputPart->getURI().stringValue());
		LOG_INFO( "KaldiService: Created new part at %s", rOutputPart->getURI().stringValue().c_str() );


		pAsset outputAsset = rOutputPart->getAsset();
		if(outputAsset == nullptr){
			throw mico_critical_error("Unable to instantiate output Asset");
		}

		LOG_INFO( "KaldiService: Created new asset at %s", outputAsset->getURI().stringValue().c_str() );
		outputAsset->setFormat("text/vnd.umu-kaldis+xml");

		std::ostream* out = outputAsset->getOutputStream();

		LOG_INFO("Creating XML");
		stringstream xml_ss;

		xml_ss << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" << std::endl;
		xml_ss << "<Speech2TextResults>" << std::endl;
		xml_ss << "<meta framesPerSecond=\"" << 100 << "\" samplingRate=\"";
		xml_ss << 8000 << "\" lang=\"english\" />" << std::endl;
		xml_ss << "<words>" << std::endl;


		for (size_t i = 0; i < words.size(); i++) {
			xml_ss << "<word timestamp=\"";
			xml_ss << times[i]*10;
			xml_ss << "\" length=\"";
			xml_ss << lengths[i]*10;
			xml_ss << "\">";
			xml_ss << escapeXMLcharacters(word_syms->Find(words[i]));
			xml_ss << "</word>" << std::endl;
		}

		xml_ss << "</words>" << std::endl;
		xml_ss << "</Speech2TextResults>" << std::endl;

		LOG_INFO("Writing XML");
		LOG_INFO(xml_ss.str().c_str());
		(*out) << xml_ss.str().c_str() << std::flush;
		delete out;
		outputPart->addInput(rAudioInputPart);
		outputPart->addInput(rXmlInputPart);

		LOG_INFO("KaldiService: DONE PUBLISHING THE ASSET");


		LOG_INFO("KaldiService: START PUBLISHING THE ANNOTATION");

		//TODO: create body
		//---- 0. retrieve the persistence service and init the jnipp scope
		PersistenceService& ps=rOutputPart->getPersistenceService();
		jnipp::Env::Scope scope(ps.getJVM());
		LOG_INFO("KaldiService: set scope");

		auto check_java_object=[&] (bool obj_exists )->void{
			ps.checkJavaExceptionThrow();
			if(! obj_exists) {
				throw mico_critical_error("KaldiService: Unable to instantiate java object from jvm");
			}
		};

		//---- 1. create a Body
		LOG_INFO("KaldiService: Creating a Body");
		jnipp::LocalRef<SpeechToTextBodyMMM> speechToTextBody=item->createObject(SpeechToTextBodyMMM::clazz());
		check_java_object(speechToTextBody);


		outputPart->setBody(speechToTextBody);
		LOG_INFO("KaldiService: Added Body to Part");

		//---- 2. create a Target
		LOG_INFO("KaldiService: Creating a Target");
		jnipp::LocalRef<SpecificResourceMMM>   targetMMM = item->createObject(SpecificResourceMMM::clazz());
		check_java_object(targetMMM);

		targetMMM->setSource(rAudioInputPart->getRDFObject());
		ps.checkJavaExceptionThrow();

		outputPart->addTarget(targetMMM);
		LOG_INFO("KaldiService: Added Target to Part");


		LOG_INFO("KaldiService: DONE PUBLISHING THE ANNOTATION");

		response.sendNew(item, rOutputPart->getURI());
		LOG_INFO("KaldiService: DONE");
	}
	catch(mico_critical_error &e){
		response.sendErrorMessage(item,
				ErrorCodes::UNEXPECTED_ERROR,
				"KaldiService: CRITICAL ERROR",
				e.what());
	}
	catch(std::exception& e){
		response.sendErrorMessage(item,ErrorCodes::UNEXPECTED_ERROR,typeid(e).name(),e.what());
	}

	std::remove(tempFileForInputXML.c_str());
	std::remove(tempFileForInputWAV.c_str());

	response.sendFinish(item);
}
}
}    
