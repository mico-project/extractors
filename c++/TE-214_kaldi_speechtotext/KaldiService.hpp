/*   	Copyright 2015 Adam Dahlgren <dali@cs.umu.se>
 * 		  2015 Umea University
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */


#ifndef HAVE_KALDI_SERVICE_HPP
#define HAVE_KALDI_SERVICE_HPP 1

// Kaldi has been compiled with these options ...
#define HAVE_ATLAS 1
#define HAVE_CXXABI_H 1
#define HAVE_POSIX_MEMALIGN 1
#define KALDI_DOUBLEPRECISION 0
#define HAVE_EXECINFO_H 1

#include "feat/wave-reader.h"
#include "online2/online-nnet2-feature-pipeline.h"
#include "online2/online-nnet2-decoding.h"
#include "online2/onlinebin-util.h"
#include "online2/online-timing.h"
#include "online2/online-endpoint.h"
#include "fstext/fstext-lib.h"
#include "lat/lattice-functions.h"
#include "lat/word-align-lattice.h"

#include "AnalysisService.hpp"
#include <memory>
#include <iosfwd>

// configuration options for Kaldi

// number of milliseconds of pause to consider a sentence break (default: 1/2 second)
#define SENTENCE_SPLIT 500

#define DEFAULT_SEGMENT_SIZE 30000
#define DEFAULT_CHUNK_SIZE 15000
#define SAMPLING_RATE 8000
// TODO Should perhaps be command line argument?
#define MAX_SEGMENT_SIZE_SEC 30

#define WORD_SYMBOLS_TABLE "/usr/share/mico-kaldi-data/graph/words.txt"
#define SMBR_EPOCH2_MODEL  "/usr/share/mico-kaldi-data/nnet_a_gpu_online/smbr_epoch2.mdl"
#define HCLG_FST           "/usr/share/mico-kaldi-data/graph/HCLG.fst"

namespace mico {
    namespace extractors {
        class KaldiService : public mico::event::AnalysisService {

            typedef kaldi::int32 int32;
            typedef kaldi::int64 int64;


            kaldi::OnlineEndpointConfig endpoint_config;

            kaldi::WordBoundaryInfoNewOpts wordboundary_config;

            // feature_config includes configuration for the iVector adaptation,
            // as well as the basic features.
            kaldi::OnlineNnet2FeaturePipelineConfig feature_config;
            kaldi::OnlineNnet2DecodingConfig nnet2_decoding_config;

            kaldi::TransitionModel trans_model;
            kaldi::nnet2::AmNnet nnet;

            fst::Fst<fst::StdArc> *decode_fst;
            fst::SymbolTable *word_syms;

        public:
            KaldiService();

            virtual ~KaldiService();


            void call(mico::event::AnalysisResponse& response, std::shared_ptr< mico::persistence::model::Item > item, std::vector< std::shared_ptr<mico::persistence::model::Resource>>inputResources, std::map<std::string,std::string>& params);
	    
            std::vector< std::pair<int32, int32> > createDummySegments(int dim);
            std::vector< std::pair<int32, int32> > getSegments( std::string xmlPath);
	    void testKaldi(std::string audiofile);
	    std::vector<std::pair<int32, int32> > breakDownTooLongSegments(std::vector<std::pair<int32,int32>> segments);

	    std::shared_ptr< mico::persistence::model::Resource > getInputXmlResource(std::vector< std::shared_ptr<mico::persistence::model::Resource>>inputResources);
	    std::shared_ptr< mico::persistence::model::Resource > getInputWavResource(std::vector< std::shared_ptr<mico::persistence::model::Resource>>inputResources);

	    std::shared_ptr< mico::persistence::model::Part > getOriginalAudioContentPart(std::shared_ptr< mico::persistence::model::Item > item);
	    std::string escapeXMLcharacters(std::string word);
        };

    };
}

#endif
