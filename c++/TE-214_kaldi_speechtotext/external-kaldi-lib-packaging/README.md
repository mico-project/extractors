# Packaging of the Kaldi Library

In order to package the Kaldi dependency, we need first to download and compile Kaldi, then to build the debian packages (lib and dev)

# 1. Download and install kaldi

Run the script export-kaldi.sh in this directory (requires to confirm few operations). The script is going to download and install Kaldi into '~/kaldi-install/'.

The dynamic libraries and the headers required by kaldi are going to be installed into ~/kaldi-install/dynamic.

# 2. Build the debian packages

The packaging is provided via cmake, which needs to run independently for both the lib package (-> only dynamic libraries) and for the dev package (-> only headers, depends on lib).

Kaldi depends on some external packages, which are however built with specific configuration by the KALDI framework itsels. 

In order to avoid conflicts with other installation of these packages, the rpath of the libraries is set to /lib/kaldi:/usr/lib/kaldi. As a consequence, in order to build the package the libraries must me moved temporally to that location

An example build, step-by-step, can be performed as follows:


```

sudo cp -R ~/kaldi-install/dynamic/lib/kaldi/ /usr/lib/

mkdir ~/build-kaldi-dist

mkdir ~/build-kaldi-dist/lib
cd ~/build-kaldi-dist/lib
cmake   -DKaldi_HOME=~/kaldi-install/dynamic   <path/to>/extractors/c++/TE-214_kaldi_speechtotext/external-kaldi-lib-packaging/mico-kaldi-lib
make package

mkdir ~/build-kaldi-dist/dev
cd ~/build-kaldi-dist/dev
cmake   -DKaldi_HOME=~/kaldi-install/dynamic   <path/to>/extractors/c++/TE-214_kaldi_speechtotext/external-kaldi-lib-packaging/mico-kaldi-lib-dev
make package

sudo rm -Rf /usr/lib/kaldi

```

