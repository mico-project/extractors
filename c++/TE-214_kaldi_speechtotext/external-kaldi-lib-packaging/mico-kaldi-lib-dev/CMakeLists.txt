cmake_minimum_required(VERSION 2.8.12)

project("mico-kaldi-lib-dev")


set(TARGET_NAME mico-kaldi-lib-dev)
set(TARGET_DESC "Headers of the OSS Kaldi speech to text framework required for building the kaldi speech to text extractor")
set(TARGET_SHORT_DESC "MICO headers of OSS Kaldi speech to text library.")
set(TARGET_VERSION_MAJOR 1)
set(TARGET_VERSION_MINOR 0)
set(TARGET_VERSION_PATCH 2)
set(TARGET_VERSION ${TARGET_VERSION_MAJOR}.${TARGET_VERSION_MINOR}.${TARGET_VERSION_PATCH})

get_filename_component(SHARED_HOME ${CMAKE_CURRENT_SOURCE_DIR}/../../../../share/ ABSOLUTE)
get_filename_component(ADD_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../../../modules/ ABSOLUTE)
set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ADD_MODULE_PATH})

find_package( Kaldi REQUIRED )

# The find package defines
# - Kaldi_Home
# - Kaldi_INCLUDE_DIRS
# - Kaldi_LIBRARIES

foreach (f ${Kaldi_INCLUDE_DIRS})
message(STATUS "+++++++++" ${f})
endforeach()

install(DIRECTORY ${Kaldi_INCLUDE_DIRS} DESTINATION include)

IF(EXISTS "${CMAKE_ROOT}/Modules/CPack.cmake")
	INCLUDE(InstallRequiredSystemLibraries)
	SET(CPACK_GENERATOR "DEB")
	SET(CPACK_PACKAGE_NAME ${TARGET_NAME})
	SET(CPACK_PACKAGE_FILE_NAME "${TARGET_NAME}_${TARGET_VERSION}_${CMAKE_SYSTEM_NAME}_${CMAKE_SYSTEM_PROCESSOR}")
	SET(CPACK_PACKAGE_VENDOR "Umea University")
	SET(CPACK_PACKAGE_DESCRIPTION ${TARGET_DESC})
	SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY ${TARGET_SHORT_DESC})
	SET(CPACK_SET_DESTDIR On)
	SET(CPACK_INSTALL_PREFIX /usr)
	SET(CPACK_PACKAGE_CONTACT "Adam Dahlgren <dali@cs.umu.se>")
	SET(CPACK_PACKAGE_VERSION_MAJOR "${TARGET_VERSION_MAJOR}")
  	SET(CPACK_PACKAGE_VERSION_MINOR "${TARGET_VERSION_MINOR}")
  	SET(CPACK_PACKAGE_VERSION_PATCH "${TARGET_VERSION_PATCH}")
	SET(CPACK_STRIP_FILES On)
	#Debian specifc package informations (add package dependencies here!)
	SET(CPACK_DEBIAN_PACKAGE_DEPENDS "mico-kaldi-lib")
	SET(CPACK_DEBIAN_PACKAGE_SECTION "non-free/science")
  	INCLUDE(CPack)
ENDIF(EXISTS "${CMAKE_ROOT}/Modules/CPack.cmake")