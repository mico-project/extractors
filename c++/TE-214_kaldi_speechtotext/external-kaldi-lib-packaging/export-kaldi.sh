#!/bin/bash

CURRENT_DIR=$PWD
SRC_DIR=~/kaldi-git-import/
TARGET_DIR=~/kaldi-install/
KALDI_GIT=https://github.com/kaldi-asr/kaldi.git
KALDI_EXPORTED_LIBRARIES=( base decoder feat fstext gmm hmm itf lat matrix cudamatrix online2 nnet2 ivector transform tree thread util )

if [ $# -eq 1 ]
  then
	TARGET_DIR=$1
fi

read -r -p "Running this script will download and install kaldi to ${TARGET_DIR}. Are you sure? [y/N] " response
case $response in
    [yY][eE][sS]|[yY])
        ;;
    *)
        exit 0
        ;;
esac

echo " ============= Installing required packages (requires sudo password) ..."
sudo apt-get install  automake autoconf libtool subversion libatlas3-base patchelf

echo " ============= Downloading kaldi via git from $KALDI_GIT"

git clone $KALDI_GIT $SRC_DIR
cd $SRC_DIR
git checkout b111a8baf83248ad0320e4bc071034848f3bdb73
cd $CURRENT_DIR

sed -i '/OPENFST_VERSION = 1.4.1/c\OPENFST_VERSION = 1.4.1' $SRC_DIR/tools/Makefile


echo " ============= Building kaldi dependencies ... "

cd $SRC_DIR/tools
make -j4

echo " ============= Building kaldi static and shared libraries ... "

cd ../src
./configure --shared
make depend -j4
make -j4

cd $CURRENT_DIR

echo " ============= Copying header files ..."

mkdir -p ${TARGET_DIR}



for MODE_DIR in static dynamic
do 

  mkdir -p $TARGET_DIR/$MODE_DIR/
  mkdir -p $TARGET_DIR/$MODE_DIR/src/

  for library in "${KALDI_EXPORTED_LIBRARIES[@]}"
  do
    mkdir -p $TARGET_DIR/$MODE_DIR/src/$library
    cp $SRC_DIR/src/$library/*.h       $TARGET_DIR/$MODE_DIR/src/$library/
  done

done

mkdir -p $TARGET_DIR/dynamic/include/
mkdir -p $TARGET_DIR/dynamic/include/kaldi

cp -R $TARGET_DIR/dynamic/src/* $TARGET_DIR/dynamic/include/kaldi
rm -Rf $TARGET_DIR/dynamic/src

echo " ============= Copying libraries ..."

for library in "${KALDI_EXPORTED_LIBRARIES[@]}"
do
  cp $SRC_DIR/src/$library/*.a       $TARGET_DIR/static/src/$library/ 2>/dev/null
done

mkdir -p $TARGET_DIR/dynamic/lib
mkdir -p $TARGET_DIR/dynamic/lib/kaldi
cp $SRC_DIR/src/lib/*.so  $TARGET_DIR/dynamic/lib/kaldi


SO_LIBS=$TARGET_DIR/dynamic/lib/kaldi/*.so

for f in $SO_LIBS
do
  echo "Removing rpath from $f ..."
  patchelf --set-rpath /lib/kaldi:/usr/lib/kaldi $f
done

echo " ============= Copying dependencies ..."

mkdir -p $TARGET_DIR/static/tools
mkdir -p $TARGET_DIR/static/tools/openfst
cp -R $SRC_DIR/tools/openfst/include $TARGET_DIR/static/tools/openfst/
cp -R $SRC_DIR/tools/openfst/lib $TARGET_DIR/static/tools/openfst/

cp -R $SRC_DIR/tools/openfst/include/fst $TARGET_DIR/dynamic/include/kaldi/
cp -R $SRC_DIR/tools/openfst/lib/libfst.so.3.0.0 $TARGET_DIR/dynamic/lib/kaldi/
ln -s libfst.so.3.0.0  $TARGET_DIR/dynamic/lib/kaldi/libfst.so.3
ln -s libfst.so.3  $TARGET_DIR/dynamic/lib/kaldi/libfst.so


echo "Done installing kaldi-dev into ${TARGET_DIR}."
read -r -p "Do you want to delete the local git export and full build in ${SRC_DIR}? [y/N] " response
case $response in
    [yY][eE][sS]|[yY])
        rm -Rf $SRC_DIR
        ;;
    *)
        exit 0
        ;;
esac


exit 0
