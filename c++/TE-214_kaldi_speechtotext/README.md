![MICO](http://www.mico-project.eu/wp-content/uploads/2014/04/mico_logo.png)

[TOC]

# TE-214 - Kaldi Speech to text

This extractor uses Kaldi to provide a speech to text service extracting timestamped speech.

# Copyright notice
   Copyright 2015 Adam Dahlgren <dali@cs.umu.se>
   			 2015 Umea University

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

# Dependencies

external dependencies:

* Kaldi library (with internal dependencies)
* Boost library
* MICO Platform API 
* Protobuf (transitive dependency through mico_event)
* ATLAS
* BLAS
* gfortran3 library

located in 3rd_party folder:

* RapidXML


# Before Running KALDIService

This extractor builds on the open source speech to text library Kaldi.

## 1. Download KALDI

Kaldi is currently available in its MICO-specific distribution as a debian package. This can be acquired by calling

	sudo apt-get install mico-kaldi-lib-dev

If you need a local build, follow the instructions in external-kaldi-lib-packaging/README.md, and install the resulting packages using e.g., gdebi.

Alternatively, Kaldi can be acquiredy by downloading KALDI with 

	git clone https://github.com/kaldi-asr/kaldi.git kaldi-trunk
	
Before compiling make sure that your Kaldi installation is using OpenFST 1.4.1 and not any previous version, since OpenFST 1.3.4 needs to be compiled with a C++ standard prior to -std=c++0x as it uses tr1/unordered_{set,map,multiset,multimap} and the platform and API is dependent on C++11. This is done by editing
	
	kaldi-trunk/tools/Makefile

and commenting in the line containing

	OPENFST_VERSION = 1.4.1

and then compile

	cd kaldi-trunk/tools/; make; cd ../src; ./configure; make

## 2. Download online-nnet2 models for KALDI

To use KALDI a language model is needed. KALDI provides some free models that can be downloaded and extracted, one for english (which should be used together with this extractor) can be installed with two approaches. 

The first approach consists of acquiring the model by calling

	sudo apt-get install mico-kaldi-data
	
An alternative manual installation can be performed as follows:

	wget http://kaldi-asr.org/downloads/build/5/trunk/egs/fisher_english/s5/exp/nnet2_online/nnet_a_gpu_online/archive.tar.gz -O nnet_a_gpu_online.tar.gz
	wget http://kaldi-asr.org/downloads/build/2/sandbox/online/egs/fisher_english/s5/exp/tri5a/graph/archive.tar.gz -O graph.tar.gz
	mkdir -p nnet_a_gpu_online graph
	tar zxvf nnet_a_gpu_online.tar.gz -C nnet_a_gpu_online
	tar zxvf graph.tar.gz -C graph
	
Now move the graph and nnet_a_gpu_online to /usr/share/mico-kaldi-data, followed by editing the models configuration files accordingly: 
	
	mkdir /usr/share/mico-kaldi-data
	cd /usr/share/mico-kaldi-data
	for x in nnet_a_gpu_online/conf/*conf; do
	cp $x $x.orig
	sed s:/export/a09/dpovey/kaldi-clean/egs/fisher_english/s5/exp/nnet2_online/:$(pwd)/: < $x.orig > $x
	done
	
## 3. Set correct paths

In order for the extractor to compile and run properly, the location of the kaldi distribution must be known to the extractor. The path to the libraries can be set as follows:

	sudo echo /lib/kaldi > /etc/ld.so.conf.d/mico-kaldi.conf
    sudo echo /usr/lib/kaldi >> /etc/ld.so.conf.d/mico-kaldi.conf
    sudo ldconfig

# 4 Compiling the extractor

If all steps are followed the extractor should now be compiled with

    cmake -DMICOPlatformAPI_HOME=[path to MICO platform API] [path to extractor source]

## 1. Internal Kaldi parameters and performance

The extractor is very CPU, memory and time consuming. However, there are internal parameters that can be tuned to specific needs. The current implementation does not support changing the parameters via the CLI, this needs to be done compile time. The default settings tries to maximize accuracy and minimize the transcription time. Below are some other example settings taken from 60 seconds of (british) english (more will follow when memory footprint tests are analyzed):

        0.1, 10.0, 4.0, 4000: 21.1, 32 (default)
        0.15, 7.5, 4.0, 2000: 29.6, 20

where the settings are listed as {acoustic Scaling, beam size, lattice beam size, nr. of states: Word error rate, transcription time}.


# 5 Running the extractor

The extractor can be called as a standalone or as a daemon.
	mico-extractor-speechtotext <host> <user> <password> [--daemon [-k]]
	
The two optional arguments switch to executing as a daemon 

  **--daemon**  Start as daemon  
  **-k**        Kill daemon

# 6 Debian package

The extractor can be built as a Debian package as well with the following procedure

	cd [Extractor root]; cmake . ; make package;

This produces a .deb file containing the extractor and can be installed with

	sudo gdebi mico-extractor-speech-to-text_<version>_<arch>.deb
    
Installing the extactor will install everything necessary to run the extractor, including the language model.
Keep in mind that the model is nearly 1GB in size but if space is needed the file HCLGa.fst located under /usr/share/mico-kaldi-data/graph can be removed as this is not used at the moment.
