#include "AudioConverterService.h"
#include "AudioDemuxer.h"

#include <Logging.hpp>
#include <string>
#include <fstream>
#include <EventManager.hpp>
#include <vocabularies.hpp>

#include <PersistenceService.hpp>
#include <Item.hpp>
#include <Part.hpp>
#include <Asset.hpp>

using namespace mico::event;
using namespace mico::event::model;
using namespace mico::persistence;
using namespace mico::persistence::model;


using pResource = std::shared_ptr<Resource>;
using pItem  = std::shared_ptr<Item>;
using pPart  = std::shared_ptr<Part>;
using pAsset = std::shared_ptr<Asset>;

#include <anno4cpp.h>
#include <jnipp.h>

using namespace jnipp::eu::mico::platform::anno4j::model;
using namespace jnipp::eu::mico::platform::anno4j::model::namespaces;
using namespace jnipp::eu::mico::platform::anno4j::model::impl::bodymmm;
using namespace jnipp::eu::mico::platform::anno4j::model::impl::targetmmm;
using namespace jnipp::com::github::anno4j::model;
using namespace jnipp::com::github::anno4j::model::impl::selector;
using namespace jnipp::java::lang;

#include <MICOExtractorID.h>

AudioConverterService::AudioConverterService(AudioConverterService::TargetSamplingFrequency f) :
          AnalysisService(mico::extractors::getPrecompExtractorId(),mico::extractors::getPrecompExtractorModeId(),mico::extractors::getPrecompExtractorVersion(),
                          "audio/mpeg", "audio/wav"), m_audioDemuxer(0), m_chosenFrequency(f)
{
    switch(f) {
    case AudioConverterService::_8kHz  : m_audioDemuxer.reset(new mico::AudioDemuxer(8000));  break;
    case AudioConverterService::_16kHz : m_audioDemuxer.reset(new mico::AudioDemuxer(16000)); break;
    case AudioConverterService::_24kHz : m_audioDemuxer.reset(new mico::AudioDemuxer(24000)); break;
    default : std::string error_msg="AudioConverterService() : CRITICAL ERROR: unhandled valid input frequency";
    throw std::logic_error(error_msg);
    }
}

AudioConverterService::~AudioConverterService() {

}


void AudioConverterService::call(mico::event::AnalysisResponse &resp, std::shared_ptr< mico::persistence::model::Item > item, std::vector< std::shared_ptr<mico::persistence::model::Resource>>inputResources, std::map<std::string,std::string>& params){

    LOG_INFO("AudioConverterService: START");
    //1 verify correctness of inputs:

    //non-empty item
    if(item == nullptr){
        resp.sendErrorMessage(item,
                ErrorCodes::UNEXPECTED_ERROR,
                "AudioConverter: CRITICAL ERROR",
                "No input item is present");
        return;
    }

    //only one part
    if(inputResources.size() != 1){
        resp.sendErrorMessage(item,
                ErrorCodes::UNEXPECTED_ERROR,
                "AudioConverter: CRITICAL ERROR",
                "Received " + std::to_string(inputResources.size()) + " inputs, should be 1");
        return;
    }

    //TODO: parameters are there and are correct


    //2. retrieve the asset

    pResource rInputPart = inputResources.front();

    if(rInputPart == nullptr){
        resp.sendErrorMessage(item,
                ErrorCodes::UNEXPECTED_ERROR,
                "AudioConverter: CRITICAL ERROR",
                "Cannot retrieve input Resource");
        return;
    }


    if(rInputPart->hasAsset()) {

        pAsset    asset  = rInputPart->getAsset();

        //check correctness of the input mime type:

        //if the format is not starting with 'audio/', raise exception
        if (asset->getFormat().find("audio/") != 0){
            resp.sendErrorMessage(item,
                     ErrorCodes::UNSUPPORTED_CONTENT_TYPE,
                    "AudioConverter: Unsupported mime type",
                    "Cannot handle input with format  " + asset->getFormat() +
                                        "because asset->getFormat().find('audio/') == " + std::to_string(asset->getFormat().find("audio/")));
                return;
        }

        //declare pointers and filenames of temporary files for cleanup during exceptions

        std::istream* inputStream = nullptr;
        std::ostream* outputStream = nullptr;

        std::string iFilename;
        std::string oFilename;

        auto cleanup = [](std::string iFile,std::string oFile, std::istream* iStream, std::ostream* oStream) -> void{

            //remove temporary files, if needed (std::remove is exception free)
            std::remove(iFile.c_str());
            std::remove(oFile.c_str());

            //and deallocate objects from the heap

            if(oStream!=nullptr)  delete oStream;
            if(iStream!=nullptr)  delete iStream;

        };

        auto abort_call = [&](const std::exception & e, mico::event::model::ErrorCodes code, std::string iFile,std::string oFile, std::istream* iStream, std::ostream* oStream)->void{

            // notify broker that the execution terminated, but that we didn't create any new content item
            LOG_ERROR("Convertion terminated after throwing an instance of '%s'",typeid(e).name());
            LOG_ERROR("  what(): %s",e.what());

            resp.sendErrorMessage(item,code,typeid(e).name(),e.what());

            //and cleanup every local pointer
            cleanup(iFile,oFile,iStream,oStream);
            m_audioDemuxer.reset(new mico::AudioDemuxer(m_audioDemuxer->getTargetFrequency()));

        };

        try{

            LOG_INFO("AudioConverterService: START CONVERTING");

            //---- 1: Save content item as local file. Its name depends on the mime type

            //load istream of <mime-type> file
            inputStream = asset->getInputStream();

            //set name, including extension, depending on the mime type
            std::string basename=std::to_string((long unsigned int) &inputStream);

            std::string iFilename;
            std::string oFilename=basename+".wav";

            //ffmpeg is using the extension to identify the decoder
            iFilename=basename + "." + getAssetExtension(asset->getFormat());

            //store the istream locally
            std::ofstream oFileStream;
            oFileStream.open(iFilename);
            if(!oFileStream.is_open()){
                abort_call(std::runtime_error("AudioConverterService::call() unable to open temporary file " + iFilename),ErrorCodes::INSUFFICIENT_STORAGE,iFilename,oFilename,inputStream,outputStream);
                return;
            }
            std::vector<char> buff = std::vector<char>(std::istreambuf_iterator<char>(*inputStream), std::istreambuf_iterator<char>());
            for(uint i = 0;i < buff.size();++i)
            {
                oFileStream << buff[i];
            }

            oFileStream.close();
            inputStream->seekg(0);



            //---- 2: Perform the convertion operation


            m_audioDemuxer->openInputFile((char *)iFilename.c_str());
            m_audioDemuxer->openOutputFile((char *)oFilename.c_str());
            m_audioDemuxer->demux();

            LOG_INFO("AudioConverterService: DONE CONVERTING");

            //---- 3. read convertion and store in the output content

            LOG_INFO("AudioConverterService: START PUBLISHING THE ASSET");

            //load output file
            std::ifstream iFileStream;
            iFileStream.open(oFilename);
            if(!iFileStream.is_open()){
                throw std::runtime_error("AudioConverterService::call() unable to open temporary file " + iFilename);
            }

            //---- 4. create a part, and assign it the newly created asset


            // Instantiate content part to hold the annotation
            pPart      outputPart = item->createPart(mico::persistence::model::URI(getServiceID()));
            if(outputPart == nullptr){
                throw std::runtime_error("AudioConverterService::call() unable to instantiate output Part");
            }


            pResource  rOutputPart = std::dynamic_pointer_cast<Resource>(outputPart);
            LOG_INFO( "AudioConverterService: Created new part at %s", rOutputPart->getURI().stringValue().c_str() );


            pAsset outputAsset = rOutputPart->getAsset();
            if(outputAsset == nullptr){
                throw std::runtime_error("AudioConverterService::call() unable to instantiate output Asset");
            }


            LOG_INFO( "AudioConverterService: Created new asset at %s", outputAsset->getURI().stringValue().c_str() );
            outputAsset->setFormat("audio/wav");

            rOutputPart->setSyntacticalType("mico:Audio");
            rOutputPart->setSemanticType("Audio decoded from the asset of "+rInputPart->getURI().stringValue());


            //get output stream for the content part
            outputStream = outputAsset->getOutputStream();

            //and copy all the content
            buff = std::vector<char>(std::istreambuf_iterator<char>(iFileStream), std::istreambuf_iterator<char>());
            for(uint i = 0;i < buff.size();++i)
                (*outputStream) << buff[i];

            //delete the temporary wav files
            iFileStream.close();
            outputPart->addInput(rInputPart);

            LOG_INFO("AudioConverterService: DONE PUBLISHING THE ASSET");




            LOG_INFO("AudioConverterService: START PUBLISHING THE ANNOTATION");

            //---- 0. retrieve the JVM scope
            PersistenceService& ps=rInputPart->getPersistenceService();
            jnipp::Env::Scope scope(ps.getJVM());

            LOG_INFO("AudioConverterService: set scope");

            auto check_java_object=[&] (bool obj_exists )->void{
                ps.checkJavaExceptionThrow();
                if(! obj_exists) {
                    throw std::runtime_error("AudioConverterService::call(): unable to instantiate java object from jvm");
                }
            };


            //---- 1. create a Body
            jnipp::LocalRef<AudioDemuxBodyMMM> audioDemuxBody=item->createObject(AudioDemuxBodyMMM::clazz());
            check_java_object(audioDemuxBody);
            LOG_INFO("AudioConverterService: created body");


            std::string samplingRate;
            switch(m_chosenFrequency) {
            case AudioConverterService::_8kHz  : samplingRate="8000";  break;
            case AudioConverterService::_16kHz : samplingRate="16000"; break;
            case AudioConverterService::_24kHz : samplingRate="24000"; break;
            default : std::string error_msg="AudioConverterService() : CRITICAL ERROR: unhandled valid input frequency";
            throw std::logic_error(error_msg);
            }
            audioDemuxBody->setFrameRate(String::create(samplingRate));
            ps.checkJavaExceptionThrow();
            LOG_INFO("AudioConverterService: annotated body");


            outputPart->setBody(audioDemuxBody);
            LOG_INFO("AudioConverterService: added body to part");




            //---- 2. create a Target

            jnipp::LocalRef<SpecificResourceMMM>   targetMMM = item->createObject(SpecificResourceMMM::clazz());
            check_java_object(targetMMM);
            LOG_INFO("AudioConverterService: created target mmm");


            LOG_INFO("AudioConverterService: trying to retrieve Assetmmm");

            jnipp::LocalRef<AssetMMM> assetMMM = item->findObject(outputAsset->getURI(),AssetMMM::clazz());


            check_java_object(assetMMM);
            LOG_INFO("AudioConverterService: retrieved Assetmmm");

            targetMMM->setSource(assetMMM);
            ps.checkJavaExceptionThrow();

            outputPart->addTarget(targetMMM);



            LOG_INFO("AudioConverterService: DONE PUBLISHING THE ANNOTATION");

            // notify broker that we produced the new mico:Audio part

            resp.sendNew(item, rOutputPart->getURI());


            cleanup(iFilename,oFilename,inputStream,outputStream);

        }
        catch(const std::runtime_error & e)
        {
            abort_call(e,ErrorCodes::UNEXPECTED_ERROR,iFilename,oFilename,inputStream,outputStream);
        }
        catch(const std::invalid_argument & e)
        {
            abort_call(e,ErrorCodes::UNEXPECTED_ERROR,iFilename,oFilename,inputStream,outputStream);
        }
        catch(const std::logic_error & e)
        {
            abort_call(e,ErrorCodes::UNEXPECTED_ERROR,iFilename,oFilename,inputStream,outputStream);
        }
        catch(const std::exception & e)
        {
            abort_call(e,ErrorCodes::UNEXPECTED_ERROR,iFilename,oFilename,inputStream,outputStream);
        }


    }
    else{

        // notify broker that there was no asset to be processed
        resp.sendErrorMessage(item,
                ErrorCodes::MISSING_ASSET,
                "Audio Converter: MISSING ASSET",
                "No asset found for part at at " + rInputPart->getURI().stringValue());
    }

    resp.sendFinish(item);
    LOG_INFO("AudioConverterService: DONE");
}


std::string AudioConverterService::getServiceName(AudioConverterService::TargetSamplingFrequency f){
    std::string out ="mico-extractor-audioconverter-";
    switch(f) {
        case AudioConverterService::_8kHz  : out=(out+std::string("8kHz"));  break;
        case AudioConverterService::_16kHz : out=(out+std::string("16kHz")); break;
        case AudioConverterService::_24kHz : out=(out+std::string("24kHz")); break;
        default : std::string error_msg="AudioConverterService() : CRITICAL ERROR: unhandled valid input frequency";
                  LOG_ERROR(error_msg.c_str());
                  throw std::logic_error(error_msg);
    }
    return out;
}


std::string AudioConverterService::getAssetExtension(std::string mimeType){

    if( mimeType.compare("audio/wav")   == 0 ) return std::string("wav");
    if( mimeType.compare("audio/x-wav") == 0 ) return std::string("wav");
    if( mimeType.compare("audio/mpeg")  == 0 ) return std::string("mp3");
    if( mimeType.compare("audio/mp4")   == 0 ) return std::string("aac");
    if( mimeType.find("audio/") == 0 ){
        LOG_WARN("The audio extension associated with mimeType '%s' is unknown, returning 'audio'",mimeType.c_str());
        return "audio";
    }
    std::string error_msg="AudioConverterService() : CRITICAL ERROR: mime type '"+mimeType+"' is not of type 'audio/*'";
    LOG_ERROR(error_msg.c_str());
    throw std::logic_error(error_msg);
}
