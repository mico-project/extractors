#ifndef AUDIOCONVERTERSERVICE_H
#define AUDIOCONVERTERSERVICE_H

#include "AnalysisService.hpp"

#include <memory>
#include <iosfwd>


namespace mico {
    class AudioDemuxer;
}


class AudioConverterService : public mico::event::AnalysisService
{
public:

    enum TargetSamplingFrequency { _8kHz, _16kHz, _24kHz };

    AudioConverterService(AudioConverterService::TargetSamplingFrequency f);

    static std::string getServiceName(AudioConverterService::TargetSamplingFrequency f);
    static std::string getAssetExtension(std::string mimeType);

    virtual ~AudioConverterService();

    void call(mico::event::AnalysisResponse& response, std::shared_ptr< mico::persistence::model::Item > item, std::vector< std::shared_ptr<mico::persistence::model::Resource>>inputResources, std::map<std::string,std::string>& params);

private:
    std::shared_ptr<mico::AudioDemuxer> m_audioDemuxer;
    AudioConverterService::TargetSamplingFrequency m_chosenFrequency;
};

#endif // AUDIOCONVERTERSERVICE_H
