#include "MediaInfoService.h"
#include <MediaInfoDLL/MediaInfoDLL.h>

#include <Logging.hpp>
#include <string>
#include <fstream>
#include <EventManager.hpp>
#include <vocabularies.hpp>

#include <PersistenceService.hpp>
#include <Item.hpp>
#include <Part.hpp>
#include <Asset.hpp>

using namespace mico::event;
using namespace mico::event::model;
using namespace mico::persistence;
using namespace mico::persistence::model;


using pResource = std::shared_ptr<Resource>;
using pItem  = std::shared_ptr<Item>;
using pPart  = std::shared_ptr<Part>;
using pAsset = std::shared_ptr<Asset>;

#include <MICOExtractorID.h>

MediaInfoService::MediaInfoService() 
: AnalysisService(mico::extractors::getPrecompExtractorId(), mico::extractors::getPrecompExtractorModeId(), mico::extractors::getPrecompExtractorVersion(),
		"video/mp4", "text/vnd.fhg-mediainfo+xml"), m_info(new MediaInfoDLL::MediaInfo())
{

	LOG_INFO(("Starting Mico Mediatag Extractor Service: " + m_info->Option("Info_Version", "1")).c_str());
}

MediaInfoService::~MediaInfoService() {
	LOG_INFO("Shutting down Mico Mediatag Extractor Service");
}


void storeStream(std::istream* in, std::string tmpName){
	FILE* pFile;
	pFile = fopen(tmpName.c_str(), "wb");
	char * buffer = new char [1024];
	std::streamsize all = 0;
	std::cout << "try to read first 1024 bytes";
	in->read(buffer, 1024);
	std::streamsize n = in->gcount();
	std::cout << ".. got "<< n << "bytes" << std::endl;
	while (n != 0) {
		all +=n;
		fwrite(buffer, sizeof(char), n, pFile);
		if (in) { in->read(buffer, 1024); n = in->gcount();}
	}
	delete buffer;
}

std::string MediaInfoService::analyzeFile(std::string sAudioFile){

	LOG_INFO(("analyzing file: " + sAudioFile + "..").c_str());

	m_info->Open(sAudioFile);
	m_info->Option("Complete_name", "");
	m_info->Option("Inform", "XML");
	std::string ret = m_info->Inform();
	m_info->Close();
	LOG_INFO("... done.");
	return ret;
}


void MediaInfoService::call(AnalysisResponse& resp, pItem item, std::vector< pResource> inputResources, std::map<std::string,std::string>& params)
{

	//1 verify correctness of inputs:

	//non-empty item
	if(item == nullptr){
		resp.sendErrorMessage(item,
				ErrorCodes::UNEXPECTED_ERROR,
				"MediaInfo: CRITICAL ERROR",
				"No input item is present");
		return;
	}

	//only one part
	if(inputResources.size() != 1){
		resp.sendErrorMessage(item,
				ErrorCodes::UNEXPECTED_ERROR,
				"MediaInfo: CRITICAL ERROR",
				"Received " + std::to_string(inputResources.size()) + " inputs, should be 1");
		return;
	}

	//2. retrieve the asset

	pResource rInputPart = inputResources.front();

	if(rInputPart == nullptr){
		resp.sendErrorMessage(item,
				ErrorCodes::UNEXPECTED_ERROR,
				"MediaInfo: CRITICAL ERROR",
				"Cannot retrieve input Resource");
		return;
	}


	if(rInputPart->hasAsset()) {

		pAsset    asset  = rInputPart->getAsset();

		//check correctness of the input mime type:
		if(std::string("video/mp4").compare(asset->getFormat()) != 0)
		{
			resp.sendErrorMessage(item,
					ErrorCodes::UNSUPPORTED_CONTENT_TYPE,
					"MediaInfo: Unsupported mime type",
					"Cannot handle input with format  " + asset->getFormat());
			return;
		}



		auto cleanup = [](std::string outputXmlFileName) -> void{

			//remove temporary files, if needed (std::remove is exception free)
			std::remove(outputXmlFileName.c_str());

		};

		auto abort_call = [&](const std::exception & e, mico::event::model::ErrorCodes code, std::string outputXmlFileName)->void{

			// notify broker that the execution terminated, but that we didn't create any new content item
			LOG_ERROR("Processing terminated after throwing an instance of '%s'",typeid(e).name());
			LOG_ERROR("  what(): %s",e.what());

			resp.sendErrorMessage(item,code,typeid(e).name(),e.what());

			//and cleanup everything
			cleanup(outputXmlFileName);

		};


		std::string inputFileName =std::to_string((long unsigned int) &item);

		try{

			LOG_INFO("MediaInfo: START ANALYSIS");

			// TODO: replace storing with direct stream analyzing from mediaInfo

			//1. store the file asset locally
			std::shared_ptr<std::istream> in(asset->getInputStream());
			LOG_INFO("Storing data at: %s", inputFileName.c_str());
			storeStream(in.get(), inputFileName);
			LOG_INFO("File stored locally");


			LOG_INFO("MediaInfo: START PUBLISHING THE ASSET");

			// Instantiate content part to hold the annotation
			pPart      outputPart = item->createPart(mico::persistence::model::URI(getServiceID()));
			if(outputPart == nullptr){
				throw std::runtime_error("MediaInfo::call() unable to instantiate output Part");
			}


			pResource  rOutputPart = std::dynamic_pointer_cast<Resource>(outputPart);
			LOG_INFO( "MediaInfo: Created new part at %s", rOutputPart->getURI().stringValue().c_str() );


			pAsset outputAsset = rOutputPart->getAsset();
			if(outputAsset == nullptr){
				throw std::runtime_error("MediaInfo::call() unable to instantiate output Asset");
			}


			LOG_INFO( "MediaInfo: Created new asset at %s", outputAsset->getURI().stringValue().c_str() );
			outputAsset->setFormat("text/vnd.fhg-mediainfo+xml");

			rOutputPart->setSyntacticalType("mico:MediaInfoXml");
			rOutputPart->setSemanticType("Information retrieved by MediaInfo from the asset of "+rInputPart->getURI().stringValue());

			//get output stream for the content part
			std::shared_ptr<std::ostream> assetOutputStream( outputAsset->getOutputStream());

			//and copy all the content
			*assetOutputStream << analyzeFile(inputFileName);
			outputPart->addInput(rInputPart);

			LOG_INFO("MediaInfo: DONE PUBLISHING THE ASSET");
			LOG_INFO("MediaInfo: DONE ANALYSIS");

			// notify broker that we produced the new part

			resp.sendNew(item, rOutputPart->getURI());


			cleanup(inputFileName);

		}
		catch(const std::runtime_error & e)
		{
			abort_call(e,ErrorCodes::UNEXPECTED_ERROR,inputFileName);
		}
		catch(const std::invalid_argument & e)
		{
			abort_call(e,ErrorCodes::UNEXPECTED_ERROR,inputFileName);
		}
		catch(const std::logic_error & e)
		{
			abort_call(e,ErrorCodes::UNEXPECTED_ERROR,inputFileName);
		}
		catch(const std::exception & e)
		{
			abort_call(e,ErrorCodes::UNEXPECTED_ERROR,inputFileName);
		}


	}
	else{

		// notify broker that there was no asset to be processed
		resp.sendErrorMessage(item,
				ErrorCodes::MISSING_ASSET,
				"MediaInfo: MISSING ASSET",
				"No asset found for part at at " + rInputPart->getURI().stringValue());
	}

	resp.sendFinish(item);

};
