[TOC]

# TE-227 Media-Tag-Extractor

# Copyright notice

All rights reserved by Fraunhofer IDMT. This software represents background know-how / technologies owned by Fraunhofer IDMT. As outlined
in the Consortium Agreement, Fraunhofer IDMT grants all MICO consortium partners a limited, non-exclusive, non-transferable, non-assignable,
non-sublicensable, royalty-free license to use the software for the sole purpose of conducting R&D work within MICO including demonstration and
evaluation purposes until Oct 31, 2016, provided that (i) this software remains under strict control of the respective
consortium partner and (ii) the software and all related material is removed completely from the third parties' premises after use.
Usages beyond the described scope, including commercial use and exploitation, are not allowed, and require separate license agreements.

For the sake of simplification, a respective agreement is provided in addition to this disclaimer, listing all MICO-relevant background work
from Fraunhofer IDMT.


# Dependencies

external dependencies:

* boost library
* MICO Platform API 
* protobuf (transitive dependency through mico_event)
* libmediainfo-dev (>=0.7.70-1)


# Build

    cmake -DMICO_PLATFORM_API_HOME=[path to MICO platform API] -DCMAKE_PREFIX_PATH=[path to local installs] [path to extractor source]
   
# Installation

**Notice:** There's no installation target yet!
