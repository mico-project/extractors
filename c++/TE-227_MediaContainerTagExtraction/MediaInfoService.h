#ifndef MEDIAINFOSERVICE_H_
#define MEDIAINFOSERVICE_H_

#include "AnalysisService.hpp"
#include <memory>
#include <iosfwd>

namespace MediaInfoDLL {
	class MediaInfo;
}

class MediaInfoService : public mico::event::AnalysisService {

public:

  MediaInfoService();
  ~MediaInfoService();

  void call(mico::event::AnalysisResponse& response, std::shared_ptr< mico::persistence::model::Item > item, std::vector< std::shared_ptr<mico::persistence::model::Resource>>inputResources, std::map<std::string,std::string>& params);
  std::string analyzeFile(std::string sAudioFile);

private:
  std::shared_ptr<MediaInfoDLL::MediaInfo> m_info;
    
};

#endif /* MEDIAINFOSERVICE_H_ */
