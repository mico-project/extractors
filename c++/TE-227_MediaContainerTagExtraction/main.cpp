#include "MediaInfoService.h"

#include <Daemon.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <string>


// this namespace contains EventManager and AnalysisService
using namespace mico::event;



/****** globals ******/


// return values
const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t SUCCESS = 0;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;


// auxiliary variables and function when running as service

EventManager* mgr = 0;
MediaInfoService* g_MediaInfoService = 0;
bool loop = true;
const char * serviceId="mico-extractor-mediainfo";


void kill_service(int signum)
{
  std::cout << "Shutting down MediaInfo service" << std::endl;

  if (mgr)
    mgr->unregisterService(g_MediaInfoService);

  if (g_MediaInfoService)
    delete g_MediaInfoService;

  if (mgr)
    delete mgr;

  loop = false;
}

void start_service(const char * server_name, const char *mico_user, const char * mico_pass )
{
  std::cout << "Setting up MediaInfo service" << std::endl;

  mgr = new EventManager(server_name, mico_user, mico_pass);
  g_MediaInfoService = new MediaInfoService();

  mgr->registerService(g_MediaInfoService);

  signal(SIGINT,  &kill_service);
  signal(SIGTERM, &kill_service);
  signal(SIGHUP,  &kill_service);

  while(loop) {
    sleep(1);
  }

}

int kill_daemon(std::string service_id)
{
	std::cout << "Shutting down daemon for MediaInfo service" << std::endl;

	return mico::daemon::stop(service_id.c_str());
}

int start_daemon(const char * server_name, const char *mico_user, const char * mico_pass )
{
	std::cout << "Setting up daemon for MediaInfo service" << std::endl;

	return mico::daemon::start(serviceId, server_name, mico_user, mico_pass, {new MediaInfoService()});
}



int main(int argc, char** argv)
{
	enum selected_operation {START_DAEMON, KILL_DAEMON, START_SERVICE};
	selected_operation operation=START_SERVICE;
	try
	  {
	    std::string appName = boost::filesystem::basename(argv[0]);
	    std::string server_ip;
	    std::string user;
	    std::string password;
	    std::string mime;
	    unsigned int freq=0;

	    /** Define and parse the program options
	     */
	    namespace po = boost::program_options;
	    po::options_description desc("Options");
	    desc.add_options()
	      ("help,h", "Print help messages")
	      ("daemon", "run as daemon")
	      ("kill,k", "kill running daemon")
	      ("server_ip", po::value<std::string>(&server_ip)->required(), "ip of the mico platform")
	      ("user", po::value<std::string>(&user)->required(), "user name")
	      ("pass", po::value<std::string>(&password)->required(), "password");


	    po::positional_options_description positionalOptions;
	    positionalOptions.add("server_ip", 1);
	    positionalOptions.add("user", 1);
	    positionalOptions.add("pass", 1);

	    po::variables_map vm;

	    auto print_usage = [&]()->void{
	    	std::cout << "Usage: "<< appName << " SERVER_IP USER PASSWORD [--daemon [-k]]" << std::endl;
	    };

	    try
	    {
	      po::store(po::command_line_parser(argc, argv).options(desc)
	                  .positional(positionalOptions).run(),
	                vm); // throws on error, e.g. unexpected inputs

	      // --help option

	      if ( vm.count("help")  )
	      {
	        print_usage();
	        std::cout << desc << std::endl;

	        return SUCCESS;
	      }


	      // --daemon option
	      if ( vm.count("daemon")  )
	      {
	    	operation=START_DAEMON;
	      }
	      // -k option
	      if (vm.count("kill")){
	    	operation=KILL_DAEMON;
	      }


	      po::notify(vm); // retrieve the input values, and throw if any of them is missing
	    }
	    catch(boost::program_options::required_option& e)
	    {

	      std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
	      print_usage();

	      return ERROR_IN_COMMAND_LINE;
	    }
	    catch(boost::program_options::error& e)
	    {
	      std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
	      print_usage();

	      return ERROR_IN_COMMAND_LINE;
	    }

	    // server, user name and password are going to be checked later on



	    switch(operation)
	    {
	      case START_SERVICE : start_service(server_ip.c_str(), user.c_str(), password.c_str()); break;
	      case START_DAEMON  : return start_daemon(server_ip.c_str(), user.c_str(), password.c_str());
	      case KILL_DAEMON  :  return kill_daemon(serviceId);
	    }

	  }
	  catch(std::exception& e)
	  {
	    std::cerr << "Unhandled Exception reached the top of main: "
	              << e.what() << ", application will now exit" << std::endl;
	    return ERROR_UNHANDLED_EXCEPTION;

	  }

	  return SUCCESS;

}
