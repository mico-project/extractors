[TOC]

# TE-204 FDR Face detection and recognition 

# Copyright notice

All rights reserved by Fraunhofer IDMT. This software represents background know-how / technologies owned by Fraunhofer IDMT. As outlined in the Consortium Agreement, Fraunhofer IDMT grants all MICO consortium partners a limited, non-exclusive, non-transferable, non-assignable, non-sublicensable, royalty-free license to use the software for the sole purpose of conducting R&D work within MICO including demonstration and evaluation purposes until Oct 31, 2016, provided that (i) this software remains under strict control of the respective consortium partner and (ii) the software and all related material is removed completely from the third parties' premises after use. Usages beyond the described scope, including commercial use and exploitation, are not allowed, and require separate license agreements.

For the sake of simplification, a respective agreement is provided in addition to this disclaimer, listing all MICO-relevant background work from Fraunhofer IDMT.

#Overview 
This is a mock-up extractor that delivers pre-processed results of face detection. It matches the received video file name against a pre-processed xml and delivers it. Resource directory must be configured during build time (USE_DIRECTORY). Video can be found at http://goo.gl/KL02Tw.

**IMPORTANT**: The example results in the etc folder do not comply 100% with the xsd (coordinates are not normed!) yet. 

# Dependencies

external dependencies:

* boost library (system, log, log_setup, program_options, filesystem, regex)
* MICO Platform API 
* protobuf (transitive dependency through mico_event)

located in 3rd_party folder:

* mico_bemvisual (Proprietary A/V Q component Fraunhofer)
* ipsavanalysiscommonmodules (Proprietary A/V Q component Fraunhofer)
* ipstemporalvideosegmentationmodules (Proprietary A/V Q component Fraunhofer)
* ipsvisualmodules (Proprietary component Fraunhofer)
* xml_1_0 (Proprietary xml component Fraunhofer)

# Resources

The extractor in its current version serves as a mock up. It deliveres pre-processed XMLs with detection results. by matching the file name of the source.

# Build

    cmake -DMICO_PLATFORM_API_HOME=[path to MICO platform API] -DCMAKE_PREFIX_PATH=[path to local installs such as protobuf] -DUSE_DIRECTORY=[path to pre proc. XMLs] [path to extractor source]
    
If you want to build the extractor server as daemon you must add:

    -DDAEMON=1 
    
to the cmake call. If not specified, the extractor will be build as executable running in an endless loop. You must then stop it by pressing Ctrl+C.
            
   
# Installation

tbd.
