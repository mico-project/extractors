#include "FaceDetectionService.h"
#include <Daemon.hpp>

/****** globals ******/
EventManager* mgr = 0;
FaceDetectionService* faceDetectService = 0;
bool loop = true;

void signal_handler(int signum) {
  std::cout << "shutting down FaceDetection service ... " << std::endl;

  if (mgr)
    mgr->unregisterService(faceDetectService);

  if (faceDetectService)
    delete faceDetectService;

  if (mgr)
    delete mgr;

  loop = false;
}

/****** main ******/
int main(int argc, char **argv) {

    if(argc != 4) {
        std::cerr << "Usage: "<< argv[0] << " SERVER_IP USER PASSWORD" << std::endl;
        exit(1);
    }

    const char* server_name = argv[1];
    const char* mico_user   = argv[2];
    const char* mico_pass   = argv[3];

#if defined EXTRACTOR_BUILD_AS_DAEMON
    if(!strcmp(argv[1], "-k")) {
        return mico::daemon::stop(argv[0]);
    } else {
        // create a new instance of a MICO daemon, auto-registering two instances of the FaceDetection analysis service
        return mico::daemon::start(argv[0], server_name, mico_user, mico_pass, {new FaceDetectionService()});

    }
#else
    mgr = new EventManager(server_name, mico_user, mico_pass);

    faceDetectService = new FaceDetectionService();

    mgr->registerService(faceDetectService);

    signal(SIGINT,  &signal_handler);
    signal(SIGTERM, &signal_handler);
    signal(SIGHUP,  &signal_handler);

    while(loop) {
      sleep(1);
    }
#endif
}
