#include <ctime>
#include <fstream>
#include <boost/filesystem.hpp>

#include "FaceDetectionService.h"

// define dublin core vocabulary shortcut
namespace DC = mico::rdf::vocabularies::DC;

// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2011-10-08T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}

FaceDetectionService::FaceDetectionService() 
  : AnalysisService("http://www.mico-project.org/services/facedetection", "video/mp4", "text/vnd.fhg-faces+xml", "facedetection-queue")
{
  m_filedir = std::string ( USE_DIRECTORY );
  m_filedir += "/";
  std::cout << "directory path for xml's is: " << m_filedir << std::endl;
};

FaceDetectionService::~FaceDetectionService() {}

void FaceDetectionService::call(std::function<void(const ContentItem& ci, const URI& object)> resp, ContentItem& ci, URI& object)
{
  // retrieve the content part identified by the object URI
  Content* imgPart = ci.getContentPart(object);
   
  if(imgPart != NULL) {
    std::string filename = imgPart->getProperty(DC::source);
    boost::filesystem::path my_path( filename );
    filename = my_path.filename().string();

    // write xml text to a new content part
    Content *txtPartXML  = ci.createContentPart();

    txtPartXML->setType("text/vnd.fhg-faces+xml");
    
    // set some metadata properties (provenance information etc)
    txtPartXML->setRelation(DC::creator, getServiceID());
    txtPartXML->setRelation(DC::provenance, getServiceID());
    txtPartXML->setProperty(DC::created, getTimestamp());
    txtPartXML->setRelation(DC::source, object.stringValue());
    
    std::ostream* out = txtPartXML->getOutputStream();

    std::cout << "send output to stream" << std::endl;


    std::string file = m_filedir + filename + ".xml";
    std::cout << "complete path is: " << file << std::endl;
    
    std::string line;
    std::ifstream fs ( file, std::ios::binary | std::ios::in );
    if (fs.is_open()) {
      std::vector<char> buffer = std::vector<char>(std::istreambuf_iterator<char>(fs), std::istreambuf_iterator<char>());
      out->write(buffer.data(), buffer.size());
      std::cout << "closing fs, buffer size was: "<< buffer.size() << std::endl;
      fs.close();
    } else { std::cout << "file not opened!" << std::endl; }

    delete out;

    // notify broker that we created a new content part by calling the callback function passed as argument
    resp(ci, txtPartXML->getURI());
    
    // clean up
    delete imgPart;
    delete txtPartXML;
   } else {
    std::cerr << "content item part " << object.stringValue() << " of content item " << ci.getURI().stringValue() << " does not exist!\n";
  }
};
