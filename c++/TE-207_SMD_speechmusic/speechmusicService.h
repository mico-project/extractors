#include "ISemanticSpeechMusicClassificationInterface.h"

#include "EventManager.hpp"
#include "outputHandler.h"

// for constant RDF property definitions of common vocabularies
#include "vocabularies.hpp"


// this namespace contains EventManager and AnalysisService
using namespace mico::event;

// this namespace contains Content, ContentItem, etc
using namespace mico::persistence;

// this namespace contains the RDF data model
using namespace mico::rdf::model;

class SpeechmusicService : public AnalysisService {

public:

  SpeechmusicService();

  ~SpeechmusicService();

  void call(std::function<void(const ContentItem& ci, const URI& object)> resp, ContentItem& ci, URI& object);
  vaoutput::qualityType analyzeFile(std::string sAudioFile);

private:
  avanalysis::EAVStatus m_status;
  semanticspeechmusic::ISemanticSpeechMusicClassificationInterface* m_semanticspeechmusicInterface;
  
    
};
