#include <ctime>
#include <fstream>

#include "speechmusicService.h"
#include "outputHandler.h"
#include "AudioFramer.h"

// define dublin core vocabulary shortcut
namespace DC = mico::rdf::vocabularies::DC;

// helper function to get time stamp
std::string getTimestamp() {
    time_t now;
    time(&now);
    char buf[sizeof "2011-10-08T07:07:09Z"];
    strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));
    return std::string(buf);
}

#ifdef EXTRACTOR_BUILD_AS_DAEMON
void copyFile(std::string name, std::string fromPath) {
    std::ifstream source(fromPath + "/" + name, std::ios::binary);
    std::ofstream dest(name, std::ios::binary);

    std::istreambuf_iterator<char> begin_source(source);
    std::istreambuf_iterator<char> end_source;
    std::ostreambuf_iterator<char> begin_dest(dest);
    std::copy(begin_source, end_source, begin_dest);

    source.close();
    dest.close();
}
#endif

SpeechmusicService::SpeechmusicService()
        : AnalysisService("http://www.mico-project.org/services/speechmusic", "video/mp4", "text/vnd.fhg-spechmusic+xml", "speechmusic-queue"),
          m_status(avanalysis::AV_OK)
{
#ifdef EXTRACTOR_BUILD_AS_DAEMON
    // move over kefir files to current working directory
    copyFile("kefir.silence.classifier.xcf", "/usr/share/mico-data-3rdparty");
    copyFile("kefir.speechmusic.classifier.xcf", "/usr/share/mico-data-3rdparty");

#endif

    m_semanticspeechmusicInterface = semanticspeechmusic::ISemanticSpeechMusicClassificationInterface::getInstance ("4HpuH8A7kCJKBfyfmpr6vcpH");

    if(!m_semanticspeechmusicInterface) {
        std::cerr << "Could not get Speechmusic instance!" << std::endl;
        throw std::string("Could not get Speechmusic instance!");
    } else {
        char *libInfo = new char[128];
        m_semanticspeechmusicInterface->getLibraryId( libInfo );
        std::cout << libInfo << std::endl;
        delete []libInfo;
    }
};

SpeechmusicService::~SpeechmusicService() {
    delete m_semanticspeechmusicInterface;
}

void storeStream(std::istream* in, std::string tmpName){
    FILE* pFile;
    pFile = fopen(tmpName.c_str(), "wb");
    char * buffer = new char [1024];
    std::streamsize all = 0;
    in->read(buffer, 1024);
    std::streamsize n = in->gcount();
    while (n != 0) {
        all +=n;
        fwrite(buffer, sizeof(char), n, pFile);
        if (in) { in->read(buffer, 1024); n = in->gcount();}
    }
    delete buffer;
    fclose(pFile);
}

vaoutput::qualityType SpeechmusicService::analyzeFile(std::string sAudioFile){
    //START Speechmusic stuff:
    // activate all features
    std::vector<avanalysis::AVAnalysisFeatureType> allFeatures = semanticspeechmusic::events::all();
    for (std::vector<avanalysis::AVAnalysisFeatureType>::const_iterator it = allFeatures.begin (); it != allFeatures.end (); ++it)
    {
        m_status = m_semanticspeechmusicInterface->setFeatureEnabled (*it, true);
        if (m_status == avanalysis::AV_ERROR) {
            std::cerr << m_semanticspeechmusicInterface->getLastErrorMessage () << std::endl;
        }
    }

    // set audio window length to appropriate time
    int iMsPerProcess = 3000;
    m_status = m_semanticspeechmusicInterface->setParameter (avanalysis::properties::AUDIO_PROCESS_MS, iMsPerProcess);
    if (m_status == avanalysis::AV_ERROR)
    {
        std::cerr << m_semanticspeechmusicInterface->getLastErrorMessage () << std::endl;
        exit (1);
    }

    // initialize interface
    m_status = m_semanticspeechmusicInterface->init ();
    if (m_status == avanalysis::AV_ERROR)
    {
        std::cerr << m_semanticspeechmusicInterface->getLastErrorMessage () << std::endl;
        exit (1);
    }

    float winLenSec= 3.0f;  //iMSPerProcess / 1000.0f;
    float hopSizeSec=winLenSec;

    AudioFramer af(winLenSec,hopSizeSec);
    af.readFile(sAudioFile);

    SAudioBuffer buffer;
    unsigned long lFrameId = 0;

    //result map for saving
    vaoutput::qualityType qresults;

    do
    {
        // load and process next frame
        buffer=af.getFrameNumber(lFrameId);

        if ( buffer.iSize ){
            m_status = m_semanticspeechmusicInterface->processAudioSamples ( buffer.p_sSamples, buffer.iSampleRate,
                    buffer.iChannels,
                    buffer.iSize,
                    buffer.iFrameId);
        }

        if (m_status == avanalysis::AV_ERROR)
        {
            std::cerr << m_semanticspeechmusicInterface->getLastErrorMessage () << std::endl;
        }
        else
        {
            // print out all results
            float result;

            for (std::vector<avanalysis::AVAnalysisFeatureType>::const_iterator it =
                    allFeatures.begin (); it != allFeatures.end (); ++it)
            {
                m_status = m_semanticspeechmusicInterface->getResult (*it, lFrameId, result);
                if (m_status == avanalysis::AV_ERROR)
                {
                    std::cerr << m_semanticspeechmusicInterface->getLastErrorMessage () << std::endl;
                }
                else
                {
                    qresults[(*it)][lFrameId] = result;
                }
            }
        }
        ++lFrameId;
        delete[] buffer.p_sSamples;
    }while(buffer.bIsComplete);




    return qresults;

}

void SpeechmusicService::call(std::function<void(const ContentItem& ci, const URI& object)> resp, ContentItem& ci, URI& object)
{
    // retrieve the content part identified by the object URI
    Content* imgPart = ci.getContentPart(object);

    if(imgPart != NULL) {
        std::cout << "store file locally ... " << std::endl;
        std::istream* in = imgPart->getInputStream();
        std::string sAudioFile("./currentSmdFile.mp4");
        storeStream(in, sAudioFile);
        std::cout << "finished" << std::endl;

        delete in;

        // write json or xml text to a new content part
        Content *resultPart = ci.createContentPart();
        resultPart->setType("text/vnd.fhg-spechmusic+xml"); //"text/xml");

        // set some metadata properties (provenance information etc)
        resultPart->setRelation(DC::creator, getServiceID());
        resultPart->setRelation(DC::provenance, getServiceID());
        resultPart->setProperty(DC::created, getTimestamp());
        resultPart->setRelation(DC::source, object.stringValue());

        std::ostream* out = resultPart->getOutputStream();

        std::cout << "start processing" << std::endl;
        vaoutput::qualityType qresults = analyzeFile(sAudioFile);
        std::cout << "prepare response" << std::endl;
        vaoutput::globalsType globals;
        vaoutput::moduleConfigType parameters;
        vaoutput::outputHandler oH("speechmusic", qresults, globals, parameters);
        oH.toXmlStream( *out );
        delete out;

        // notify broker that we created a new content part by calling the callback function passed as argument
        resp(ci, resultPart->getURI());

        // clean up
        std::cout << "cleaning up ..." << std::endl;
        delete imgPart;
        delete resultPart;
        if (remove( sAudioFile.c_str() )==0){
            std::cout << "tmpFile removed" << std::endl;
        }else{
            std::cout << "could not remove tmpFile" << std::endl;
        }

        std::cout << "process finished" << std::endl << std::endl;


    } else {
        std::cerr << "ERROR: ";
        std::cerr << "content item part " << object.stringValue() << " of content item " << ci.getURI().stringValue() << " does not exist!\n";
    }
};
