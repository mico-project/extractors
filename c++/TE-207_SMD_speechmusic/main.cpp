// for configuring logging levels
#include "logging.h"

#include "speechmusicService.h"
#include <Daemon.hpp>


/****** globals ******/
#ifndef EXTRACTOR_BUILD_AS_DAEMON
EventManager* mgr;
SpeechmusicService* speechmusicAnalyser;
bool loop = true;

void signal_handler(int signum) {
    std::cout << "shutting down speechmusic service ... " << std::endl;

    mgr->unregisterService(speechmusicAnalyser);

    delete speechmusicAnalyser;
    delete mgr;

    loop = false;
}
#endif

/****** main ******/
int main(int argc, char **argv) {
#ifndef EXTRACTOR_BUILD_AS_DAEMON
    if (argc == 2){
        if (strcmp(argv[1], "test")==0){
            std::cout << "run test" << std::endl;
            speechmusicAnalyser = new SpeechmusicService();
            std::string sAudioFile ("/var/tmp/smd_test.mp4");
            vaoutput::qualityType qresults = speechmusicAnalyser->analyzeFile(sAudioFile);
            std::cout << "prepare response" << std::endl;
            vaoutput::globalsType globals;
            vaoutput::moduleConfigType parameters;
            vaoutput::outputHandler oH("speechmusic", qresults, globals, parameters);
            oH.toXmlStream( std::cout );

            return 0;
        }else
        {std::cout << "no test : " << argv[1] << std::endl;
        }
    }
#endif

    if(argc != 4) {
        std::cerr << "Usage: "<< argv[0] << " SERVER_IP [USER PASSWORD]" << std::endl;
        exit(1);
    }

    const char* server_name = argv[1];
    const char* mico_user   = argv[2];
    const char* mico_pass   = argv[3];

#ifdef EXTRACTOR_BUILD_AS_DAEMON
    if(!strcmp(argv[1], "-k")) {
        return mico::daemon::stop(argv[0]);
    } else {
        // create a new instance of a MICO daemon, auto-registering two instances of the OCR analysis service
        return mico::daemon::start(argv[0], server_name, mico_user, mico_pass, {new SpeechmusicService()});

    }
#else

    boost::log::core::get()->set_filter
            (
                    boost::log::trivial::severity >= boost::log::trivial::info
            );

    mgr = new EventManager(server_name, mico_user, mico_pass);

    speechmusicAnalyser = new SpeechmusicService();

    mgr->registerService(speechmusicAnalyser);

    signal(SIGINT,  &signal_handler);
    signal(SIGTERM, &signal_handler);
    signal(SIGHUP,  &signal_handler);

    while(loop) {
        sleep(1);
    }
#endif
}
