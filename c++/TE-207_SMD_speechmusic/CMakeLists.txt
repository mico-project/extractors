cmake_minimum_required(VERSION 2.8.12)

project("TE-207_SMD_mico_speechmusic_service")
option(DAEMON "DAEMON" OFF)

set(TARGET_NAME mico_speechmusic_service)
set(TARGET_VERSION 1.0)

if (UNIX)
  add_definitions(-std=c++11)
endif()

if(DAEMON)
  message(STATUS "DAEMON defined - extractor will be build als daemon")
  add_definitions(-DEXTRACTOR_BUILD_AS_DAEMON)
else()
  message(STATUS "DAEMON not defined - extractor will be build als executable")
endif()

get_filename_component(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../modules/ ABSOLUTE)
get_filename_component(3RD_PARTY_HOME ${CMAKE_CURRENT_SOURCE_DIR}/../3rd_party/ ABSOLUTE)
find_path(BEM_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../3rd_party/ )

find_package( Boost REQUIRED COMPONENTS system log log_setup )
find_package( MICOPlatformAPI 1.2.0 REQUIRED)
find_package( FFMpeg REQUIRED )
find_package( Protobuf REQUIRED )

include_directories(
  ${Boost_INCLUDE_DIRS}
  ${MICOPlatformAPI_INCLUDE_DIRS}
  ${3RD_PARTY_HOME}/include/speechmusic
  ${3RD_PARTY_HOME}/include/AudioFraming
  ${3RD_PARTY_HOME}/include/xml
  ${3RD_PARTY_HOME}/include/io
)

link_directories(
  ${3RD_PARTY_HOME}/lib
)

add_executable( ${TARGET_NAME}
  main.cpp
  speechmusicService.h
  speechmusicService.cpp
)

target_link_libraries(${TARGET_NAME}
  mico_speechmusic
  xml_1_0
  AF_1_0
  ${MICOPlatformAPI_LIBRARIES}
  ${PROTOBUF_LIBRARIES} #this is somehow a workaround->needed by libmico_event.so
  ${Boost_LIBRARIES}
  ${FFMPEG_LIBRARIES}
)

install(TARGETS ${TARGET_NAME} RUNTIME DESTINATION bin)
