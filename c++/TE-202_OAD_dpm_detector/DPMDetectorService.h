#include "EventManager.hpp"

#include "opencv2/opencv.hpp"

// for constant RDF property definitions of common vocabularies
#include "vocabularies.hpp"

#include "DPMDetector.h"

// this namespace contains EventManager and AnalysisService
using namespace mico::event;

// this namespace contains Content, ContentItem, etc
using namespace mico::persistence::model;

namespace objectreco {
  class ObjectRecognitionResults;
}

class DPMDetector;

class DPMDetectorService : public AnalysisService {

public:

  /**
   * @brief DPMDetectorService::DPMDetectorService
   * @param mimeType The accepted input mime type (image/jpeg, image/png)
   * @param detectionModel
   *
   */
  DPMDetectorService(std::string mimeType, std::map<std::string, DPMmodel> detectionModels);

  ~DPMDetectorService();

  void call(mico::event::AnalysisResponse& resp,
            std::shared_ptr< mico::persistence::model::Item > item,
            std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources,
            std::map<std::string,std::string>& params);

private:
  void mergeDifferentAnimalDetections(const std::vector<cv::Rect>& allRegionsFound,
                                      objectreco::ObjectRecognitionResults& res,
                                      double overlapThreshold);

  boost::scoped_ptr<DPMDetector> m_pDPM;
  std::map<std::string, DPMmodel> m_detectionModels;
};
