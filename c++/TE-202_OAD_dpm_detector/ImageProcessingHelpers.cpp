#include "ImageProcessingHelpers.h"

// unit vectors used to compute gradient orientation
// no idea where this comes from....see MATLAB_REPOSITORY/Projects/MICO/voc-release3-win/features.cc
const double uu[9] = {1.0000, 
  0.9397, 
  0.7660, 
  0.500, 
  0.1736, 
  -0.1736, 
  -0.5000, 
  -0.7660, 
  -0.9397};

const double vv[9] = {0.0000, 
  0.3420, 
  0.6428, 
  0.8660, 
  0.9848, 
  0.9848, 
  0.8660, 
  0.6428, 
  0.3420};

const uint numOrientations = 18u;

template<typename Type>
void ImageProcessingHelpers::find(const cv::Mat& input, const Type& thresh, std::vector<int>* x, std::vector<int>* y){
  
  for (int idxX = 0; idxX < input.cols; idxX++){
    for (int idxY = 0; idxY < input.rows; idxY++){
      if (input.at<Type>(idxY,idxX) > thresh){
        x->push_back( static_cast<uint>(idxX) );
        y->push_back( static_cast<uint>(idxY) );
      }
    }
  }

}

cv::Mat ImageProcessingHelpers::getDownsampledSubMatrix(const cv::Mat& input, const int& startRow, const int& startCol, const int& endRow, const int& endCol, const int& step){

  cv::Size outputSize;
  outputSize.width = static_cast<int>( std::ceil( (endCol - startCol + 1)/step ) );
  outputSize.height = static_cast<int>( std::ceil( (endRow - startRow + 1)/step ) );

  cv::Mat output(outputSize.height, outputSize.width, input.type());

  for (int idxRow = 0; idxRow < outputSize.height; idxRow++){
    for (int idxCol = 0; idxCol < outputSize.width; idxCol++){
      int idxRowInput = startRow+idxRow*step;
      int idxColInput = startCol+idxCol*step;
            
      output.at<double>(idxRow, idxCol) = input.at<double>(idxRowInput,idxColInput);
    }
  }

  return output;
}

void ImageProcessingHelpers::dt_helper(double *src, double *dst, double *ptr, int step, int s1, int s2, int d1, int d2, double a, double b){
  if (d2 >= d1) {
    int d = (d1+d2) >> 1;
    int s = s1;
    for (int p = s1+1; p <= s2; p++)
      if (src[s*step] + a*std::pow(d-s,2) + b*(d-s) > 
        src[p*step] + a*std::pow(d-p,2) + b*(d-p))
        s = p;
    dst[d*step] = src[s*step] + a*std::pow(d-s,2) + b*(d-s);
    ptr[d*step] = s;
    dt_helper(src, dst, ptr, step, s1, s, d1, d-1, a, b);
    dt_helper(src, dst, ptr, step, s, s2, d+1, d2, a, b);
  }
}

void ImageProcessingHelpers::dt1d(double *src, double *dst, double *ptr, int step, int n, double a, double b) {
  dt_helper(src, dst, ptr, step, 0, n-1, 0, n-1, a, b);
}

template<typename Type>
void ImageProcessingHelpers::generalizedDistanceTransform(const cv::Mat& input, const std::vector<Type>& defs, cv::Mat& M, cv::Mat& Ix, cv::Mat& Iy){
  // create output variable
  M.create(input.rows, input.cols, CV_64FC1); M.setTo(cv::Scalar::all(0));
  Ix.create(input.rows, input.cols, CV_64FC1); Ix.setTo(cv::Scalar::all(0));
  Iy.create(input.rows, input.cols, CV_64FC1); Iy.setTo(cv::Scalar::all(0));

  cv::Mat tmpM(input.rows, input.cols, CV_64FC1); tmpM.setTo(cv::Scalar::all(0));
  cv::Mat tmpIx(input.rows, input.cols, CV_64FC1); tmpIx.setTo(cv::Scalar::all(0));
  cv::Mat tmpIy(input.rows, input.cols, CV_64FC1); tmpIy.setTo(cv::Scalar::all(0));

  /********** Convert images to Arrays ****************/
  std::vector<double> inputVec = ImageProcessingHelpers::convertMatToArray<double>(input);
  std::vector<double> tmpMVec = ImageProcessingHelpers::convertMatToArray<double>(tmpM);
  std::vector<double> tmpIyVec = ImageProcessingHelpers::convertMatToArray<double>(tmpIy);
  std::vector<double> tmpIxVec = ImageProcessingHelpers::convertMatToArray<double>(tmpIx);
  std::vector<double> MVec = ImageProcessingHelpers::convertMatToArray<double>(M);

  double* inputArray = &inputVec[0];
  double* tmpMArray = &tmpMVec[0];
  double* tmpIyArray = &tmpIyVec[0];
  double* tmpIxArray = &tmpIxVec[0];
  double* MArray = &MVec[0];
  /********** Convert images to Arrays ****************/

  double ax = static_cast<double>( defs.at(0) );
  double bx = static_cast<double>( defs.at(1) );
  double ay = static_cast<double>( defs.at(2) );
  double by = static_cast<double>( defs.at(3) );
  
  for (int x = 0; x < input.cols; x++)
    ImageProcessingHelpers::dt1d(inputArray+x*input.rows, tmpMArray+x*input.rows, tmpIyArray+x*input.rows, 1, input.rows, ay, by );

  for (int y = 0; y < input.rows; y++)
    ImageProcessingHelpers::dt1d(tmpMArray+y, MArray+y, tmpIxArray+y, input.rows, input.cols, ax, bx);

  // get argmins
  for (int x = 0; x < input.cols; x++) {
    for (int y = 0; y < input.rows; y++) {
      int p = x*input.rows+y;
      tmpIxArray[p] = tmpIxArray[p];
      tmpIyArray[p] = tmpIyArray[static_cast<int>(tmpIxArray[p])*input.rows+y];
    }
  }

  /********** Convert arrays back to images ****************/
  ImageProcessingHelpers::convertArrayToMat(MVec, M);
  ImageProcessingHelpers::convertArrayToMat(tmpIxVec, Ix);
  ImageProcessingHelpers::convertArrayToMat(tmpIyVec, Iy);
  /********** Convert arrays back to images ****************/

}

template<typename Type>
void ImageProcessingHelpers::convertArrayToMat(const std::vector<Type>& vec, cv::Mat& m){
  
  for (int x = 0; x < m.cols; x++){
    for (int y = 0; y < m.rows; y++){
      int p = x*m.rows+y;
      m.at<Type>(y,x) = vec.at(p);
    }
  }
}


template<typename Type>
std::vector<Type> ImageProcessingHelpers::convertMatToArray(const cv::Mat& mat){
  std::vector<Type> arrayVec;

  for (int x = 0; x < mat.cols; x++){
    for (int y = 0; y < mat.rows; y++){
      arrayVec.push_back( mat.at<Type>(y,x) );
    }
  }

  return arrayVec;
}

std::vector<uint> ImageProcessingHelpers::getDimensionsOfOpenCVMat(const cv::Mat& Array3D)
{
  std::vector<uint> sizes(3, 0u);
  for (int i=0; i < Array3D.dims; i++)
    sizes.at(i) = Array3D.size[i];

  return sizes;
}

void ImageProcessingHelpers::getSliceFromOpenCVMat(const cv::Mat& Array3D, const int z, cv::Mat* slice)
{
  // create the roi 
  cv::Range ranges[3];
  ranges[0] = cv::Range::all();
  ranges[1] = cv::Range::all();
  ranges[2] = cv::Range(z, z+1);

  // get the roi from the image; 
  // calling clone() makes sure the data is continuous 
  Array3D(ranges).copyTo(*slice);

  // create a temporarily 2d image and copy its size 
  // to make our image a real 2d image 
  cv::Mat temp2d; 
  temp2d.create( 2, &(Array3D.size[0]), Array3D.type() ); 
  slice->copySize( temp2d );
}

template<typename Type>
void ImageProcessingHelpers::featpyramid(const cv::Mat& image, const DPMmodel& model, std::vector< cv::Mat >* pyramid, std::vector<Type>* scales)
{
  /*see MATLAB_REPOSITORY/Projects/MICO/voc-release3-win/featpyramid.m

  sbin is the size of a HOG cell - it should be even.
  interval is the number of scales in an octave of the pyramid.
  feat{i} is the i-th level of the feature pyramid.
  scale{i} is the scaling factor used for the i-th level.
  feat{i+interval} is computed at exactly half the resolution of feat{i}.
  first octave halucinates higher resolution data.*/
  pyramid->clear();
  scales->clear();

  double sc = std::pow(2,(1.0/static_cast<double>(model.interval)));
  cv::Size imsize = image.size();
  uint max_scale = static_cast<uint>(1 + std::floor(std::log(std::min(imsize.width,imsize.height)/(5*model.sbin))/std::log(sc)));
  pyramid->resize(max_scale+model.interval);
  scales->resize(max_scale+model.interval);

  cv::Mat scaled;
  cv::Mat scaled2;
  cv::Mat feature;
  for(uint idx = 0; idx < model.interval; idx++){
    // scale image
    scaled.release();
    cv::resize(image, scaled, cvSize(0,0), 1/(std::pow(sc,idx)), 1/(std::pow(sc,idx)), cv::INTER_CUBIC );

    // do feature extraction: "first" 2xinterval
    feature.release();
    ImageProcessingHelpers::HOGExtraction(scaled, static_cast<uint>(model.sbin/2), &feature);

    feature.copyTo( pyramid->at(idx) );
    scales->at(idx) = static_cast<Type>( 2/( std::pow(sc,idx) ) );

    // do feature extraction: "second" 1xinterval
    feature.release();
    ImageProcessingHelpers::HOGExtraction(scaled, model.sbin, &feature);
    feature.copyTo( pyramid->at(idx+model.interval) );
    scales->at(idx+model.interval) = static_cast<Type>( 1/( std::pow(sc,idx) ) );

    //remaining intervals
    for(uint idx2 = idx + model.interval; idx2 < max_scale; idx2+=model.interval){
      scaled2.release();
      feature.release();
      cv::resize(scaled, scaled2, cvSize(0,0), 0.5, 0.5, cv::INTER_CUBIC );
      ImageProcessingHelpers::HOGExtraction(scaled2, model.sbin, &feature);
      feature.copyTo( pyramid->at(idx2+model.interval) );
      scales->at(idx2+model.interval) = static_cast<Type>( 0.5 * scales->at(idx2) );
      scaled.release();
      scaled2.copyTo(scaled);
    }
  }
}

void ImageProcessingHelpers::HOGExtraction(const cv::Mat& scaledImg, const uint& sbin, cv::Mat* feature)
{
  /*see MATLAB_REPOSITORY/Projects/MICO/voc-release3-win/features.cc

  Takes a double color image and a bin size and returns HOG features*/

  // we need color images
  if (scaledImg.channels() != 3)
    std::cerr << "ImageProcessingHelpers::HOGExtraction: Image should be colored!" << std::endl;

  cv::Mat image;
  if (scaledImg.type() != CV_64FC3)
    scaledImg.convertTo(image, CV_64FC3);
  else
    scaledImg.copyTo(image);

  cv::Size dim = scaledImg.size();

  uint dims[2];
  dims[0] = dim.height;
  dims[1] = dim.width;

  // memory for caching orientation histograms & their norms
  uint blocks[2];
  blocks[0] = static_cast<int>( round(static_cast<double>(dims[0])/static_cast<double>(sbin)) );
  blocks[1] = static_cast<int>( round(static_cast<double>(dims[1])/static_cast<double>(sbin)) );

  std::vector<double> hist(blocks[0]*blocks[1]*numOrientations, 0.0);
  std::vector<double> norm(blocks[0]*blocks[1], 0.0);

  uint visible[2];
  visible[0] = blocks[0]*sbin;
  visible[1] = blocks[1]*sbin;

  // split cv::Mat into separate channels
  std::vector<cv::Mat> channels(3);
  cv::split(image,channels);

  for (uint x = 1; x < visible[1]-1; x++) {
    for (uint y = 1; y < visible[0]-1; y++) {
      // first color channel
      cv::Mat channel1 = channels.at(0);
      double UpperPixel = channel1.at<double>(std::min(y, dims[0]-2)-1, std::min(x, dims[1]-2));
      double LowerPixel = channel1.at<double>(std::min(y, dims[0]-2)+1, std::min(x, dims[1]-2));
      double dy = LowerPixel - UpperPixel;

      double LeftPixel = channel1.at<double>(std::min(y, dims[0]-2), std::min(x, dims[1]-2)-1);
      double RightPixel = channel1.at<double>(std::min(y, dims[0]-2), std::min(x, dims[1]-2)+1);
      double dx = RightPixel - LeftPixel;

      double v = dx*dx + dy*dy;

      // second color channel
      cv::Mat channel2 = channels.at(1);
      UpperPixel = channel2.at<double>(std::min(y, dims[0]-2)-1, std::min(x, dims[1]-2));
      LowerPixel = channel2.at<double>(std::min(y, dims[0]-2)+1, std::min(x, dims[1]-2));
      double dy2 = LowerPixel - UpperPixel;

      LeftPixel = channel2.at<double>(std::min(y, dims[0]-2), std::min(x,dims[1]-2)-1);
      RightPixel = channel2.at<double>(std::min(y, dims[0]-2), std::min(x, dims[1]-2)+1);
      double dx2 = RightPixel - LeftPixel;

      double v2 = dx2*dx2 + dy2*dy2;

      // third color channel
      cv::Mat channel3 = channels.at(2);
      UpperPixel = channel3.at<double>(std::min(y, dims[0]-2)-1, std::min(x, dims[1]-2));
      LowerPixel = channel3.at<double>(std::min(y, dims[0]-2)+1, std::min(x, dims[1]-2));
      double dy3 = LowerPixel - UpperPixel;

      LeftPixel = channel3.at<double>(std::min(y, dims[0]-2), std::min(x, dims[1]-2)-1);
      RightPixel = channel3.at<double>(std::min(y, dims[0]-2), std::min(x, dims[1]-2)+1);
      double dx3 = RightPixel - LeftPixel;

      double v3 = dx3*dx3 + dy3*dy3;

      // pick channel with strongest gradient
      if (v2 > v) {
        v = v2;
        dx = dx2;
        dy = dy2;
      } 
      if (v3 > v) {
        v = v3;
        dx = dx3;
        dy = dy3;
      }

      // snap to one of 18 orientations
      double best_dot = 0;
      int best_o = 0;

      for (uint o = 0; o < numOrientations/2; o++) {
        double dot = uu[o]*dx + vv[o]*dy;
        if (dot > best_dot) {
          best_dot = dot;
          best_o = o;
        } 
        else if (-dot > best_dot) {
          best_dot = -dot;
          best_o = o+9;
        }
      }

      // add to 4 histograms around pixel using linear interpolation
      double xp = (static_cast<double>(x)+0.5)/static_cast<double>(sbin) - 0.5;
      double yp = (static_cast<double>(y)+0.5)/static_cast<double>(sbin) - 0.5;

      int ixp = static_cast<int>( floor(xp) );
      int iyp = static_cast<int>( floor(yp) );

      double vx0 = xp-static_cast<double>(ixp);
      double vy0 = yp-static_cast<double>(iyp);
      double vx1 = 1.0-vx0;
      double vy1 = 1.0-vy0;
      v = sqrt(v);

      if ( ixp >= 0 && iyp >= 0 )
        hist.at( ixp*blocks[0] + iyp + best_o*blocks[0]*blocks[1] ) += vx1*vy1*v;

      if ( ixp+1 < static_cast<int>(blocks[1]) && iyp >= 0 )
        hist.at( (ixp+1)*blocks[0] + iyp + best_o*blocks[0]*blocks[1] ) += vx0*vy1*v;

      if ( ixp >= 0 && iyp+1 < static_cast<int>(blocks[0]) )
        hist.at( ixp*blocks[0] + (iyp+1) + best_o*blocks[0]*blocks[1] ) +=  vx1*vy0*v;

      if ( ixp+1 < static_cast<int>(blocks[1]) && iyp+1 < static_cast<int>(blocks[0]) )
        hist.at( (ixp+1)*blocks[0] + (iyp+1) + best_o*blocks[0]*blocks[1] ) += vx0*vy0*v;       

    }
  }

  // compute energy in each block by summing over orientations
  for (uint o = 0; o < numOrientations/2; o++) {
    uint src1 = o*blocks[0]*blocks[1]; // position in hist
    uint src2 = (o+numOrientations/2)*blocks[0]*blocks[1]; // position in hist

    uint dst = 0; //position in norm
    uint end = blocks[1]*blocks[0]; // position in norm

    while (dst < end) {
      norm.at(dst++) += (hist.at(src1) + hist.at(src2)) * (hist.at(src1) + hist.at(src2));
      src1++;
      src2++;
    }
  }

  // memory for HOG features
  int out[3];
  out[0] = std::max(blocks[0]-2, 0u);
  out[1] = std::max(blocks[1]-2, 0u);
  out[2] = numOrientations + numOrientations/2 + 4;

  feature->create(3, out, CV_64FC1);
  feature->setTo(cv::Scalar::all(0));

  // compute features
  for (uint x = 0; x < static_cast<uint>(out[1]); x++) {
    for (uint y = 0; y < static_cast<uint>(out[0]); y++) {
      uint p;
      double n1, n2, n3, n4;

      p = (x+1)*blocks[0] + y + 1;
      n1 = 1.0 / sqrt( norm.at(p) + norm.at(p+1) + norm.at(p+blocks[0]) + norm.at(p+blocks[0]+1) + std::numeric_limits<double>::epsilon() );
      p = (x+1)*blocks[0] + y;
      n2 = 1.0 / sqrt( norm.at(p) + norm.at(p+1) + norm.at(p+blocks[0]) + norm.at(p+blocks[0]+1) + std::numeric_limits<double>::epsilon() );
      p = x*blocks[0] + y+1;
      n3 = 1.0 / sqrt( norm.at(p) + norm.at(p+1) + norm.at(p+blocks[0]) + norm.at(p+blocks[0]+1) + std::numeric_limits<double>::epsilon() );
      p = x*blocks[0] + y;      
      n4 = 1.0 / sqrt( norm.at(p) + norm.at(p+1) + norm.at(p+blocks[0]) + norm.at(p+blocks[0]+1) + std::numeric_limits<double>::epsilon() );

      double t1 = 0;
      double t2 = 0;
      double t3 = 0;
      double t4 = 0;

      // contrast-sensitive features
      uint src = (x+1)*blocks[0] + (y+1);
      for (uint o = 0; o < numOrientations; o++) {
        double h1 = std::min(hist.at(src) * n1, 0.2);
        double h2 = std::min(hist.at(src) * n2, 0.2);
        double h3 = std::min(hist.at(src) * n3, 0.2);
        double h4 = std::min(hist.at(src) * n4, 0.2);

        feature->at<double>(y, x, o) = 0.5 * (h1 + h2 + h3 + h4);

        t1 += h1;
        t2 += h2;
        t3 += h3;
        t4 += h4;

        src += blocks[0]*blocks[1];
      }

      // contrast-insensitive features
      src = (x+1)*blocks[0] + (y+1);
      for (uint o = 0; o < 9; o++) {
        double sum = hist.at(src) + hist.at(src + 9*blocks[0]*blocks[1]);
        double h1 = std::min(sum * n1, 0.2);
        double h2 = std::min(sum * n2, 0.2);
        double h3 = std::min(sum * n3, 0.2);
        double h4 = std::min(sum * n4, 0.2);

        feature->at<double>(y, x, o+numOrientations) = 0.5 * (h1 + h2 + h3 + h4);

        src += blocks[0]*blocks[1];
      }

      // texture features
      feature->at<double>(y, x, numOrientations+numOrientations/2) =  0.2357 * t1;
      feature->at<double>(y, x, numOrientations+numOrientations/2 + 1) =  0.2357 * t2;
      feature->at<double>(y, x, numOrientations+numOrientations/2 + 2) =  0.2357 * t3;
      feature->at<double>(y, x, numOrientations+numOrientations/2 + 3) =  0.2357 * t4;

    }
  }
}

template void ImageProcessingHelpers::find<double>(const cv::Mat& input, const double& thresh, std::vector<int>* x, std::vector<int>* y);
template void ImageProcessingHelpers::find<float>(const cv::Mat& input, const float& thresh, std::vector<int>* x, std::vector<int>* y);

template void ImageProcessingHelpers::convertArrayToMat<double>(const std::vector<double>& vec, cv::Mat& m);
template void ImageProcessingHelpers::convertArrayToMat<int>(const std::vector<int>& vec, cv::Mat& m);
template void ImageProcessingHelpers::convertArrayToMat<float>(const std::vector<float>& vec, cv::Mat& m);

template std::vector<double> ImageProcessingHelpers::convertMatToArray<double>(const cv::Mat& mat);
template std::vector<float> ImageProcessingHelpers::convertMatToArray<float>(const cv::Mat& mat);
template std::vector<int> ImageProcessingHelpers::convertMatToArray<int>(const cv::Mat& mat);

template void ImageProcessingHelpers::generalizedDistanceTransform<double>(const cv::Mat& input, const std::vector<double>& defs, cv::Mat& M, cv::Mat& Ix, cv::Mat& Iy);
template void ImageProcessingHelpers::generalizedDistanceTransform<float>(const cv::Mat& input, const std::vector<float>& defs, cv::Mat& M, cv::Mat& Ix, cv::Mat& Iy);

template void ImageProcessingHelpers::featpyramid<double>(const cv::Mat& image, const DPMmodel& model, std::vector< cv::Mat >* pyramid, std::vector<double>* scales);
template void ImageProcessingHelpers::featpyramid<float>(const cv::Mat& image, const DPMmodel& model, std::vector< cv::Mat >* pyramid, std::vector<float>* scales);
