[TOC]

# TE-202 DPM Detector for images

# Dependencies

external dependencies:

* boost library (system, log, log_setup, program_options, filesystem, serialization)
* MICO Platform API 
* protobuf (transitive dependency through mico_event)
* OpenCV
* xml_1_0 (TiXml)


# Build setting and environment

The DPM extractor uses the following additional CMake environment variables during build configuration:

* TE202_RESOURCE_DIR - directory containing the training model files [default: CMAKE_CURRENT_SOURCE_DIR/model]

A typical build call could look like:

    cmake -DMICOPlatformAPI_HOME=[path to MICO platform API] -DCMAKE_PREFIX_PATH=[semicolon seperated list of paths to local installs such as protobuf and opencv] -DTE202_RESOURCE_DIR=[path to *.model files] [path to extractor source]
