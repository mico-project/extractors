#include "DPMDetector.h"
#include <ctime>
#include <boost/filesystem.hpp>
#include <utility>

namespace bf = boost::filesystem;

#define tic double tic_t = clock();
#define toc std::cout << (clock() - tic_t)/CLOCKS_PER_SEC \
                           << " seconds" << std::endl;

DPMDetector::DPMDetector():
  m_isModelSet(false)
{
  m_model.reset();
}

std::map<std::string, ConfidenceFunction> DPMDetector::confFromYamlFile(const std::string& filepath) {
  
  std::map<std::string, ConfidenceFunction> retMap;
  cv::FileStorage fs(filepath, cv::FileStorage::READ);
  
  cv::FileNode entries = fs["entries"];
  cv::FileNodeIterator it = entries.begin(), it_end = entries.end();
  int idx = 0;

  // iterate through a sequence using FileNodeIterator
  for( ; it != it_end; ++it, idx++ )
  {
    std::string name = (std::string) (*it)["name"];
    double thresh = (double) (*it)["thresh"];
    std::vector<float> bins;
    std::vector<float> conf;
    (*it)["conf"] >> conf;
    (*it)["bins"] >> bins;

    retMap[name] = ConfidenceFunction(name,thresh, bins, conf);
                
  }
  fs.release();
  return retMap;
}

std::vector<DetectionResults> DPMDetector::detect(const cv::Mat &image, double& thresh, const double& overlap)
{
  tic
  if (!m_isModelSet)
    std::cerr << "Error : No detection model has been set!" << std::endl;

  if (std::numeric_limits<double>::infinity() == thresh)
    thresh = m_model.thresh;

  std::cout << "threshold set to " << thresh << std::endl;

  m_imgSize = image.size();

  // we pad the feature maps to detect partially visible objects
  uint padx = static_cast<uint>( std::ceil(static_cast<double>( m_model.maxsize.at(1) )/2.0 + 1) );
  uint pady = static_cast<uint>( std::ceil(static_cast<double>( m_model.maxsize.at(0) )/2.0 + 1) );

  std::vector< cv::Mat > pyramid;
  std::vector<double> scales;

  ImageProcessingHelpers::featpyramid(image, m_model, &pyramid, &scales);

  double best = -std::numeric_limits<double>::infinity();
  std::vector<DetectionResults> boxes;
  boxes.reserve(1000000);

  for (uint level = m_model.interval; level < pyramid.size(); level++){
    double scale = m_model.sbin/scales.at(level);

    // get current pyramid level & size
    //Root
    cv::Mat currentPyramidLevelRoot = pyramid.at(level);
    std::vector<uint> dimensionsRoot = ImageProcessingHelpers::getDimensionsOfOpenCVMat(currentPyramidLevelRoot);
    //Part
    cv::Mat currentPyramidLevelPart = pyramid.at(level-m_model.interval);
    std::vector<uint> dimensionsPart = ImageProcessingHelpers::getDimensionsOfOpenCVMat(currentPyramidLevelPart);

    if (dimensionsRoot.at(0) + 2*pady < m_model.maxsize.at(0) || dimensionsRoot.at(1) + 2*padx < m_model.maxsize.at(1) )
      continue;

    // convolve feature maps with filters
    for (uint idxComponents = 0; idxComponents < m_model.numcomponents; idxComponents++){
      cv::Mat temp;

      // the ROOT filter
      for (uint idxFeat = 0; idxFeat < dimensionsRoot.at(2); idxFeat++){

        cv::Mat currentFeatRoot, currentFeatPaddedRoot, convolvedImageRoot;
        // get slice
        ImageProcessingHelpers::getSliceFromOpenCVMat(currentPyramidLevelRoot, idxFeat, &currentFeatRoot);
        // zero padding
        
        cv::copyMakeBorder(currentFeatRoot, currentFeatPaddedRoot, pady, pady, padx, padx, cv::BORDER_CONSTANT, cv::Scalar(0));
        // convolve
        cv::filter2D( currentFeatPaddedRoot, convolvedImageRoot, -1, m_model.rootfilters.at(idxComponents).w.at(idxFeat), cv::Point(-1,-1), 0, cv::BORDER_CONSTANT );
        
        // extract valid region
        cv::Size kernelSize;
        kernelSize.height = m_model.rootfilters.at(idxComponents).w.at(idxFeat).rows;
        kernelSize.width = m_model.rootfilters.at(idxComponents).w.at(idxFeat).cols;

        convolvedImageRoot = convolvedImageRoot.colRange((kernelSize.width-1)/2, convolvedImageRoot.cols - kernelSize.width/2)
          .rowRange((kernelSize.height-1)/2, convolvedImageRoot.rows - kernelSize.height/2);
        
        if (idxFeat == 0)
          convolvedImageRoot.copyTo(temp);
        else
          temp += convolvedImageRoot;
        
      }

      cv::Mat score = temp + m_model.offsets.at( m_model.components.at(idxComponents).offsetindex ).w;

      // the PART filters
      std::vector<cv::Mat> vecIx( m_model.components.at(idxComponents).parts.size() );
      std::vector<cv::Mat> vecIy( m_model.components.at(idxComponents).parts.size() );

      for (uint idxParts = 0; idxParts < m_model.components.at(idxComponents).parts.size(); idxParts++){
        cv::Mat temp2;

        for (uint idxFeat = 0; idxFeat < dimensionsPart.at(2); idxFeat++){
          cv::Mat currentFeatPart, currentFeatPaddedPart, convolvedImagePart;
          // get slice
          ImageProcessingHelpers::getSliceFromOpenCVMat(currentPyramidLevelPart, idxFeat, &currentFeatPart);
          // zero padding
          cv::copyMakeBorder(currentFeatPart, currentFeatPaddedPart, 2*pady, 2*pady, 2*padx, 2*padx, cv::BORDER_CONSTANT, cv::Scalar(0));
          // convolve
          uint partindex = m_model.components.at(idxComponents).parts.at(idxParts).partindex;
          cv::filter2D( currentFeatPaddedPart, convolvedImagePart, -1, m_model.partfilters.at(partindex).w.at(idxFeat), cv::Point(-1,-1), 0, cv::BORDER_CONSTANT );
          // extract valid region
          cv::Size kernelSize;
          kernelSize.height = m_model.partfilters.at(partindex).w.at(idxFeat).rows;
          kernelSize.width = m_model.partfilters.at(partindex).w.at(idxFeat).cols;

          convolvedImagePart = convolvedImagePart.colRange((kernelSize.width-1)/2, convolvedImagePart.cols - kernelSize.width/2)
            .rowRange((kernelSize.height-1)/2, convolvedImagePart.rows - kernelSize.height/2);

          if (idxFeat == 0)
            convolvedImagePart.copyTo(temp2);
          else
            temp2 += convolvedImagePart;
        }

        std::vector<float> def = m_model.defs.at(m_model.components.at(idxComponents).parts.at(idxParts).defindex).w;
        std::vector<uint> anchor = m_model.defs.at(m_model.components.at(idxComponents).parts.at(idxParts).defindex).anchor;

        cv::Mat M, Ix, Iy;
        ImageProcessingHelpers::generalizedDistanceTransform(-1*temp2, def, M, Ix, Iy);

        vecIx.at( idxParts ) = Ix;
        vecIy.at( idxParts ) = Iy; 

        int startRow = anchor.at(1);
        int startCol = anchor.at(0);
        int endRow = anchor.at(1)+2*(score.rows);
        int endCol = anchor.at(0)+2*(score.cols);

        cv::Mat MSub = ImageProcessingHelpers::getDownsampledSubMatrix(M, startRow, startCol, endRow, endCol, 2); 

        score -= MSub;
      }

      std::vector<int> X, Y;
      std::vector<uint> rsize(2 , 0u);
      rsize.at(0) = static_cast<uint>(m_model.rootfilters.at(m_model.components.at(idxComponents).rootindex).w.at(0).rows);
      rsize.at(1) = static_cast<uint>(m_model.rootfilters.at(m_model.components.at(idxComponents).rootindex).w.at(0).cols);

      ImageProcessingHelpers::find(score, thresh, &X, &Y);
      // [x1Root y1Root x2Root y2Root | x1Part1 y1Part1 x2Part1 y2Part1 ... x1PartN y1PartN x2PartN y2PartN | component score]
      for (uint i = 0; i < Y.size(); i++){

        DetectionResults tmpResults;

        int x = X.at(i);
        int y = Y.at(i);

        tmpResults.RootBox= DPMDetector::rootbox(x, y, scale, padx, pady, rsize);
        tmpResults.PartBoxes.resize( m_model.components.at(idxComponents).parts.size() );

        for (uint j = 0; j < m_model.components.at(idxComponents).parts.size(); j++){
          std::vector<uint> anchor = m_model.defs.at(m_model.components.at(idxComponents).parts.at(j).defindex).anchor;
          std::vector<uint> psize;
          psize.push_back(m_model.partfilters.at(m_model.components.at(idxComponents).parts.at(j).partindex).w.at(0).rows);
          psize.push_back(m_model.partfilters.at(m_model.components.at(idxComponents).parts.at(j).partindex).w.at(0).cols);

          cv::Mat Ix = vecIx.at(j);
          cv::Mat Iy = vecIy.at(j);


          tmpResults.PartBoxes.at(j) = DPMDetector::partbox(x, y, anchor, scale, padx, pady, psize, Ix, Iy);
        }
        tmpResults.component = idxComponents;
        tmpResults.score = score.at<double>(y,x);

        boxes.push_back(tmpResults);
      }
    }  
  }
  std::vector<DetectionResults> out = DPMDetector::nms(boxes, overlap);
  for (unsigned int i = 0; i<out.size(); ++i) {
    out.at(i).conf = m_model.confmaps["max(f1)"].confOf( out.at(i).score );
  }
  toc
  return out;
}

/****************** PRIVATE ****************************/

BoxCoordinates DPMDetector::rootbox(const int& x, const int& y, const double& scale, const int& padx, const int& pady, const std::vector<uint>& rsize){

  int x1 = static_cast<int>( (x - padx)*scale );
  int y1 = static_cast<int>( (y - pady)*scale );

  int x2 = static_cast<int>( x1 + static_cast<int>(rsize.at(1))*scale );
  int y2 = static_cast<int>( y1 + static_cast<int>(rsize.at(0))*scale );

  // Some checks
  BoxCoordinates rootcoordinates;
  rootcoordinates.X1 = std::max(x1,0);
  rootcoordinates.Y1 = std::max(y1,0);

  rootcoordinates.X2 = std::min(std::max(x2,0),m_imgSize.width);
  rootcoordinates.Y2 = std::min(std::max(y2,0),m_imgSize.height);

  return rootcoordinates;
}

BoxCoordinates DPMDetector::partbox(const int& x, const int& y, const std::vector<uint>& anchor,
  const double& scale, const int& padx, const int& pady, const std::vector<uint>& psize, 
  const cv::Mat& Ix, const cv::Mat& Iy){

    int probex = x*2+static_cast<int>(anchor.at(0));
    int probey = y*2+static_cast<int>(anchor.at(1));

    double px = Ix.at<double>(probey,probex);
    double py = Iy.at<double>(probey,probex);

    int px1 = static_cast<int>( ( (px-2)/2+1-padx )*scale);
    int py1 = static_cast<int>( ( (py-2)/2+1-pady )*scale);
    int px2 = static_cast<int>( px1 + static_cast<int>(psize.at(1))*scale/2);
    int py2 = static_cast<int>( py1 + static_cast<int>(psize.at(0))*scale/2);

    BoxCoordinates partcoordinates;

    partcoordinates.X1 = std::max(px1,0);
    partcoordinates.Y1 = std::max(py1,0);

    partcoordinates.X2 = std::min(std::max(px2,0),m_imgSize.width);
    partcoordinates.Y2 = std::min(std::max(py2,0),m_imgSize.height);

    return partcoordinates;
}

std::vector<DetectionResults> DPMDetector::nms(const std::vector<DetectionResults>& detections, const double& overlap){

  /*	Non-maximum suppression.
  Greedily select high-scoring detections and skip detections
  that are significantly covered by a previously selected detection.*/

  std::vector<DetectionResults> output;
  if (!detections.size())
    return output;

  std::vector<double> x1(detections.size(),0.0);
  std::vector<double> y1(detections.size(),0.0);
  std::vector<double> x2(detections.size(),0.0);
  std::vector<double> y2(detections.size(),0.0);
  std::vector<double> s(detections.size(),0.0);
  std::vector<double> area(detections.size(),0.0);

  for(uint idx=0; idx < detections.size(); idx++){
    x1.at(idx)   = detections.at(idx).RootBox.X1;
    y1.at(idx)   = detections.at(idx).RootBox.Y1;
    x2.at(idx)   = detections.at(idx).RootBox.X2;
    y2.at(idx)   = detections.at(idx).RootBox.Y2;
    s.at(idx)    = detections.at(idx).score;
    area.at(idx) = (x2.at(idx) - x1.at(idx))*(y2.at(idx) - y1.at(idx));
  }

  // sort boxes by confidence and keep track of the index
  std::vector<std::pair<double, std::size_t> > vp; // This is a vector of {value,index} pairs
  vp.reserve(s.size());
  for (std::size_t i = 0 ; i < s.size() ; i++) {
    vp.push_back(std::make_pair(s[i], i));
  }

  // Sorting will put lower values ahead of larger ones,
  // resolving ties using the original index
  std::sort(vp.begin(), vp.end());
  std::vector<size_t> I(vp.size(), 0);
  for (std::size_t i = 0; i < vp.size(); i++)
    I.at(i) = vp.at(i).second;

  std::vector<size_t> pick;

  while ( I.size() ){
    size_t last = I.size() - 1;
    size_t i = I.at(last);
    pick.push_back(i);
    std::vector<size_t> suppress;
    suppress.push_back(last);

    // pos and last musn't be a uint because it crashes at the last loop
    for (int pos=0; pos < last; pos++){
      size_t j = I.at(pos);

      double xx1 = std::max(x1.at(i),x1.at(j));
      double yy1 = std::max(y1.at(i),y1.at(j));
      double xx2 = std::min(x2.at(i),x2.at(j));
      double yy2 = std::min(y2.at(i),y2.at(j));

      double w = xx2-xx1;
      double h = yy2-yy1;

      if ((w > 0) && (h > 0)){
        double o = w*h / area.at(j);
        if (o > overlap)
          suppress.push_back(pos);
      }
    }

    // erase indices in suppress from I (suppress needs to be sorted descendingly first, then loop through the indices)
    std::sort(suppress.begin(),suppress.end(),std::greater<int>());
    for (uint idx=0; idx < suppress.size(); idx++)
      I.erase(I.begin()+suppress.at(idx));
  }
  output.resize(pick.size());
  for (uint idxRow=0; idxRow < pick.size(); idxRow++){
    output[idxRow] = detections.at(static_cast<uint>( pick.at(idxRow) ) );
  }
    
  return output;
}
