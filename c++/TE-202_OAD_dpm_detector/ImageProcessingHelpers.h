#include "DPMModel.h"

class ImageProcessingHelpers{
public:
  template<typename Type>
  static void find(const cv::Mat& input, const Type& thresh, std::vector<int>* x, std::vector<int>* y);

  static cv::Mat getDownsampledSubMatrix(const cv::Mat& input, const int& startRow, const int& startCol, const int& endRow, const int& endCol, const int& step);

  template<typename Type>
  static void convertArrayToMat(const std::vector<Type>& vec, cv::Mat& m);

  template<typename Type>
  static std::vector<Type> convertMatToArray(const cv::Mat& mat);

  template<typename Type>
  static void generalizedDistanceTransform(const cv::Mat& input, const std::vector<Type>& defs, cv::Mat& M, cv::Mat& Ix, cv::Mat& Iy);

  static std::vector<uint> getDimensionsOfOpenCVMat(const cv::Mat& Array3D);

  static void getSliceFromOpenCVMat(const cv::Mat& Array3D, const int z, cv::Mat* slice);

  template<typename Type>
  static void featpyramid(const cv::Mat& image, const DPMmodel& model, std::vector< cv::Mat >* pyramid, std::vector<Type>* scales);

  static void HOGExtraction(const cv::Mat& scaledImg, const uint& sbin, cv::Mat* feat);

private:
  static void dt_helper(double *src, double *dst, double *ptr, int step, int s1, int s2, int d1, int d2, double a, double b);
  static void dt1d(double *src, double *dst, double *ptr, int step, int n, double a, double b);
};
