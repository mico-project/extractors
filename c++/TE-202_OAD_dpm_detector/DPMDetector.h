#include "DPMModel.h"
#include "DPMDetectionResults.h"
#include "ImageProcessingHelpers.h"

class DPMDetector {
public:
  DPMDetector();

  DPMmodel getModel() const { return m_model;}

  void setModel(const DPMmodel& model) { 
    m_model = model; 
    m_isModelSet = true;
  }

  std::vector<DetectionResults> detect(const cv::Mat& image, double& thresh, const double& overlap);

private:
  DPMmodel m_model;
  bool m_isModelSet;
  cv::Size m_imgSize;

  std::map<std::string, ConfidenceFunction> confFromYamlFile(const std::string& filepath);

  BoxCoordinates rootbox(const int& x, const int& y, 
    const double& scale, const int& padx, const int& pady, const std::vector<uint>& rsize);

  BoxCoordinates partbox(const int& x, const int& y, const std::vector<uint>& anchor,
    const double& scale, const int& padx, const int& pady, const std::vector<uint>& psize, 
    const cv::Mat& Ix, const cv::Mat& Iy);

  std::vector<DetectionResults> nms(const std::vector<DetectionResults>& detections, const double& overlap);
};
