#ifndef DPM_MODEL_H
#define DPM_MODEL_H

#include <limits>
#include <vector>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>


#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"


const float INFF = std::numeric_limits<float>::infinity();
const double INFFD = std::numeric_limits<double>::infinity();
const unsigned int INFU = std::numeric_limits<unsigned int>::max();// unfortunately std::numeric_limits<unsigned int>::infinity() is 0

struct rootfilter{
  std::vector<unsigned int>        size;
  unsigned int                     blocklabel;
  std::vector< cv::Mat >           w;
  //std::vector< HeapMatrix<float> >  w;

  rootfilter(): blocklabel(INFU){ }
  void reset(){
    std::vector< cv::Mat >::iterator itW = w.begin();
    for ( ; itW != w.end(); itW++ )
      itW->release();

    blocklabel = INFU;
    size.clear();
    w.clear();
  }
};

typedef std::vector<rootfilter> vRootfilter;

struct offset{
  float                            w;
  unsigned int                     blocklabel;

  offset(): w(INFF), blocklabel(INFU){ }
  void reset(){
    w = INFF;
    blocklabel = INFU;
  }
};

typedef std::vector<offset> vOffset;

struct part{
  unsigned int                      partindex;
  unsigned int                      defindex;

  part(): partindex(INFU), defindex(INFU){ }
  void reset(){
    partindex = INFU;
    defindex = INFU;
  }
};

typedef std::vector<part> vParts;

struct component{
  vParts                            parts;
  unsigned int                      dim;
  unsigned int                      numblocks;
  unsigned int                      rootindex;
  unsigned int                      offsetindex;
  std::vector<float>                x1;
  std::vector<float>                y1;
  std::vector<float>                x2;
  std::vector<float>                y2;

  component(): dim(INFU),numblocks(INFU),rootindex(INFU),offsetindex(INFU){ }
  void reset(){
    vParts::iterator itParts = parts.begin();
    for (; itParts != parts.end(); itParts++)
      itParts->reset();

    parts.clear();

    dim = INFU;
    numblocks = INFU;
    rootindex = INFU;
    offsetindex = INFU;

    x1.clear();
    y1.clear();
    x2.clear();
    y2.clear();
  }
};

typedef std::vector<component> vComponent;

struct partfilter{
  std::vector< cv::Mat >            w;
  //std::vector< HeapMatrix<float> >  w;
  bool                              fake;
  unsigned int                              partner;
  unsigned int                              blocklabel;

  partfilter(): partner(INFU), blocklabel(INFU), fake(false){ }
  void reset(){
    std::vector< cv::Mat >::iterator itW = w.begin();
    for ( ; itW != w.end(); itW++ )
      itW->release();

    w.clear();
    fake = false;
    partner = INFU;
    blocklabel = INFU;
  }
};

typedef std::vector<partfilter> vPartfilter;

struct def{
  std::vector<unsigned int>         anchor;
  std::vector<float>                w;
  unsigned int                      blocklabel;

  def():blocklabel(INFU){ }
  void reset(){
    anchor.clear();
    w.clear();
    blocklabel = INFU;
  }
};




/** Discreete Function (LUT) for mapping classification distances to
  confidence values. Retrieved from the statistics of the training data */

struct ConfidenceFunction {
  ConfidenceFunction()
    : thresh(INFFD)
  {}
  ConfidenceFunction(std::string _name, double _thresh, const std::vector<float>& _bins, const std::vector<float>& _conf) 
    : name(_name),  thresh(_thresh), bins(_bins),  conf(_conf)
  {}

  
  float confOf(double score) 
  {
    std::vector<float>::iterator binIt = std::lower_bound(bins.begin(), bins.end(), score);

    if ( binIt == bins.end() ) {
      return 1.0f;
    }

    if ( binIt == bins.begin() ) {
      return 0.0f;
    }

    return conf[binIt - bins.begin() - 1];
  }

public:
  std::string        name;
  double             thresh;
  std::vector<float> bins;
  std::vector<float> conf;  
};

typedef std::vector<def> vDef;

struct DPMmodel{
  unsigned int                              sbin;
  vRootfilter                               rootfilters;
  vOffset                                   offsets;
  std::vector<unsigned int>                 blocksizes;
  std::vector<unsigned int>                 regmult;
  std::vector<float>                        learnmult;
  std::vector< std::vector<float> >         lowerbounds;
  vComponent                                components;
  unsigned int                              interval;
  unsigned int                              numcomponents;
  unsigned int                              numblocks;
  vPartfilter                               partfilters;
  vDef                                      defs;
  std::vector<unsigned int>                 maxsize;
  std::vector<unsigned int>                 minsize;
  float                                     thresh;
  std::map<std::string, ConfidenceFunction> confmaps;

  DPMmodel(): sbin(INFU), interval(INFU), numcomponents(INFU), numblocks(INFU), thresh(INFF){}
  void reset(){
    sbin = INFU;
    vRootfilter::iterator itRootfilters = rootfilters.begin();
    for( ; itRootfilters != rootfilters.end(); itRootfilters++)
      itRootfilters->reset();

    vOffset::iterator itOffsets = offsets.begin();
    for ( ; itOffsets != offsets.end(); itOffsets++)
      itOffsets->reset();

    blocksizes.clear();
    regmult.clear();
    learnmult.clear();
    
    std::vector< std::vector<float> >::iterator itLowerbounds = lowerbounds.begin();
    for ( ; itLowerbounds != lowerbounds.end(); itLowerbounds++)
      itLowerbounds->clear();

    vComponent::iterator itComponents = components.begin();
    for( ; itComponents != components.end(); itComponents++)
      itComponents->reset();

    interval = INFU;
    numcomponents = INFU;
    numblocks = INFU;

    vPartfilter::iterator itPartfilters = partfilters.begin();
    for( ; itPartfilters != partfilters.end(); itPartfilters++)
      itPartfilters->reset();

    vDef::iterator itDef = defs.begin();
    for( ; itDef != defs.end(); itDef++)
      itDef->reset();

    maxsize.clear();
    minsize.clear();
    thresh = INFF;
  }
};

BOOST_SERIALIZATION_SPLIT_FREE(::cv::Mat)

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive & ar, DPMmodel & m, const unsigned int version)
{
  ar & m.sbin;
  ar & m.rootfilters;
  ar & m.offsets;
  ar & m.blocksizes;
  ar & m.regmult;
  ar & m.learnmult;
  ar & m.lowerbounds;
  ar & m.components;
  ar & m.interval;
  ar & m.numcomponents;
  ar & m.numblocks;
  ar & m.partfilters;
  ar & m.defs;
  ar & m.maxsize;
  ar & m.minsize;
  ar & m.thresh;
  ar & m.confmaps;
}

template<class Archive>
void serialize(Archive & ar, rootfilter & f, const unsigned int version)
{
  ar & f.size;
  ar & f.blocklabel;
  ar & f.w; 
}

template<class Archive>
void serialize(Archive & ar, offset & o, const unsigned int version)
{
  ar & o.w;
  ar & o.blocklabel;
}

template<class Archive>
void serialize(Archive & ar, component & c, const unsigned int version)
{
  ar & c.parts;
  ar & c.dim;
  ar & c.numblocks;
  ar & c.rootindex;
  ar & c.offsetindex;
  ar & c.x1;
  ar & c.y1;
  ar & c.x2;
  ar & c.y2;
}

template<class Archive>
void serialize(Archive & ar, partfilter & p, const unsigned int version)
{
  ar & p.w;
  ar & p.fake;
  ar & p.partner;
  ar & p.blocklabel;
  
}

template<class Archive>
void serialize(Archive & ar, part & pa, const unsigned int version)
{
  ar & pa.partindex;
  ar & pa.defindex;
  
}

template<class Archive>
void serialize(Archive & ar, def & d, const unsigned int version)
{
  ar & d.anchor;
  ar & d.w;
  ar & d.blocklabel;
}

template<class Archive>
void serialize(Archive & ar,  ConfidenceFunction& cf, const unsigned int version)
{
  ar & cf.name;
  ar & cf.thresh;
  ar & cf.bins;
  ar & cf.conf;
}
 
/** Serialization support for cv::Mat */
template<class Archive>
void save(Archive & ar, const ::cv::Mat& m, const unsigned int version)
{
  size_t elem_size = m.elemSize();
  size_t elem_type = m.type();
 
  ar & m.cols;
  ar & m.rows;
  ar & elem_size;
  ar & elem_type;
 
  const size_t data_size = m.cols * m.rows * elem_size;
  ar & boost::serialization::make_array(m.ptr(), data_size);
}
 
/** Serialization support for cv::Mat */
template<class Archive>
void load(Archive & ar, ::cv::Mat& m, const unsigned int version)
{
  int cols, rows;
  size_t elem_size, elem_type;
 
  ar & cols;
  ar & rows;
  ar & elem_size;
  ar & elem_type;
 
  m.create(rows, cols, elem_type);
 
  size_t data_size = m.cols * m.rows * elem_size;
  ar & boost::serialization::make_array(m.ptr(), data_size);
}


} // namespace serialization
} // namespace boost

#endif
