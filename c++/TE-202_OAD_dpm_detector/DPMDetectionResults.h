#include <vector>

struct BoxCoordinates{
  int X1;
  int X2;
  int Y1;
  int Y2;

  void reset(){
    X1 = -1;
    X2 = -1;
    Y1 = -1;
    Y2 = -1;
  }
};

struct DetectionResults{
  BoxCoordinates RootBox;

  double score;
  float  conf;
  int component;

  std::vector<BoxCoordinates> PartBoxes; 
  void reset(){
    std::vector<BoxCoordinates>::iterator itPartBoxes = PartBoxes.begin();
    for ( ; itPartBoxes != PartBoxes.end(); itPartBoxes++ )
      itPartBoxes->reset();

    score = 0;
    PartBoxes.clear();
  }
};
