#include <ctime>
#include <fstream>
#include <algorithm>
//#include <map>

#include <boost/regex.hpp>


#include <Logging.hpp>

#include "DPMDetectorService.h"
#include "ObjectRecoOutputHandler.h"

#include <MICOExtractorID.h>


// define dublin core vocabulary shortcut
namespace DC = mico::rdf::vocabularies::DC;

// helper function to get time stamp
std::string getTimestamp() {
  time_t now;
  time(&now);
  char buf[sizeof "2011-10-08T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));        
  return std::string(buf);
}


void runtime_error(std::string what)
{

}


DPMDetectorService::DPMDetectorService(std::string mimeType, std::map<std::string, DPMmodel> detectionModels)
  : AnalysisService(mico::extractors::getPrecompExtractorId(),mico::extractors::getPrecompExtractorModeId(),mico::extractors::getPrecompExtractorVersion(),
    mimeType, "text/vnd.fhg-objectdetection+xml"),
    m_detectionModels(detectionModels)
{
}

DPMDetectorService::~DPMDetectorService()
{
}


void DPMDetectorService::call(mico::event::AnalysisResponse& resp, std::shared_ptr< mico::persistence::model::Item > item, std::vector<std::shared_ptr<mico::persistence::model::Resource>> resources, std::map<std::string,std::string>& params)
{
  if (resources.size() != 1) {
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "DPM Animal Detector expects exactly one input resource",
                          "Wrong number of input resources");
    return;
  }
  std::shared_ptr<Resource> itemResource = std::dynamic_pointer_cast<Resource>(item);
  std::shared_ptr<Resource> imgInResource = resources[0];

  if(imgInResource) {
    std::shared_ptr<Asset> imgInAsset = imgInResource->getAsset();
    std::istream* in = imgInAsset->getInputStream();

    //read image
    std::vector<char> buf =
        std::vector<char>(std::istreambuf_iterator<char>(*in), std::istreambuf_iterator<char>());
    cv::Mat matImage = cv::imdecode(cv::Mat(buf), 1);
    delete in;


    std::vector<cv::Rect> allRegionsFound;
    objectreco::ObjectRecognitionResults res;
    objectreco::VideoMetaType meta(0, 100, matImage.cols, matImage.rows);

    //>>>>>>>>>>> DETECTION HERE <<<<<<<<<<<<<<<
    unsigned int uid = 0,counter = 0;

    auto element = m_detectionModels.begin();
    #pragma omp parallel for shared(element, uid, counter, allRegionsFound)
    for(int i = 0 ; i < m_detectionModels.size() ; ++i){
      std::string current_first;
      DPMmodel    current_second;

      #pragma omp critical
      {
        current_first = element->first;
        current_second = element->second;
        ++element;
      }


      DPMDetector dpm;
      dpm.setModel(current_second);
      double threshold = dpm.getModel().confmaps["max(f1)"].thresh;
      double overlap = 0.3;
      LOG_INFO("DPMDetector for %s starts with threshold: %f and overlap: %f", current_first.c_str(), threshold, overlap);
      std::vector<DetectionResults> detections = dpm.detect(matImage, threshold, overlap);

      LOG_INFO("DPMDetector detected for %s: %i objects in picture.", current_first.c_str(), detections.size());
      for (unsigned int i = 0; i<detections.size(); ++i) {
        LOG_INFO("    [%i] with score %f -> confOf max(f1): %f", i, detections.at(i).score, detections.at(i).conf );
        objectreco::Object o;
        o.uid = uid;
        o.type = objectreco::ANIMAL;
        o.region.push_back(objectreco::Point(detections.at(i).RootBox.X1, detections.at(i).RootBox.Y1, 0, "topleft"));
        o.region.push_back(objectreco::Point(detections.at(i).RootBox.X2, detections.at(i).RootBox.Y1, 1));
        o.region.push_back(objectreco::Point(detections.at(i).RootBox.X2, detections.at(i).RootBox.Y2, 2));
        o.region.push_back(objectreco::Point(detections.at(i).RootBox.X1, detections.at(i).RootBox.Y2, 3));

        #pragma omp critical
        {
          allRegionsFound.push_back(cv::Rect(detections.at(i).RootBox.X1,
                                             detections.at(i).RootBox.Y1,
                                             detections.at(i).RootBox.X2 - detections.at(i).RootBox.X1,
                                             detections.at(i).RootBox.Y2 - detections.at(i).RootBox.Y1));
          res.objects.push_back(o);
          res.labelMapping[uid].push_back(objectreco::ObjectLabel(uid, current_first, detections.at(i).conf));
        }
        ++uid;
      }
      #pragma omp atomic
      counter++;

      resp.sendProgress(item, imgInResource->getURI(), (float) counter/m_detectionModels.size() );
    }
    mergeDifferentAnimalDetections(allRegionsFound, res, 0.5);
    //>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<

    // write xml text to a new content part
    std::shared_ptr<Part> txtOutPart = item->createPart(mico::persistence::model::URI(getServiceID()));
    std::shared_ptr<Resource> txtOutResource = std::dynamic_pointer_cast<Resource>(txtOutPart);
    txtOutResource->setSyntacticalType( "mico:ObjectDetectionXml" );
    txtOutResource->setSemanticType("Animal detection annotation in xml format");
    txtOutPart->addInput(imgInResource);


    std::shared_ptr<Asset> txtOutAsset = txtOutResource->getAsset();
    std::ostream* out = txtOutAsset->getOutputStream();

    txtOutAsset->setFormat("text/vnd.fhg-objectdetection+xml");

    std::map<uint64_t, objectreco::ObjectRecognitionResults > resMap;
    resMap[0] = res;
    objectreco::ObjectRecoOutputHandler outHandler(resMap, meta);
    outHandler.toXmlStream(*out);

    delete out;

    // notify broker that we created a new part
    resp.sendNew(item, txtOutResource->getURI());
    resp.sendFinish(item);

  } else {
    LOG_ERROR("Resource of item %s is null!", itemResource->getURI().stringValue().c_str());
    resp.sendErrorMessage(item, mico::event::model::INSUFFICIENT_RESOURCE,
                          "DPM Animal Detector - Null resource received",
                          "Null Resource");
  }
}

//exactly the same method as in HOGDetector:
void DPMDetectorService::mergeDifferentAnimalDetections(const std::vector<cv::Rect>& allRegionsFound,
                                                        objectreco::ObjectRecognitionResults& res,
                                                        double overlapThreshold)
{
  //check overlapping of all rectangles is over 50% -> delete obj with lower confidence
  for(size_t i = 0; i <= allRegionsFound.size(); ++i) {
    for(size_t j = 0; j <= allRegionsFound.size(); ++j) {
      if( i<j ) {
        cv::Rect intersection = allRegionsFound[i] & allRegionsFound[j];
        unsigned int intersectionArea = intersection.width * intersection.height;
        unsigned int unionArea = (allRegionsFound[i].width * allRegionsFound[i].height) +
                                 (allRegionsFound[j].width * allRegionsFound[j].height) - intersectionArea;

        if((unionArea - intersectionArea) !=0 && intersectionArea / (unionArea - intersectionArea) >= overlapThreshold) {
          std::map<long, std::vector<objectreco::ObjectLabel> >::iterator find_i = res.labelMapping.find(i);
          std::map<long, std::vector<objectreco::ObjectLabel> >::iterator find_j = res.labelMapping.find(j);

          if(find_i != res.labelMapping.end() && find_j != res.labelMapping.end()) {
            float conf_i = find_i->second.at(0).confidence;
            float conf_j = find_j->second.at(0).confidence;

            if(conf_i < conf_j) {
              res.labelMapping.erase(i);
              for(std::vector<objectreco::Object>::iterator iter = res.objects.begin(); iter<res.objects.end(); ++iter)
                if (iter->uid == i) { res.objects.erase(iter); break; }
            } else {
              res.labelMapping.erase(j);
              for(std::vector<objectreco::Object>::iterator iter = res.objects.begin(); iter<res.objects.end(); ++iter)
                if (iter->uid == j) { res.objects.erase(iter); break; }
            }
          }
        }
      }
    }
  }
}
