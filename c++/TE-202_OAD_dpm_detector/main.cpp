#include "DPMDetectorService.h"
#include <Daemon.hpp>
#include <Logging.hpp>

#include <FileOperations.h>

#include <string>
#include <fstream>

#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>

namespace MEX = mico::extractors;
namespace po  = boost::program_options;


/****** globals ******/
EventManager* mgr = 0;
DPMDetectorService* dpmdetectorService = 0;
bool loop = true;

void signal_handler(int signum) {
  std::cout << "shutting down DPMDetector service ... " << std::endl;

  if (mgr) {
    mgr->unregisterService(dpmdetectorService);
  }

  if (dpmdetectorService)
    delete dpmdetectorService;

  if (mgr)
    delete mgr;

  loop = false;
}

static  std::string ip;
static  std::string user;
static  std::string passw;
static  std::string fmt;
static  std::string detectionModelPath;


static bool setCommandLineParams(int argc, char** argv, po::variables_map& vm)
{
  po::options_description desc("Allowed options");
  desc.add_options()
    ("serverIP,i",  po::value<std::string>(&ip)->required(), "IP of the MICO system server.")
    ("userName,u",  po::value<std::string>(&user)->required(), "MICO system user name")
    ("userPassword,p", po::value<std::string>(&passw)->required(), "MICO system user password")
    ("model,m", po::value<std::string>(&detectionModelPath)->required(), "The path to *.model files for this animal detector instance.")
    ("inputFormat,f", po::value<std::string>(&fmt)->default_value(std::string("JPEG"),"JPEG"), "Expected input type (JPEG or PNG)")
    ("kill,k","Shuts down the service.")
    ("foreground,g","Runs the extractor as foreground process (by default it runs as daemon)")
    ("help,h","Prints this help message.");

  po::positional_options_description p;
  p.add("serverIP",1);
  p.add("userName",1);
  p.add("userPassword",1);

  bool printHelp = false;
  std::string ex("") ;
  try {
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run() , vm);
    po::notify(vm);
  } catch (std::exception& e) {
    ex = e.what();
    printHelp = true;
  }
  if (vm.count("help")) {
    printHelp = true;
  }

  if (printHelp) {
    if (ex.size() > 0)
      std::cout << std::endl << ex << "\n";
    std::cout << "\nUsage:   " << argv[0];
    for (unsigned int i=0; i < p.max_total_count(); ++i)
      std::cout << " " << p.name_for_position(i);
    std::cout << " [options]" << "\n";
    std::cout << "\n" << desc << "\n";
    return false;
  }
  return true;
}

/****** main ******/
int main(int argc, char **argv)
{
  po::variables_map vm;

  if (!setCommandLineParams(argc, argv, vm)) {
   exit(EXIT_FAILURE);
  }

  bool   doKill    = false;
  bool   asDaemon  = true;

  if (vm.count("kill")) {
    doKill = true;
  }

  if (vm.count("foreground")) {
    doKill   = false;
    asDaemon = false;
  }

  boost::to_lower(fmt);

  std::string img_type = "jpeg";

  if(fmt == "png"){
    img_type = "png";
  }

  std::string mime_type = std::string("image/") + img_type;

  std::string s_daemon_id(argv[0]);
  s_daemon_id += std::string("-") + std::string(img_type);

  if (!doKill && asDaemon)
     std::cout << "Launching animal detection daemon with id [" << s_daemon_id << "]" << std::endl;
  else if (doKill && asDaemon)
     std::cout << "Shutting down animal detection daemon with id ["<< s_daemon_id <<"]" << std::endl;
  else
     std::cout << "Starting animal detection extractor to analyse "<< mime_type << std::endl;

  if(doKill) {
    return mico::daemon::stop(s_daemon_id.c_str());
  }

  std::map<std::string, DPMmodel> detectionModels;
  std::vector<std::string> modelList;
  MEX::FileOperations::get_all(detectionModelPath,".model", modelList);
  for(auto modelIter = modelList.begin(); modelIter != modelList.end(); ++modelIter) {
    std::cout << "load model for: " << modelIter->substr(0,modelIter->size()-6) << std::endl;
    std::ifstream ifs( detectionModelPath +"/"+ *modelIter );
    boost::archive::text_iarchive ia(ifs);
    ia >> detectionModels[modelIter->substr(0,modelIter->size()-6)];
  }

  if (asDaemon) {
    mico::log::LoggingBackend* log_back = mico::daemon::createDaemonLogBackend();
    mico::log::set_log_backend (log_back);
    //log_back->setLevel(mico::log::INFO);

    return mico::daemon::start(s_daemon_id.c_str(), ip.c_str(), user.c_str(), passw.c_str(),

    {new DPMDetectorService(mime_type, detectionModels)});
  } else {
    mico::log::LoggingBackend* log_back = new mico::log::StdOutBackend();

    mico::log::set_log_backend (log_back);
    //log_back->setLevel(mico::log::INFO);
    dpmdetectorService = new DPMDetectorService(mime_type, detectionModels);

    mgr = new EventManager(ip.c_str(), user.c_str(), passw.c_str());

    mgr->registerService(dpmdetectorService);

    signal(SIGINT,  &signal_handler);
    signal(SIGTERM, &signal_handler);
    signal(SIGHUP,  &signal_handler);

    while(loop) {
      sleep(1);
    }
  }
}

