/*
 * AudioDemuxer.cpp
 *
 *  Created on: Jul 7, 2nullptr15
 *  Author: cvo
 */

#include "AudioDemuxer.h"
#include <iostream>
#include <stdexcept>

extern "C"{
  #include <libavcodec/avcodec.h>
  #include <libavformat/avformat.h>
  #include <libavutil/avutil.h>
  #include <libavutil/opt.h>
  #include <libavutil/avassert.h>
  #include <libavutil/avstring.h>
  #include <libavutil/samplefmt.h>
  #include <libavutil/audio_fifo.h>

  //#include <libswresample/swresample.h>
  #include <libavresample/avresample.h>
}

#ifndef AV_ERROR_MAX_STRING_SIZE
  #define AV_ERROR_MAX_STRING_SIZE 128
#endif

namespace mico {

AudioDemuxer::AudioDemuxer(unsigned int targetFrequency) : encContext(nullptr) , decContext(nullptr), fifo(nullptr),
	         inputContainer(nullptr), outputContainer(nullptr),
	         resample_context(nullptr), aStreamId(-1), targetFrequency(targetFrequency){

  // initialize avlib
  av_register_all();
}

void AudioDemuxer::throwRuntimeException(std::string functionName, std::string contextExplanation, const int errorCode)
{
	auto get_error_text = [](const int error)->std::string
	{
	    char error_buffer[AV_ERROR_MAX_STRING_SIZE];
	    av_strerror(error, error_buffer, sizeof(error_buffer));
	    return std::string(error_buffer);
	};

        reset();
	throw std::runtime_error("ERROR in "+ functionName + " -- " + contextExplanation+get_error_text(errorCode));
}
void AudioDemuxer::throwRuntimeException(std::string functionName, std::string contextExplanation)
{
        reset();
	throw std::runtime_error("ERROR in "+ functionName + " -- " + contextExplanation);
}

void AudioDemuxer::openInputFile(char* f)
{
  if(f == nullptr) throw std::invalid_argument("ERROR in AudioDemuxer::openInputFile(char* f): f is NULL");
  std::string scope="AudioDemuxer::openInputFile()";

  int err;
  // initialize the inputContainer holding the bitstream
    if ((err=avformat_open_input(&inputContainer, f, nullptr, nullptr)) < 0) {
      throwRuntimeException(scope,"Could not open file: ",err);
  }

  // retrieve the information
  if ((err=avformat_find_stream_info(inputContainer, nullptr)) < 0) {
	  throwRuntimeException(scope,"Could not find file info: ",err);
  }

  // look for the audio stream
  aStreamId = av_find_best_stream(inputContainer, AVMEDIA_TYPE_AUDIO, -1, -1, nullptr, 0);
  if (aStreamId < 0) {
	  throwRuntimeException(scope,"Could not find an audio stream: ",aStreamId);
  }


  // initialize the decoder
  decContext = inputContainer->streams[aStreamId]->codec;
  AVCodec* decoder = avcodec_find_decoder(decContext->codec_id);
  if(decoder == nullptr) throwRuntimeException(scope,"Could not determine input file format");

  //and check its presence on the system
  if ((err=avcodec_open2(decContext, decoder, nullptr)) < 0) {
	  throwRuntimeException(scope,"Could not open the needed codec: ",err);
  }

  // dump information about the input stream
  av_dump_format(inputContainer,aStreamId,"",0);

}


void AudioDemuxer::openOutputFile(char * f)
{
  if(f == nullptr) throw std::invalid_argument("ERROR in AudioDemuxer::openOutputFile(char* f): f is NULL");
  std::string scope="AudioDemuxer::openOutputFile()";

  /*
   * Setup the output file
   */

  AVIOContext *output_io_context = nullptr;
  AVStream *stream               = nullptr;
  AVCodec *encoder               = nullptr;
  int error;

  // Open the outpuf file
  if( (error = avio_open(&output_io_context, f, AVIO_FLAG_WRITE)) < 0)
	throwRuntimeException(scope,"Could not open output file for demuxing: ",error);


  // Create a container for the output file
  if ((outputContainer = avformat_alloc_context())==nullptr)
	throwRuntimeException(scope,"Could not allocate output format context");


  // and link it to the output file
  outputContainer->pb = output_io_context;
  outputContainer->oformat=nullptr;

  if ((outputContainer->oformat = av_guess_format(nullptr, f, nullptr))==nullptr)
	throwRuntimeException(scope,"Could not determine output file format");

  av_strlcpy(outputContainer->filename, f ,sizeof(outputContainer->filename));

  if ((encoder = avcodec_find_encoder(AV_CODEC_ID_PCM_S16LE))==nullptr)
	throwRuntimeException(scope,"Could not find the PCM encoder");


  // Create a new audio stream in the output file
  if ((stream = avformat_new_stream(outputContainer, encoder))==nullptr)
	throwRuntimeException(scope,"Could not create new stream");

  encContext = stream->codec;


  //setup the encoder
  encContext->channels       = decContext->channels;
  encContext->channel_layout = av_get_default_channel_layout(encContext->channels);
  encContext->sample_fmt     = encoder->sample_fmts[0];
  encContext->bit_rate       = 0;

  //including the information about the sampling frequency

  encContext->sample_rate    = targetFrequency;
  stream->time_base.den = targetFrequency;
  stream->time_base.num = 1;


  // open the encoder for the audio stream to use it later. */
  if ((error = avcodec_open2(encContext, encoder, nullptr)) < 0)
	throwRuntimeException(scope,"Could not open output codec: ",error);

  encContext->frame_size     = 4096; //safety parameter to avoid frame size = 0 later on

  initResampler();
}

void AudioDemuxer::initResampler()
{
  int error;
  std::string scope="AudioDemuxer::initResampler()";

  //setup global parameters for the resampler
  resample_context = avresample_alloc_context();
  if (!resample_context)
	throwRuntimeException(scope,"Could not allocate resample context");

  av_opt_set_int(resample_context,"out_channel_layout",av_get_default_channel_layout(encContext->channels), 0);
  av_opt_set_int(resample_context,"out_sample_fmt",    encContext->sample_fmt, 0);
  av_opt_set_int(resample_context,"out_sample_rate",   encContext->sample_rate, 0);

  av_opt_set_int(resample_context,"in_channel_layout", av_get_default_channel_layout(decContext->channels), 0);
  av_opt_set_int(resample_context,"in_sample_fmt",     decContext->sample_fmt, 0);
  av_opt_set_int(resample_context,"in_sample_rate",    decContext->sample_rate, 0);



  /** Open the resampler with the specified parameters. */
  if ((error = avresample_open(resample_context)) < 0)
  {
    avresample_free(&resample_context);
    throwRuntimeException(scope,"Could not open resample context",error);
  }

  if ((fifo = av_audio_fifo_alloc(encContext->sample_fmt, encContext->channels, 1))==nullptr)
	throwRuntimeException(scope,"Could not allocate FIFO buffer");

}

void AudioDemuxer::demux(){

  auto decode_audio_frame = [&] (AVFrame *frame, int &data_present, int &finished)  -> void
  {
	if(frame == nullptr) throw std::invalid_argument("ERROR in AudioDemuxer::demux()::decode_audio_frame(AVframe *frame): frame is NULL");
	std::string scope="AudioDemuxer::demux()::decode_audio_frame()";

	while(1){
      // packet used for temporary storage. */
      AVPacket packet;

      // init packet to be empty
      av_init_packet(&packet); packet.data = nullptr; packet.size = 0;

      int error;

      // read the current frame
      if ((error=av_read_frame(inputContainer, &packet)) < 0) {

        //if we are at the end of the file, flush the decoder below. */
        if (error == AVERROR_EOF){
          finished = 1;
        }
        else {
          av_free_packet(&packet);
    	  throwRuntimeException(scope,"During decoding: ",error);
        }
      }

      if(packet.stream_index == aStreamId){

        // decode the frame
        if((error=avcodec_decode_audio4(decContext, frame, &data_present, &packet)) < 0) {
          av_free_packet(&packet);
          throwRuntimeException(scope,"During decoding: ",error);
        }

        // If the decoder has not been flushed completely this function has to be called again
        if (finished && data_present)
          finished = 0;
        av_free_packet(&packet);

        break;
      }
      av_free_packet(&packet);

      if(finished){
    	break;
      }
	}
  };


  // Global timestamp for the audio frames
  int64_t pts = 0;

  auto encode_audio_frame = [&] (AVFrame *frame, int &data_present) -> void
  {
	  std::string scope="AudioDemuxer::demux()::encode_audio_frame()";
	  int error;

      // Packet used for temporary storage
      AVPacket packet;

      // init packet to be empty
      av_init_packet(&packet); packet.data = nullptr; packet.size = 0;

      //Set a timestamp based on the sample rate for the container.
      if (frame) {
          frame->pts = pts;
          pts += frame->nb_samples;
      }

      // Encode the audio frame and store it in the temporary packet.
      if ((error=avcodec_encode_audio2(encContext, &packet, frame, &data_present)) < 0) {
    	throwRuntimeException(scope,"During encoding: ",error);
      }

      // Write one audio frame from the temporary packet to the output file
      if (data_present) {
          if ((error=av_write_frame(outputContainer, &packet)) < 0) {
        	  throwRuntimeException(scope,"During encoding: ",error);
          }

          av_free_packet(&packet);
      }
  };



  auto resample_and_store = [&] ( int &finished ) -> void
  {

    AVFrame *input_frame = nullptr;
    uint8_t **ioBuffer = nullptr;

    std::string scope="AudioDemuxer::demux()::resample_and_store()";
    int error;

    int data_present;

    // Initialize temporary storage for one input frame
    if((input_frame = av_frame_alloc())==nullptr)
		throwRuntimeException(scope,"Unable to allocate frames for the decoding");

    // Decode one frame
	try{
      decode_audio_frame(input_frame, data_present, finished);
	}
	catch (const std::runtime_error & e){
		//free memory on error
		av_frame_free(&input_frame);
	}

    // If there is decoded data, convert and store it
    if (data_present && !finished) {


      // compute destination number of samples required by the resampler (including delay)
      int nbOutputSamplesResampler = av_rescale_rnd(avresample_get_delay(resample_context) + input_frame->nb_samples, targetFrequency, input_frame->sample_rate, AV_ROUND_UP);

      // Allocate the memory for the output samples
      if((ioBuffer = (uint8_t **) (calloc(encContext->channels, sizeof(*ioBuffer))))==nullptr)
    	throwRuntimeException(scope,"Unable to allocate ioBuffer");

      if((error=av_samples_alloc(ioBuffer, nullptr, encContext->channels, nbOutputSamplesResampler, encContext->sample_fmt, 1))<0)
      {
    	free(ioBuffer);
    	throwRuntimeException(scope,"Unable to allocate samples within the ioBuffer: ",error);
      }

      // Convert the input samples to the desired output sample format (and resample)
      int nbOutputSamples = avresample_convert(resample_context, (uint8_t**) ioBuffer, 0, nbOutputSamplesResampler, (uint8_t**)input_frame->extended_data, 0, input_frame->nb_samples);
      if( nbOutputSamples<0 ){
      	free(ioBuffer);
    	throwRuntimeException(scope,"Unable to convert the current frame: ",nbOutputSamples);
      }

      // Increase the size of the fifo buffer depending on the new amount of samples to be written
      if((error=av_audio_fifo_realloc(fifo, av_audio_fifo_size(fifo) + nbOutputSamples))<0)
      {
    	av_freep(&ioBuffer[0]);
    	free(ioBuffer);
    	throwRuntimeException(scope,"Unable to increase capacity of the FIFO buffer: ",error);
      }

      // Add the converted input samples to the FIFO buffer for later processing
      if((error=av_audio_fifo_write(fifo, (void **)ioBuffer, nbOutputSamples))<0)
      {
    	av_freep(&ioBuffer[0]);
    	free(ioBuffer);
    	throwRuntimeException(scope,"Unable to update FIFO buffer: ",error);
      }
    }



    if (ioBuffer) {
     av_freep(&ioBuffer[0]);
     free(ioBuffer);
    }
    av_frame_free(&input_frame);

  };

  auto load_encode_and_write = [&]() -> void {

	std::string scope="AudioDemuxer::demux()::load_encode_and_write()";
	int error;

	//Setup output frame
    AVFrame *frame=nullptr;

    const int frame_size = FFMIN(av_audio_fifo_size(fifo),
                                 encContext->frame_size);

    // Create a new frame to store the audio samples
    if ((frame = av_frame_alloc())==nullptr) {
      throwRuntimeException(scope,"Could not allocate output frame");
    }

    frame->nb_samples     = frame_size;
    frame->channel_layout = encContext->channel_layout;
    frame->format         = encContext->sample_fmt;
    frame->sample_rate    = encContext->sample_rate;


    int buffer_size=av_samples_get_buffer_size(0, encContext->channels, encContext->frame_size,	encContext->sample_fmt, 0);
    if(buffer_size<0){
      av_frame_free(&frame);
      throwRuntimeException(scope,"Cannot determine number of output samples: ", buffer_size);
    }


    uint16_t *samples = (uint16_t *) av_malloc(buffer_size);
    if (samples==nullptr) {
      av_frame_free(&frame);
      throwRuntimeException(scope,"Could not allocate " + std::to_string(buffer_size) + " bytes for samples buffer");
    }

    if((error=avcodec_fill_audio_frame(frame, encContext->channels, encContext->sample_fmt, (const uint8_t*)samples, buffer_size, 0))<0) {
      av_frame_free(&frame);
      av_freep(&samples);
      throwRuntimeException(scope,"Cannot fill the output frame with the samples: ", error);
    }




    int data_written;


    // Read as many samples from the FIFO buffer as required to fill the frame.
    if ((error=av_audio_fifo_read(fifo, (void **)frame->data, frame_size)) < frame_size) {
        av_frame_free(&frame);
        av_freep(&samples);
        throwRuntimeException(scope,"Could not read data from FIFO");
    }

    // Encode one frame worth of audio samples
    encode_audio_frame(frame, data_written); //TODO add try catch clause here, if only partial output may be useful
    av_frame_free(&frame);
    av_freep(&samples);
  };



  avformat_write_header(outputContainer, nullptr);

  // For the full datastream
  while(1)
  {
    int finished = 0;
    int i=0;

    // Fill the fifo with enough decoded samples to encode one frame
    while (av_audio_fifo_size(fifo) < encContext->frame_size) {

        resample_and_store( finished );

        if (finished)
            break;

    }

    // Take one frame of audio samples from the FIFO buffer, encode it and write it to the output file.
    while (av_audio_fifo_size(fifo) >= encContext->frame_size ||
           (finished && av_audio_fifo_size(fifo) > 0))
    {
    	load_encode_and_write();
    }

    //handle the end of file (decoding is done, but the fifo still contains samples)
    if (finished) {
        int data_written;
        /** Flush the encoder as it may have delayed frames. */
        do {
            encode_audio_frame(nullptr, data_written);
        } while (data_written);
        break;
    }
  }

  av_write_trailer(outputContainer);
  reset();
}

void AudioDemuxer::reset() {

  if (resample_context)  avresample_free(&resample_context);

  if(fifo)  av_audio_fifo_free(fifo);

  if (encContext)  avcodec_close(encContext);
  if (outputContainer) {
    avio_closep(&(outputContainer->pb));
    avformat_free_context(outputContainer);
  }

  if (decContext)  avcodec_close(decContext);
  if (inputContainer) avformat_close_input(&inputContainer);

}

AudioDemuxer::~AudioDemuxer() {
}

} /* namespace mico */
