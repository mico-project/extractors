[TOC]

# TE-214 Audio demultiplexing and resampling

The service for audio demultiplexing and resampling is meant to demultiplex an audio stream embedded in a video stream, and to downsample it to the desired target frequency.

The downsampled audio content is attached to the input content file as a separate wav file, and annotated .

In order to ease the further processing, the format of the output wav file is one of the most common standard, i.e. signed PCM 16-bit, Little Endian. 

No downmixing is performed, in order for the following extractors to work either on both channel separately, or on the downmixed one depending on the application scenario.

# Dependencies

external dependencies:

* boost library (system, filesystem, log, log_setup, program_options)
* MICO Platform API 
* protobuf (transitive dependency through mico_event)
* ffmpeg av-libraries (avformat, avcodec, avutil, avfilter, avresample)


# Build setting and environment

No additional build setting is required. A typical build call could look like:

    cmake -DMICOPlatformAPI_HOME=[path to MICO platform API] -DCMAKE_PREFIX_PATH=[semicolon seperated list of paths to local installs such as protobuf] [path to extractor source]
    
# Example call

The service has two additional mandatory inputs in addition to the ones required by the mico platform api, i.e.
    
  **--mime (-m)**  Input mime type (currently supported: video/mp4)  
  **--frequency (-f)**  Target resampling frequency (currently supported: 8000, 16000, 24000)

And two optional switch to start the executable as a daemon 

  **--daemon**  Start as daemon  
  **-k**        Kill daemon

A typical call would look like:

    ~/[...]/mico-extractor-audiodemux <host> <user> <pass> [ [-k] --daemon ] -m <mime/type> -f <frequency>
    
    
