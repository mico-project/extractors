/*
 * AudioDemuxService.cpp
 *
 *  Created on: Jul 8, 2015
 *      Author: cvo
 */

#include "AudioDemuxService.h"
#include "AudioDemuxer.h"

#include <Logging.hpp>
#include <string>
#include <fstream>
#include <EventManager.hpp>
#include <vocabularies.hpp>

#include <PersistenceService.hpp>
#include <Item.hpp>
#include <Part.hpp>
#include <Asset.hpp>

using namespace mico::event;
using namespace mico::event::model;
using namespace mico::persistence;
using namespace mico::persistence::model;


using pResource = std::shared_ptr<Resource>;
using pItem  = std::shared_ptr<Item>;
using pPart  = std::shared_ptr<Part>;
using pAsset = std::shared_ptr<Asset>;

#include <anno4cpp.h>
#include <jnipp.h>

using namespace jnipp::eu::mico::platform::anno4j::model;
using namespace jnipp::eu::mico::platform::anno4j::model::namespaces;
using namespace jnipp::eu::mico::platform::anno4j::model::impl::bodymmm;
using namespace jnipp::eu::mico::platform::anno4j::model::impl::targetmmm;
using namespace jnipp::com::github::anno4j::model;
using namespace jnipp::com::github::anno4j::model::impl::selector;
using namespace jnipp::java::lang;

#include <MICOExtractorID.h>

AudioDemuxService::AudioDemuxService(AudioDemuxService::InputMimeType m, AudioDemuxService::TargetSamplingFrequency f) :
		  AnalysisService(mico::extractors::getPrecompExtractorId(),mico::extractors::getPrecompExtractorModeId(),mico::extractors::getPrecompExtractorVersion(),
				          getInputMimeType(m), "audio/wav"), m_audioDemuxer(0), m_chosenMimeType(m)
{
	switch(f) {
	case AudioDemuxService::_8kHz  : m_audioDemuxer.reset(new mico::AudioDemuxer(8000));  break;
	case AudioDemuxService::_16kHz : m_audioDemuxer.reset(new mico::AudioDemuxer(16000)); break;
	case AudioDemuxService::_24kHz : m_audioDemuxer.reset(new mico::AudioDemuxer(24000)); break;
	default : std::string error_msg="AudioDemuxService() : CRITICAL ERROR: unhandled valid input frequency";
	throw std::logic_error(error_msg);
	}
}

AudioDemuxService::~AudioDemuxService() {

}


void AudioDemuxService::call(mico::event::AnalysisResponse &resp, std::shared_ptr< mico::persistence::model::Item > item, std::vector< std::shared_ptr<mico::persistence::model::Resource>>inputResources, std::map<std::string,std::string>& params){

	LOG_INFO("AudioDemuxService: START");
	//1 verify correctness of inputs:

	//non-empty item
	if(item == nullptr){
		resp.sendErrorMessage(item,
				ErrorCodes::UNEXPECTED_ERROR,
				"AudioDemux: CRITICAL ERROR",
				"No input item is present");
		return;
	}

	//only one part
	if(inputResources.size() != 1){
		resp.sendErrorMessage(item,
				ErrorCodes::UNEXPECTED_ERROR,
				"AudioDemux: CRITICAL ERROR",
				"Received " + std::to_string(inputResources.size()) + " inputs, should be 1");
		return;
	}

	//TODO: parameters are there and are correct


	//2. retrieve the asset

	pResource rInputPart = inputResources.front();

	if(rInputPart == nullptr){
		resp.sendErrorMessage(item,
				ErrorCodes::UNEXPECTED_ERROR,
				"AudioDemux: CRITICAL ERROR",
				"Cannot retrieve input Resource");
		return;
	}


	if(rInputPart->hasAsset()) {

		pAsset    asset  = rInputPart->getAsset();

		//check correctness of the input mime type:

		//if the selected input is video/*, but the format is not starting with 'video/', raise exception
		if(m_chosenMimeType == VIDEO_STAR){
                        if (asset->getFormat().find("video/") != 0){
				resp.sendErrorMessage(item,
					ErrorCodes::UNSUPPORTED_CONTENT_TYPE,
					"AudioDemux: Unsupported mime type",
					"Cannot handle input with format  " + asset->getFormat() + 
                                        "because asset->getFormat().find('video/') == " + std::to_string(asset->getFormat().find("video/")));
				return;
                        }
		}

		//else, check for exact matching of the format
		else if(getInputMimeType(m_chosenMimeType).compare(asset->getFormat()) != 0)
		{
			resp.sendErrorMessage(item,
					ErrorCodes::UNSUPPORTED_CONTENT_TYPE,
					"AudioDemux: Unsupported mime type",
					"Cannot handle input with format  " + asset->getFormat());
			return;
		}

		//declare pointers and filenames of temporary files for cleanup during exceptions

		std::istream* inputStream = nullptr;
		std::ostream* outputStream = nullptr;

		std::string iFilename;
		std::string oFilename;

		auto cleanup = [](std::string iFile,std::string oFile, std::istream* iStream, std::ostream* oStream) -> void{

			//remove temporary files, if needed (std::remove is exception free)
			std::remove(iFile.c_str());
			std::remove(oFile.c_str());

			//and deallocate objects from the heap

			if(oStream!=nullptr)  delete oStream;
			if(iStream!=nullptr)  delete iStream;

		};

		auto abort_call = [&](const std::exception & e, mico::event::model::ErrorCodes code, std::string iFile,std::string oFile, std::istream* iStream, std::ostream* oStream)->void{

			// notify broker that the execution terminated, but that we didn't create any new content item
			LOG_ERROR("Demuxing terminated after throwing an instance of '%s'",typeid(e).name());
			LOG_ERROR("  what(): %s",e.what());

			resp.sendErrorMessage(item,code,typeid(e).name(),e.what());

			//and cleanup every local pointer
			cleanup(iFile,oFile,iStream,oStream);
			m_audioDemuxer.reset(new mico::AudioDemuxer(m_audioDemuxer->getTargetFrequency()));

		};

		try{

			LOG_INFO("AudioDemuxService: START DEMUXING");

			//---- 1: Save content item as local file. Its name depends on the mime type

			//load istream of <mime-type> file
			inputStream = asset->getInputStream();

			//set name, including extension, depending on the mime type
			std::string basename=std::to_string((long unsigned int) &inputStream);

			std::string iFilename;
			std::string oFilename=basename+".wav";

			//ffmpeg is using the extension to identify the decoder
			iFilename=basename + "." + getExtension(asset->getFormat());

			//store the istream locally
			std::ofstream oFileStream;
			oFileStream.open(iFilename);
			if(!oFileStream.is_open()){
				abort_call(std::runtime_error("AudioDemuxService::call() unable to open temporary file " + iFilename),ErrorCodes::INSUFFICIENT_STORAGE,iFilename,oFilename,inputStream,outputStream);
				return;
			}
			std::vector<char> buff = std::vector<char>(std::istreambuf_iterator<char>(*inputStream), std::istreambuf_iterator<char>());
			for(uint i = 0;i < buff.size();++i)
			{
				oFileStream << buff[i];
			}

			oFileStream.close();
			inputStream->seekg(0);



			//---- 2: Perform the demuxing operation


			m_audioDemuxer->openInputFile((char *)iFilename.c_str());
			m_audioDemuxer->openOutputFile((char *)oFilename.c_str());
			m_audioDemuxer->demux();

			LOG_INFO("AudioDemuxService: DONE DEMUXING");

			//---- 3. read demux and store in the output content

			LOG_INFO("AudioDemuxService: START PUBLISHING THE ASSET");

			//load output file
			std::ifstream iFileStream;
			iFileStream.open(oFilename);
			if(!iFileStream.is_open()){
				throw std::runtime_error("AudioDemuxService::call() unable to open temporary file " + iFilename);
			}

			//---- 4. create a part, and assign it the newly created asset


			// Instantiate content part to hold the annotation
			pPart      outputPart = item->createPart(mico::persistence::model::URI(getServiceID()));
			if(outputPart == nullptr){
				throw std::runtime_error("AudioDemuxService::call() unable to instantiate output Part");
			}


			pResource  rOutputPart = std::dynamic_pointer_cast<Resource>(outputPart);
			LOG_INFO( "AudioDemuxService: Created new part at %s", rOutputPart->getURI().stringValue().c_str() );


			pAsset outputAsset = rOutputPart->getAsset();
			if(outputAsset == nullptr){
				throw std::runtime_error("AudioDemuxService::call() unable to instantiate output Asset");
			}


			LOG_INFO( "AudioDemuxService: Created new asset at %s", outputAsset->getURI().stringValue().c_str() );
			outputAsset->setFormat("audio/wav");

			//TODO: the syntactic type is required by Broker v3, but should be modified into mico:Audio
			rOutputPart->setSyntacticalType("mico:Audio");
			rOutputPart->setSemanticType("Audio decoded from the asset of "+rInputPart->getURI().stringValue());


			//get output stream for the content part
			outputStream = outputAsset->getOutputStream();

			//and copy all the content
			buff = std::vector<char>(std::istreambuf_iterator<char>(iFileStream), std::istreambuf_iterator<char>());
			for(uint i = 0;i < buff.size();++i)
				(*outputStream) << buff[i];

			//delete the temporary wav files
			iFileStream.close();
			outputPart->addInput(rInputPart);

			LOG_INFO("AudioDemuxService: DONE PUBLISHING THE ASSET");




			LOG_INFO("AudioDemuxService: START PUBLISHING THE ANNOTATION");

			//---- 0. retrieve the JVM scope
			PersistenceService& ps=rInputPart->getPersistenceService();
			jnipp::Env::Scope scope(ps.getJVM());

			LOG_INFO("AudioDemuxService: set scope");

			auto check_java_object=[&] (bool obj_exists )->void{
				ps.checkJavaExceptionThrow();
				if(! obj_exists) {
					throw std::runtime_error("AudioDemuxService::call(): unable to instantiate java object from jvm");
				}
			};


			//---- 1. create a Body
			jnipp::LocalRef<AudioDemuxBodyMMM> audioDemuxBody=item->createObject(AudioDemuxBodyMMM::clazz());
			check_java_object(audioDemuxBody);
			LOG_INFO("AudioDemuxService: created body");


			std::string samplingRate;
			switch(m_chosenFrequency) {
			case AudioDemuxService::_8kHz  : samplingRate="8000";  break;
			case AudioDemuxService::_16kHz : samplingRate="16000"; break;
			case AudioDemuxService::_24kHz : samplingRate="24000"; break;
			default : std::string error_msg="AudioDemuxService() : CRITICAL ERROR: unhandled valid input frequency";
			throw std::logic_error(error_msg);
			}
			audioDemuxBody->setFrameRate(String::create(samplingRate));
			ps.checkJavaExceptionThrow();
			LOG_INFO("AudioDemuxService: annotated body");


			outputPart->setBody(audioDemuxBody);
			LOG_INFO("AudioDemuxService: added body to part");




			//---- 2. create a Target

			jnipp::LocalRef<SpecificResourceMMM>   targetMMM = item->createObject(SpecificResourceMMM::clazz());
			check_java_object(targetMMM);
			LOG_INFO("AudioDemuxService: created target mmm");


			LOG_INFO("AudioDemuxService: trying to retrieve Assetmmm");

			jnipp::LocalRef<AssetMMM> assetMMM = item->findObject(outputAsset->getURI(),AssetMMM::clazz());


			check_java_object(assetMMM);
			LOG_INFO("AudioDemuxService: retrieved Assetmmm");

			targetMMM->setSource(assetMMM);
			ps.checkJavaExceptionThrow();

			outputPart->addTarget(targetMMM);



			LOG_INFO("AudioDemuxService: DONE PUBLISHING THE ANNOTATION");

			// notify broker that we produced the new mico:Audio part

			resp.sendNew(item, rOutputPart->getURI());


			cleanup(iFilename,oFilename,inputStream,outputStream);

		}
		catch(const std::runtime_error & e)
		{
			abort_call(e,ErrorCodes::UNEXPECTED_ERROR,iFilename,oFilename,inputStream,outputStream);
		}
		catch(const std::invalid_argument & e)
		{
			abort_call(e,ErrorCodes::UNEXPECTED_ERROR,iFilename,oFilename,inputStream,outputStream);
		}
		catch(const std::logic_error & e)
		{
			abort_call(e,ErrorCodes::UNEXPECTED_ERROR,iFilename,oFilename,inputStream,outputStream);
		}
		catch(const std::exception & e)
		{
			abort_call(e,ErrorCodes::UNEXPECTED_ERROR,iFilename,oFilename,inputStream,outputStream);
		}


	}
	else{

		// notify broker that there was no asset to be processed
		resp.sendErrorMessage(item,
				ErrorCodes::MISSING_ASSET,
				"Audio Demux: MISSING ASSET",
				"No asset found for part at at " + rInputPart->getURI().stringValue());
	}

	resp.sendFinish(item);
	LOG_INFO("AudioDemuxService: DONE");
}


std::string AudioDemuxService::getServiceName(AudioDemuxService::InputMimeType m, AudioDemuxService::TargetSamplingFrequency f){
	std::string out ="mico-extractor-audiodemux-";
	switch(f) {
		case AudioDemuxService::_8kHz  : out=(out+std::string("8kHz-"));  break;
		case AudioDemuxService::_16kHz : out=(out+std::string("16kHz-")); break;
		case AudioDemuxService::_24kHz : out=(out+std::string("24kHz-")); break;
		default : std::string error_msg="AudioDemuxService() : CRITICAL ERROR: unhandled valid input frequency";
				  LOG_ERROR(error_msg.c_str());
                  throw std::logic_error(error_msg);
	}
	return out+getExtension(getInputMimeType(m));
}

std::string AudioDemuxService::getInputMimeType(AudioDemuxService::InputMimeType m){
	switch(m) {
		case AudioDemuxService::VIDEO_MP4       : return std::string("video/mp4");
		case AudioDemuxService::VIDEO_QUICKTIME : return std::string("video/quicktime");
		case AudioDemuxService::VIDEO_X_MSVIDEO : return std::string("video/x-msvideo");
		case AudioDemuxService::VIDEO_MPEG      : return std::string("video/mpeg");
		case AudioDemuxService::VIDEO_STAR      : return std::string("video/*");
		default : std::string error_msg="AudioDemuxService() : CRITICAL ERROR: unhandled valid input mime type";
			      LOG_ERROR(error_msg.c_str());
		          throw std::logic_error(error_msg);
	}
}

std::string AudioDemuxService::getExtension(std::string mimeType){

	if( mimeType.compare(getInputMimeType(VIDEO_MP4))       == 0 ) return std::string("mp4");
	if( mimeType.compare(getInputMimeType(VIDEO_QUICKTIME)) == 0 ) return std::string("mov");
	if( mimeType.compare(getInputMimeType(VIDEO_X_MSVIDEO)) == 0 ) return std::string("avi");
	if( mimeType.compare(getInputMimeType(VIDEO_MPEG))      == 0 ) return std::string("mpeg");
	if( mimeType.find("video/") == 0 ){
		LOG_WARN("The video extension associated with mimeType '%s' is unknown, returning 'video'",mimeType.c_str());
		return "video";
	}
	std::string error_msg="AudioDemuxService() : CRITICAL ERROR: mime type '"+mimeType+"' is not of tipe 'video/*'";
	LOG_ERROR(error_msg.c_str());
	throw std::logic_error(error_msg);
}
