/*
 * AudioDemuxer.h
 *
 *  Created on: Jul 7, 2015
 *      Author: cvo
 */

#ifndef AUDIODEMUXER_H_
#define AUDIODEMUXER_H_

class AVFormatContext;
class AVCodecContext;
class AVAudioResampleContext;
class AVAudioFifo;

#include <string>

namespace mico {

class AudioDemuxer {
public:
	AudioDemuxer(unsigned int targetFrequency);

	void openInputFile(char* filename);
	void openOutputFile(char* filename);

	void demux();
        void reset();
        
        unsigned int getTargetFrequency(){return targetFrequency;}

	virtual ~AudioDemuxer();

private:
    AVFormatContext *inputContainer;
    AVFormatContext *outputContainer;
    AVCodecContext *decContext;
    AVCodecContext *encContext;
    AVAudioResampleContext *resample_context;
    AVAudioFifo *fifo;

    int aStreamId;
    unsigned int targetFrequency;
	void initResampler();

	void throwRuntimeException(std::string functionName, std::string contextExplanation, const int errorCode);
	void throwRuntimeException(std::string functionName, std::string contextExplanation);
};

} /* namespace mico */

#endif /* AUDIODEMUXER_H_ */
