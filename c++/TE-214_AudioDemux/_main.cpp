extern "C"{
  #include <libavcodec/avcodec.h>
  #include <libavformat/avformat.h>
  #include <libavutil/avutil.h>
  #include <libavutil/samplefmt.h>
  #include <libavformat/avformat.h>
}

#include <iostream>
#include "AudioDemuxer.h"



int main(int argc, char* argv[])
{

    if (argc < 2) {
        std::cerr << "Please provide the file path as the first argument";
        return -1;
    }

    mico::AudioDemuxer a(8000);

    a.openInputFile(argv[1]);
    a.openOutputFile((char*) "decoded.wav");
    a.demux();

    std::cerr <<  "Done playing. Exiting..." << std::endl;
    return 0;
}
