/*
 * AudioDemuxService.h
 *
 *  Created on: Jul 8, 2015
 *      Author: cvo
 */

#ifndef AUDIODEMUXSERVICE_H_
#define AUDIODEMUXSERVICE_H_


#include "AnalysisService.hpp"
#include "AudioDemuxer.h"

#include <memory>
#include <iosfwd>


namespace mico {
	class AudioDemuxer;
}

class AudioDemuxService : public mico::event::AnalysisService{
public:

	enum TargetSamplingFrequency { _8kHz, _16kHz, _24kHz };
	enum InputMimeType  { VIDEO_MP4, VIDEO_QUICKTIME, VIDEO_X_MSVIDEO, VIDEO_MPEG, VIDEO_STAR	};


	AudioDemuxService(AudioDemuxService::InputMimeType m, AudioDemuxService::TargetSamplingFrequency f);

	static std::string getServiceName(AudioDemuxService::InputMimeType m, AudioDemuxService::TargetSamplingFrequency f);
	static std::string getInputMimeType(AudioDemuxService::InputMimeType m);
	static std::string getExtension(std::string mimeType);

	virtual ~AudioDemuxService();

	void call(mico::event::AnalysisResponse& response, std::shared_ptr< mico::persistence::model::Item > item, std::vector< std::shared_ptr<mico::persistence::model::Resource>>inputResources, std::map<std::string,std::string>& params);

private:
	std::shared_ptr<mico::AudioDemuxer> m_audioDemuxer;
	AudioDemuxService::InputMimeType m_chosenMimeType;
	AudioDemuxService::TargetSamplingFrequency m_chosenFrequency;
};

#endif /* AUDIODEMUXSERVICE_H_ */
