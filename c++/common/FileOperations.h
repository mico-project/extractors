
#ifndef __FILE_OPERATIONS_H__
#define __FILE_OPERATIONS_H__

#include <string>
#include <vector>
#include <map>

namespace mico {
namespace extractors {


class FileOperations {

public:

  /** @brief Searches for files in different directories
   *
   *  @param fileList List of file names to find
   *  @param searchPathList List of paths to check for each file
   *
   *  @return Map of first occurence of found files filename->location
   */
  static std::map<std::string,std::string> findFiles(
      const std::vector<std::string>& fileList,
      const std::vector<std::string>& searchPathList);

  // return the filenames of all files that have the specified extension
  // in the specified directory and all subdirectories
  static void get_all(const std::string& root, const std::string& ext, std::vector<std::string>& ret);

protected:

  static bool findFile( const std::string & dir_path,
                        const std::string & file_name,
                        std::string& path_found );
};

}
}

#endif
