
#ifndef __MICO_EXTRACTOR_ID_H__
#define __MICO_EXTRACTOR_ID_H__

#include <string>

namespace mico {
namespace extractors {

#define mico_stringfy(symbol) __mico_str(symbol)
#define __mico_str(s) #s

std::string getPrecompExtractorId(){
	std::string out = mico_stringfy( MICO_EXTRACTOR_ID );
	return out;
}
std::string getPrecompExtractorVersion(){
	std::string out = mico_stringfy(MICO_EXTRACTOR_VERSION);
	return out;
}
std::string getPrecompExtractorModeId(){
	std::string out = mico_stringfy(MICO_EXTRACTOR_MODE_ID);
	return out;
}


}
}


#endif
