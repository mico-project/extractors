#!/usr/bin/env bash
#
# 
# script automating the CMake builds of released extractors
#
# This is a workaround for now due to the limitations of CMake packaging
#

#extractor directories

declare -a extractor_dirs

extractor_dirs=(
	"TE-202_OAD_hog_detector"
	"TE-202_OAD_yolo_detector"
	"TE-202_OAD_dpm_detector"
	"TE-204_FDR_ccv_facedetection"
	"TE-206_TVS_temporalvideosegmentation_video"
	"TE-214_AudioDemux"
	"TE-214_kaldi_speechtotext"
	"TE-227_MediaContainerTagExtraction"
)

function check_installed() {
	res=$(dpkg -s "${1}")
	stat=$(echo $?)

	[[ ${stat} != 0 ]] && { echo "Missing package [${1}] detected - please install package!"; exit 1; }
}

#check known dependencies that are installable via Debian packaging
check_installed "libboost-all-dev"
check_installed "libavcodec-dev"
check_installed "libavformat-dev" 
check_installed "libswscale-dev" 
check_installed "libavresample-dev"
check_installed "libmagick++-dev"
check_installed "libatlas-dev"
check_installed "libmediainfo-dev"

SRC_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
HOME_DIR=$(pwd)

if [[ ${DIR} == ${HOME_DIR} ]]; then
	echo "The directory running this script should not be the same as the script location!"
	exit 1 
fi

if [ -d "packages" ]; then
	echo "WARNING: Package directory exists - removing content!"
	rm -rf packages
fi
mkdir packages

for curr_dir in "${extractor_dirs[@]}"; do
 	EXTRACTOR_SRC_DIR="${SRC_DIR}/${curr_dir}"
	if [[ ! -e "${EXTRACTOR_SRC_DIR}/CMakeLists.txt" ]]; then
		echo "WARNING: The directory ${EXTRACTOR_SRC_DIR} does not contain a CMakeLists.txt.!"
		continue
	else
		echo "Found CMakeLists.txt for directory [${curr_dir}] - starting build"
		
		if [ -d "${curr_dir}" ]; then
			echo "WARNING: Build directory [${curr_dir}] exists - removing content!"
			rm -rf "${curr_dir}"
		fi
		mkdir ${curr_dir}
		cd ${curr_dir}
		
		cmake -DCMAKE_INSTALL_PREFIX=./install $1 ${EXTRACTOR_SRC_DIR}
		make package -j
		debs=$(ls *.deb)
		for f in "${debs[@]}"; do
			target_path="${HOME_DIR}/packages"
			echo "copying package [${f}] ....."
			eval "cp ${f} ${target_path}"
		done
		cd ${HOME_DIR}
	fi
done
