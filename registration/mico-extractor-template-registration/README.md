# MICO extractor registration package template
This is a template for MICO extractor registration packages. It will install the registration XML to the system and register it using the registration command line tool. The following steps are necessary to adapt it:

1. Make a copy of this directory (without this README.md) and name it _mico-extractor-<extractor name>-registration_.
2. Replace the file _mico-extractor-template-registration.xml_ with your registration XML and name it _mico-extractor-<extactor name>-registration.xml_. If the name of this file does not match the name of the package the automatic registration on package installation will __fail__.
3. There are some files you have to adapt. Make sure to update all occurences of the package name _mico-extractor-template-registration_ to the proper name.
    *  _debian/changelog_: Update at least the package name, the maintainer information and date (you can use _dch -i_ and remove the template part)
    *  _debian/control_: Update at least the _Source_, _Maintainer_, _Package_, _Description_ and long description.
    *  You might also want to update _debian/copyright_.
4. Now you can build the package using _dpkg-buildpackage -uc -us -sa_ and commit.
